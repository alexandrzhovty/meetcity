#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "InstagramSimpleOAuth.h"
#import "InstagramSimpleOAuthViewController.h"
#import "InstagramLoginResponse.h"
#import "InstagramUser.h"

FOUNDATION_EXPORT double InstagramSimpleOAuthVersionNumber;
FOUNDATION_EXPORT const unsigned char InstagramSimpleOAuthVersionString[];

