//
//  NotificationService.swift
//  NotificationServiceExtension
//
//  Created by Sergey Krotkih on 5/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UserNotifications
import UIKit
import Firebase

public enum HandlerType: String {
   case Default
   case URL
   case Resources
}

public enum MediaType: String {
   case image = "image"
   case gif = "gif"
   case video = "video"
   case audio = "audio"
}

fileprivate struct Media {
   private var data: Data
   private var ext: String
   private var type: MediaType
   
   init(forMediaType mediaType: MediaType, withData data: Data, fileExtension ext: String) {
      self.type = mediaType
      self.data = data
      self.ext = ext
   }
   
   var attachmentOptions: [String: Any] {
      switch(self.type) {
      case .image:
         return [UNNotificationAttachmentOptionsThumbnailClippingRectKey: CGRect(x: 0.0, y: 0.0, width: 1.0, height: 0.50).dictionaryRepresentation]
      case .gif:
         return [UNNotificationAttachmentOptionsThumbnailTimeKey: 0]
      case .video:
         return [UNNotificationAttachmentOptionsThumbnailTimeKey: 0]
      case .audio:
         return [UNNotificationAttachmentOptionsThumbnailHiddenKey: 1]
      }
   }
   
   var fileIdentifier: String {
      return self.type.rawValue
   }
   
   var fileExt: String {
      if self.ext.characters.count > 0 {
         return self.ext
      } else {
         switch(self.type) {
         case .image:
            return "jpg"
         case .gif:
            return "gif"
         case .video:
            return "mp4"
         case .audio:
            return "mp3"
         }
      }
   }
   
   var mediaData: Data? {
      return self.data
   }
}

private func resourceURL(forUrlString urlString: String) -> URL? {
   return URL(string: urlString)
}


final class NotificationService: UNNotificationServiceExtension {

   fileprivate var contentHandler: ((UNNotificationContent) -> Void)?
   fileprivate var bestAttemptContent: UNMutableNotificationContent?

   fileprivate var currentFireBaseUser: User?
   fileprivate var remoteConfig: RemoteConfig!
   var displayName: String?

   fileprivate var handlerType: HandlerType = .Resources
   
   lazy var onceFIRConfigure: Void  = {
      FirebaseApp.configure()
      return
   }()
   
   override internal func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void){
      self.contentHandler = contentHandler
      
      bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
      
      if let bestAttemptContent = bestAttemptContent {
         // Modify the notification content here...
         let userInfo = bestAttemptContent.userInfo
         
         switch self.handlerType {
         case .Default:
            handleDefaultRequest(for: userInfo, contentHandler: contentHandler)
         case .URL:
            handleUrlDownload(contentHandler: contentHandler)
         case .Resources:
            handleResources(for: userInfo, contentHandler: contentHandler)
         }
      }
   }
   
   override func serviceExtensionTimeWillExpire() {
      // Called just before the extension will be terminated by the system.
      // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
      if let contentHandler = contentHandler, let bestAttemptContent = bestAttemptContent {
         contentHandler(bestAttemptContent)
      }
   }
}

extension NotificationService {

   fileprivate func handleDefaultRequest(for userInfo: [AnyHashable : Any],  contentHandler: @escaping (UNNotificationContent) -> Void) {
      guard let bestAttemptContent = self.bestAttemptContent else {
         return
      }
      guard let name = userInfo["attachment-url"] as? String else {
         contentHandler(bestAttemptContent)
         return
      }
      loadAttachment(name: name, completionHandler: { attachment in
         if let attachment = attachment {
            bestAttemptContent.attachments = [attachment]
         }
         contentHandler(bestAttemptContent)
      })
   }
   
   fileprivate func handleUrlDownload(contentHandler: @escaping (UNNotificationContent) -> Void) {
      guard let bestAttemptContent = self.bestAttemptContent else {
         return
      }
      let url = "https://pbs.twimg.com/profile_images/520582570480640000/V7kuLdAd.jpeg"
      let mediaType = MediaType(rawValue: "image")!
      loadAttachment(forMediaType: mediaType, withUrlString: url, completionHandler: { attachment in
         if let attachment = attachment {
            bestAttemptContent.attachments = [attachment]
         }
         contentHandler(bestAttemptContent)
      })
   }

   fileprivate func handleResources(for userInfo: [AnyHashable : Any],  contentHandler: @escaping (UNNotificationContent) -> Void) {
      guard let bestAttemptContent = self.bestAttemptContent else {
         return
      }
      loadAttachment(completionHandler: { attachment in
         if let attachment = attachment {
            bestAttemptContent.attachments = [attachment]
         }
         contentHandler(bestAttemptContent)
      })
   }
   
   fileprivate func loadAttachment(forMediaType mediaType: MediaType, withUrlString urlString: String, completionHandler: ((UNNotificationAttachment?) -> Void)) {
      guard let url = resourceURL(forUrlString: urlString) else {
         completionHandler(nil)
         return
      }
      
      do {
         let data = try Data(contentsOf: url)
         let media = Media(forMediaType: mediaType, withData: data, fileExtension: url.pathExtension)
         if let attachment = UNNotificationAttachment.create(fromMedia: media) {
            completionHandler(attachment)
            return
         }
         completionHandler(nil)
      } catch {
         print("error " + error.localizedDescription)
         completionHandler(nil)
      }
   }
   
   fileprivate func loadAttachment(completionHandler: ((UNNotificationAttachment?) -> Void)) {
      if let image = UIImage(named: "profile.png") {
         if let data = UIImagePNGRepresentation(image) as NSData? {
            if let attachment = UNNotificationAttachment.create(imageFileIdentifier: "profile.png", data: data, options: nil) {
               completionHandler(attachment)
               return
            }
         }
      }
      completionHandler(nil)
   }
}

extension NotificationService {
   
   fileprivate func loadAttachment(name: String, completionHandler: @escaping ((UNNotificationAttachment?) -> Void)) {
      downloadImageFor(name: name) { image in
         if let image = image {
            if let data = UIImagePNGRepresentation(image) as NSData? {
               if let attachment = UNNotificationAttachment.create(imageFileIdentifier: "attach.png", data: data, options: nil) {
                  completionHandler(attachment)
                  return
               }
            }
         }
      }
      completionHandler(nil)
   }
   
   fileprivate func downloadImageFor(name: String, completion: @escaping (UIImage?) -> Void) {

      _ = onceFIRConfigure
      
      self.signIn() {
         let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
         let documentsDirectory = paths[0]
         let filePath = "file:\(documentsDirectory)/\(name).jpg"
         guard let fileURL = URL.init(string: filePath) else {
            print("Error downloading")
            completion(nil)
            return
         }
         let imageURL = "\(name).jpg"
         Storage.storage().reference().child(imageURL).write(toFile: fileURL, completion: { url, error in
            if let error = error {
               print("Error downloading:\(error)")
               completion(nil)
            } else if let imagePath = url?.path {
               let image = UIImage.init(contentsOfFile: imagePath)
               completion(image)
            } else {
               completion(nil)
            }
         })
      }
   }
   
   func signIn(completion: @escaping () -> Void) {
      let email = "meetcity@vesedia.com"
      let password = "bRooEMnV"
      
      signIn(email: email, password: password) { user in
         self.currentFireBaseUser = user
         DispatchQueue.main.async {
            completion()
         }
      }
   }
   
   fileprivate func signIn(email: String, password: String, completion: @escaping (User?) -> Void) {
      Auth.auth().signIn(withEmail: email, password: password, completion: { (firebaseuser, error) in
         if let err = error {
            print(err.localizedDescription)
            completion(nil)
         } else {
            completion(firebaseuser)
         }
      })
   }
   
   fileprivate func configureRemoteConfig() {
      remoteConfig = RemoteConfig.remoteConfig()
      // Create Remote Config Setting to enable developer mode.
      // Fetching configs from the server is normally limited to 5 requests per hour.
      // Enabling developer mode allows many more requests to be made per hour, so developers
      // can test different config values during development.
      let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true)
      remoteConfig.configSettings = remoteConfigSettings!
   }
   
}


fileprivate extension UNNotificationAttachment {
   
   /// Save the image to disk
   static func create(imageFileIdentifier: String, data: NSData, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
      let fileManager = FileManager.default
      let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
      if let tmpSubFolderURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true) {
         do {
            try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
            let fileURL = tmpSubFolderURL.appendingPathComponent(imageFileIdentifier)
            try data.write(to: fileURL, options: [])
            let imageAttachment = try UNNotificationAttachment(identifier: imageFileIdentifier, url: fileURL, options: options)
            return imageAttachment
         } catch let error {
            print("error \(error)")
         }
      }
      return nil
   }
   
   static func create(fromMedia media: Media) -> UNNotificationAttachment? {
      let fileManager = FileManager.default
      let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
      let tmpSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true)
      do {
         try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
         let fileIdentifier = "\(media.fileIdentifier).\(media.fileExt)"
         let fileURL = tmpSubFolderURL.appendingPathComponent(fileIdentifier)
         
         guard let data = media.mediaData else {
            return nil
         }
         
         try data.write(to: fileURL)
         return self.create(fileIdentifier: fileIdentifier, fileUrl: fileURL, options: media.attachmentOptions)
      } catch {
         print("error " + error.localizedDescription)
      }
      return nil
   }
   
   static func create(fileIdentifier: String, fileUrl: URL, options: [String : Any]? = nil) -> UNNotificationAttachment? {
      var n: UNNotificationAttachment?
      do {
         n = try UNNotificationAttachment(identifier: fileIdentifier, url: fileUrl, options: options)
      } catch {
         print("error " + error.localizedDescription)
      }
      return n
   }

}

