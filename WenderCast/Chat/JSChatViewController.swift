//
//  ChatViewController.swift
//  SwiftExample
//
//  Created by Dan Leonard on 5/11/16.
//  Copyright © 2016 MacMeDan. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class JSChatViewController: JSQMessagesViewController {
   
   var mediaInteractor: JSChatMediaInteractor!
   
   var messages = [JSQMessage]()
   var incomingBubble: JSQMessagesBubbleImage!
   var outgoingBubble: JSQMessagesBubbleImage!
   
   override var senderId: String! {
      get {
         if let currentUser = UsersManager.currentUser {
            return currentUser.userId
         } else {
            return nil
         }
      }
      set {
         super.senderId = newValue
      }
   }
   
   override var senderDisplayName: String! {
      get {
         if let currentUser = UsersManager.currentUser {
            return currentUser.name
         } else {
            return nil
         }
      }
      set {
         super.senderDisplayName = newValue
      }
   }
   
// MARK: - View's life cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      JSChatViewConfigurator.configure(self)
      
      // Setup navigation
      setupBackButton()
      
      // Bubbles with tails
      incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
      outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.lightGray)
      
      // With Avatars
      collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
      collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
      
      // This is a beta feature that mostly works but to make things more stable it is diabled.
      collectionView?.collectionViewLayout.springinessEnabled = false
      
      automaticallyScrollsToMostRecentMessage = true
      
      self.collectionView?.reloadData()
      self.collectionView?.layoutIfNeeded()
   }

   
// MARK: - Handling of pressing on buttons
   
   override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
      /**
       *  Sending a message. Your implementation of this method should do *at least* the following:
       *
       *  1. Play sound (optional)
       *  2. Add new id<JSQMessageData> object to your data source
       *  3. Call `finishSendingMessage`
       */
      
      if let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text) {
         self.messages.append(message)
         self.finishSendingMessage(animated: true)
      }
   }
   
   override func didPressAccessoryButton(_ sender: UIButton) {
      self.inputToolbar.contentView!.textView!.resignFirstResponder()
      mediaInteractor.presentMediaMenu()
   }
   
   func setupBackButton() {
      let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
      navigationItem.leftBarButtonItem = backButton
   }
   
   func backButtonTapped() {
      dismiss(animated: true, completion: nil)
   }
}

// MARK: - ChatMediaInteractorOutput add media to the current message

extension JSChatViewController: ChatMediaInteractorOutput {
   
   func addMedia(_ media: JSQMediaItem) {
      if let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media) {
         self.messages.append(message)
         //Optional: play sent sound
         self.finishSendingMessage(animated: true)
      }
   }
}

//MARK: JSQMessages CollectionView DataSource

extension JSChatViewController {
   
   fileprivate func getAvatar(_ senderId: String) -> JSQMessageAvatarImageDataSource? {
      let avatar = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "AV", backgroundColor: UIColor.blue, textColor: UIColor.black, font: UIFont.systemFont(ofSize: 12), diameter: 22)
      return avatar
   }
   
   override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return messages.count
   }
   
   override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
      return messages[indexPath.item]
   }
   
   override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource {
      let message = messages[indexPath.item]
      if message.senderId == self.senderId {
         return outgoingBubble
      } else {
         return incomingBubble
      }
   }
   
   override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
      let message = messages[indexPath.item]
      if let avatar = getAvatar(message.senderId) {
         return avatar
      } else {
         return nil
      }
   }
   
   override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
      // Show a timestamp for every 3rd message
      if (indexPath.item % 3 == 0) {
         let message = self.messages[indexPath.item]
         return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
      } else {
         return nil
      }
   }
   
   override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
      // Show sender name
      let message = messages[indexPath.item]
      if message.senderId == self.senderId {
         return nil
      } else {
         return NSAttributedString(string: message.senderDisplayName)
      }
   }
   
   override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
      // Show a timestamp for every 3rd message
      if indexPath.item % 3 == 0 {
         return kJSQMessagesCollectionViewCellLabelHeightDefault
      } else {
         return 0.0
      }
   }
   
   override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
      // Show sender name
      let currentMessage = self.messages[indexPath.item]
      if currentMessage.senderId == self.senderId {
         return 0.0
      } else if indexPath.item - 1 > 0 {
         let previousMessage = self.messages[indexPath.item - 1]
         if previousMessage.senderId == currentMessage.senderId {
            return 0.0
         }
      }
      return kJSQMessagesCollectionViewCellLabelHeightDefault;
   }
}
