//
//  JSChatMediaInteractor.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 4/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import JSQMessagesViewController

protocol ChatMediaInteractorOutput {
   func addMedia(_ media: JSQMediaItem)
}


class JSChatMediaInteractor: NSObject {
   
   weak var viewController: UIViewController!
   var output: ChatMediaInteractorOutput!
   
   // MARK: - Add Media
   
   func presentMediaMenu() {
      let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
      
      let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in
         self.addPhoto()
      }
      
      let locationAction = UIAlertAction(title: "Send location", style: .default) { (action) in
         self.addLocation()
      }
      
      let videoAction = UIAlertAction(title: "Send video", style: .default) { (action) in
         self.addVideo()
      }
      
      let audioAction = UIAlertAction(title: "Send audio", style: .default) { (action) in
         self.addAudio()
      }
      
      let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
      
      sheet.addAction(photoAction)
      sheet.addAction(locationAction)
      sheet.addAction(videoAction)
      sheet.addAction(audioAction)
      sheet.addAction(cancelAction)
      
      viewController.present(sheet, animated: true, completion: nil)
   }
   
   fileprivate func addPhoto() {
      // Create fake photo
      if let photoItem = JSQPhotoMediaItem(image: UIImage(named: "goldengate")) {
         self.output.addMedia(photoItem)
      }
   }
   
   fileprivate func addLocation() {
      if let locationItem = self.buildLocationItem() {
         self.output.addMedia(locationItem)
      }
   }
   
   fileprivate func addVideo() {
      if let videoItem = self.buildVideoItem() {
         self.output.addMedia(videoItem)
      }
   }
   
   fileprivate func addAudio() {
      if let audioItem = self.buildAudioItem() {
         self.output.addMedia(audioItem)
      }
   }
   

   fileprivate func buildVideoItem() -> JSQVideoMediaItem? {
      
      let videoURL = URL(fileURLWithPath: "file://")
      
      if let videoItem = JSQVideoMediaItem(fileURL: videoURL, isReadyToPlay: true) {
         return videoItem
      } else  {
         return nil
      }
   }
   
   fileprivate func buildAudioItem() -> JSQAudioMediaItem? {
      let sample = Bundle.main.path(forResource: "audio_sample", ofType: "m4a")
      let audioData = try? Data(contentsOf: URL(fileURLWithPath: sample!))
      let audioItem = JSQAudioMediaItem(data: audioData)
      return audioItem
   }
   
   fileprivate func buildLocationItem() -> JSQLocationMediaItem? {
      if let myCurrentLocation = LocationManager.myCurrentLocation {
         let location = CLLocation(latitude: myCurrentLocation.latitude, longitude: myCurrentLocation.longitude)
         let locationItem = JSQLocationMediaItem()
         locationItem.setLocation(location) {}
         return locationItem
      } else {
         return nil
      }
   }
}
