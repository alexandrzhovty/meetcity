//
//  EditProfileViewController.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/21/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var navigatorBarView: UIView!
   
   
   var configurator: EditProfileConfigurator!
   var output: EditProfilePresenter!
   var interactor: EditProfileInteractor!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      configurator = EditProfileConfigurator()
      configurator.configure(self)
      navigatorBarView.backgroundColor = GlobalColors.kTabBarColor
      titleLabel.text = "EDIT PROFILE"
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      tableView.reloadData()
   }
   
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any vresources that can be recreated.
   }
   
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      let destinationViewController = segue.destination as! ProfileBaseViewController
      destinationViewController.interactor = interactor
   }
   
   @IBAction func backButtonPressed(_ sender: Any) {
      if interactor.checkData() {
         let _ = self.navigationController?.popViewController(animated: true)
      }
   }
}

// MARK: - UITableView Management

extension EditProfileViewController: UITableViewDataSource, UITableViewDelegate {
   
   public func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return output.numberOfRows
   }
   
   public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      return output.cellForRow(indexPath.row)
   }
   
   public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
      performSegue(withIdentifier: output.segueForRow(indexPath.row), sender: self)
   }
   
   public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 56.0
   }
}

