//
//  AppDelegate.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/11/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import UserNotifications
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps
import Firebase
import SwiftyJSON
import CoreData
import CoreLocation
import SVProgressHUD
import SwiftBackgroundLocation
import ReachabilitySwift
import SwiftyNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
    
    let reachability = Reachability()!
    var reachabilityNotificatin: SwiftyNotifications? = nil
	
	var locationManager = TrackingHeadingLocationManager()
	var backgroundLocationManager = BackgroundLocationManager(regionConfig: RegionConfig(regionRadius: 1.0))
	
	
	var mainContext: NSManagedObjectContext = CoredataStack.mainContext
	
	enum DatingStatus {
		case dating
		case searching
	}
	
    var datingStatus: DatingStatus {
        let request: NSFetchRequest<DMInvite> = DMInvite.fetchRequest()
        let predicates = [
            NSPredicate(format: "%K in %@", #keyPath(DMInvite.stateValue) ,InviteState.openValues.map{ $0.rawValue }),
            NSPredicate(format: "%K > %d", #keyPath(DMInvite.scheduled), Date().timeIntervalSince1970 - 3600 * 2)
        ]
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        
        do {
            let qty = try AppDelegate.shared.mainContext.count(for: request)
            if qty == 0 {
                return .searching
            } else {
                return .dating
            }
        } catch {
            print(error.localizedDescription)
            return .searching
        }
        
        
        
    }
	
	
	var application: UIApplication!
	
	static var shared: AppDelegate {
		guard let `self` = UIApplication.shared.delegate as? AppDelegate else {
			fatalError()
		}
		return self
	}
	
	
	dynamic var userLocation = CLLocation(latitude: kCLLocationCoordinate2DInvalid.latitude, longitude: kCLLocationCoordinate2DInvalid.longitude)
	
	var rootVC: UINavigationController {
		guard let navVC = self.window?.rootViewController as? UINavigationController else {
			fatalError()
		}
		return navVC
	}
	
	static var me: DMUser! {
        get { return AppDelegate.shared._generalUser }
        set {
            let contexgt = AppDelegate.shared.mainContext
            if newValue == nil || newValue.managedObjectContext == contexgt {
                AppDelegate.shared._generalUser = newValue
            } else {
                AppDelegate.shared._generalUser = contexgt.object(with: newValue.objectID) as? DMUser
            }
            
        }
	}
	
	fileprivate var _generalUser: DMUser!
    
    @available(*, deprecated, message: "Use static property `me` instead")
	var generalUser: DMUser! {
		get { return _generalUser }
		set {
			if newValue.managedObjectContext != self.mainContext {
				_generalUser = self.mainContext.object(with: newValue.objectID) as! DMUser
			} else {
				_generalUser = newValue
			}
		}
	}
	
	// MARK: - State application changing
	
	func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
		return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
	}
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
		
		
		self.application  = application
		
		Appearance.customize()
		Settings.registerDefaults()
		
		DMUser.removeAll(from: self.mainContext)
		
		MessageManager.default.registerRemoteNotifications(for: application) {
            
			
			GMSServices.provideAPIKey(GoogleMaps.GoogleMapsAPIKey)
			FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
			// It doesn't work with old version
			if UserDefaults.standard.bool(forKey: "SecondLogin") == false {
				Presenter.presentLoginScreen()
				return
			}
			
			if let currentUser = Auth.auth().currentUser {
				
				self._generalUser = DMUser.user(with: currentUser, in: self.mainContext)
				
                let userRef = FirebaseStack.users.child( currentUser.uid)
                userRef.observeSingleEvent(of: .value, with: { snapshot in
                    if snapshot.exists(), let value = snapshot.value as? [String: Any] {
                        // Parsing user in main context
                        self._generalUser.update(with: JSON(value), into: self.mainContext)
                        
                        if let profileRef = self._generalUser.profilePhotoeRef {
                            profileRef.downloadURL(completion: { (url, _) in
                                self._generalUser.profilePhotoUrl = url?.absoluteString
                            })
                        }
                        
                        FirebaseStack.currentInvitation(for: "me", compretion: { json in
                            
                            // No open invitation/dating
                            if json  == nil {
                                
//                                if let options = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] {
//                                    let json = JSON(options)
//                                    messageType = json["messageType"].stringValue
//                                    uid = json["uid"].stringValue
//                                }
                                
                                Presenter.presentNearByList()
                                
                                
                            } else {
                                let context = AppDelegate.shared.mainContext
                                let id = json![DMInvite.CodingKeys.id].stringValue
                                
                                let invite = DMInvite.invite(for: id, in: context) ?? {
                                    let entity = DMInvite(context: context)
                                    entity.id = id
                                    return entity
                                    }()
                                
                                invite.update(with: json!, into: context)
                                
                                
                                do { try context.save() }
                                catch {
                                    print("cannot save new invire")
                                    Presenter.presentNearByList()
                                    return
                                }
                                
                                
                                let inviteModel: InviteModel
                                do { inviteModel = try InviteModel(with: invite, in: context) }
                                catch {
                                    print("cannot save new invite ", error.localizedDescription)
                                    Presenter.presentNearByList()
                                    return
                                }
                                
                                if inviteModel.isWaitingForResponse {
                                    if inviteModel.sender.uid == AppDelegate.me.uid {
                                        Presenter.presentRequest(for: inviteModel)
                                    } else {
                                        Presenter.presentMeetingArragement(for: inviteModel)
                                    }
                                }
                                else
                                if inviteModel.isWaitingForReview {
                                    if inviteModel.sender.uid == AppDelegate.me.uid {
                                        Presenter.presentRequest(for: inviteModel)
                                    } else {
                                        Presenter.presentMeetingArragement(for: inviteModel)
                                    }
                                }
                                else
                                if inviteModel.isDatingBegun {
                                    Presenter.presentDating(for: inviteModel)
                                    
                                } else {
                                    Presenter.presentNearByList()
                                }
                                
//                                switch inviteModel.state {
//                                case .datingBegin:
//                                    Presenter.presentDating(for: inviteModel)
//                                    
//                                default:
//                                    Presenter.presentNearByList()
//                                }
                                
                                
                            }
                        })
                        
                        
                        MessageManager.default.updateDeviceToken()
                        LocationManager.default.startUpdatingLocation()
                        
                    } else {
                        Presenter.presentLoginScreen()
                    }
                })
				
			} else {
				Presenter.presentLoginScreen()
				
			}
		}
		
		return true
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		FBSDKAppEvents.activateApp()
		self.startMonitoringReachability()
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		FBSDKAppEvents.activateApp()
        
        reachability.stopNotifier()
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
        LocationManager.default.startUpdatingLocation()
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
		
	}
	
}

// MARK: - Log out

extension AppDelegate {
    @discardableResult
    static func logOut() -> Result<Any?> {
        
        
        // Remove account from firebase
        do {
            try Auth.auth().signOut()
        } catch {
            return .failure(error)
        }
        
        // Remove records from the local database
        DMUser.removeAll(from: AppDelegate.shared.mainContext)
        try? AppDelegate.shared.mainContext.save()
        
        // Empty defauls
        Settings.resetStandardUserDefaults()
        
        // Remove instagram account if it is necessary
        InstagramManager.logOut()
        
        // Clear all cash & cookies
        for cookie in HTTPCookieStorage.shared.cookies ?? [] {
            HTTPCookieStorage.shared.deleteCookie(cookie)
        }
        
        UserDefaults.standard.synchronize()
        
        // Removes cache for all responses
        URLCache.shared.removeAllCachedResponses()
        
        
        return .success(nil)
    }
}

// MARK: - The toppest view controller
extension UIApplication {
    class func topViewController(base: UIViewController? = AppDelegate.shared.window!.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

// MARK: - Reachability
extension AppDelegate {
    func startMonitoringReachability() {
        
        self.reachabilityNotificatin?.dismiss()
        self.reachabilityNotificatin?.removeFromSuperview()
        self.reachabilityNotificatin = nil
        
        do{
            try reachability.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
    }
    
    
    func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            if reachabilityNotificatin != nil {
                DispatchQueue.main.async { [unowned self] in
                    self.reachabilityNotificatin?.dismiss()
                    self.reachabilityNotificatin?.removeFromSuperview()
                    self.reachabilityNotificatin = nil
                }
                
            }
        } else {
            print("Network not reachable")
            
            DispatchQueue.main.async {
                
                if let window = self.window {
                    
                    self.reachabilityNotificatin?.dismiss()
                    
                    let notification = SwiftyNotifications.withStyle(style: .warning,
                                                                     title: "Internet",
                                                                     subtitle: "connection lost")
                    window.addSubview(notification)
                    notification.show()
                    
                    self.reachabilityNotificatin = notification
                    
                }
            }
        }
    }
}
