//
//  UserView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

final class UserView: UIView {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var occupationLabel: UILabel!
	
	var userModel: UserModel! {
		didSet {
			unregisterObservers()
			configureView()
			registerObservers()
		}
	}
	
	// MARK: Private
	fileprivate var _observers = [DatabaseReference]()
	
	
	deinit {
		unregisterObservers()
	}
	
	
	private func configureView() {
		titleLabel.text = userModel.title
		occupationLabel.text = userModel.occupation
	}
	
	
	// MARK: - Observers
	func registerObservers() {
		
		// Occupation
		let refOccupation = FirebaseStack.users.child(userModel.uid).child(DMUser.CodingKeys.occupation)
		refOccupation.observe(.value, with: {
			[weak self] snapshot in
			guard let `self` = self else { return }
			self.configureView()
		})
		_observers.append(refOccupation)
		
		
	}
	
	
	func unregisterObservers() {
		_observers.forEach{ $0.removeAllObservers() }
		_observers.removeAll()
	}
	
	
}
