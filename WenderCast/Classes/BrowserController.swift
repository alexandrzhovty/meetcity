//
//  BrowserController.swift
//  ModelCarWorld
//
//  Created by Aleksandr Zhovtyi on 5/31/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import WebKit

/*
 SegueHandler implementation should be added to the ModelCarWorld
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */


enum BrowserControllerPresentationType: Int {
    case start
    case settings
    case impressum
    case privacy
}

final class BrowserController: UIViewController {
    //    MARK: - Properties & variables
    //    MARK: Public
    var url: URL?
    
    //    MARK: Outlets
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var backButton: UIBarButtonItem!
    
    //    MARK: Private
    private var webView: WKWebView!
    private var observerWasCreated = false
    
    //    MARK: Enums & Structures
    internal enum SegueIdentifier: String {
        case none
    }
    
    
    
    //    MARK: - View life cycle
    deinit {
        unregisterObservers()
//        NetworkActivity.stop()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Appearance.customize(viewController: self)
        
        navigationItem.leftBarButtonItem = nil
        
        setUp()
        
        // Customize web view
        webView = { [unowned self] in
            var frame = self.view.bounds
            frame.origin.y = 0
            frame.size.height = frame.size.height - frame.origin.y
            
            let webView = WKWebView(frame: frame)
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = true
            webView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            self.view.insertSubview(webView, at: 0)
            return webView
            }()
        
        
        registerObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if webView.url == nil, let url = self.url {
            webView.load(URLRequest(url: url))
        }
    }
    
    //    MARK: - Observer
    func registerObservers() {
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: "canGoBack", options: .new, context: nil)
        observerWasCreated = true
    }
    
    func unregisterObservers() {
        if observerWasCreated {
            webView.removeObserver(self, forKeyPath: "estimatedProgress")
            webView.removeObserver(self, forKeyPath: "canGoBack")
        }
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case "estimatedProgress"?:
            self.progressView.isHidden = self.webView.estimatedProgress == 1
            self.progressView.progress = Float(webView.estimatedProgress)

        case "canGoBack"?:
            navigationItem.leftBarButtonItem = webView.canGoBack ? backButton : nil
        
        default:
            return super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    //    MARK: - Outlet functions
    @IBAction func didTapBack(_ sender: Any) {
        webView.goBack()
    }
    
    @IBAction func didTapClose(_ button: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //    MARK: - Utilities
    private func setUp() {
        var idx: Int = 0
        if let tabbarItem = navigationController?.tabBarItem {
            if let tabbar = navigationController?.tabBarController?.tabBar, let items = tabbar.items {
                if let i = items.index(of: tabbarItem) {
                    idx = i
                }
            }
        }
        
        print("current item: \(idx)")
        
    }
    
}

//    MARK: - Web view protocols
extension BrowserController: WKNavigationDelegate {
    //    MARK: Navigation delegate
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        progressView.isHidden = false
//        NetworkActivity.start()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.isHidden = true
        progressView.progress = 0.0
//        NetworkActivity.stop()
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        NetworkActivity.stop()
    }
    
    // Recognize an Apple Maps URL. This will trigger the native app. But only if a search query is present. Otherwise
    // it could just be a visit to a regular page on maps.apple.com.
    fileprivate func isAppleMapsURL(_ url: URL) -> Bool {
        if url.scheme == "http" || url.scheme == "https" {
            if url.host == "maps.apple.com" && url.query != nil {
                return true
            }
        }
        return false
    }
    
    // Recognize a iTunes Store URL. These all trigger the native apps. Note that appstore.com and phobos.apple.com
    // used to be in this list. I have removed them because they now redirect to itunes.apple.com. If we special case
    // them then iOS will actually first open Safari, which then redirects to the app store. This works but it will
    // leave a 'Back to Safari' button in the status bar, which we do not want.
    fileprivate func isStoreURL(_ url: URL) -> Bool {
        if url.scheme == "http" || url.scheme == "https" {
            if url.host == "itunes.apple.com" {
                return true
            }
        }
        return false
    }
    
    // Mark: User Agent Spoofing
    
    fileprivate func resetSpoofedUserAgentIfRequired(_ webView: WKWebView, newURL: URL) {
        // Reset the UA when a different domain is being loaded
//        if webView.url?.host != newURL.host {
//            webView.customUserAgent = nil
//        }
    }
    
    fileprivate func restoreSpoofedUserAgentIfRequired(_ webView: WKWebView, newRequest: URLRequest) {
        // Restore any non-default UA from the request's header
//        let ua = newRequest.value(forHTTPHeaderField: "User-Agent")
//        webView.customUserAgent = ua != UserAgent.defaultUserAgent() ? ua : nil
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.cancel)
            return
        }
        
        if url.scheme == "about" {
            decisionHandler(WKNavigationActionPolicy.allow)
            return
        }
        
        if !navigationAction.isAllowed && navigationAction.navigationType != .backForward {
            print("\(navigationAction.isAllowed) \(navigationAction.navigationType == .backForward) \(String(describing: navigationAction.request.url))")
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        // First special case are some schemes that are about Calling. We prompt the user to confirm this action. This
        // gives us the exact same behaviour as Safari.
        
        if url.scheme == "tel" || url.scheme == "facetime" || url.scheme == "facetime-audio" {
            if let phoneNumber = url.path.removingPercentEncoding, !phoneNumber.isEmpty {
                let formatter = PhoneNumberFormatter()
                let formattedPhoneNumber = formatter.formatPhoneNumber(phoneNumber)
                let alert = UIAlertController(title: formattedPhoneNumber, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:"Label for Cancel button"), style: UIAlertActionStyle.cancel, handler: nil))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Call", comment:"Alert Call Button"), style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                    
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    
                }))
                present(alert, animated: true, completion: nil)
            }
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        // Second special case are a set of URLs that look like regular http links, but should be handed over to iOS
        // instead of being loaded in the webview. Note that there is no point in calling canOpenURL() here, because
        // iOS will always say yes. TODO Is this the same as isWhitelisted?
        
        if isAppleMapsURL(url) || isStoreURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        // Handles custom mailto URL schemes.
        if url.scheme == "mailto" {
            let app = UIApplication.shared
            if app.canOpenURL(url) {
                app.open(url, options: [:], completionHandler: nil)
            }
            
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
//            if let mailToMetadata = url.mailToMetadata(), let mailScheme = self.profile.prefs.stringForKey(PrefsKeys.KeyMailToOption), mailScheme != "mailto" {
//                self.mailtoLinkHandler.launchMailClientForScheme(mailScheme, metadata: mailToMetadata, defaultMailtoURL: url)
//            } else {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            }
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        // This is the normal case, opening a http or https url, which we handle by loading them in this WKWebView. We
        // always allow this. Additionally, data URIs are also handled just like normal web pages.
        if url.scheme == "http" || url.scheme == "https" || url.scheme == "data" {
            if navigationAction.navigationType == .linkActivated {
                resetSpoofedUserAgentIfRequired(webView, newURL: url)
            } else if navigationAction.navigationType == .backForward {
                restoreSpoofedUserAgentIfRequired(webView, newRequest: navigationAction.request)
            }
            decisionHandler(WKNavigationActionPolicy.allow)
            return
        }
        
        // Default to calling openURL(). What this does depends on the iOS version. On iOS 8, it will just work without
        // prompting. On iOS9, depending on the scheme, iOS will prompt: "Firefox" wants to open "Twitter". It will ask
        // every time. There is no way around this prompt. (TODO Confirm this is true by adding them to the Info.plist)
        
        UIApplication.shared.open(url, options: [:]) { (success) in
            if !success {
                Alert.default.showOk(Strings.UnableToOpenURLErrorTitle, message: Strings.UnableToOpenURLError)
            }
        }
       
        decisionHandler(WKNavigationActionPolicy.cancel)
    }
}
