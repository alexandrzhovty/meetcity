//
//  Settings.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

struct Settings {}

//    MARK: - Google map
extension Settings {
    struct GoogleMap {
//        static let apiKey = "AIzaSyA4Vj3JCRk8YmXvalKX1ptmqy_fE42eSps"
        
        // https://console.developers.google.com/apis/credentials?project=meetcity-155714
        // meetcity-155714
        static let apiKey = "AIzaSyA_Eg0zQ7auzd9iVK7a66wZ1VKiZIvgvBA"
        static let directionsAPIKey = "AIzaSyD6I5M0R5EA5YrAUaqtjSFZPZ33gVp9Iws"
        
        private init(){}
    }
}


extension Settings {
    struct Facebook {
        static let FacebookAppId = "1864841137133416"
        static let FacebookSecret = "c204a33d7d84fde72fa41ab112fe988a"
        
        private init(){}
    }
}

extension Settings {
    struct Firebase {
        static let ServerKey_Old = "AIzaSyDXYNZYwvlNHG8STzEqe_hUl3KPk7C1m1w"
        static let Storage_url_Old = "gs://meetcity-38a8d.appspot.com"
        
        static let ServerKey = "AIzaSyDkaQqP_W32WWX2IndxSXjle4mkz6cS2kg"
        static let Storage_url = "gs://meetcity-57e45.appspot.com"
        
        static let email = "meetcity@vesedia.com"
        static let password = "bRooEMnV"
        
        static let PushNotificationUrl = "https://fcm.googleapis.com/fcm/send"
        private init(){}
    }
}

extension Settings {
    struct DataBaseName {
        static let main = "main"
        static let test = "test"
        private init(){}
    }
}
