//
//  ImageTourCollectionCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class ImageTourCollectionCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension ImageTourCollectionCell: ImageTourCellProtocol {
    var message: String? {
        get { return messageLabel.text }
        set { messageLabel.text = newValue }
    }
    
    var image: UIImage? {
        get { return imageView.image }
        set { imageView.image = newValue }
    }
}
