//
//  NavigationAction.swift
//  ModelCarWorld
//
//  Created by Aleksandr Zhovtyi on 5/31/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import WebKit

internal extension WKNavigationAction {
    /// Allow local requests only if the request is privileged.
    var isAllowed: Bool {
        guard let url = request.url else {
            return true
        }
        return !url.isWebPage(includeDataURIs: false) || !url.isLocal || request.isPrivileged
    }
    
    var isDisallowed: Bool {
        return !self.isAllowed
    }
}
