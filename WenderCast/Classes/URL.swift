//
//  URL.swift
//  ModelCarWorld
//
//  Created by Aleksandr Zhovtyi on 5/31/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

extension URL {
    public func isWebPage(includeDataURIs: Bool = true) -> Bool {
        let schemes = includeDataURIs ? ["http", "https", "data"] : ["http", "https"]
        if let scheme = scheme, schemes.contains(scheme) {
            return true
        }
        
        return false
    }
    
    public var isLocal: Bool {
        guard isWebPage(includeDataURIs: false) else {
            return false
        }
        // iOS forwards hostless URLs (e.g., http://:6571) to localhost.
        guard let host = host, !host.isEmpty else {
            return true
        }
        
        return host.lowercased() == "localhost" || host == "127.0.0.1"
    }
}
