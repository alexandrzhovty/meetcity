//
//  TourListController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */
//    MARK: -
extension TourListController: SegueHandler {
    //    MARK: SegueHandler protocol
    internal enum SegueType: String {
        case none
    }
}

//    MARK: -
final class TourListController: UIViewController  {
    //    MARK: Properties & variables
    //    MARK: Public
    
    //    MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: CustomImagePageControl!
    
    //    MARK: Private
    fileprivate lazy var dataProvider: TourListDataProvider = TourListDataProvider()
    
    //    MARK: Enums & Structures
    
    
    //    MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        // Colletion view
        TourListViewModel.registerNibs(for: collectionView)
        
        // Page controll
//        pageControl.numberOfPages = datasource.count
        
        // Customize table view
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 44
        
        // Configure view
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pageControl.updateDots()
    }
    
    // MARK: - Navigation
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        switch segueType(for: identifier) {  // SegueHandler
//        case .none: return true
//        }
//    }
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        switch segueType(for: segue) {
//        case .none: break
//        }
//    }
    
    //    MARK: - Outlet functions
    
    
    //    MARK: - Utilities
    
}

//    MARK: - Collection view protocol
extension TourListController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    //    MARK: Data source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataProvider.numberOfSection
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = dataProvider.numberOfItems(in: section)
        return dataProvider.numberOfItems(in: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let tour = dataProvider.object(at: indexPath)
        return collectionView.dequeueReusableCell(with: tour.type, for: indexPath)
    }
    
    //    MARK: Displaying
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? TourCellProtocol else {
            assertionFailure(); return
        }
        let model = TourListViewModel(item: dataProvider.object(at: indexPath))
        model.configure(cell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? VideoTourCellProtocol else {
            return
        }
        cell.stopVideo()
    }
    
    
    
    //    MARK: - Flowlayout delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    //    MARK: - Scroll view delegagte
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        pageControl.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        pageControl.updateDots()
    }
    
}
