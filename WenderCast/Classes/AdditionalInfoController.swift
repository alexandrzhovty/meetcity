//
//  AdditionalInfoController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/2/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */
//    MARK: -
extension AdditionalInfoController: SegueHandler {
    //    MARK: SegueHandler protocol
    internal enum SegueType: String {
        case none
    }
}

//    MARK: -
class AdditionalInfoController: UITableViewController  {
    //    MARK: Properties & variables
    //    MARK: Public
    
    //    MARK: Outlets
    
    //    MARK: Private
    
    //    MARK: Enums & Structures
    
    
    //    MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEVELOPMENT
            navigationItem.title = NSLocalizedString("Development", comment: "").localizedUppercase
        #endif
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        // Customize table view
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 44
        
        // Configure view
        
        
    }
    
    // MARK: - Navigation
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {  // SegueHandler
        case .none: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .none: break
        }
    }
    
    //    MARK: - Outlet functions
    
    
    //    MARK: - Utilities
    
}
