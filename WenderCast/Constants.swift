//
//  Constants.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/9/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//
import UIKit


struct Constants {
	private init(){}
}

// MARK: - Registration Controller
extension Constants {
	static let AboutMeMaxCount = 500
	static let OccupationMaxCount = 100
	static let MainMargin: CGFloat = 24
}

extension Constants {
	enum allowedAge {
		static let min = 17
		static let max = 60
	}
}

extension Constants {
	enum TableView {
		static let profileCellHeight: CGFloat = 56
	}
}

extension Constants {
    enum ReadyToMeet {
        /// Time from last visit, if time is more then user isn't **ReadyToMeet**
        static let timeIntervalFromLastVisit: TimeInterval = 60 * 60 * 6
    }
}

extension Constants {
	enum Invitation {
		/// Time to wait for opponent response to your request
		static let waitForResponseToRequest: TimeInterval = 2 * 60
      
      /// If we modify the previously accepted invitation, we give much more time to
      static let waitForResponseToModifyRequest: TimeInterval = 10 * 60
      
      /// Time to review the request for dating
		static let waitingForReviewTime: TimeInterval = 3 * 60
      
      /// If we modify the previously accepted invitation, we give much more time to review the request
      static let waitingForModifyeReview: TimeInterval = 60 * 60
	}
}

struct UserNotifications {
   static let requestIdentifier = "requestIdentifier"
   static let replyIdentifier = "replyIdentifier"
   static let categoryIdentifier = "catgeoryIdentifier"
   static let imageIdentifier = "imageIdentifier"
   static let meetCityContentCategory = "meetCityContentCategory"
}

struct PushNotifications {
   static let simpleNotificationIdentifier = "SimplePushNotificationIdentifier"
   static let actionNotificationIdentifier = "ActionPushNotificationIdentifier"
   static let silentNotificationIdentifier = "SilentPushNotificationIdentifier"
   
   struct id {
      static let requestForDating = "RequestForDatingPushNotification"
      static let cancelDating = "CancelDatingPushNotification"
      static let checkin = "CheckinPushNotification"
      static let accept = "AcceptPushNotification"
      static let refuse = "RefusePushNotification"
      static let chat = "ChatPushNotification"
      static let warning = "WarningPushNotification"
   }
}

struct Notifications {
   
   struct Application {
      static let WillEnterForeground = "WillEnterForeground"
      static let DidBecomeActive = "DidBecomeActive"
      static let WillResignActive = "WillResignActive"
      static let DidEnterBackground = "DidEnterBackground"
   }
   
   struct ChangeData {
      static let UsersDataBase = "UsersDataBaseChanged"
      static let RequestDataBase = "RequestDataBaseChanged"
      static let CurrentUser = "CurrentUserChanged"
      static let MyCurrentLocation = "MyCurrentLocation"
      static let FavoritesDataBase = "FavoritesDataBaseChanged"
      static let AvatarDataBase = "AvatarDataBaseChanged"
      static let BanUsersDataBase = "BanUsersDataBaseChanged"
      static let OnlinesDataBase = "OnlinesDataBase"      
   }
   
   struct Badge {
      static let Nearby = "BadgeNearby"
      static let Request = "BadgeRequest"
      static let Date = "BadgeDate"
      static let Messages = "BadgeMessages"
      static let profileBadgeUpNotification = "ProfileBadgeUpNotification"
   }
   
   struct Messages {
      static let newIncomingMessageNotification = "NewIncomingMessageNotification"
      static let newOutgouingMessageNotification = "NewOutgouingMessageNotification"
   }

   struct Location {
      static let changeAuthorization = "changeAuthorization"
      static let goToOnMapPosition = "goToOnMapPosition"
   }
   
   static let didChangeDeviceToken = "DeviceTokenChanged"
   static let hideUserDetailsWithOnMapPosution = "HideUserDetailsWithOnMapPosution"
   static let hideUserDetailsView = "HideUserDetailsView"
   static let inputRequestDatePlace = "InputRequestDatePlace"
   static let WillEnterMainScreen = "WillEnterMainScreen"
   static let DidSelectTabBarItem = "DidSelectTabBarItem"
   
   struct Internet {
      static let Connected = "InernetConnectedKey"
      static let DisConnected = "InernetDisConnectedKey"
   }
   
}



// MARK: - Fields



// MARK: -

struct Facebook {
   static let FacebookAppId = "1889037364669985"
   static let FacebookSecret = "d3cad67ddf4b9807036aff00bcb80ab9"
}

struct GoogleMaps {
   // https://console.developers.google.com/apis/credentials?project=meetcity-155714
   // meetcity-155714
   static let GoogleMapsAPIKey = "AIzaSyA_Eg0zQ7auzd9iVK7a66wZ1VKiZIvgvBA"
   static let GoogleMapsDirectionsAPIKey = "AIzaSyD6I5M0R5EA5YrAUaqtjSFZPZ33gVp9Iws"
}

struct CheckinDate {
   static let kMinDistanceToUserForCheckin = 500.0
   static let kWatchIntervalInSec = 1.0
}

struct TimeIntervalDistance {
   static let kUpdateViewIntervalInSec = 3.0
   static let kWaitingDaysForAnswerRequest = 1
   static let kWaitingMinutesForReviewRequest = 3
   static let kReadyToMeetIntervalInHours = 6
}

struct Firebase {
   static let ServerKey_Old = "AIzaSyDXYNZYwvlNHG8STzEqe_hUl3KPk7C1m1w"
   static let Storage_url_Old = "gs://meetcity-38a8d.appspot.com"
   
   static let ServerKey = "AIzaSyDkaQqP_W32WWX2IndxSXjle4mkz6cS2kg"
   static let Storage_url = "gs://meetcity-57e45.appspot.com"
   
   static let email = "meetcity@vesedia.com"
   static let password = "bRooEMnV"
   
   static let PushNotificationUrl = "https://fcm.googleapis.com/fcm/send"
}

struct DataBaseName {
   static let main = "main"
   static let test = "test"
}

struct AppConstants {
   
   static let ApplicationName = "MeetCity"
   
   static let kDaysDecisionNewUser = 7
   static let kMinTimeRequestDate = 5
   // Time interval before alarm
   static let kAlarmBeforeDateMinuts = 15
   
   static let kMaxUserGalleryPhotosNumber = 7
   static let SizeAvatarImage = 72.0
   static let MaxDistanceToUpdateLocation: Double = 3.0
   static let ReminderTimerIntervalMin = 20
   static let TimerIntervalToCheckExpiredRequestSec = 10
   static let LaunchAppTimeIntervalWatchLoadUsersSec = 15.0
   
   static let DaysActualRequest = 7
   
   static let kEnableForgotPasswordFunctionality = false
   
   static let DefaultDataBase = DataBaseName.test  //  DataBaseName.main // 
}

struct DEBUG {
   static let Enable = false
   static let LocationEnable = false
   static let PushEnable = false
}




