//
//  StatusView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class StatusView: UIView {
	@IBOutlet weak var statusLabel: UILabel!
	
	var status: Bool = false { didSet { configureView() } }
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		layer.borderColor = UIColor(.pink).cgColor
		layer.borderWidth = 1
		backgroundColor = UIColor.clear
		
		statusLabel.text = nil
		
	}
	
	private func configureView() {
		if status {
			statusLabel.text = Strings.ReadyToMeet
            statusLabel.textColor = UIColor(rgbValue: 0x5dad1d)
            
		} else {
			statusLabel.text = Strings.NotReadyToMeet
            statusLabel.textColor = UIColor.readyToMeet.false
		}
        
        layer.borderColor = statusLabel.textColor.cgColor
	}
}
