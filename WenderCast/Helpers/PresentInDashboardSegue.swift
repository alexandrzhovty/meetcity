//
//  PresentInDashboardSegue.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class PresentInDashboardSegue: UIStoryboardSegue {
	
	override func perform() {
		let navVC = AppDelegate.shared.rootVC
		let first = navVC.viewControllers.first!
		destination.navigationItem.setHidesBackButton(true, animated: false)
		
		if navVC.presentedViewController != nil {
			navVC.setViewControllers([first, destination], animated: false)
			navVC.presentedViewController?.dismiss(animated: true, completion: nil)
			
		} else {
			navVC.setViewControllers([first, destination], animated: true)
		}
		
	}

}
