//
//  RequestingController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData
import SVProgressHUD
import Firebase

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class RequestingController: UIViewController {
    //    MARK: Public
    var invite: DMInvite!
    var managedObjectContext: NSManagedObjectContext!
    
	var observers = [Any]()
	
    //    MARK: Outlets
	@IBOutlet weak var imageView: UIImageView!
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var stopWatchLabel: UILabel!
	@IBOutlet weak var cancelButton: UIButton!
	
	
	
    //    MARK: Private
    fileprivate var inviteModel: InviteModel!
	fileprivate var _timer: Timer?
	fileprivate var _remainedTime: TimeInterval =  -1
	
    
    //    MARK: Enums & Structures
	deinit {
		stopTimer()
		unregisterObserver()
	}
}

//    MARK: - View life cycle
extension RequestingController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
		assert(invite != nil, "The MOC should be main")
        assert(managedObjectContext != nil, "The MOC should be main")
        assert(managedObjectContext == AppDelegate.shared.mainContext, "The MOC should be main")
        
		self.inviteModel = try! InviteModel(with: invite, in: managedObjectContext)
        
		
		navigationItem.title = inviteModel.opponentModel.title
		navigationItem.hidesBackButton = true
		
		
        // Customize appearance
        Appearance.customize(viewController: self)
        
		// Configure tableView
		configure(titleLabel)
		configure(imageView)
		configure(descriptionLabel)
		configure(cancelButton)
		configure(stopWatchLabel)
		
		
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpTimer()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
        
        // Notify oppoent only if then invite hasn't been sent yet
        if  inviteModel.state == InviteState.open  {
            notifyOpponent()
            
        } else {
            registerObserver()
            startTimer()
        }
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		if SVProgressHUD.isVisible() {
			SVProgressHUD.dismiss()
		}
		
		unregisterObserver()
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		stopTimer()
	}
}

// MARK: - Observer Protocol
extension RequestingController: ObserverProtocol {
	func registerObserver() {
        
        unregisterObserver()
		
		let ref = FirebaseStack.invitation(for: invite.identifier).child(DMInvite.CodingKeys.state)
		ref.observe(.value, with: { [weak self] snapshot in
			guard let `self` = self else { return }
			print(String(describing: type(of: self)),":", #function)
			print(snapshot.value ?? "Nothing recieved")
			
			if let newValue = snapshot.value as? Int64 {
				print("newValue \(newValue)")
				
				let newState = InviteState(rawValue: newValue) ?? InviteState.error
				if self.inviteModel.state != newState {
					self.inviteModel.state = newState
					
					if newState == .reviewing {
						SVProgressHUD.showSuccess(withStatus: "In review")
						DispatchQueue.main.async {
							//  Stop timer
                            self.stopTimer()
                            
                            // Modificaition request
                            self.invite.countdownStartedAt = Date().timeIntervalSince1970
                            self.setUpTimer()
                            
							// Start timer
							self.startTimer()
						}
						
						do { try self.inviteModel.context.save() }
						catch {
							Alert.default.showError(message: error.localizedDescription)
						}
					}
					
					else if newState.isDeclined {
						if self.shouldPerformSegue(withIdentifier: SegueType.showCanceled.rawValue, sender: nil) {
							self.performSegue(.showCanceled, sender: nil)
						}
					}
					else if newState == InviteState.declinedExpired {
						if self.shouldPerformSegue(withIdentifier: SegueType.showExpired.rawValue, sender: nil) {
							self.performSegue(.showExpired, sender: nil)
						}
					}
					else if newState == InviteState.datingBegin {
						if self.shouldPerformSegue(withIdentifier: SegueType.showSuccess.rawValue, sender: nil) {
							self.performSegue(.showSuccess, sender: nil)
						}
					}
					
					self.configure(self.titleLabel)
					
				}
			}
		})
		
		observers.append(ref)
        
        
        // Get opponent info
        observers.append(fetchOpponent())
		
	}
    
    private func fetchOpponent() -> DatabaseReference {
        let opponentRef = FirebaseStack.user(for: invite.opponent.uid!)
        opponentRef.observe(.value, with: { [weak self] snapshot in
            guard let `self` = self else { return }
            
            let nestedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            nestedContext.parent = AppDelegate.shared.mainContext
            nestedContext.perform {
                let user = nestedContext.object(with: self.invite.opponent.objectID) as! DMUser
                user.update(with: snapshot, into: nestedContext)
                
                if nestedContext.hasChanges {
                    do { try nestedContext.save() }
                    catch {
                        print("Cannot not parse the server response")
                    }
                }
                
                DispatchQueue.main.async {
                    self.configure(self.imageView)
                    self.configure(self.titleLabel)
                    self.configure(self.descriptionLabel)
                    self.configure(self.cancelButton)
                    self.configure(self.stopWatchLabel)
                }
                
            }
            
        })
        
        return opponentRef
    
    
    }
	
	func unregisterObserver() {
		observers.forEach {
			if let dbRef = $0 as? DatabaseReference {
				dbRef.removeAllObservers()
			}
		}
		observers.removeAll()
		
	}
}

//    MARK: - Dispalying
extension RequestingController  {
	// MARK: Interface
    fileprivate func configure(_ view: UIView) {
        switch view {
		case imageView:
			if let fileRef = inviteModel.opponentModel.profilePhotoeRef {
				ImageCache.default.load(forKey: fileRef) { [weak self] image  in
					guard let `self` = self else { return }
					self.imageView.image = image
				}
			}
		case titleLabel:
			
			print(String(describing: type(of: self)),":", #function, " ", inviteModel.state)
			
			switch inviteModel.state {
			case .sent:
				let format = Strings.Invitation.sentTo
				titleLabel.text = String.localizedStringWithFormat(format, inviteModel.opponentModel.name)
				
			case .reviewing:
				let format = Strings.Invitation.inReview
				titleLabel.text = String.localizedStringWithFormat(format, inviteModel.opponentModel.name)

				
			default:
				let format = Strings.Invitation.sentTo
				titleLabel.text = String.localizedStringWithFormat(format, inviteModel.opponentModel.name)
//				titleLabel.text = nil
			}
			
			
			
			
		case descriptionLabel:
			let format = Strings.Invitation.WaitForResponse
			let gender = inviteModel.opponentModel.gender == .girl ? Strings.Invitation.her : Strings.Invitation.his
			descriptionLabel.text = String.localizedStringWithFormat(format, gender)
			
		case cancelButton:
			cancelButton.setTitle(Strings.Invitation.CancelInvite, for: .normal)
            
		case stopWatchLabel:
            if _remainedTime < 0 {
                stopWatchLabel.text = nil
            } else {
                let ti = Int(_remainedTime)
                let seconds = ti % 60
                let minutes = (ti / 60) % 60
                stopWatchLabel.text = String(format: "%zd:%0.2d", minutes, seconds)
                
            }
        default: break
        }
    }
}

    
// MARK: - Timer
extension RequestingController  {
    fileprivate func setUpTimer() {
        
        
        switch inviteModel.state {
        case .open:
            if self.isInviteModification {
                self._remainedTime = Constants.Invitation.waitForResponseToModifyRequest
            } else {
                self._remainedTime = Constants.Invitation.waitForResponseToRequest
            }
            
        case .sent:
            if self.isInviteModification {
                self._remainedTime =  self.invite.created.timeIntervalSince1970 + Constants.Invitation.waitForResponseToModifyRequest - Date().timeIntervalSince1970
            } else {
                self._remainedTime = self.invite.created.timeIntervalSince1970 + Constants.Invitation.waitForResponseToRequest - Date().timeIntervalSince1970
            }
            
        case .reviewing:
            let startTime = invite.countdownStartedAt == 0 ? invite.created.timeIntervalSince1970 : invite.countdownStartedAt
            if self.isInviteModification {
                self._remainedTime = startTime + Constants.Invitation.waitingForModifyeReview - Date().timeIntervalSince1970
            } else {
                self._remainedTime = startTime + Constants.Invitation.waitingForReviewTime - Date().timeIntervalSince1970
            }
            
        default:
            
            print(inviteModel.state)
            break
        
        }
        
        configure(stopWatchLabel)
        
        
        if self._remainedTime < 1 && ( invite.stateValue <= InviteState.reviewing.rawValue )  {
            self.stopTimer()
            
            self.executeSegue(.showExpired, sender: nil)
        }
        
        
    }
    
    
	fileprivate func startTimer() {
		if _timer != nil {
			_timer?.invalidate()
		}
		
		_timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
			[weak self] timer in
			guard let `self` = self else { return }
			self._remainedTime -= 1
			
			self.configure(self.stopWatchLabel)
			
			if self._remainedTime < 1  {
				self.stopTimer()
				self.executeSegue(.showExpired, sender: nil)
			}
			
		})
	}
	
	fileprivate func stopTimer() {
		_timer?.invalidate()
		_timer = nil
	}
}

// MARK: - Utilities
extension RequestingController  {
    // MARK: Notification
	/// Send notificaiton to opponnet
	fileprivate func notifyOpponent() {
        
        invite.stateValue = InviteState.sent.rawValue
        invite.createdValue = Date().timeIntervalSince1970
        
        
        invite.synchronize { [weak self] (result) in
            
            guard let `self` = self else { return }
            
            switch result {
            case let .failure(error):
                Alert.default.showError(message: error.localizedDescription)
                
            case .success:
                if self.isInviteModification {
                    self._remainedTime = Constants.Invitation.waitForResponseToModifyRequest
                } else {
                    self._remainedTime = Constants.Invitation.waitForResponseToRequest
                }
                
                
                do { try self.managedObjectContext.save() }
                catch {
                    Alert.default.showOk(Strings.Error, message: error.localizedDescription)
                    return
                }
                
                self.registerObserver()
                
                SVProgressHUD.show()
                MessageManager.default.send(.invite(request: self.inviteModel.invite)) { [weak self] in
                    guard let `self` = self else { return }
                    self.startTimer()
                    SVProgressHUD.dismiss()
                }
            }
        }
		
		
	}
    
    /// Shows if the current invite was inherited or completely new
    var isInviteModification: Bool {
        return !self.inviteModel.invite.inheritedFrom.isEmptyOrNil
    }
    
    
    
}

//    MARK: - Outlet functions
extension RequestingController  {
    //    MARK: Buttons
	@IBAction func didTapTest(_ sender: Any) {
		self.performSegue(.showSuccess, sender: nil)
	}
}

// MARK: - Navigation & SegueHandler protocol
extension RequestingController: SegueHandler {
    enum SegueType: String {
        case showCanceled
		case showExpired
		case showSuccess
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .showCanceled:
			if inviteModel.state.isDeclined == false {
				inviteModel.state = InviteState.declinedBySender
                
                MessageManager.default.send(.cancel(request: invite, reason: InviteState.declinedBySender), completion: nil)
                
			}
			
		case .showExpired:
			inviteModel.state = InviteState.declinedExpired
		
		case .showSuccess:
			if inviteModel.state != InviteState.datingBegin {
				inviteModel.state = InviteState.datingBegin
			}
        }
        
        invite.synchronize { _ in
            // TODO: Nothing
        }
		
		do { try managedObjectContext.save() }
		catch {
			Alert.default.showError(message: error.localizedDescription)
			return false
		}
		
		return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
		case .showExpired, .showCanceled:
			let vc = segue.destination as! FailureController
			vc.inviteModel = self.inviteModel
			
		case .showSuccess:
			let vc = segue.destination as! SuccessController
			vc.inviteModel = self.inviteModel
        }
    }
}
