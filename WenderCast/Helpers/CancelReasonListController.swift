//
//  CancelReasonListController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/7/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */


extension Strings {
	enum cancelDateReason {
		public static let outOfDate = NSLocalizedString("Out of date", comment: "")
		public static let plansChanged = NSLocalizedString("Changed plans", comment: "")
		
	}
}

enum CancelDateReasons: Int {
	case none = -1
	case outOfTime
	case plansChanged
	
	static var cancelValues: [CancelDateReasons] = [.outOfTime, .plansChanged]

	var description: String? {
		switch self {
		case .none: return nil
		case .outOfTime: return Strings.cancelDateReason.outOfDate
		case .plansChanged: return Strings.cancelDateReason.plansChanged
		}
	}
}


//    MARK: - Properties & variables
final class CancelReasonListController: UIViewController {
    //    MARK: Public
	var inviteModel: InviteModel!
	
	typealias CompletionHandler = (_ reason: CancelDateReasons) -> Void
	fileprivate var _competionHandler: CompletionHandler!
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    
    //    MARK: Private
	
    
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension CancelReasonListController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
	    // Customize appearance
        Appearance.customize(viewController: self)
	
		self.preferredContentSize = CGSize(width: self.view.frame.width, height: CGFloat(CancelDateReasons.cancelValues.count) * tableView.rowHeight)
    }
	
	
	
	@discardableResult
	func competion(_ competion: @escaping CompletionHandler) -> Self {
		self._competionHandler = competion
		return self
	}
}


//    MARK: - Outlet functions
extension CancelReasonListController  {
	//    MARK: Buttons
	@IBAction func didTapCancel(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
}
// MARK: - Navigation & SegueHandler protocol
extension CancelReasonListController: SegueHandler {
    enum SegueType: String {
        case none
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .none: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .none: break
        }
    }
}


// MARK: - UITableViewDatasource, UITableViewDelegate
extension CancelReasonListController: UITableViewDataSource, UITableViewDelegate {
	//MARK: Datasource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return CancelDateReasons.cancelValues.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(CancelReasonCell.self, for: indexPath)
		guard let type = CancelDateReasons(rawValue: indexPath.row) else {
			fatalError()
		}
		cell.titleLabel.text = type.description
		return cell
	}
	
	//MARK: Delegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let type = CancelDateReasons(rawValue: indexPath.row) else {
			fatalError()
		}
		let deadlineTime = DispatchTime.now() + .milliseconds(5)
		DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
			self.dismiss(animated: true, completion: {
				self._competionHandler(type)
			})
		}
	}
	
}
























