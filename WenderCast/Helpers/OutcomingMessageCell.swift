//
//  OutcomingMessageCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

final class OutcomingMessageCell: UITableViewCell {
	@IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var bubbleImageView: UIImageView!
}
