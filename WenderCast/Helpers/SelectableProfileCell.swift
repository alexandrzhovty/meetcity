//
//  SelectableProfileCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

protocol SelectableProfileCellProtocol: class {
	var title: String? { get set }
}

class SelectableProfileCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var shevronImageView: UIImageView!
	
	
	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
		size.height = max(size.height, Constants.TableView.profileCellHeight)
		return size
	}

}

extension SelectableProfileCell: NibLoadableView {}

extension SelectableProfileCell: SelectableProfileCellProtocol {
	var title: String? {
		get { return titleLabel.text }
		set { titleLabel.text = newValue }
	}
}
