//
//  WeartherInteractor.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

// https://developer.apple.com/documentation/foundation/measurementformatter

import Foundation
import MapKit
import SwiftyJSON

struct Weather {
	var temperature: Double = 0
	var icon: URL?
	var place: String = ""
	
	init(){
	}
}

class WeartherInteractor {
	let session: URLSession
	var dataTask: URLSessionDataTask!
	init(session: URLSession = URLSession.shared) {
		self.session = session
	}
	
	func fetch(for coordinate: CLLocationCoordinate2D, completion: @escaping (_ weather: Weather) -> Void) {
		let request = OpenWeatherMap.request(for: coordinate)
		
		dataTask = session.dataTask(with: request, completionHandler: { (data, _, _) in
			guard let data = data else {
				return
			}
			
			completion(self.decode(data))
		})
		dataTask.resume()
		
	}
	
	fileprivate func decode(_ data: Data) -> Weather {
		let json = JSON(data: data)
		
		var weather = Weather()
		weather.temperature = json["main"]["temp"].doubleValue  // Temp in Kelvin
		weather.place = json["name"].stringValue
		
		
		if let iconName = json["weather"][0]["icon"].string {
			weather.icon = OpenWeatherMap.url(for: iconName)
		}
		
		return weather
	}
	
	
	
	
}
