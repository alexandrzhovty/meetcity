//
//  AttributedString.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    func singleOut(_ occurrence: String, with attr: [String: Any])   {
        if let foundRange = self.string.range(of: occurrence) {
            let range = self.string.nsRange(from: foundRange)
            self.setAttributes(attr, range: range)
        }
    }
}
