//
//  MapPresenter.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import GoogleMaps
import Firebase

enum MapPresenterType {
	case date(invite: DMInvite)
	case nearby
    case venues
	
	var zoom: (`default`: Float, min: Float, max: Float) {
		switch self {
		case .date: return (12.0, 6.0, 20.0)
		case .nearby: return (14.0, 10.0, 17.0)
        case .venues: return (14.0, 10.0, 17.0)
		}
	}
	
	
}



class MapPresenter: NSObject {
	// MARK: - Properties
	fileprivate var _mapView: GMSMapView
	fileprivate var _type: MapPresenterType
	fileprivate var _venues = [GMSMarker]()
	
	
	fileprivate var _circle: GMSCircle!
	fileprivate var _venueMarker: GMSMarker!
	fileprivate var _myMarker: GMSMarker!
	fileprivate var _opponentMarker: GMSMarker!
	
	typealias PositionChangedHandler = (_ coordinate: CLLocationCoordinate2D, _ insideVenue: Bool) -> Void
	fileprivate var _myPositionInsideCircle: PositionChangedHandler?
	fileprivate var _opponentPostiionInsideCircle: PositionChangedHandler?
    
    fileprivate lazy var _locateMeButton: UIButton = UIButton(type: .custom)
	
	
	var observers = [Any] ()
	
	
	
	
	// MARK: - Initialization
	init(with mapView: GMSMapView, type: MapPresenterType) {
		_mapView = mapView
		_type = type
		super.init()
		
		_mapView.isMyLocationEnabled = false // CLLocationManager.locationServicesEnabled()
		_mapView.settings.compassButton = false
		_mapView.settings.myLocationButton = false
		
		switch _type {
		case let .date(invite):
			self.display(invite)
            
        case .venues:
            
            // My Position
            if _myMarker == nil {
                _myMarker = GMSMarker()
            } else {
                _myMarker.map = nil
            }
            
            
            if CLLocationCoordinate2DIsValid(AppDelegate.me.location.coordinate) {
                _myMarker.position = AppDelegate.me.location.coordinate
                _myMarker.icon = UIImage(named: "Icon-MyLocation")
                _myMarker.map = self._mapView
                
            }
            
//            let bounds = GMSCoordinateBounds().includingCoordinate(_myMarker.position)
//            let update = GMSCameraUpdate.fit(bounds, withPadding: 400)
//            self._mapView.animate(with: update)
            
            configure(_locateMeButton)
            
            
		default:
			break
		}
		
//		mapView.setMinZoom(_type.zoom.min, maxZoom: _type.zoom.max)
		
//		_mapView.delegate = self
//		updateMarkers()
		
		registerObserver()
	}
	
	deinit {
		unregisterObserver()
	}
	
    
    private func configure(_ view: UIView) {
        switch view {
        case _locateMeButton:
            
            if _locateMeButton.superview == nil {
                _locateMeButton.setImage(UIImage.icon.locateMe, for: .normal)
                
                _mapView.addSubview(_locateMeButton)
                
                _locateMeButton.translatesAutoresizingMaskIntoConstraints = false
                _locateMeButton.leadingAnchor.constraint(equalTo: _mapView.leadingAnchor, constant: 24).isActive = true
                _mapView.bottomAnchor.constraint(equalTo: _locateMeButton.bottomAnchor, constant: 24).isActive = true
                
                _locateMeButton.addTarget(self, action: #selector(didTapLocationMe(_:)), for: .touchUpInside)
                
            }
            
                
        default: break
        }
    }

    @objc func didTapLocationMe(_ button: UIButton) {
        guard _myMarker != nil else {
            return
        }
        
        let camera = GMSCameraPosition.camera(withTarget: _myMarker.position, zoom: 16)
        self._mapView.animate(to: camera)
    }
}

// MARK: - Display invitation markers
extension MapPresenter {
     func display(_ invite: DMInvite) {
		
		let context = AppDelegate.shared.mainContext
		
        // Venue marker
        let inviteModel: InviteModel
        do {
            inviteModel = try InviteModel(with: (context.object(with: invite.objectID) as! DMInvite), in: context)
        }
		catch {
			Alert.default.showError(message: error.localizedDescription)
			return
		}
        
		
		// Circle marker
		if _circle == nil {
			_circle = GMSCircle()
			_circle.radius = Settings.circleRadius
			_circle.fillColor = Settings.circleFillColor
			_circle.strokeColor = Settings.circleStrokeColor
			_circle.strokeWidth = Settings.circleStrokeWidth
			
		} else {
			_circle.map = nil
		}
		_circle.position = inviteModel.coordinate
		_circle.map = _mapView

		
		// Venue marker
		
		if _venueMarker == nil {
			_venueMarker = GMSMarker()
		} else {
			_venueMarker.map = nil
		}
		
		_venueMarker.icon = UIImage(named: "Icon-Venue")
		_venueMarker.position = _circle.position
		_venueMarker.title = inviteModel.title
		_venueMarker.map = self._mapView
        
		var bounds = GMSCoordinateBounds().includingCoordinate(_venueMarker.position)
		
		// Add opponent
        let opponentModel = inviteModel.opponentModel
        
        if _opponentMarker == nil {
            _opponentMarker = GMSMarker()
        } else {
            _opponentMarker.map = nil
        }
        
        
        let markerMapView = MapMarkerView.instantiateFromInterfaceBuilder()
        _opponentMarker.title = opponentModel.title
        _opponentMarker.iconView = markerMapView
        
        if let photoRef = opponentModel.profilePhotoeRef {
            ImageCache.default.load(forKey: photoRef, completion: { [weak markerMapView] image in
                guard let markerMapView = markerMapView else { return }
                markerMapView.iconImageView.image = image
            })
        }
        
        _opponentMarker.userData = opponentModel
        let coordinate = opponentModel.user.location.coordinate
        
        if  CLLocationCoordinate2DIsValid(coordinate) {
            if coordinate.latitude != 0 && coordinate.longitude != 0 {
                _opponentMarker.position = coordinate
                _opponentMarker.map = self._mapView
                bounds = bounds.includingCoordinate(_opponentMarker.position)
            }
        }
        
        // My Position
        if _myMarker == nil {
            _myMarker = GMSMarker()
        } else {
            _myMarker.map = nil
        }
        
        
        if CLLocationCoordinate2DIsValid(AppDelegate.me.location.coordinate) {
            _myMarker.position = AppDelegate.me.location.coordinate
            _myMarker.icon = UIImage(named: "Icon-MyLocation")
            _myMarker.map = self._mapView
            
        }
        bounds = bounds.includingCoordinate(_myMarker.position)

        let update = GMSCameraUpdate.fit(bounds, withPadding: 400)
        self._mapView.animate(with: update)
		
    }
    
    func locateGroup(padding: CGFloat = 100) {
        var bounds = GMSCoordinateBounds().includingCoordinate(_venueMarker.position)
        
        bounds = bounds.includingCoordinate(_opponentMarker.position)
        bounds = bounds.includingCoordinate(_myMarker.position)
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: padding)
        self._mapView.animate(with: update)
        
    }
    
    @discardableResult
    func updateMy(_ location: CLLocation) -> Bool {
        
        _myMarker.position = location.coordinate
        
        switch _type {
        case let .date(invite):
            let inviteModel = try! InviteModel(with: invite, in: AppDelegate.shared.mainContext)
            let dist = location.distance(from: inviteModel.venue.location)
            return dist <= MapPresenter.Settings.circleRadius
        default:
            return false
        }
    }
}


// MARK: - Venues protocol
extension MapPresenter {
    func displayMarkers(for venues: [Venue]) {
        // Remove the previous markers
        _venues.forEach { $0.map = nil }
        _venues.removeAll()
        
        // Place new markers
        var bounds = GMSCoordinateBounds()
        venues.forEach {
            let marker = GMSMarker()

            marker.icon = UIImage(named: "Icon-Venue")
            marker.position = $0.location.coordinate
            marker.title = $0.name
            marker.map = self._mapView
            marker.userData = $0
            
            bounds = bounds.includingCoordinate(marker.position)
            
            _venues.append(marker)
        }
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 0)
        self._mapView.animate(with: update)
        
    }
}


// Mark: - Dating protocol
extension MapPresenter {
	@discardableResult
	func myPosition(chanded completion: @escaping PositionChangedHandler) -> Self {
		_myPositionInsideCircle = completion
		return self
	}
	
	@discardableResult
	func opponentPosition(chanded completion: @escaping PositionChangedHandler) -> Self {
		_opponentPostiionInsideCircle = completion
		return self
	}
}

extension MapPresenter: ObserverProtocol {
	
	func registerObserver() {
        
        switch _type {
        case let .date(invite):
            let inviteModel = try! InviteModel(with: invite, in: AppDelegate.shared.mainContext)
            
            let opponentRef = FirebaseStack.user(for: inviteModel.opponent.uid!)
            print(opponentRef)
            observers.append(
                opponentRef.observe(.value, with: { [weak self] snapshot in
                    guard let `self` = self else { return }
                    
                    let nestedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                    nestedContext.parent = AppDelegate.shared.mainContext
                    nestedContext.perform {
                        let user = nestedContext.object(with: inviteModel.opponent.objectID) as! DMUser
                        user.update(with: snapshot, into: nestedContext)
                        
                        if nestedContext.hasChanges {
                            do { try nestedContext.save() }
                            catch {
                                print("Cannot not parse the server response")
                            }
                        }
                        
                        let userModel = UserModel(with: user)
                        let photoRef = userModel.profilePhotoeRef
                        
                       
                        
                        
                        if CLLocationCoordinate2DIsValid(user.location.coordinate) {
                            let coordinate = user.location.coordinate
                            DispatchQueue.main.async {
                                
                                
                                if photoRef != nil {
                                    ImageCache.default.load(forKey: photoRef!, completion: { [weak self] image in
                                        guard let markerMapView = self?._opponentMarker.iconView as?  MapMarkerView else { return }
                                        markerMapView.iconImageView.image = image
                                    })
                                    
                                }
                                
                                if self._opponentMarker.position.latitude != coordinate.latitude
                                    ||  self._opponentMarker.position.longitude != coordinate.longitude {
                                    
                                    self._opponentMarker.position = coordinate
                                    self._opponentMarker.map = self._mapView
                                    
                                    self.locateGroup()
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    
                })
            )
            
            
        default: break
        }
        
        
	}

	func unregisterObserver(){
        func unregisterObserver() {
            observers.forEach {
                if let dbRef = $0 as? DatabaseReference {
                    dbRef.removeAllObservers()
                }
            }
            observers.removeAll()
        }
	}
	
	
}

extension MapPresenter: GMSMapViewDelegate {
	
}
