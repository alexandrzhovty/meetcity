//
//  InviteUserViewModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/12/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData

/// Helper struct intended to work with invite users
struct InviteUsersViewModel {
    fileprivate(set) var invite: DMInvite
    fileprivate(set) var managedObjectContext: NSManagedObjectContext
    
    init(with invite: DMInvite, for context: NSManagedObjectContext = AppDelegate.shared.mainContext) {
        self.invite = invite
        self.managedObjectContext = context
    }
}

// MARK: - Oppoenet
extension InviteUsersViewModel {
    var opponent: DMUser {
        return invite.sender.identifier == AppDelegate.me.identifier ? invite.recipient : invite.sender
    }
}
