//
//  NearbyMapController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import AlamofireImage

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class NearbyMapController: UIViewController {
    //    MARK: Public
	var parentController: NearbyListControllerProtocol?
	var observers = [Any]()
    
    var bigMarkers: Bool = false { didSet { updateMarkers() } }
    
    //    MARK: Outlets
	@IBOutlet weak var mapView: GMSMapView!
	@IBOutlet weak var peopleButton: UIButton!
	
	
    
    //    MARK: Private
	fileprivate var _didFindMyLocation = false
	
	fileprivate var markers = [GMSMarker]()
	
    
    //    MARK: Enums & Structures
	
	deinit {
		unregisterObservers()
	}
	
}

//    MARK: - View life cycle
extension NearbyMapController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
        
        // Customize appearance
        Appearance.customize(viewController: self)
		
		// Configure tableView
		configure(mapView)
		configure(peopleButton)
		
		LocationManager.default.requestAlwaysAuthorization()
//		LocationManager.default.startUpdatingLocation()
		
		registerObservers()
		
    }

}



//    MARK: - Observers
extension NearbyMapController {
	fileprivate func registerObservers() {
		mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
		
		let center = NotificationCenter.default
		let queue = OperationQueue.main
		
		if observers.count > 0 {
			observers.forEach{ center.removeObserver($0) }
			observers.removeAll()
		}
		
		observers.append(center.addObserver(forName: Notification.Name.LocationManager.didChangeAuthorization, object: nil, queue: queue, using: {
			[weak self] note in
			guard let `self` = self else { return }
			guard let status = note.object as? CLAuthorizationStatus else { return }
			
			if status == .authorizedWhenInUse || status == .authorizedAlways {
				self.mapView.isMyLocationEnabled = true
				
//				if let coordinate = manager.location?.coordinate {
//					//				print(String(describing: type(of: self)),":", #function, "  EXISTS COORDINATE")
//					//				var model = LocationModel(location: Location())
//					//				model.coordinate = coordinate
//					
//					
////					AppDelegate.userModel.coordinate = coordinate
////					AppDelegate.userModel.synchronizeCoordinates()
////					
////					
////					let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
////					mapView.animate(to: camera)
//					
//					
//				} else
				
				if UIDevice.isSimulator {
                    AppDelegate.me.location = CLLocation(latitude: 50.052351, longitude: 36.204860)
                    AppDelegate.me.synchronizeCoordinates()
				}
			}
			
		}))
		
	}
	
	fileprivate func unregisterObservers() {
		mapView.removeObserver(self, forKeyPath: "myLocation")
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		switch keyPath {
		case "myLocation"?:
			if !_didFindMyLocation {
				let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
				mapView.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 1.0)
				mapView.settings.myLocationButton = true
				
				_didFindMyLocation = true
			}
		
			
		default:
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
	
}

//    MARK: - Utilities
extension NearbyMapController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case peopleButton:
			peopleButton.isHidden = true
		case mapView:
			mapView.isMyLocationEnabled = CLLocationManager.locationServicesEnabled()
			mapView.settings.compassButton = false
			mapView.settings.myLocationButton = false
			mapView.delegate = self
			
			
//			// Style
//			let kMapStyle = try! String(contentsOf: Resources.JSON.mapStyle.url)
//			
//			do {
//				// Set the map style by passing a valid JSON string.
//				mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
//			} catch {
//				NSLog("One or more of the map styles failed to load. \(error)")
//			}
			
			// Delegation
//			mapView.delegate = self
			
        default: break
        }
		
    }
}

//    MARK: - Outlet functions
extension NearbyMapController  {
    //    MARK: Buttons
	@IBAction func didTapPeople(_ button: Any) {
		parentController?.setNearbyListHidden(false, animated: true)
	}
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension NearbyMapController: SegueHandler {
    enum SegueType: String {
        case showNearByProfile
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .showNearByProfile:
			if let _ = sender as? UserModel {
				return true
			} else {
				let msg = sender == nil ? "Cannot open empty object" : "Cannot open \(String(describing: type(of: sender)))"
				Alert.default.showOk("", message: msg)
				return false
			}
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .showNearByProfile:
			let vc = segue.destination as! NearbyProfileController
			vc.managedObjectContext = AppDelegate.shared.mainContext
			vc.user = sender as! DMUser
        }
    }
}


//    MARK: - Google map
extension NearbyMapController {
	func updateUsers(_ users: [UserModel]) {
		print(String(describing: type(of: self)),":", #function)
	}

	func deleteUsers(_ users: [UserModel]) {
		print(String(describing: type(of: self)),":", #function)
	}

	func addUsers(_ users: [UserModel]) {
//		print(String(describing: type(of: self)),":", #function)
		
		markers.forEach { $0.map = nil }
		markers.removeAll()
		
		
		users.forEach {
			
            let coordinate = $0.user.location.coordinate
            if CLLocationCoordinate2DIsValid(coordinate) {
				
                if self.bigMarkers {
                    
                    let markerMapView = MapMarkerView.instantiateFromInterfaceBuilder()
                    
                    let marker = GMSMarker()
                    marker.position = coordinate
                    marker.title = $0.title
                    marker.map = self.mapView
                    
                    if let photoRef = $0.profilePhotoeRef {
                        ImageCache.default.load(forKey: photoRef, completion: { [weak markerMapView] image in
                            guard let markerMapView = markerMapView else { return }
                            markerMapView.iconImageView.image = image?.af_imageRoundedIntoCircle()
                        })
                    }
                    
                    marker.iconView = markerMapView
                    marker.userData = $0
                    //				marker.groundAnchor = CGPoint(x: 1, y: 0.5)
                    markers.append(marker)
                    
                } else {
                    
                    let markerMapView = SmallMapMarkerView.instantiateFromInterfaceBuilder()
                    
                    let marker = GMSMarker()
                    marker.position = coordinate
                    marker.title = $0.title
                    marker.map = self.mapView
                    
                    //				if let photoRef = $0.profilePhotoeRef {
                    //					ImageCache.default.load(forKey: photoRef, completion: { [weak markerMapView] image in
                    //						guard let markerMapView = markerMapView else { return }
                    //						markerMapView.iconImageView.image = image
                    //					})
                    //				}
                    marker.iconView = markerMapView
                    marker.userData = $0
                    //				marker.groundAnchor = CGPoint(x: 1, y: 0.5)
                    markers.append(marker)
                }
                
			}
		}
	}
    
    
    
    fileprivate func updateMarkers() {
        
        let users = markers.map{ $0.userData as? UserModel }.flatMap{ $0 }
        
        markers.forEach { $0.map = nil }
        markers.removeAll()
        
        
        users.forEach {
            
            let coordinate = $0.user.location.coordinate
            if CLLocationCoordinate2DIsValid(coordinate) {
                
                if self.bigMarkers {
                    
                    let markerMapView = MapMarkerView.instantiateFromInterfaceBuilder()
                    
                    let marker = GMSMarker()
                    marker.position = coordinate
                    marker.title = $0.title
                    marker.map = self.mapView
                    
                    if let photoRef = $0.profilePhotoeRef {
                        ImageCache.default.load(forKey: photoRef, completion: { [weak markerMapView] image in
                            guard let markerMapView = markerMapView else { return }
                            markerMapView.iconImageView.image = image
                        })
                    }
                    
                    marker.iconView = markerMapView
                    marker.userData = $0.user
                    //				marker.groundAnchor = CGPoint(x: 1, y: 0.5)
                    markers.append(marker)
                    
                } else {
                    
                    let markerMapView = SmallMapMarkerView.instantiateFromInterfaceBuilder()
                    
                    let marker = GMSMarker()
                    marker.position = coordinate
                    marker.title = $0.title
                    marker.map = self.mapView
                    
                    marker.iconView = markerMapView
                    marker.userData = $0.user
                    markers.append(marker)
                }
                
            }
        }
        
        
    }
	
}



//    MARK: - Google map
extension NearbyMapController: GMSMapViewDelegate {
	func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
		// Show Bar profile
		if shouldPerformSegue(.showNearByProfile, sender: marker.userData) {
			performSegue(.showNearByProfile, sender: marker.userData)
		}
	}
}

//    MARK: - Location manager protocol
extension NearbyMapController: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		switch status {
		case CLAuthorizationStatus.authorizedAlways, CLAuthorizationStatus.authorizedWhenInUse:
			mapView.isMyLocationEnabled = true
			manager.stopUpdatingLocation()
			
			if let location = manager.location {
                
                AppDelegate.me.location = location
                AppDelegate.me.synchronizeCoordinates()
				
                
				
				let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15)
				mapView.animate(to: camera)
				
				
			} else if UIDevice.isSimulator {
                
                AppDelegate.me.location = CLLocation(latitude: 50.052351, longitude: 36.204860)
				AppDelegate.me.synchronizeCoordinates()
                
			}
			
		default: break
		}
	}
}
