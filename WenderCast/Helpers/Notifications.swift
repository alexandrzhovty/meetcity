
//
//  Notofocations.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/28/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

extension Notification.Name {
	/// Used as a namespace for all **LocationManager** related notifications.
	public struct LocationManager {
		/// Posted when **LocationManager** did change Authorization satur. The notification **object** contains new **status**
		public static let didChangeAuthorization = Notification.Name(rawValue: "\(Bundle.main.bundleIdentifier!).LocationManager.didChangeAuthorization")
      
      /// Posted when the applicaiton user changed his/her location. The notification **object** contains new **CLLocation**
      public static let didChangeLocation = Notification.Name(rawValue: "\(Bundle.main.bundleIdentifier!).LocationManager.didChangeLocation")
		
	}
   
}
