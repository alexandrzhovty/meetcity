//
//  ImagePreviewController.swift
//  LIQR
//
//  Created by Aleksandr Zhovtyi on 4/18/17.
//  Copyright © 2017 Lindenvalley GmbH. All rights reserved.
//

import UIKit
import Firebase

final class ImagePreviewController: UIViewController {
    //    MARK: - Properties & variables
    //    MARK: Public
    var pageIndex: Int = 0
    
    //    MARK: Private
    fileprivate var minimalZoomScale: CGFloat = 0
    fileprivate let maximalZoomScale: CGFloat = 2
    private var lastScale: CGFloat = 0
    
    fileprivate lazy var panGesture: UIPanGestureRecognizer = { [unowned self] in
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(_:)))
        pan.delegate = self
        return pan
    }()
    
    fileprivate lazy var doubleTapGesture: UITapGestureRecognizer = { [unowned self] in
        let double = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapGesture(_:)))
        double.numberOfTapsRequired = 2
        double.delegate = self
        return double
    }()
    
    fileprivate lazy var pinchGesture: UIPinchGestureRecognizer = { [unowned self] in
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(_:)))
        pinch.delegate = self
        return pinch
    }()
    
    fileprivate var currentScale: CGFloat {
        return self.imageView.layer.value(forKeyPath: "transform.scale.x") as! CGFloat
    }
    
    private var boundsCenter: CGPoint {
        return CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
    }
    
    
    public var imageRef: StorageReference? {
		didSet { if imageRef != nil { self.configureView() } }
    }
	
    private lazy var imageView: UIImageView = { [unowned self] in
        let imageView = UIImageView()
        self.view.addSubview(imageView)
        
//        if self.galleryViewType == .embed {
//            imageView.translatesAutoresizingMaskIntoConstraints = false
//            if let view = self.view {
//                imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
//                imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
//                imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//                imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
//                
//            }
//            
//            imageView.contentMode = .scaleAspectFill
//        
//        }
		
        return imageView
    }()
	
	fileprivate var _activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    
    //    MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.clipsToBounds = true
		
		
		_activityIndicator.hidesWhenStopped = true

		self.view.insertSubview(_activityIndicator, aboveSubview: self.imageView)
		
		
		_activityIndicator.translatesAutoresizingMaskIntoConstraints = false
		_activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
		_activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
		
		
		
        
//        if galleryViewType == .fullScreen {
            // Gesture recognizers
            self.view.addGestureRecognizer(panGesture)
            self.view.addGestureRecognizer(doubleTapGesture)
            self.view.addGestureRecognizer(pinchGesture)
//        }
		
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
//        if galleryViewType == .fullScreen {
            calculateMinMaxZoomScale()
//        }
		
    }
    
    //    MARK: - Outlet functions
    //    MARK: Gestures
    @objc func handleDoubleTapGesture(_ recognizer: UITapGestureRecognizer) {
        
        if self.currentScale > self.minimalZoomScale {
            restoreMinZoomAndCenter()
        } else {
            UIView.animate(withDuration: 0.25) { [unowned self] in
                let delta = self.maximalZoomScale / self.currentScale
                let tramsform = self.imageView.transform.scaledBy(x: delta, y: delta)
                self.imageView.transform = tramsform
                
                self.imageView.center = self.boundsCenter
            }
        }
        
    }
    
    @objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        
        switch recognizer.state {
        case .began: break
        case .changed:
            let translation = recognizer.translation(in: self.view)
            var imageViewPosition = self.imageView.center
            imageViewPosition.x += translation.x
            imageViewPosition.y += translation.y
            self.imageView.center = imageViewPosition
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        default:
            updatePosition()
        }
        
        
        
    }
    
    @objc func handlePinchGesture(_ recognizer: UIPinchGestureRecognizer) {
        
        switch recognizer.state {
        case .began:
            lastScale = recognizer.scale
        case .changed:
            let range = minimalZoomScale...maximalZoomScale
            guard range.contains(recognizer.scale) else {
                return
            }
            
            let tramsform = imageView.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
            imageView.transform = tramsform
            imageView.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
            recognizer.scale = 1
            
            
            
        case .ended:
            if currentScale < self.minimalZoomScale {
                restoreMinZoomAndCenter()
            }
        default: break
        }
        
    }
    
    //    MARK: - Utilities
    //    MARK: Display
    private func configureView() {
		
		if let imageRef = self.imageRef {
			_activityIndicator.startAnimating()
			ImageCache.default.load(forKey: imageRef, completion: { [weak self] (image) in
				guard let `self` = self else { return }
				
				self._activityIndicator.startAnimating()
				
				if let image = image {
					self.imageView.image = image
					self.imageView.isHidden = false
					
//					if self.galleryViewType == .fullScreen {
						self.imageView.frame.size = image.size
					// }
				} else {
					self.imageView.image = nil
					self.imageView.isHidden = true
				}
				
				
			})
		} else {
            imageView.image = nil
            imageView.isHidden = true
        }
		
//		if galleryViewType == .embed {
//			imageView.contentMode = .scaleAspectFill
//		}
		
    }
    
    //    MARK: Calculation
    private func calculateMinMaxZoomScale() {
        
        guard let image = self.imageView.image else { return }
        
        
        let xScale = self.view.bounds.width / image.size.width
        let yScale = self.view.bounds.height / image.size.height
        
        self.minimalZoomScale = min(xScale, yScale)
        
        imageView.transform = CGAffineTransform.identity
        
        let tramsform = imageView.transform.scaledBy(x: minimalZoomScale, y: minimalZoomScale)
        imageView.transform = tramsform
        imageView.center = self.boundsCenter
        
    }
    
    private func restoreMinZoomAndCenter() {
        UIView.animate(withDuration: 0.25) { [unowned self] in
            if self.currentScale != self.minimalZoomScale {
                let delta = self.minimalZoomScale / self.currentScale
                let tramsform = self.imageView.transform.scaledBy(x: delta, y: delta)
                self.imageView.transform = tramsform
            }
            
            self.imageView.center = self.boundsCenter
        }
    }
    
    private func updatePosition() {
        if currentScale <= self.minimalZoomScale && imageView.center != self.boundsCenter {
            restoreMinZoomAndCenter()
        } else {
            var center = self.imageView.center
            
            if self.imageView.frame.width < self.view.bounds.width {
                center.x = self.boundsCenter.x
            }
            else if self.imageView.frame.minX > 0 {
                center.x = (self.imageView.bounds.width * self.currentScale) / 2
            }
            else if self.imageView.frame.maxX < self.view.bounds.width {
                center.x = (self.view.bounds.width - self.imageView.frame.width) + self.imageView.frame.width / 2
            }
            
            
            if self.imageView.frame.height < self.view.bounds.height {
                center.y = self.boundsCenter.y
                
            }
            else if self.imageView.frame.minY > 0 {
                center.y = (self.imageView.bounds.height * self.currentScale) / 2
            }
            else if self.imageView.frame.maxY < self.view.bounds.height {
                center.y = (self.view.bounds.height - self.imageView.frame.height) + self.imageView.frame.height / 2
            }
            
            if center != self.imageView.center {
                UIView.animate(withDuration: 0.25) { [unowned self] in
                    self.imageView.center = center
                }
            }
            
        }
    }
}


//    MARK: -
extension ImagePreviewController: UIGestureRecognizerDelegate {
    //    MARK: - Gesture recognizer protocol
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        switch gestureRecognizer {
        case panGesture where currentScale <= minimalZoomScale:
            return false
        default:
            return true
        }
    }
}
