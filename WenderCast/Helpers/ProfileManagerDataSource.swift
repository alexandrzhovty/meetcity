//
//  ProfileManagerDataSource.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import UIKit

//protocol ProfileManagerwControllerProcolol: class {
//	var userModel: UserModel! { get set }
//	var viewController: UIViewController { get }
//}
//
//extension ProfileManagerwControllerProcolol where Self: UIViewController {
//	var viewController: UIViewController { return self }
//}


final class ProfileManagerDataSource: DataSource {
	
	fileprivate weak var _viewController: UIViewController?
	
	init(with viewController: ProfileManagerController) {
		
		_viewController = viewController
		guard var model = viewController.userModel else {
			fatalError()
		}
		
		
		let segments = [
			Segment(title: Strings.AboutMe, with: [
				Profile(type: .textEditabled(text: model.aboutMe, textLmit: Constants.AboutMeMaxCount, action: {
//					[weak profileManager]
					textView in
					model.aboutMe = textView.text
				}))
				
			]),
			Segment(title: Strings.Occupation, with: [
				
				Profile(type: .textEditabled(text: model.occupation, textLmit: Constants.OccupationMaxCount, action: {
					textView in
					model.occupation = textView.text
                    
                    FirebaseStack.user(for: "me").child(DMUser.CodingKeys.occupation).setValue(textView.text)
                    
				}))
			]),
			Segment(title: Strings.ShowInstagramPhotos, with: [
				Profile(title: Strings.AddInstagramAccound, type: ProfileType.buttonLike {
					sender in
//					guard let viewController = self._viewController as? ProfileManagerController else { return }
////					viewController.performSegue(ProfileManagerController.SegueType.showHistory, sender: sender)
//                    viewController.executeSegue(ProfileManagerController.SegueType.showHistory, sender: sender)
                
					
//					Alert.default.showOk("Instgram", message: "Under development")
					
				})
			]),
			Segment(title: Strings.Iam, with: [
				Profile(title: model.gender.description, type: ProfileType.buttonLike {
					[weak _viewController]  sender in
					guard let viewController = _viewController else { return }
					guard let cell = sender as? ActionProfileCellProtocol else { return }
					
					let alertVC = UIAlertController(title: Strings.Gender, message: nil, preferredStyle: .actionSheet)
					alertVC.addAction(UIAlertAction(title: Strings.Boy, style: .default) { _ in
						model.gender = Gender.boy
						cell.title = Gender.boy.description
					})
					alertVC.addAction(UIAlertAction(title: Strings.Girl, style: .default) { _ in
						model.gender = Gender.girl
						cell.title = Gender.girl.description
					})
					alertVC.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
					
					viewController.present(alertVC, animated: true, completion: nil)
                    
                    
					
				})
			])
		]
		
		super.init(with: segments)
		
	}
	
}
