//
//  IncomingMessageCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

final class IncomingMessageCell: UITableViewCell {
	@IBOutlet weak var messageLabel: UILabel!
	@IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var bubbleImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.cornerRadius = userImageView.bounds.width / 2
        userImageView.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
         userImageView.layer.cornerRadius = userImageView.bounds.width / 2
    }

}
