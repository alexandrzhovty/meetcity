//
//  UserFeedbackCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import AlamofireImage

class UserFeedbackCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var userImage: UIImage? {
        get { return iconImageView.image }
        set {
            activityIndicatorView.stopAnimating()
            if let image = newValue {
                let circularImage = image.af_imageRoundedIntoCircle()
                iconImageView.image = circularImage
                
            } else {
                iconImageView.image = newValue
            }
        }
        
    }
    
    var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.selectionStyle = .none
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.iconImageView.backgroundColor = UIColor.clear
        
        
        
    }
    
    
}
