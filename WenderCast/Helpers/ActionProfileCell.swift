//
//  ActionProfileCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/6/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit


protocol ActionProfileCellProtocol: class {
	var title: String? { get set }
}

extension ActionProfileCell: NibLoadableView {}

class ActionProfileCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		let view = UIView()
		view.backgroundColor = UIColor.white
		backgroundView = view
    }
	
	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
		size.height = max(size.height, Constants.TableView.profileCellHeight)
		return size
	}
}


extension ActionProfileCell: ActionProfileCellProtocol {
	var title: String? {
		get { return titleLabel.text }
		set { titleLabel.text = newValue }
	}
}
