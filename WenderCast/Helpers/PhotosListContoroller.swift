//
//  PhotoListContoroller.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/22/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import AlamofireImage

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

enum PhotoListContorollerType {
    case `default`
    case instagram
}

//    MARK: - Properties & variables
final class PhotoListContoroller: UIViewController {
    //    MARK: Public
    var userModel: UserModel!
	var managedObjectContext: NSManagedObjectContext = AppDelegate.shared.mainContext
    var dataSource: [URL]!
    
    
    var type: PhotoListContorollerType = .default
	
    //    MARK: Outlets
	@IBOutlet weak var collectionView: UICollectionView!    
	@IBOutlet weak var pageControl: PhotosPageControl!
    //    MARK: Private
	fileprivate var _fetchedResultsController: NSFetchedResultsController<DMPhoto>!
	fileprivate var _startingScrollingOffset = CGPoint.zero
	
    
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension PhotoListContoroller  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
        // Customize appearance
        Appearance.customize(viewController: self)
		
		// Configure 
		configure(pageControl)
		
    }
}

//    MARK: - Utilities
extension PhotoListContoroller  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case pageControl:
			pageControl.hidesForSinglePage = false
			pageControl.numberOfPages = self.numberOfObjects()
        default: break
        }
        
    }
	
	fileprivate func numberOfObjects() -> Int {
        
        switch type {
        case .instagram:
            return dataSource.count
        
        case .default:
            var qty: Int = 0
            for section in 0..<(self.fetchedResultsController.sections?.count ?? 0) {
                let sectionInfo = self.fetchedResultsController.sections![section]
                qty = qty + sectionInfo.numberOfObjects
            }
            
            return qty
            
        }
	}
}



// MARK: - Navigation & SegueHandler protocol
extension PhotoListContoroller: SegueHandler {
    enum SegueType: String {
        case presentGallery
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .presentGallery:
            switch type {
            case .instagram:
                return false
            case .default:
                return true
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .presentGallery:
			let vc = segue.destination as! GalleryController
			vc.currentPage = pageControl.currentPage
			vc.galleryViewModel = GalleryViewModel(userModel)
		}
    }
}



//	MARK: - Collection view protocol
extension PhotoListContoroller: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDataSourcePrefetching {
	//    MARK: Datatsourse
	func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        switch type {
        case .instagram:
            return 1
            
        case .default:
            return self.fetchedResultsController.sections?.count ?? 0
            
            
        }
	}
	
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch type {
        case .instagram:
            return dataSource.count
            
        case .default:
            let sectionInfo = self.fetchedResultsController.sections![section]
            return sectionInfo.numberOfObjects
        }
        
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(PhotosListCollectionCell.self, for: indexPath) as! PhotosListCollectionCell
		
        
        switch type {
        case .instagram:
            cell.imageView.af_setImage(withURL: dataSource[indexPath.row])
            cell.imageView.contentMode = .scaleAspectFit
            cell.activityIndicator.stopAnimating()
            
        case .default:
            let photo = fetchedResultsController.object(at: indexPath)
            
            let fileRef = FirebaseStack.storageRef.child(userModel.uid).child(photo.fileName!)
            cell.activityIndicator.startAnimating()
            ImageCache.default.load(forKey: fileRef) { [weak cell] image  in
                cell?.imageView.image = image
                cell?.activityIndicator.stopAnimating()
            }
            
        }
        
        
		
		return cell
	}
	
	
	//	MARK: Prefetching
	func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        switch type {
        case .instagram: break
            
        case .default:
            indexPaths.forEach {
                let photo = fetchedResultsController.object(at: $0)
                let fileRef = FirebaseStack.storageRef.child(userModel.uid).child(photo.fileName!)
                ImageCache.default.load(forKey: fileRef, completion: { _ in })
            }
        }
		
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return collectionView.bounds.size
	}
	
	
	//    MARK: - Scroll View Delegate
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let offset = scrollView.contentOffset.x
		let idx =  Int(round(offset / scrollView.bounds.width))
		pageControl.currentPage = idx
	}
	
}


//	MARK: - Fetched resutls controller protocol
extension PhotoListContoroller: NSFetchedResultsControllerDelegate {
	// MARK: - Fetched results controller
	
	var fetchedResultsController: NSFetchedResultsController<DMPhoto> {
		if _fetchedResultsController != nil {
			return _fetchedResultsController!
		}
		
		let fetchRequest: NSFetchRequest<DMPhoto> = DMPhoto.fetchRequest()
		fetchRequest.fetchBatchSize = 20
		
		fetchRequest.sortDescriptors = [
			NSSortDescriptor(key: #keyPath(DMPhoto.sortOrder), ascending: true)
		]
		
		fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(DMPhoto.user), userModel.user)
		
		let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
		aFetchedResultsController.delegate = self
		_fetchedResultsController = aFetchedResultsController
		
		do {
			try _fetchedResultsController!.performFetch()
		} catch {
			let nserror = error as NSError
			fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
		}
		
		return _fetchedResultsController!
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		//		self.tableView.endUpdates()
		pageControl.numberOfPages = self.numberOfObjects()
		collectionView.reloadData()
	}
}
