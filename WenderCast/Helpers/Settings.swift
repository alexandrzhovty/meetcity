//
//  Settings.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

struct Settings {
	
	static func registerDefaults() {
		let defaults = [
			FiltersController.Settings.minValueKey: Constants.allowedAge.min,
			FiltersController.Settings.maxValueKey: Constants.allowedAge.max
		]
		
		UserDefaults.standard.register(defaults: defaults)
		
		
	}
    
    static func resetStandardUserDefaults() {
        
        for key in Array(UserDefaults.standard.dictionaryRepresentation().keys) {
            UserDefaults.standard.removeObject(forKey: key)
        }
        
        if let bid = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bid)
        }
        Settings.registerDefaults()
        UserDefaults.standard.synchronize()
    }

}

extension Settings {
    enum Photos {
        static let smallProfileName = "150.jpg"
        static let smallProfileSize = CGSize(width: 150, height: 150)
    }
}




extension Settings {
    struct Facebook {
        static let FacebookAppId = "1889037364669985"
        static let FacebookSecret = "d3cad67ddf4b9807036aff00bcb80ab9"
        
        private init(){}
    }
}

extension Settings {
    struct Firebase {
        static let ServerKey_Old = "AIzaSyDXYNZYwvlNHG8STzEqe_hUl3KPk7C1m1w"
        static let Storage_url_Old = "gs://meetcity-38a8d.appspot.com"
        
        static let ServerKey = "AIzaSyDkaQqP_W32WWX2IndxSXjle4mkz6cS2kg"
        static let Storage_url = "gs://meetcity-57e45.appspot.com"
        
        static let email = "meetcity@vesedia.com"
        static let password = "bRooEMnV"
        
        static let PushNotificationUrl = "https://fcm.googleapis.com/fcm/send"
        private init(){}
    }
}

extension Settings {
    struct DataBaseName {
        static let main = "main"
        static let test = "test"
        private init(){}
    }
}


//    MARK: - Google map
extension Settings {
   struct GoogleMap {
      //        static let apiKey = "AIzaSyA4Vj3JCRk8YmXvalKX1ptmqy_fE42eSps"
      
      // https://console.developers.google.com/apis/credentials?project=meetcity-155714
      // meetcity-155714
      static let apiKey = "AIzaSyA_Eg0zQ7auzd9iVK7a66wZ1VKiZIvgvBA"
      static let directionsAPIKey = "AIzaSyD6I5M0R5EA5YrAUaqtjSFZPZ33gVp9Iws"
      
      private init(){}
   }
   
   static let borderRadius = 5.0
   static let borderThickness = 1.0
   
//   static let linkAttributes = [
//      NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue,
//      NSFontAttributeName: UIFont(name: "DINPro-Regular", size: 12)!,
//      NSForegroundColorAttributeName: GlobalColors.kTextFieldTextColorBlueGrey
//      ] as [String : Any]
	
   struct Map {
      
      static let MyFakeLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 55.7096170633836, longitude: 12.4536056384519)
      static let MyHomeLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 50.0404504733626, longitude: 36.3571203220974)
      
      static let viewingAngle: Double = 0.0
      // GMSMapViewType.normal, GMSMapViewType.satellite, GMSMapViewType.hybrid
      struct Nearby {
         static let type = GMSMapViewType.normal
         static let Zoom: Float = 14.0
         static let minZoom: Float = 13.0
         static let maxZoom: Float = 15.0
      }
      struct Request {
         static let type = GMSMapViewType.normal
         static let Zoom: Float = 17.0
         static let minZoom: Float = 3.0
         static let maxZoom: Float = 20.0
      }
      struct Date {
         static let type = GMSMapViewType.normal
         static let Zoom: Float = 14.0
         static let minZoom: Float = 13.0
         static let maxZoom: Float = 15.0
      }
   }
}

extension MapPresenter {
    enum Settings {
        static let circleRadius: CLLocationDistance = 2000
        static let circleFillColor = UIColor.blue.withAlphaComponent(0.1)
        static let circleStrokeColor = UIColor.blue.withAlphaComponent(0.5)
        static let circleStrokeWidth: CGFloat = 1
    }
}

