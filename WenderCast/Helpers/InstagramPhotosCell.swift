//
//  InstagramPhotosCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit


class InstagramPhotosCell: UITableViewCell {
	
	static let cellHeight: CGFloat = 200
    
    var status: InstagramDataProvider.Status = .loading {
        didSet {
            if status == .loading {
                activityIndicatorView.startAnimating()
            } else {
                activityIndicatorView.stopAnimating()
            }
        }
    }

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var pageControl: UIPageControl!
	@IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		collectionView.register(InstagramCollectionCell.self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        
		let width = (collectionView.bounds.width - (4.0 * 8.0)) / 3
        
    
        print(String(describing: type(of: self)),":", #function, " ", width)
        
        collectionView.collectionViewLayout.invalidateLayout()
        let qty = collectionView.numberOfItems(inSection: 0)
        
        let minY: CGFloat = collectionView.frame.minY
        
        switch qty {
        case 0:
            titleLabel.isHidden = true
            titleLabel.isHidden = true
            pageControl.isHidden = true
            size.height = activityIndicatorView.frame.height + 24 * 2
        case 1..<4:
            titleLabel.isHidden = false
            pageControl.isHidden = true
            size.height = minY + width + 8 * 2
            
        default:
            titleLabel.isHidden = false
            pageControl.isHidden = true
            size.height = minY + width * 2 + 8 * 3
        }
        
		return size
	}
    
}

extension InstagramPhotosCell: NibLoadableView {}
