//
//  Storyboard.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

public enum StroyboadType: String, Iteratable {
    case main
	case profile
    case utility
    case login
    case editor
	case document
	case dashboard
	case gallery
	case invite
	case selector
	case meeting
	case dating
	case venues
	case chat
    case nearby
    
    var filename: String { return rawValue.capitalized }
}


extension UIStoryboard {
    convenience init(_ storyboard: StroyboadType) {
        self.init(name: storyboard.filename, bundle: nil)
    }
    
    /// Instantiates and returns the view controller with the specified identifier.
    ///
    /// - Parameter identifier: uniquely identifies equals to Class name
    /// - Returns: The view controller corresponding to the specified identifier string. If no view controller is associated with the string, this method throws an exception.
    public func instantiateViewController<T>(_ identifier: T.Type) -> T where T: UIViewController {
        let className = String(describing: identifier)
        guard let vc =  self.instantiateViewController(withIdentifier: className) as? T else {
            fatalError("Cannot find controller with identifier \(className)")
        }
        return vc
    }
}
