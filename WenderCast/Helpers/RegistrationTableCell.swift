//
//  RegistrationTableCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/3/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 http://candycode.io/self-sizing-uitextview-in-a-uitableview-using-auto-layout-like-reminders-app/
 */

class RegistrationTableCell: UITableViewCell {
   
   @IBOutlet weak var textView: UITextView!
   
   override func awakeFromNib() {
      super.awakeFromNib()
      
      assert(textView.isScrollEnabled == false, "Scrolling should be disabled")
      
      textView.tintColor = UIColor.tintColor.textView
      backgroundColor = UIColor.background.forAdditionalInfo
      
   }
}
