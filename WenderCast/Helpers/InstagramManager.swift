//
//  InstagramManager.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import WebKit
import Firebase
import KeychainSwift
import Alamofire



struct InstagramManager {
    
    enum CodingKeys {
        static let accessToken = "InstagramManager.AccessToken"
    }
    
    static var isAuthorized: Bool {
        guard AppDelegate.me.instagramUserId != nil else { return false }
        guard InstagramManager.accessToken != nil else { return false }
        
        return true
    }
    
    static var accessToken: String? {
        get {
            // Restore user from keychain
            return KeychainSwift().get(CodingKeys.accessToken)
        }
        set {
            let keychain = KeychainSwift()
            if let str = newValue {
                keychain.set(str, forKey: CodingKeys.accessToken)
            } else {
                keychain.delete(CodingKeys.accessToken)
            }
            
        }
        
    }
    
	static let cleintID =   "7ff0d833ec08479f883c95ad592b3a41"
    static let clientSecret = "ea557781370f4afc90010357a668e36a"
	static let redirectUri = "http://gomeetcity.com"
    
    
    static func imagesFetchUrl(for user: DMUser) -> URL {
        
        /*
         String urlString = "https://api.instagram.com/v1/users/"
         + user.getID()
         + "/media/recent/?access_token="
         + session.getAccessToken()
         + "&count="
         + user.getImageCount();
        */
        
        
        var components = URLComponents()
        
        components.scheme = "https"
        components.host = "api.instagram.com"
        components.path = String(format: "/v1/users/%@/media/recent/", user.instagramUserId!)
        components.queryItems = [
            URLQueryItem(name: "access_token", value: InstagramManager.accessToken),
            URLQueryItem(name: "count", value: "20")
        ]
        return components.url!
    }
	
//	static var loginUrl: URL {
////        let scope = InstagramKitLoginScope.relationships | InstagramKitLoginScope.comments | InstagramKitLoginScope.likes
//        let loginUrl = InstagramManager.shared().authorizationURL()
//         print(loginUrl)
//        print(loginUrl)
//        
//        return loginUrl
//
//		
//		return URL(string: "https://instagram.com/accounts/logout")!
//        
//        
//        
//        
////        InstagramKitLoginScope scope = InstagramKitLoginScopeRelationships | InstagramKitLoginScopeComments | InstagramKitLoginScopeLikes;
////        
////        NSURL *authURL = [[InstagramEngine sharedEngine] authorizationURLForScope:scope];
////        [self.webView loadRequest:[NSURLRequest requestWithURL:authURL]];
//        
//        
////        let scope: [InstagramKitLoginScope] = [.relationships, .comments, .likes]
////
////        let loginUrl = InstagramEngine.shared().authorizationURL(for: scope)
//        
//        
////        let engine = InstagramEngine.shared()
////        engine.se
//
//		
////		var components = URLComponents()
////		
////		components.scheme = "https"
////		components.host = "api.instagram.com"
////		components.path = "/oauth/authorize/"
////		components.queryItems = [
////			URLQueryItem(name: "client_id", value: InstagramManager.cleintID),
////			URLQueryItem(name: "redirect_uri", value: InstagramManager.redirectUri),
////			URLQueryItem(name: "response_type", value: "token"),
////			URLQueryItem(name: "scope", value: "public_content"),
////			URLQueryItem(name: "DEBUG", value: "True")
////		]
////
////		guard let url = components.url else {
////			fatalError()
////		}
////		return url
//	}
}

extension InstagramManager {
    static func logOut() {
        
//        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//        [[storage cookies] enumerateObjectsUsingBlock:^(NSHTTPCookie *cookie, NSUInteger idx, BOOL *stop) {
//            [storage deleteCookie:cookie];
//            }];
        print("START")
        
        // Loops through each of the cookies and deletes them.
        
        for cookie in HTTPCookieStorage.shared.cookies ?? [] {
            
            print(cookie.domain)
            
            if cookie.domain == "www.instagram.com" || cookie.domain == "api.instagram.com"{
                
                HTTPCookieStorage.shared.deleteCookie(cookie)
                
            }
            
        }
        
        
        
        print("DONE")
        UserDefaults.standard.synchronize()
        InstagramManager.accessToken = nil
        
        // Removes cache for all responses
//        URLCache.shared.removeAllCachedResponses()
        
    }
}



