//
//  ResourceTests.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import XCTest
@testable import MeetCity

class ResourcesTests: XCTestCase {
    func testJSONResources() {
        checkResources(Resources.JSON.self)
        checkResources(Resources.MOV.self)
        checkResources(Resources.PDF.self)
    }
    
    func checkResources<T: RawRepresentable>(_: T.Type) where T: Iteratable, T: ResourceConvertible, T: Hashable, T.RawValue == String {
        for resource in T.hashValues() {
            XCTAssertNotNil(resource.url, "\(resource.rawValue)")
            XCTAssertNotNil(Bundle.main.url(for: resource), "\(resource.rawValue)")
        }
    }
}
