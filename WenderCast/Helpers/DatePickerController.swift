//
//  DatePickerController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class DatePickerController: UIViewController {
    //    MARK: Public
	var minimumDate: Date? = Date() {
		didSet {
			if self.datePicker != nil {
				self.datePicker.minimumDate = minimumDate
			}
		}
	}
	var maximumDate: Date? {
		didSet {
			if self.datePicker != nil {
				self.datePicker.maximumDate = maximumDate
			}
		}
	}
	var date = Date() {
		didSet { self.doneButton.isEnabled = self.minimumDate! < self.date }
	}
	
	typealias CompletionHandler = (_ date: Date) -> Void
	var completion: CompletionHandler!
    
	@IBOutlet weak var doneButton: UIBarButtonItem!
    //    MARK: Outlets
	
	@IBOutlet weak var datePicker: UIDatePicker!
    
    //    MARK: Private
	

    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension DatePickerController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
		// Configure tableView
		configure(datePicker)
		
		
    }
	
//	override var preferredContentSize: CGSize {
//		return CGSize(width: self.view.bounds.width, height: self.datePicker.bounds.height + 40)
//	}
	
	
}

//    MARK: - Utilities
extension DatePickerController  {
    fileprivate func configure(_ view: UIView) {
		switch view {
		case datePicker:
			datePicker.minimumDate = minimumDate
			datePicker.maximumDate = maximumDate
            datePicker.date = self.date
			datePicker.addTarget(self, action: #selector(datePickerDidChangeValue(_:)), for: .valueChanged)
        default: break
        }
        
    }
}

//    MARK: - Outlet functions
extension DatePickerController  {
    //    MARK: Buttons
	@IBAction func didTapCancel(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	
	@IBAction func didTapDone(_ sender: Any) {
		dismiss(animated: true) { [unowned self] in
			self.completion(self.datePicker.date)
		}
	}
	
    //    MARK: Date picker
	@objc func datePickerDidChangeValue(_ sender: UIDatePicker) {
		self.date = sender.date
	}
}













