//
//  ImageCache.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
https://bitbucket.org/snippets/alexandrzhovty/xnzpA
 http://swiftandpainless.com/creating-a-smart-file-template/
 
 */

import UIKit
import Firebase

class ImageCache {
    static let `default` = ImageCache()
    
    typealias DownloadIdentifier = StringIdentifier
	
	static var budleID: String {
		return  "\(Bundle.main.bundleIdentifier!).\(String(describing: ImageCache.self))"
	}
	
	fileprivate let concurrentPhotoQueue = DispatchQueue(label: ImageCache.budleID, attributes: .concurrent)
	fileprivate var _memoryCache: NSCache<NSString, UIImage>
	
    private init() {
		_memoryCache = NSCache<NSString, UIImage>()
	}
}

protocol ImageCacheProtocol {
	
}

extension ImageCache: ImageCacheProtocol {
	func image(forKey storageRef: StorageReference) -> UIImage? {
		// Handling readers, writers problem
//		var imageCopy: UIImage?
//		concurrentPhotoQueue.sync {
//			imageCopy = self._memoryCache.object(forKey: storageRef.cacheKey)
//		}
		return self._memoryCache.object(forKey: storageRef.cacheKey)
	}
	
	func set(_ image: UIImage?, forKey storageRef: StorageReference) {
		// handling readers, writers problem
		concurrentPhotoQueue.async(flags: .barrier) {
			if let image = image {
				self._memoryCache.setObject(image, forKey: storageRef.cacheKey)
			} else {
				self._memoryCache.removeObject(forKey: storageRef.cacheKey)
			}
		}
	}
	
	@discardableResult
	func load(forKey storageRef: StorageReference, completion: @escaping (_ image: UIImage?) -> Void) -> StorageDownloadTask? {
        
		if let cachedImage = ImageCache.default.image(forKey: storageRef) {
			completion(cachedImage)
			return nil
		} else {
			return storageRef.getData(maxSize: 1 * 1024 * 1024) {
				(data, error) -> Void in
				guard error == nil else {
					print("Cannot load image: ", error!.localizedDescription)
					completion(nil)
					return
				}
				
				let image: UIImage! = UIImage(data: data!)
				completion(image)
				ImageCache.default.set(image, forKey: storageRef)
			}
			
		}
	}
	
	@discardableResult
	func load(forKey storageRef: StorageReference, downloadID: DownloadIdentifier?, completion: @escaping (_ image: UIImage?, _ downloadID: DownloadIdentifier? ) -> Void) -> StorageDownloadTask? {
		if let cachedImage = ImageCache.default.image(forKey: storageRef) {
			completion(cachedImage, downloadID)
			return nil
		} else {
			return storageRef.getData(maxSize: 1 * 1024 * 1024) {
				(data, error) -> Void in
				guard error == nil else {
					print("Cannot load image: ", error!.localizedDescription)
					completion(nil, downloadID)
					return
				}
				
				let image: UIImage! = UIImage(data: data!)
				completion(image, downloadID)
				ImageCache.default.set(image, forKey: storageRef)
			}
			
		}
	}
	
}

extension StorageReference {
	var cacheKey: NSString { return self.fullPath as NSString }
}
