//
//  Venue.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreLocation


class Venue: NSObject {
	let id: String
	let name: String
	let rate: Int
	let address: String
	let distance: Double
	let location: CLLocation
	
	init(id: String, name: String, address: String, rate: Int, coordinate: CLLocationCoordinate2D, distance: CLLocationDistance) {
		self.id = id
		self.name =  name
		self.rate = rate
		self.address = address
		self.distance = distance
		self.location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
		super.init()
	}
	
	var isValid: Bool {
		return CLLocationCoordinate2DIsValid(location.coordinate) && name.isEmpty == false
	}
	
}
