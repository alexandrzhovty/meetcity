//
//  RecepientArangementTableCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class RecepientArangementTableCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var occupationLabel: UILabel!
	
	@IBOutlet weak var stopwatchView: StopwatchView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension RecepientArangementTableCell: NibLoadableView {}
