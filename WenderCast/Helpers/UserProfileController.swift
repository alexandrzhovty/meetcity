//
//  UserProfileController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase
import CoreData


//    MARK: - Properties & variables
extension UserProfileController: ProfilePresenterControllerProtocol {}

final class UserProfileController: UIViewController {
	//    MARK: Public
    var user: DMUser = AppDelegate.me
    var managedObjectContext: NSManagedObjectContext = AppDelegate.shared.mainContext
    
	var userModel: UserModel!
    
    
    
	var dataSource: DataSource!
	var viewControllerToReturn: UIViewController?
    
	
	//    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var heightTableViewConstraint: NSLayoutConstraint!
	
	@IBOutlet weak var userView: UserView!
	
	
	//    MARK: Private
	fileprivate var _presenter: ProfilePresenter!
	fileprivate var _photoManger: PhotoManager!
	fileprivate var _observers = [Any]()
	fileprivate var _galleryController: PhotoListContoroller!
	
	
	
	//    MARK: Initializations
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commotInt()
	}
	
	private func commotInt() {
		
		guard let user = AppDelegate.me else {
			fatalError()
		}
		
		userModel = UserModel(with: user)
		dataSource = UserProfileDataSource(with: self)
		_presenter = ProfilePresenter(with: self)
	}
	
	
	
}

//    MARK: - View life cycle
extension UserProfileController  {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
        managedObjectContext = AppDelegate.shared.mainContext
        userModel = UserModel(with: user)
		
		// Customize appearance
		Appearance.customize(viewController: self)
		
		
		// Configure tableView
		_presenter.setup(tableView)
		
		

	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		heightTableViewConstraint.constant = tableView.contentSize.height
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if let indexPath = tableView.indexPathForSelectedRow {
			tableView.deselectRow(at: indexPath, animated: true)
		}
        
        if let profileRef = AppDelegate.me.profilePhotoeRef {
            profileRef.downloadURL(completion: { (url, _) in
                AppDelegate.me.profilePhotoUrl = url?.absoluteString
            })
        }
		
		configure(userView)
		
//		_galleryController.galleryViewModel = GalleryViewModel(self.userModel, galleryViewType: .embed)
	}
}

//    MARK: - Utilities
extension UserProfileController  {
	fileprivate func configure(_ view: UIView) {
		switch view {
		case userView:
			userView.userModel = self.userModel
		default: break
		}
		
	}
}


//    MARK: - Outlet functions
extension UserProfileController  {
	//	MARK: Buttons
	@IBAction func didTapDone(_ sender: Any) {
		if let vc = self.viewControllerToReturn {
			navigationController?.pushViewController(vc, animated: true)
			self.viewControllerToReturn = nil
			
		} else {
			if shouldPerformSegue(.showNearbyList, sender: nil) {
				performSegue(.showNearbyList, sender: nil)
			}
		}
	}
	
	//    MARK: Switcher
	@IBAction func switchDidChangeValue(_ switcher: UISwitch) {
		
	}
}

// MARK: - Navigation & SegueHandler protocol
extension UserProfileController: SegueHandler {
	enum SegueType: String {
		case showSettings
		case showHistory
		case showEditProfile
		case showNearbyList
		case showEmbedGallery
	}
	
	@IBAction func exitToUserProfileController(_ segue: UIStoryboardSegue) {}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segueType(for: segue) {
		case .showHistory: break
//			let barButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
//			navigationController?.navigationItem.backBarButtonItem = barButton
		case .showNearbyList: break
//			let barButton = UIBarButtonItem(image: UIImage(Icon.profile), style: .plain, target: nil, action: nil)
//			navigationController?.navigationItem.backBarButtonItem = barButton
			
		case .showEditProfile:
			let vc = (segue.destination as! UINavigationController).topViewController as! ProfileManagerController
			vc.userModel = self.userModel
		
		case .showSettings:
			let vc = (segue.destination as! UINavigationController).topViewController as! SettingsController
			vc.userModel = self.userModel
			
		case .showEmbedGallery:
			_galleryController = segue.destination as! PhotoListContoroller
			_galleryController.userModel = self.userModel
//			_galleryController.galleryViewModel = GalleryViewModel(self.userModel, galleryViewType: .embed)
		}
	}
}


