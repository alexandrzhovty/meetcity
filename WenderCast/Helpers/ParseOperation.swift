//
//  ParseOperation.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/21/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData

class ParseOperation: Operation {
   let mainContext: NSManagedObjectContext
   let privateContext: NSManagedObjectContext
   
   
   typealias ParsingHandler = (_ context: NSManagedObjectContext) -> Void
   
   var parsingHandler: ParsingHandler
   
   init(parsingHandler: @escaping ParsingHandler) {
      self.parsingHandler = parsingHandler
      mainContext = AppDelegate.shared.mainContext
      privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
      super.init()
      
      privateContext.persistentStoreCoordinator = AppDelegate.shared.mainContext.persistentStoreCoordinator
      privateContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
      
      
      NotificationCenter.default.addObserver(self, selector: #selector(managedObjectContextDidSave(_:)), name: Notification.Name.NSManagedObjectContextDidSave, object: privateContext)
      
   }
   
   deinit {
      NotificationCenter.default.removeObserver(self)
   }
   
   override func main() {
      privateContext.performAndWait {
         
         self.parsingHandler(self.privateContext)
         
         if self.privateContext.hasChanges {
            do { try self.privateContext.save() }
            catch { print(error.localizedDescription) }
         }
      }
   }
   
   func managedObjectContextDidSave(_ notification: Notification) {
      AppDelegate.shared.mainContext.performAndWait {
         AppDelegate.shared.mainContext.mergeChanges(fromContextDidSave: notification)
      }
   }
}
