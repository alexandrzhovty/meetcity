//
//  Resource.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
 https://bitbucket.org/snippets/alexandrzhovty/eLGqA/resources
 http://swiftandpainless.com/creating-a-smart-file-template/
 
 */

import UIKit

protocol ResourceConvertible {}

struct Resources {
    enum JSON: String, ResourceConvertible, Testable {
        case mapStyle = "map_style"
    }
    
    enum MOV: String, ResourceConvertible, Testable  {
        case promo = "promo"
    }
    
    enum PDF: String, ResourceConvertible, Testable  {
        case agreements = "Agreements"
        case privacyPolicy = "PrivacyPolicy"
    }
}


extension ResourceConvertible where Self: RawRepresentable, Self.RawValue == String {
    var url: URL {
        return Bundle.main.url(for: self)
    }
}

extension Bundle {
    func url<T: ResourceConvertible>(for resource: T) -> URL where T: RawRepresentable, T.RawValue == String {
        let ext = String(describing: T.self).lowercased()
        guard let url = self.url(forResource: resource.rawValue, withExtension: ext) else {
            fatalError("Could not find resource \(resource.rawValue).\(ext)")
        }
        return url
    }
    
    func path<T: ResourceConvertible>(for resource: T) -> String where T: RawRepresentable, T.RawValue == String {
        let ext = String(describing: T.self).lowercased()
        guard let path = self.path(forResource: resource.rawValue, ofType: ext) else {
            fatalError("Could not find resource \(resource.rawValue).\(ext)")
        }
        return path
    }
}
