//
//  Appearance.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

final class Appearance {
   private init(){}
   //    MARK: - Class functions
   static func customize() {
      
      
      // Color file template should be added into the project.
      UIApplication.shared.keyWindow?.tintColor  = UIColor.tint
      
      Appearance.customizeNavigationBar()
      Appearance.customizeSwithController()
      Appearance.customizeSegmentedControl()
      
//      UIFont.printAllFonts()
//		UIFont.printFonts(for: "Avenir")
		
   }
   
   
   static func customizeNavigationBar() {
      let navBar = UINavigationBar.appearance()
      navBar.tintColor = UIColor.navigation.tintColor
      
      // Navigation bar title
      let attr: [String : Any] = [
         NSForegroundColorAttributeName: UIColor.white,
         NSFontAttributeName: UIFont.title.navigationBar
      ]
      navBar.titleTextAttributes = attr
      
      // Background color
      navBar.isTranslucent = false
      navBar.setBackgroundImage( UIImage(Background.navigationBar), for: .default)
      navBar.shadowImage = UIImage(Icon.blank)
    
      // Navigation bar item
      let navBarButton = UIBarButtonItem.appearance()
      let barButtonAttr = [ NSFontAttributeName: UIFont.title.barButton]
      navBarButton.setTitleTextAttributes(barButtonAttr, for: .normal)
      
   }
   
   
   
   static func customize(viewController: UIViewController) {
      if viewController.parent is UINavigationController {
         // Hides text in back barbutton item
         viewController.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
      }
   }
   
}

//   MARK: - UISwitch
extension Appearance {
   static func customizeSwithController() {
      let appearance = UISwitch.appearance()
      appearance.tintColor = UIColor(.pink)
      appearance.onTintColor = UIColor(.pink)
//      appearance.thumbTintColor = UIColor.brown
   }
}

//   MARK: - Segmented control
extension Appearance {
   static func customizeSegmentedControl() {
      let appearance = UISegmentedControl.appearance()
      appearance.tintColor = UIColor(.pink)
      
      let attr = [
         NSFontAttributeName: FontFamily.Avenir.book.font(size: 14),
         NSForegroundColorAttributeName: UIColor.black
      ] as [String: Any]
      
      appearance.setTitleTextAttributes(attr, for: .normal)
   }
}








