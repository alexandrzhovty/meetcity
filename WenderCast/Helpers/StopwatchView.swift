//
//  StopwatchView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/28/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

final class StopwatchView: UIView {
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	

	var timeInterval: TimeInterval = -1 { didSet { configure(titleLabel) } }
	
	override func awakeFromNib() {
		super.awakeFromNib()
		configure(titleLabel)
	}

	private func configure(_ view: UIView) {
		switch view {
		case titleLabel:
			if timeInterval < 0 {
				imageView.isHidden = true
				titleLabel.text = nil
			} else {
				let ti = Int(timeInterval)
				let seconds = ti % 60
				let minutes = (ti / 60) % 60
				titleLabel.text = String(format: "%zd:%0.2d", minutes, seconds)
				imageView.isHidden = false
			}
		default: break
		}
		
	}
	
}
