//
//  DMUser.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/21/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import CoreData
import SwiftyJSON
import CoreLocation
import Firebase
import CoreLocation


extension DMUser {
    enum CodingKeys {
        static let uid = "userId"
        static let name = "name"
        static let email = "email"
        static let photos = "photos"
        
        static let deviceToken = "deviceToken"
        
        static let tokensAndroid = "tokensAndroid"
        static let tokensIOS = "tokensIOS"
        
        
        static let invisible = "invisible"
        static let notifications = "notifications"
        static let aboutMe = "aboutme"
        static let showMe = "showMe"
        static let instagramUserId = "instagramUserId"
        static let instagramUserName = "instagramUserName"
        static let locationHidden = "locationHidden"
        static let intentions = "intentions"
        static let gender = "gender"
        static let lookFor = "lookFor"
        static let birthday = "birthday"
        static let joined = "joined"
        static let online = "online"
        static let readyToMeet = "readytomeet"
        static let intervalReadyToMeet = "avatarurl"
        static let warning = "warning"
        static let offline = "offline"
        static let facebookphoto = "facebookphoto"
        static let occupation = "occupation"
        
        static let latitude = "lat"
        static let longitude = "lng"
        static let updatedLatLng = "updatedLatLng"
    }
}

// MARK: - Fields

typealias UserIdentifier = StringIdentifier

extension DMUser {
    var identifier: UserIdentifier {
        get { return UserIdentifier(rawValue: self.uid!) }
        set { self.uid = newValue.rawValue }
    }
}

extension DMUser {
	
	// MARK: - Overrides
	
	public override func awakeFromInsert() {
		super.awakeFromInsert()
		self.joined = Date().timeIntervalSince1970
        
	}
	
	class func removeAll(from context: NSManagedObjectContext) {
		let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
		let restuls = try? context.fetch(request)
		for user in restuls ?? [] {
			context.delete(user)
		}
		
		if context.hasChanges {
			try? context.save()
		}
		
	}
	
	// MARK: - Utilities
	class func user(with firebaseUser: User, in context: NSManagedObjectContext = CoredataStack.mainContext) -> DMUser {
		
		let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
		request.predicate = NSPredicate(format: "%K = %@", #keyPath(DMUser.uid), firebaseUser.uid)
		
		let user: DMUser = try! context.fetch(request).first ?? DMUser.new(with: firebaseUser.uid, in: context)
		
		user.name = firebaseUser.displayName?.components(separatedBy: " ").first ?? ""
		user.email = firebaseUser.email
		user.joined = Date().timeIntervalSince1970
		
		return user
	}
	
	
	
	/// Creares hash table of existing entities
	///
	/// - Parameters:
	///   - ids: array of ID used to filter enitities
	///   - context: where enitities are taken from
	/// - Returns: list of Category entities
	class func list(for ids: [String], from context: NSManagedObjectContext) -> [String: DMUser] {
		
		guard ids.count > 0 else { return [:] }
		
		let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
		request.predicate = NSPredicate(format: "%K in %@", #keyPath(DMUser.uid),  ids)
		let results = try! context.fetch(request)
		
		if results.count > 0 {
			let keys = results.map({ $0.uid }) as! [NSCopying]
			let dictionary = NSDictionary(objects: results, forKeys: keys)
			return dictionary as! [String: DMUser]
			
		} else {
			return [:]
		}
	}
	
	/// Try to find an user by given ID
	///
	/// - Parameters:
	///   - id: user id used to search
	///   - context: where it is neccessary to search in
	/// - Returns: found user
	class func user(for id: UserIdentifier, from context: NSManagedObjectContext) -> DMUser? {
		let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
		request.predicate = NSPredicate(format: "%K = %@", #keyPath(DMUser.uid),  id.rawValue)
		let results = try! context.fetch(request)
		return results.first
	}
	
	/// Creates new entity
	///
	/// - Parameters:
	///   - id: unique ID for entity
	///   - context: where the new entity will created in
	/// - Returns: new entity
	class func new(with uid: String, in context: NSManagedObjectContext) -> DMUser {
		let entity: DMUser
        
		if #available(iOS 10.0, *) {
			entity = DMUser(context: context)
		} else {
			entity = NSEntityDescription.insertNewObject(forEntityName: String(describing: DMUser.self), into: context) as! DMUser
		}
		entity.uid = uid
        return entity
	}
	
	//    MARK: - Parsing
    
    /// Remove entitites that were removed in the server site
    ///
    /// - Parameters:
    ///   - response: array of JSON objects
    ///   - context: where the entities should be removed from
    class func removeUnneccessary(_ response: [[String: Any]], from context: NSManagedObjectContext) {
        let ids = response.flatMap{ $0[CodingKeys.uid] as? String }
        // Delete old values
        let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
        request.predicate =	NSPredicate(format: "NOT (%K in %@)", #keyPath(DMUser.uid),  ids)
        do {
            let results = try context.fetch(request)
            results.forEach{
                
                print("WE ARE HERE")
                
                context.delete($0)
            }
        } catch {}
    }
    
    
	/// Parse JSON response from server
	///
	/// - Parameters:
	///   - json: array of JSON objects
	///   - context: context where parsed entities will be stored into
	class func parse(_ response: [[String: Any]], into context: NSManagedObjectContext) {
		
		
		let ids = response.flatMap{ $0[CodingKeys.uid] as? String }
		let list = DMUser.list(for: ids, from: context)
		
		
		let json = JSON(response)
		
		for record in json.array ?? [] {
			// Main fields
			guard let uid = record[CodingKeys.uid].string else { continue }
			let entity: DMUser = list[uid] ?? DMUser.new(with: uid, in: context)
			entity.update(with: record, into: context)
			
			
			
//			do {
//				if entity.isInserted {
//					try entity.validateForInsert()
//				} else {
//					try entity.validateForUpdate()
//				}
//				
//			} catch {
//				context.delete(entity)
//			}
		}
	}
	
	/// Parse JSON response from server
	///
	/// - Parameters:
	///   - json: array of JSON objects
	func update(with record: JSON, into context: NSManagedObjectContext) {
		assert(self.managedObjectContext == context, "User stored in another context")
		
		// Main fields
		self.name = record[CodingKeys.name].string
		
        
		self.readyToMeet = record[CodingKeys.readyToMeet].bool ?? false
		self.lookForValue = record[CodingKeys.lookFor].int16 ?? LookFor.none.rawValue
		self.joined = record[CodingKeys.joined].double ?? 0
		self.genderValue = record[CodingKeys.gender].int16 ?? Gender.none.rawValue
		self.showMe = record[CodingKeys.showMe].bool ?? false
		self.birthday = record[CodingKeys.birthday].double ?? 0
		self.aboutMe = record[CodingKeys.aboutMe].string
        
        
        // TODO: CodingKeys.deviceToken should be removed
		self.tokenIOSValues = record[CodingKeys.tokensIOS].string ?? record[CodingKeys.deviceToken].string ?? nil
        self.tokenAndroidValues = record[CodingKeys.tokensAndroid].string ?? nil
        
        
        self.occupation = record[CodingKeys.occupation].string ?? nil
        
        // Instagram
        self.instagramUserId = record[CodingKeys.instagramUserId].string
        self.instagramUserName = record[CodingKeys.instagramUserName].string
        
        
		// Location
		let lat = record[CodingKeys.latitude].double ?? kCLLocationCoordinate2DInvalid.latitude
		let lng = record[CodingKeys.longitude].double ?? kCLLocationCoordinate2DInvalid.longitude
        
    
        
		self.location = CLLocation(latitude: lat, longitude: lng)
		
		self.lastUpdate = record[CodingKeys.updatedLatLng].doubleValue
		
		
		// Photos
		let photos = record[CodingKeys.photos].arrayObject as? [[String: Any]] ?? []
		DMPhoto.parse(photos, for: self, into: context)
		

        
        if CLLocationCoordinate2DIsValid(AppDelegate.me.location.coordinate) && CLLocationCoordinate2DIsValid(self.location.coordinate) {
            self.distance = AppDelegate.me.location.distance(from: self.location)
        } else {
            self.distance = -1
        }
		 
	}
	
	/// Parse JSON response from server
	///
	/// - Parameters:
	///   - snapshot: Firebase response
	func update(with snapshot: DataSnapshot, into context: NSManagedObjectContext) {
		assert(self.managedObjectContext == context, "User stored in another context")
		guard let value = snapshot.value as? [String: AnyObject] else {
			return
		}
		self.update(with: JSON(value), into: context)
	}
}

// MARK: - Common propperties
extension DMUser {
    var title: String {
        var array: [String?] = [self.name]
        if self.birthday > 0 {
            let date1 = Date(timeIntervalSince1970: self.birthday)
            if let age = Calendar.current.dateComponents([.year], from: date1, to: Date()).year {
                array.append("\(age)")
            }
        }
        
        return array.flatMap{ $0 }.joined(separator: ", ")
        
    }
    
    var readyToMeet: Bool {
        get {
            return self.readyToMeetValue && self.lastUpdate > Date().timeIntervalSince1970 - Constants.ReadyToMeet.timeIntervalFromLastVisit
        }
        set { self.readyToMeetValue = newValue }
    }
    
    var lookFor: LookFor {
        get { return LookFor(rawValue: self.lookForValue) ?? LookFor.none }
        set { self.lookForValue = newValue.rawValue}
    }
    
    
    var gender: Gender {
        get { return Gender(rawValue: self.genderValue) ?? Gender.none }
        set { self.genderValue = newValue.rawValue }
    }
    
    var photos: [DMPhoto] {
        get {
            let sortOrder = NSSortDescriptor(key: #keyPath(DMPhoto.sortOrder), ascending: true)
            return self.photoValues?.sortedArray(using: [sortOrder]) as? [DMPhoto] ?? []
        }
    }
    
    var isCompletelyRegistered: Bool {
        return self.gender != Gender.none && self.lookFor != LookFor.none
    }
    
}

// MARK: - Profile photo
extension DMUser  {
    var profilePhotoeRef: StorageReference? {
        let sortOrder = NSSortDescriptor(key: #keyPath(DMPhoto.sortOrder), ascending: true)
        let array = self.photoValues?.sortedArray(using: [sortOrder]) as? [DMPhoto]
        guard let fileName = array?.first?.fileName else { return nil }
        return FirebaseStack.storageRef.child(self.uid!).child(fileName)
    }
}

// MARK: - Location
extension DMUser {
    var distanceToMe: Double? {
        return self.location.distance(from: AppDelegate.me.location)
    }
}

// MARK: - Device tokens
extension DMUser {
    enum Settings {
        static let separator = ","
        static let tokenStoreKey = "CurrentDeviceToken"
    }
    
    /// List of user device tokens
    @available(*, deprecated, message: "Use `tokens(for deviceSystem: DeviceSystem) -> [String]` property instead")
    var deviceTokens: [String] {
        get {
            let tokens = self.tokenIOSValues ?? ""
            return tokens.components(separatedBy: Settings.separator)
        }
        set {
            self.tokenIOSValues = newValue.joined(separator: Settings.separator)
        }
    }
    
    func tokens(for deviceSystem: DeviceSystem) -> [String] {
        switch deviceSystem {
        case .apple:
            let str = self.tokenIOSValues ?? ""
            return str.components(separatedBy: Settings.separator)
        case .android:
            let str = self.tokenAndroidValues ?? ""
            return str.components(separatedBy: Settings.separator)
        }
    }
}



// MARK: - Location
extension DMUser {
    var location: CLLocation {
        get { return CLLocation(latitude: self.locationLatValue, longitude: self.locationLngValue) }
        set {
            self.locationLatValue = newValue.coordinate.latitude
            self.locationLngValue = newValue.coordinate.longitude
        }
    }
}




// MARK: - SynchronizationProtocol
extension DMUser: SynchronizationProtocol {
    
    func synchronize(completion: @escaping (Result<DatabaseReference>) -> Swift.Void) {
        
        var json = JSON([:])
        
        json[CodingKeys.uid].string = self.uid
        json[CodingKeys.name].string = self.name
        json[CodingKeys.email].string = self.email
        json[CodingKeys.aboutMe].string = self.aboutMe
        json[CodingKeys.joined].double = self.joined
        json[CodingKeys.readyToMeet].bool = self.readyToMeetValue
        json[CodingKeys.birthday].double = self.birthday
        json[CodingKeys.gender].int16 = self.genderValue
        json[CodingKeys.lookFor].int16 = self.lookForValue
        json[CodingKeys.occupation].string = self.occupation
        json[CodingKeys.showMe].bool = self.showMe
        json[CodingKeys.locationHidden].bool = self.locationHidden
        json[CodingKeys.instagramUserId].string = self.instagramUserId
        json[CodingKeys.instagramUserName].string = self.instagramUserName
        
        json[CodingKeys.tokensIOS].string = self.tokenIOSValues
        json[CodingKeys.tokensAndroid].string = self.tokenAndroidValues
        
        
        // Photos
        let sortOrder = NSSortDescriptor(key: #keyPath(DMPhoto.sortOrder), ascending: true)
        let array = self.photoValues?.sortedArray(using: [sortOrder]) as? [DMPhoto] ?? []
        
        let fileNames = array.map { [DMPhoto.Fields.fileName: $0.fileName] }
        json[CodingKeys.photos].arrayObject = fileNames
        
        
        // Location
        
        json[CodingKeys.latitude].double = self.locationLatValue
        json[CodingKeys.longitude].double = self.locationLngValue
        
        
        // Last update time is always new
        json[CodingKeys.updatedLatLng].double = Date().timeIntervalSince1970
        
        
      
        let userRef = FirebaseStack.user(for: self.uid!)
        
        userRef.setValue(json.dictionaryObject) { (error, dbRef) in
            if let error = error {
                completion(Result.failure(error))
            } else {
                completion(.success(dbRef))
            }
        }
    }
    
    func synchronizeCoordinates() {
        // Location
        
        guard
            CLLocationCoordinate2DIsValid(self.location.coordinate),
            self.isCompletelyRegistered
        else {
            return
        }
        
        
        
        let lat = self.location.coordinate.latitude
        let lng = self.location.coordinate.longitude
        
        if lat == 0 && lng == 0 {
            return
        }
        
        // save in user tables
        let userRef = FirebaseStack.user(for: self.identifier.rawValue)
        userRef.child(CodingKeys.latitude).setValue(lat)
        userRef.child(CodingKeys.longitude).setValue(lng)
        userRef.child(CodingKeys.updatedLatLng).setValue(Date().timeIntervalSince1970)
        
        
    }
}



