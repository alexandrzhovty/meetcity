//
//  MessageManagerProtocol.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/3/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

protocol MessageManagerProtocol {
	func registerRemoteNotifications(for application: UIApplication, completion: () -> Void)
}

