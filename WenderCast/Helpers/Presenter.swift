//
//  Presenter.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/6/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import SVProgressHUD
import SwiftyJSON
import CoreData

struct Presenter {
	
 
	static func presentProfileController(for uid: String, startUp: Bool = false) {
		let rootVC = AppDelegate.shared.rootVC
		
		
		if startUp {
			
			let vc = UIStoryboard(.profile).instantiateViewController(UserProfileController.self)
			let vc2 = UIStoryboard(.dashboard).instantiateViewController(NearbyListController.self)
			
			// Nearby profile controller
			let profileVC = UIStoryboard(.dashboard).instantiateViewController(NearbyProfileController.self)
			let context = AppDelegate.shared.mainContext
			let user = DMUser.user(for: UserIdentifier(rawValue: uid), from: context) ?? {
				let new = DMUser.new(with: uid, in: context)
				return new
				}()
			
			profileVC.refreshProfileData = true
			profileVC.user = user
			profileVC.managedObjectContext = context
			
			
			rootVC.setNavigationBarHidden(false, animated: true)
			rootVC.setViewControllers([vc, vc2, profileVC], animated: false)
			
			
			return
		}
		
		if AppDelegate.shared.datingStatus != .searching {
			return
		}
		
		
		guard let first = rootVC.viewControllers.first as? UserProfileController else {
			return
		}
		var viewControllers = [UIViewController]()
		viewControllers.append(first)
		
		
		// Nearby list controller
		let vc: UIViewController? = (rootVC.viewControllers.count > 1) ? rootVC.viewControllers[1] : nil
		if vc is NearbyListController {
			viewControllers.append(vc!)
		} else {
			let vc = UIStoryboard(.dashboard).instantiateViewController(NearbyListController.self)
			vc.managedObjectContext = AppDelegate.shared.mainContext
			viewControllers.append(vc)
		}
		
		// Nearby profile controller
		let profileVC = UIStoryboard(.dashboard).instantiateViewController(NearbyProfileController.self)
		let context = AppDelegate.shared.mainContext
		let user = DMUser.user(for: UserIdentifier(rawValue: uid), from: context) ?? {
			let new = DMUser.new(with: uid, in: context)
			return new
			}()
		
		profileVC.refreshProfileData = true
		profileVC.user = user
		profileVC.managedObjectContext = context
		viewControllers.append(profileVC)
		
		
		
		let navVC = UINavigationController()
		
		AppDelegate.shared.window?.rootViewController = navVC
		navVC.setViewControllers(viewControllers, animated: true)
		
		
	}
	
	static func presentLoginScreen() {
		
		Presenter.initRootViewControllerWithStoryboard(.login)
		////		initRootViewControllerWithStoryboard(.main)
		//		initRootViewControllerWithStoryboard(.editor)
	}
	
	
	fileprivate static func initRootViewControllerWithStoryboard(_ type: StroyboadType) {
		guard let window = AppDelegate.shared.window else {
			assertionFailure()
			return
		}
		window.rootViewController = UIStoryboard(type).instantiateInitialViewController()
	}
}

// MARK: - Nearby list
extension Presenter {
    static func presentNearByList() {
        
        
        
        let rootVC = AppDelegate.shared.rootVC
        let first: UIViewController
        if rootVC.viewControllers.first is UserProfileController {
            first = rootVC.viewControllers.first!
        } else {
            first = UIStoryboard(.profile).instantiateViewController(UserProfileController.self)
        }
        
//        DispatchQueue.main.async {
//            rootVC.setNavigationBarHidden(false, animated: false)
//            rootVC.setViewControllers([first], animated: false)
//        }
//
//        
//        return
        
        
        
        if let profileRef = AppDelegate.me.profilePhotoeRef {
            profileRef.downloadURL(completion: { (url, _) in
                AppDelegate.me.profilePhotoUrl = url?.absoluteString
            })
        }
        
        let second = UIStoryboard(.dashboard).instantiateViewController(NearbyListController.self)
        DispatchQueue.main.async {
            rootVC.setNavigationBarHidden(false, animated: false)
            rootVC.setViewControllers([first, second], animated: false)
        }
        
    }
}

// MARK: - Request
extension Presenter {
    static func presentRequest(for inviteModel: InviteModel) {
        let rootVC = AppDelegate.shared.rootVC
        let first = Presenter.userProfileController
        let second = UIStoryboard(.invite).instantiateViewController(RequestingController.self)
        second.invite = inviteModel.invite
        second.managedObjectContext = inviteModel.context
        
        DispatchQueue.main.async {
            rootVC.setNavigationBarHidden(false, animated: false)
            rootVC.setViewControllers([first, second], animated: false)
        }
        
    }
    
    static func presentMeetingArragement(for inviteModel: InviteModel) {
        let rootVC = AppDelegate.shared.rootVC
        let first = Presenter.userProfileController
        let second = UIStoryboard(.meeting).instantiateViewController(MeetingArrangementController.self)
        second.inviteModel = inviteModel
        
        DispatchQueue.main.async {
            rootVC.setNavigationBarHidden(false, animated: false)
            rootVC.setViewControllers([first, second], animated: false)
        }
        
    }
}

// MARK: - Meeting arangement
extension Presenter {
    static func presentDating(for inviteModel: InviteModel) {
        let rootVC = AppDelegate.shared.rootVC
        let first: UIViewController
        if rootVC.viewControllers.first is UserProfileController {
            first = rootVC.viewControllers.first!
        } else {
            first = UIStoryboard(.profile).instantiateViewController(UserProfileController.self)
        }
        
        let second = UIStoryboard(.dating).instantiateViewController(DatingController.self)
        second.inviteModel = inviteModel
        
        DispatchQueue.main.async {
            rootVC.setNavigationBarHidden(false, animated: false)
            rootVC.setViewControllers([first, second], animated: false)
        }
    }
}

// MARK: - Dating controller
//extension Presenter {
//    static func presentDating(with inviteModel: InviteModel) {
//        let rootVC = AppDelegate.shared.rootVC
//        let first = Presenter.userProfileController
//        let second = UIStoryboard(.dating).instantiateViewController(DatingController.self)
//        second.inviteModel = inviteModel
//        
//        DispatchQueue.main.async {
//            rootVC.setNavigationBarHidden(false, animated: false)
//            rootVC.setViewControllers([first, second], animated: false)
//        }
//    }
//}


// MARK: - First controller should **User Profile** controller
extension Presenter {
    static var userProfileController: UserProfileController {
        let rootVC = AppDelegate.shared.rootVC
        let first: UserProfileController
        if rootVC.viewControllers.first is UserProfileController {
            first = rootVC.viewControllers.first as! UserProfileController
        } else {
            first = UIStoryboard(.profile).instantiateViewController(UserProfileController.self)
        }
        return first
    }
}

