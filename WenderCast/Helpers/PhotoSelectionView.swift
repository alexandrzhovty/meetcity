//
//  PhotoSelectionView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/6/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import AlamofireImage

extension PhotoSelectionView: NibLoadableView {}

//@IBDesignable

class PhotoSelectionView: UIView {
	
	var managerController: ProfileManagerController!
	
	// MARK: Outlets
	@IBOutlet weak var widthConstraint: NSLayoutConstraint!
	@IBOutlet weak var mainContainer: UIView!
	@IBOutlet weak var rightContainer: UIView!
	@IBOutlet weak var mainImageHolder: PhotoThumbView!
	@IBOutlet var imageHolders: [PhotoThumbView]!
	
    fileprivate var selectedImageView: PhotoThumbView?
    fileprivate var sourceThumbView: PhotoThumbView?
	
	fileprivate var _items: [PhotoThumbView] {
		return imageHolders.sorted(by: { (v1, v2) -> Bool in
			return v1.tag < v2.tag
		})
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		
		
		let size = UIScreen.main.bounds.size
		self.frame.size = PhotoSelectionView.sizeThatFits(size)
		
		mainContainer.backgroundColor = UIColor.clear
		rightContainer.backgroundColor = UIColor.clear
		backgroundColor = UIColor.clear
		
//		addButtons()
		
		configueCollection()
		
		_items.forEach{
			$0.deleteButton.addTarget(self, action: #selector(didTapSelect(_:)), for: .touchUpInside)
		}
		
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        pan.delegate = self
        addGestureRecognizer(pan)
	}
	
	private var _itemWidth: CGFloat {
		let width = (self.bounds.width - (Constants.MainMargin * 4)) / 3 + 24
		return width
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		widthConstraint.constant = _itemWidth
	}
	
	static func sizeThatFits(_ size: CGSize) -> CGSize {
		return CGSize(width: size.width, height: size.width)
	}
	
	private func configueCollection() {
		
		_items.forEach{ $0.image = nil }
		
		
		
		// Get a reference to the storage service using the default Firebase App
		let storage = Storage.storage()
		
		// Create a storage reference from our storage service
		let storageRef = storage.reference()
		
//		print(model.uid)
		
//		print(AppDelegate.me ?? "BLLIN NOT FOUND")
//		print(AppDelegate.me.uid ?? "BLLIN NOT FOUND")
		
		
        
        
		let photosRef = storageRef.child(AppDelegate.me.uid!)
        
        let photos = AppDelegate.me.photos
        
        for (idx, value) in _items.enumerated() {
            if idx < photos.count, let fileName = photos[idx].fileName {

                let imageRef = photosRef.child(fileName)
                value.fileName = fileName
                ImageCache.default.load(forKey: imageRef, completion: { [weak self] image in
                    guard let `self` = self else { return }
                    self._items[idx].image = image
                })

                
            } else {
                value.image = nil
                value.fileName = nil
            }
        }
        
        
//		for (idx, value) in model.photos.enumerated() {
//			let imageRef = photosRef.child(value.fileName!)
//            self._items[idx].fileName = value.fileName
//						
//		}
        
        

	}
	
	@objc func didTapSelect(_ button: UIButton) {
		assert(self.viewController() is ProfileManagerController, "View controller isn't set")
		
		let idx = button.superview!.superview!.tag
		let photoHolder = _items[idx]
		
		if photoHolder.image == nil {
			let title = self.viewController()?.navigationItem.title
			let alertVC = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
			alertVC.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
				let profileManagerController = self.viewController() as! ProfileManagerController
				let imagePicker = profileManagerController.imagePicker
				imagePicker.delegate = profileManagerController
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .camera
				profileManagerController.present(imagePicker, animated: true, completion: nil)
				
			}))
			
			alertVC.addAction(UIAlertAction(title: "Library", style: .default, handler: { _ in
				let profileManagerController = self.viewController() as! ProfileManagerController
				let imagePicker = profileManagerController.imagePicker
				imagePicker.delegate = profileManagerController
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .photoLibrary
				profileManagerController.present(imagePicker, animated: true, completion: nil)
				
			}))
			
			alertVC.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
			
			self.viewController()?.present(alertVC, animated: true, completion: nil)
			
			
			
			
		} else {
			let title = self.viewController()?.navigationItem.title
			let alertVC = UIAlertController(title: title, message: Strings.SelectedImageWillBeDeleted, preferredStyle: .alert)
			alertVC.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
			alertVC.addAction(UIAlertAction(title: Strings.DeleteString, style: .destructive, handler: { [unowned self] _ in
				// Delete file
                
                guard idx < AppDelegate.me.photos.count else { return }
                
                let photo = AppDelegate.me.photos[idx]
                
                guard let fileName = photo.fileName else { return }
                
                AppDelegate.me.removeFromPhotoValues(photo)
                AppDelegate.shared.mainContext.delete(photo)
                
                let photoRef = Storage.storage().reference().child(AppDelegate.me.identifier.rawValue).child(fileName)
                photoRef.delete(completion: {  _ in })
                
                let photoHolder = self._items[idx]
                photoHolder.image = nil
                photoHolder.fileName = nil

                
                try? AppDelegate.shared.mainContext.save()
                
                
                self.configueCollection()
                self.updatePhotosOrder()
                
                
			}))
            
            
            if let popoverController = alertVC.popoverPresentationController {
                popoverController.sourceView = button
            }
            
            self.viewController()?.present(alertVC, animated: true, completion: nil)
            
			
		}
	}
	
	func add(_ image: UIImage) {
		
		
		let userModel = UserModel(with: AppDelegate.me)
		
		let idx = userModel.photos.count
		let holder = _items[idx]
		holder.image = image
		
		let filename = UUID().uuidString + ".jpg"
		let photoRef = userModel.photosRef.child(filename)
		
		guard let data = UIImageJPEGRepresentation(image, 0) else {
			assertionFailure()
			return
		}
		
		photoRef.putData(data, metadata: nil) { [weak self] (metadata, error) in
			guard error == nil else {
				print("Cannot upload ", error!.localizedDescription)
				return
			}
			
			guard let `self` = self else { return }
			
			let context = AppDelegate.shared.mainContext
            
            
			let photo = DMPhoto.new(with: filename, for: AppDelegate.me, in: context)
            photo.sortOrder = Int16.max

            do { try context.save() }
			catch {
				print(String(describing: type(of: self)),":", #function)
				print(error as NSError)
			}
            
			ImageCache.default.set(image, forKey: photoRef)
			
			self.configueCollection()
            self.updatePhotosOrder()
		}
	}
	
}

// MARK: - UIGestureRecognizerDelegate & Implemenetation
extension PhotoSelectionView: UIGestureRecognizerDelegate {
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        guard self._items.count > 0 else {
            return false
        }
        
        self.selectedImageView?.removeFromSuperview()
        self.selectedImageView = nil
        
        for view in imageHolders where view.image != nil {
            let location = gestureRecognizer.location(in: view)
            if view.imageView.frame.contains(location) {
                
                self.selectedImageView = PhotoThumbView()
                self.selectedImageView?.prepareForUsing()
                self.selectedImageView?.frame = self.convert(view.frame, from: view.superview)
                self.selectedImageView?.image = view.image
                self.selectedImageView?.tag = view.tag
                self.selectedImageView?.fileName = view.fileName
                self.addSubview(self.selectedImageView!)
                
                self.sourceThumbView = view
                self.sourceThumbView?.isHidden = true
                
                return true
            }
        }
        
        return false
    }
    
    @objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
       
        let translation = recognizer.translation(in: self)
        
    
        switch recognizer.state {
        case .began:
            break
            
        case .changed:
            guard var center = selectedImageView?.center else {
                return
            }
            center.x += translation.x
            center.y += translation.y
            
            
            
            checkPosition(for: center, recognizer: recognizer)
            
            
            selectedImageView?.center = center
            recognizer.setTranslation(CGPoint.zero, in: self)
            
        default:
            guard self.selectedImageView != nil else { return }
            
            UIView.animate(withDuration: 0.25, animations: {  [unowned self] in
                let center = self.convert(self.selectedImageView!.center, from: self.selectedImageView!.superview)
                self.selectedImageView?.center = center
                
                }, completion: { [unowned self] (finished) in
                    self.sourceThumbView?.image = self.selectedImageView?.image
                    self.sourceThumbView?.fileName = self.selectedImageView?.fileName
                    self.sourceThumbView?.isHidden = false
                    self.selectedImageView?.removeFromSuperview()
                    self.selectedImageView = nil
                    
                    self.updatePhotosOrder()
            })
            
            
            recognizer.setTranslation(CGPoint.zero, in: self)
        }
    }
    
    fileprivate func updatePhotosOrder() {
        var arr = [String]()
        for value in _items where value.fileName != nil {
            arr.append(value.fileName)
        }
        
        
        
        let nestedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        nestedContext.parent = AppDelegate.shared.mainContext
        nestedContext.perform {
            let user = nestedContext.object(with: AppDelegate.me.objectID) as! DMUser
            
            for value in user.photos {
                user.removeFromPhotoValues(value)
                value.user = nil
                nestedContext.delete(value)
            }
            
            for (idx, value) in arr.enumerated() {
                let photo = DMPhoto.new(with: value, for: user, in: nestedContext)
                photo.sortOrder = Int16(idx)
            }
            
//            let photos = user.photos.map{ [DMPhoto.Fields.fileName: $0.fileName] }
            
            
//            FirebaseStack.user(for: "me").child(DMUser.CodingKeys.photos).setValue(photos)
            
//            FirebaseStack.user(for: "me").setValue(photos, forKey: DMUser.CodingKeys.photos)
            
            
            
            do { try nestedContext.save() }
            catch { Alert.default.showError(message: error.localizedDescription) }
            
            user.synchronize(completion: { (result) in
                switch result {
                case .failure(let error):
                    Alert.default.showError(message: error.localizedDescription)
                    
                case .success:
                    break
                }
            })
            
            
            
            if let image = self._items.first?.image {
                let small = image.af_imageScaled(to: Settings.Photos.smallProfileSize)
                
                let filename = Settings.Photos.smallProfileName
                let photoRef = FirebaseStack.storageRef.child(AppDelegate.me.identifier.rawValue).child(filename)
                
                guard let data = UIImageJPEGRepresentation(small, 0) else {
                    assertionFailure()
                    return
                }
                
                photoRef.putData(data, metadata: nil) { (_, error) in
                    if error != nil {
                        print(error!.localizedDescription)
                    }
                }
                
                
            }
            

        }
    }
    
    private func checkPosition(for center: CGPoint, recognizer: UIPanGestureRecognizer) {
        // Find holder that should be removed
        for view in imageHolders where view.image != nil {
            
            
            let location = recognizer.location(in: view.superview)
            if view.frame.contains(location) {
                
                let tag = self.selectedImageView!.tag
                
                if view.tag < tag {
                    for idx in (view.tag...tag).reversed() where idx > 0 {
                        self._items[idx].image = self._items[idx - 1].image
                        self._items[idx].fileName = self._items[idx - 1].fileName
                        
                    }
                } else if view.tag > tag {
                    for idx in (tag...view.tag) {
                        if idx < self._items.count - 1 {
                            self._items[idx].image = self._items[idx + 1].image
                            self._items[idx].fileName = self._items[idx + 1].fileName
                            
                        }
                        
                    }
                } else {
                    return
                }
                
                
                sourceThumbView?.isHidden = false
                sourceThumbView = view
                sourceThumbView?.isHidden = true
                self.selectedImageView?.tag = view.tag
                if self.selectedImageView!.frame.size != view.frame.size {
                    UIView.animate(withDuration: 0.5, animations: { [unowned self] in
                        self.selectedImageView?.frame.size = view.frame.size
                        
                    })
                }
                
            }
        }
    }
    
    
}

