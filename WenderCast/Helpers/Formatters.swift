//
//  File.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import MapKit


struct Formatters {
	/// Date formatter
	enum date {
        /// Returns string in format "**yyyyMMdd**"
        static var visit: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMdd"
            return dateFormatter
        }()
        
		static var long: DateFormatter = {
			let dateFormatter = DateFormatter()
			dateFormatter.dateStyle = .medium
			dateFormatter.timeStyle = .short
			return dateFormatter
		}()
	}
    
    enum distance {
        static var `default`: MKDistanceFormatter = {
            let formatter = MKDistanceFormatter()
            return formatter
        }()
    }
    
    
	
}
