//
//  Profile.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

enum ProfileType {
	case switcher(currentValue: Bool, action: SwitchedProfileCellProtocol.Action?)
	case selectable(action: (_ sender: UIView?) -> Void)
	case buttonLike(action: (_ sender: UIView?) -> Void)
	case textEditabled(text: String?, textLmit: Int, action: TextEditProfileCellProtocol.Action?)
}


final class Profile {
	let title: String?
	let type: ProfileType
	
	init(title: String? = nil, type: ProfileType) {
		self.type = type
		self.title = title
	}
}
