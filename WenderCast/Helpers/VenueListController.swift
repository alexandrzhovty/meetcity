//
//  VenueListController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/28/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GoogleMaps
import SVProgressHUD

//    MARK: - Properties & variables
final class VenueListController: UIViewController {
    //    MARK: Public
	var location: CLLocation!
	var searchText: String?
	
	typealias CompletionHandler = (_ venue: Venue) -> Void
	var completion: CompletionHandler!
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    
    //    MARK: Private
	fileprivate var _venues = [Venue]()
	fileprivate lazy var _venueProvider: VenueProvider = VenueProvider()
    fileprivate var _mapPresenter: MapPresenter!
    fileprivate var _selectedVenue: Venue? {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = _selectedVenue != nil
        }
    }
	
    //    MARK: Enums & Structures
	
	
	
	
}

//    MARK: - View life cycle
extension VenueListController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
        // Customize appearance
        Appearance.customize(viewController: self)
		navigationItem.title = Strings.SelectPlace
		
		if location == nil {
			location = AppDelegate.shared.userLocation
		}
        
		// Configure tableView
		configure(tableView)
		
		navigationItem.rightBarButtonItem?.isEnabled = false
        
        _mapPresenter = MapPresenter(with: mapView, type: .venues)

		
		fetchVenues()
    }
}

//    MARK: - Utilities
extension VenueListController  {
	// MARK: Displaying
	
    /// Configure display view
    ///
    /// - Parameter view: that should be configured
    fileprivate func configure(_ view: UIView) {
        switch view {
		case tableView:
			tableView.estimatedRowHeight = 44
			tableView.rowHeight = UITableViewAutomaticDimension

		default: break
        }
		
    }
	
	
	/// Fetch venues from Foursquare
	fileprivate func fetchVenues() {
        SVProgressHUD.show()
		_venueProvider.fetch(searchText, around: location.coordinate) { [weak self] venues in
            
            defer {
                if SVProgressHUD.isVisible() {
                    SVProgressHUD.dismiss()
                }
            }
            
			guard let `self` = self else { return }
			self._venues.removeAll()
			self._venues += venues
			
			DispatchQueue.main.async {
                self.tableView.reloadData()
                if self.searchText.isEmptyOrNil {
                    self.tableView.isHidden = true
                } else {
                    self.tableView.isHidden = false
                }
                self._mapPresenter.displayMarkers(for: self._venues)
			}
			
		}
	}
}

//	MARK: - Search bar protocol
extension VenueListController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
	}
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchText = nil
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        fetchVenues()
    }
}

//    MARK: - Table view protocol
extension VenueListController: UITableViewDataSource, UITableViewDelegate {
	//	MARK: Datasource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return _venues.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		let venue = _venues[indexPath.row]
		cell.textLabel?.text = venue.name
		cell.detailTextLabel?.text = venue.address + " - \(Formatters.distance.default.string(fromDistance: venue.distance))"
		return cell
	}
	
	//	MARK: Selection
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		navigationItem.rightBarButtonItem?.isEnabled = true
        
        _selectedVenue = _venues[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        
        let camera = GMSCameraPosition.camera(withTarget: _selectedVenue!.location.coordinate, zoom: 16)
        self.mapView.animate(to: camera)
        
        tableView.isHidden = true
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		navigationItem.rightBarButtonItem?.isEnabled = false
	}
	
}


//    MARK: - Outlet functions
extension VenueListController  {
    //    MARK: Buttons
	@IBAction func didTapCancel(_ sender: UIBarButtonItem) {
		dismiss(animated: true, completion: nil)
	}
	
	@IBAction func didTapDone(_ sender: UIBarButtonItem) {
		dismiss(animated: true) { [unowned self] in
            if let venue = self._selectedVenue {
                self.completion(venue)
            }
		}
	}
	
}

// MARK: - GMSMapViewDelegate
extension VenueListController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        _selectedVenue = marker.userData as? Venue
        return false
    }
    
    
}



