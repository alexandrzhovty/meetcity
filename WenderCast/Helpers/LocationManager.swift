//
//  LocationManager.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/28/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//



import Foundation
import CoreLocation
import SwiftBackgroundLocation

class LocationManager: NSObject {
	
	fileprivate var _lastUpdate: TimeInterval = 0
	fileprivate var _minUpdateInterval: TimeInterval = 60 * 5
	fileprivate var _backgroundTask: UIBackgroundTaskIdentifier?
	
	var observers = [Any]()
	
	
	
	var locationManager = TrackingHeadingLocationManager()
	var backgroundLocationManager = BackgroundLocationManager(regionConfig: RegionConfig(regionRadius: 0.01))
	
	fileprivate lazy var _locationManager: CLLocationManager = {
		let locationManager = CLLocationManager()
		locationManager.delegate = self
		//		locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
		locationManager.distanceFilter = kCLDistanceFilterNone
		//		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		return locationManager
	}()
	
	static let `default` = LocationManager()
	private override init() {
		super.init()
		//		registerObserver()
	}
	
	deinit {
		//		unregisterObserver()
	}
	
	func startUpdatingLocation() {
		let status = CLLocationManager.authorizationStatus()
		if status == .authorizedAlways || status == .authorizedWhenInUse {
			_locationManager.startMonitoringSignificantLocationChanges()
			
			//			_locationManager.startUpdatingLocation()
		}
	}
	
	func stopUpdatingLocation()  {
		_locationManager.stopUpdatingLocation()
	}
	
	func requestAlwaysAuthorization() {
		_locationManager.requestAlwaysAuthorization()
	}
	
	fileprivate func updateUserLocation(_ location: CLLocation) {
		
		let dist = location.distance(from: AppDelegate.shared.userLocation)
        
        if AppDelegate.shared.userLocation != location {
            AppDelegate.shared.userLocation = location
            NotificationCenter.default.post(name: Notification.Name.LocationManager.didChangeLocation, object: location)
        }
        
        
        AppDelegate.me.location = location
		
		if Date().timeIntervalSince1970 > _lastUpdate + _minUpdateInterval || dist > 100{
			AppDelegate.me.synchronizeCoordinates()
			_lastUpdate = Date().timeIntervalSince1970
		}
		
		
	}
	
	
	// MARK - Background
	func startUpdatingLocationInBackground(with radius: Int) {
		_locationManager.stopUpdatingLocation()
		
		print(String(describing: type(of: self)),":", #function)
		
		BackgroundDebug().write(string: "UIApplicationLaunchOptionsLocationKey")
		
		backgroundLocationManager.startBackground() { [weak self] result in
			guard let `self` = self else { return }
			if case let .Success(location) = result {
				self.updateUserLocation(location)
			}
		}
	}
	
	func stopUpdatingLocationInBacground() {
		backgroundLocationManager.stop()
		
		if AppDelegate.me != nil {
			_locationManager.startUpdatingLocation()
		}
		
	}
}

// MARK: - ObserverProtocol
extension LocationManager: ObserverProtocol {
	
	func registerObserver() {
		let center = NotificationCenter.default
		let queue = OperationQueue.main
		observers.append(
			center.addObserver(forName: NSNotification.Name.UIApplicationDidEnterBackground, object: nil, queue: queue) {
				[weak self] _ in
				guard let `self` = self else { return }
				self.startUpdatingLocationInBackground(with: 100)
			}
		)
		
		observers.append(
			center.addObserver(forName: NSNotification.Name.UIApplicationWillResignActive, object: nil, queue: queue) {
				[weak self] _ in
				guard let `self` = self else { return }
				self.stopUpdatingLocationInBacground()
			}
		)
		
		observers.append(
			center.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: queue) {
				[weak self] _ in
				guard let `self` = self else { return }
				self.stopUpdatingLocationInBacground()
			}
		)
		
		
	}
	
	func unregisterObserver() {
		observers.forEach {
			NotificationCenter.default.removeObserver($0)
		}
		observers.removeAll()
	}
}

// MARK: - CLLocation Manager Delegate
extension LocationManager: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		
		switch status {
		case CLAuthorizationStatus.authorizedAlways, CLAuthorizationStatus.authorizedWhenInUse:
			manager.startUpdatingLocation()
			
		default:
			manager.stopUpdatingLocation()
		}
		
		NotificationCenter.default.post(name: Notification.Name.LocationManager.didChangeAuthorization, object: status)
	}
	
	
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if let location = locations.first {
			self.updateUserLocation(location)
		}
	}
}
