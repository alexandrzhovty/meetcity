//
//  TextEditProfileCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

protocol TextEditProfileCellProtocol: class {
	typealias Action = (_ textView: UITextView) -> Void
	var message: String? { get set }
	var textLimit: Int { get set }
	var action: Action? { get set }
}

class TextEditProfileCell: UITableViewCell, TextEditProfileCellProtocol {

	@IBOutlet weak var textView: UITextView!
	@IBOutlet weak var countLabel: UILabel!
	
	var textLimit: Int = 0 { didSet { configureView() } }
	var action: TextEditProfileCellProtocol.Action?
	var message: String? {
		get { return textView.text }
		set { textView.text = newValue }
	}
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
		textView.delegate = self
	}
	
	func configureView() {
		
		countLabel.isHidden = textLimit <= 0
		
		let textLenght = textView.text.characters.count
		
		let left = textLimit - textLenght
		if left >= 0 {
			countLabel.text = "\(left)"
			countLabel.textColor = UIColor.black
		} else {
			countLabel.textColor = UIColor.red
			countLabel.text = "\(0)"
		}
	}
	
}


extension TextEditProfileCell: NibLoadableView {}


//    MARK: - Text view protocol
extension TextEditProfileCell: UITextViewDelegate {
	func textViewDidChange(_ textView: UITextView) {
		configureView()
		action?(textView)
	}
}
