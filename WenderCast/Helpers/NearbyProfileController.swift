//
//  NearbyProfileController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
import Firebase
import CoreData
import SVProgressHUD
import AlamofireImage
import Alamofire

import InstagramSimpleOAuth

//    MARK: - Properties & variables
final class NearbyProfileController: UIViewController {
    //    MARK: Public
	var managedObjectContext: NSManagedObjectContext = AppDelegate.shared.mainContext
    var user: DMUser!
    

    @available(*, deprecated, message: "Use property `user` &  `managedObjectContext` instead")
    var userModel: UserModel!
    
	var observers = [Any]()
	var refreshProfileData: Bool = false

    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var sendInviteButton: UIButton!
	@IBOutlet weak var reportBlockButton: UIButton!
    
    //    MARK: Private
	fileprivate let _distanceFormatter = MKDistanceFormatter()
	
	fileprivate var _isLikeStatusALreadyFetched: Bool = false
	
	
	fileprivate enum IndexPathTo {
		static let infoCell = IndexPath(row: 0, section: 0)
		static let instagramCell  = IndexPath(row: 2, section: 0)
	}
    
    fileprivate var _instagramDataProvider: InstagramDataProvider!
	
	
	//    MARK: Initializations
	deinit {
		unregisterObserver()
	}
}

//    MARK: - View life cycle
extension NearbyProfileController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
        assert(user != nil, "Useer cannot be nil")
        
		navigationItem.title = user.title
		
        // Customize appearance
        Appearance.customize(viewController: self)
		
		// Configure tableView
		configure(reportBlockButton)
		configure(sendInviteButton)
		configure(tableView)
		
        _instagramDataProvider = InstagramDataProvider(with: user)
        
        
        // Check if user already liked
        let recipient_sender = "\(user.uid!)_\(AppDelegate.me.uid!)"
        
        let likeRef = FirebaseStack.likes.queryOrdered(byChild: DMLike.CodingKeys.recipient_sender).queryEqual(toValue: recipient_sender)
        likeRef.observeSingleEvent(of: .value, with: { [weak self] snapshot in
            guard let `self` = self else { return }
            let uid = self.user.uid!
            let nestedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            nestedContext.parent = AppDelegate.shared.mainContext
            nestedContext.perform { [weak self] in
                
                defer {
                    // Save
                    if nestedContext.hasChanges {
                        do { try nestedContext.save() }
                        catch {
                            assertionFailure(error.localizedDescription)
                        }
                    }
                    
                    // Update interface
                    self?._isLikeStatusALreadyFetched = true
                    DispatchQueue.main.async {
                        self?.reloadInfoCell()
                    }
                }
                
                // Updare database
                if snapshot.exists() {
                    // Parse
                    DMLike.parse(snapshot, into: nestedContext)
                    
                } else {
                    // Remove if it's neccessary
                    
                    let request: NSFetchRequest<DMLike> = DMLike.fetchRequest()
                    let predicates = [
                        NSPredicate(format: "%K.%K == %@", #keyPath(DMLike.recipientValue), #keyPath(DMUser.uid), uid),
                        NSPredicate(format: "%K.%K == %@", #keyPath(DMLike.senderValue), #keyPath(DMUser.uid), AppDelegate.me.uid!)
                    ]
                    request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
                    let results = try! nestedContext.fetch(request)
                    results.forEach {
                        nestedContext.delete($0)
                    }
                }
            }
        })
        
        
        
		registerObserver()
        
        
		// Update visit info
        VisitManamger.visit(user)
		
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.registerObserver()
        
        if _instagramDataProvider.numberOfItems == 0 {
            loadInstagramData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterObserver()
    }
}


//    MARK: - Observers
extension NearbyProfileController: ObserverProtocol {
	
    func unregisterObserver() {
        observers.forEach {
            if let dbRef = $0 as? DatabaseReference {
                dbRef.removeAllObservers()
            }
        }
    }
    
	func registerObserver() {
        
		
	}
	
	
	
	
}

//    MARK: - Utilities
extension NearbyProfileController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case sendInviteButton:
			sendInviteButton.setTitle(Strings.Invitation.SendInviteForDate, for: .normal)
		
		case reportBlockButton:
			
			
			let title = String.localizedStringWithFormat(Strings.ReportBlock, user.name ?? "")
			reportBlockButton.setTitle(title, for: .normal)
			
		case tableView:
			tableView.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.tableHeaderView?.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.estimatedRowHeight = 120
			tableView.rowHeight = UITableViewAutomaticDimension
			
			tableView.register(InfoNearbyCell.self)
			tableView.register(MapNearbyCell.self)
			tableView.register(InstagramPhotosCell.self)
			tableView.register(InstagramAddAccountCell.self)
			
        default: break
        }
		
    }
    
    fileprivate func reloadInfoCell() {
        let indexPath = IndexPath(row: 0, section: 0)
        guard (tableView.indexPathsForVisibleRows ?? []).contains(indexPath) else {
            return
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    fileprivate func loadInstagramData() {
        // Instagram
        if user.instagramUserId != nil {
            _instagramDataProvider.fetch() { [weak self] result in
                guard let `self` = self else { return }
                
                guard case .success = result else { return }
                //                guard (self.tableView.indexPathsForVisibleRows ?? []).contains(IndexPathTo.instagramCell) else { return }
//                let offset = self.tableView.contentOffset
//                self.tableView.reloadData()
//                self.tableView.contentOffset = offset
//                self.tableView.reloadData()
//                self.tableView.contentOffset = offset
//                                self.tableView.beginUpdates()
//                                self.tableView.reloadRows(at: [IndexPathTo.instagramCell], with: .automatic)
//                                self.tableView.endUpdates()
                
                
                // Update row height for dynamic TextView
                let currentOffset = self.tableView.contentOffset
                UIView.setAnimationsEnabled(false)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                UIView.setAnimationsEnabled(true)
                self.tableView.setContentOffset(currentOffset, animated: false)
                
            }
            
        }
    }
}

//    MARK: - Outlet functions
extension NearbyProfileController  {
    //    MARK: Buttons
	@IBAction func didTapAddInstagramAccount(_ button: UIButton) {
        let callBackURL = URL(string: InstagramManager.redirectUri)
        let vc = InstagramSimpleOAuthViewController(clientID:  InstagramManager.cleintID, clientSecret: InstagramManager.clientSecret, callbackURL: callBackURL) {
            [weak self] (response, error) in
            
            guard let response = response else {
                let errorMsg = error?.localizedDescription ?? Strings.Instagram.CannotGetUserInfo
                Alert.default.showError(message: errorMsg)
                return
            }
            
            AppDelegate.me.instagramUserId = response.user.userID
            AppDelegate.me.instagramUserName = response.user.username
            InstagramManager.accessToken = response.accessToken
            
            
            guard let `self` = self else { return }
            
            self.dismiss(animated: true) {
                self.loadInstagramData()
            }
            
        }
        
        vc?.shouldShowErrorAlert = false
        vc?.permissionScope = ["basic", "comments", "relationships", "likes"]
        
        vc?.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismiss(_:)))
        
        let navVC = UINavigationController(rootViewController: vc!)
        present(navVC, animated: true, completion: nil)
        
        
	}
    
    @objc func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
	
	@IBAction func didTapSendInvite(_ button: UIButton) {
        
        if user.readyToMeet == false {
            let format = Strings.Invitation.UserIsNotReadyToMeet
            let msg = String.localizedStringWithFormat(format, user.name ?? Strings.user.localizedCapitalized)
            Alert.default.showOk(Strings.Invitation.name, message: msg)
            return
        }
		
		// Send notification if user is free to meet
		SVProgressHUD.show()
		let uid = user.uid!
		
		DispatchQueue.global(qos: .background).async { [weak self] in
			FirebaseStack.currentInvitation(for: uid) { json in
                print(String(describing: type(of: self)),":", #function)
                print(json ?? "Empty")
                
				DispatchQueue.main.async {
					
					SVProgressHUD.dismiss {
						if json != nil {
							// User is on dating right now or going to
							let user = DMUser.user(for: UserIdentifier(rawValue: uid), from: AppDelegate.shared.mainContext)
							let format = Strings.Invitation.UserUnaccesibleNow
							let msg = String.localizedStringWithFormat(format, user?.name ?? Strings.user.localizedCapitalized)
							
							Alert.default.showOk(Strings.Invitation.name, message: msg)
						} else {
							// User free for dating right now
							self?.executeSegue(.sendInvite, sender: nil)
						}
					}
				}
				
				
			}
		}
		
	}
	
	@IBAction func didTapReportBlock(_ button: UIButton) {
        
        // FIXME: The code below is very  ugly and must be refactored as soon as possible

        let alertVC = UIAlertController(title: user.title, message: nil, preferredStyle: .actionSheet)
        alertVC.addAction(
            UIAlertAction(title: Strings.Profile.blockUser, style: .destructive, handler: { [unowned self] (_) in
                
                SVProgressHUD.show()
                
                
                BanListManager.ban(self.user, for: .block, completion: { [weak self] result in
                    guard let `self` = self else { return }
                    
                    DispatchQueue.main.async {
                        switch result {
                        case .failure(let error):
                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                        case .success:
                            
                            SVProgressHUD.showSuccess(withStatus: "Blocked")
                            
                            self.user.banned = Int16(BanType.complain.rawValue)
                            try? self.managedObjectContext.save()
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                                SVProgressHUD.dismiss(completion: { 
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                            
                        }
                        
                    }
                    
                    
                })
            })
        )
        alertVC.addAction(
            UIAlertAction(title: Strings.Profile.reportUser, style: .destructive, handler: { [unowned self] (_) in
                SVProgressHUD.show()
                BanListManager.ban(self.user, for: .complain, completion: { [weak self] result in
                    guard let `self` = self else { return }
                    
                    DispatchQueue.main.async {
                        switch result {
                        case .failure(let error):
                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                        case .success:
                            
                            SVProgressHUD.showSuccess(withStatus: "Reported")
                            
                            self.user.banned = Int16(BanType.complain.rawValue)
                            try? self.managedObjectContext.save()
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                })
            })
        )
        alertVC.addAction(
            UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil)
        )
        
        
        if let popoverController = alertVC.popoverPresentationController {
            popoverController.sourceView = button
        }
        
        self.present(alertVC, animated: true, completion: nil)
	}
    
    
	
	@objc func didTapLike(_ button: UIButton) {
        
        // Animate Like button
        let indexPath = IndexPath(row: 0, section: 0)
        if let cell = self.tableView.cellForRow(at: indexPath) as? InfoNearbyCell {
            cell.favoritesButton.isEnabled = false
            cell.favoritesButton.scaleAndBack()
        }
        
        let nestedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        nestedContext.parent = AppDelegate.shared.mainContext
        let likedUser = nestedContext.object(with: user.objectID) as! DMUser
        nestedContext.perform {
            let like = DMLike(context: nestedContext)
            like.recipient = likedUser
            like.sender = nestedContext.object(with: AppDelegate.me.objectID) as! DMUser
            
            like.synchronize(completion: { [weak self] result in
                guard let `self` = self else { return }
                
                switch result {
                case let .failure(error):
                    Alert.default.showError(message: error.localizedDescription)
                case .success:
                    
                    // Notify user
                    MessageManager.default.send(.like(to: self.user)) {
                        print(String(describing: type(of: self)),":", #function)
                    }
                    
                    // Firebase callback always in main thread
                    nestedContext.performAndWait {
                        do { try nestedContext.save() }
                        catch {
                            Alert.default.showError(message: error.localizedDescription)
                        }
                    }
                }
                
            })
            
        }

	}
}

// MARK: - Navigation & SegueHandler protocol
extension NearbyProfileController: SegueHandler {
    enum SegueType: String {
        case embedGallery
		case sendInvite
    }
    
	@IBAction func exitToNearbyProfileController(_ segue: UIStoryboardSegue) {}
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		switch segueType(for: identifier) {
		case .embedGallery: break
		case .sendInvite: break
        }
		return true
	}
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
		case .embedGallery:
			
			let vc = segue.destination as! PhotoListContoroller
			vc.userModel = UserModel(with: user)
            
		case .sendInvite:
			let vc = (segue.destination as! UINavigationController).topViewController as! InvitationController
            
            let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            context.parent = AppDelegate.shared.mainContext
            
            vc.managedObjectContext = context
            vc.invite = DMInvite.new(for: user, in: context)
            
			
		}
    }
}

// MARK: - Table view protocol
extension NearbyProfileController: UITableViewDataSource, UITableViewDelegate {
	//	MARK: Datasource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (InstagramManager.isAuthorized, user.instagramUserId != nil) {
        case (true, true), (false, true):
            return 2  // FIXME: Instagram
        default: return 2
        }
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(InfoNearbyCell.self, for: indexPath)
			cell.titleLabel.text = user.title
			
            
            if let distanse = user.distanceToMe {
                cell.distanceLabel.text = Formatters.distance.default.string(fromDistance: distanse)
            } else {
                cell.distanceLabel.text = nil

            }
			
			cell.statusView.status = user.readyToMeet
			cell.ocuppationLabel.text = user.occupation
			cell.aboutMeLabel.text = user.aboutMe
            
            let fetchRequest: NSFetchRequest<DMLike> = DMLike.fetchRequest()
            let predicates = [
                NSPredicate(format: "%K == %@", #keyPath(DMLike.recipientValue), user),
                NSPredicate(format: "%K == %@", #keyPath(DMLike.senderValue), AppDelegate.me)
            ]
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            let qty = try! managedObjectContext.count(for: fetchRequest)
            
            
            switch (qty > 0, _isLikeStatusALreadyFetched) {
            case (false, false):
                cell.favoritesButton.isEnabled = false
                cell.favoritesButton.isHidden = true
                
                cell.favoritesButton.removeTarget(self, action: #selector(didTapLike(_:)), for: .touchUpInside)
                
            case (false, true):
                cell.favoritesButton.isEnabled = true
                cell.favoritesButton.isHidden = false
                
                cell.favoritesButton.addTarget(self, action: #selector(didTapLike(_:)), for: .touchUpInside)
            
            case (true, true), (true, false):
                cell.favoritesButton.isEnabled = false
                cell.favoritesButton.isHidden = false
                
                cell.favoritesButton.removeTarget(self, action: #selector(didTapLike(_:)), for: .touchUpInside)
            }
			
			return cell
			
		case 1:
			let cell = tableView.dequeueReusableCell(MapNearbyCell.self, for: indexPath)
			cell.user =  user
			return cell
		
        case IndexPathTo.instagramCell.row where InstagramManager.isAuthorized == false:
            let cell = tableView.dequeueReusableCell(InstagramAddAccountCell.self, for: indexPath)
            cell.button.addTarget(self, action: #selector(didTapAddInstagramAccount(_:)), for: .touchUpInside)
            return cell
            
		case IndexPathTo.instagramCell.row where InstagramManager.isAuthorized == true:
			let cell = tableView.dequeueReusableCell(InstagramPhotosCell.self, for: indexPath)
			cell.collectionView.dataSource = self
			cell.collectionView.delegate = self
//            cell.collectionView.contentOffset = _instagramDataProvider.contentOffset
			return cell
			
			
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
			return cell
		}
	}
	
	// MARK: Dispalying
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath {
        case IndexPathTo.instagramCell where InstagramManager.isAuthorized == true:
            guard let cell = cell as? InstagramPhotosCell else { return }
            cell.collectionView.dataSource = self
            cell.collectionView.delegate = self
            cell.status = _instagramDataProvider.loadingStatus
            if cell.collectionView.numberOfItems(inSection: 0) != _instagramDataProvider.numberOfItems {
                cell.collectionView.reloadData()
            }
            cell.collectionView.contentOffset = _instagramDataProvider.contentOffset
            
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath {
        case IndexPathTo.instagramCell where InstagramManager.isAuthorized == true:
            guard let cell = cell as? InstagramPhotosCell else { return }
            _instagramDataProvider.contentOffset = cell.collectionView.contentOffset
            
        default: break
        }
    }
	
	// MARK: Scroll view delegagte
	
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		if !decelerate {
			checkPosition(for: scrollView)
		}
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		checkPosition(for: scrollView)
	}
	
	/// Scroll to visible reportButton
	///
	/// - Parameter scrollView: where report button is placed int
	func checkPosition(for scrollView: UIScrollView)  {
		let delta = scrollView.contentSize.height - (scrollView.contentOffset.y + scrollView.bounds.height)
		if delta < reportBlockButton.bounds.height && delta > 0 {
			scrollView.scrollRectToVisible(reportBlockButton.superview!.frame, animated: true)
		}
	}
	
}

// MARK: - Collection view protocol
extension NearbyProfileController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _instagramDataProvider.numberOfItems
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(InstagramCollectionCell.self, for: indexPath) as!  InstagramCollectionCell
		return cell
	}
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? InstagramCollectionCell else { return }
        let record = _instagramDataProvider.object(at: indexPath)
        let url = URL(string: record["images"]["thumbnail"]["url"].stringValue)
        
        Alamofire.request(url!).responseImage { (result) in
            cell.imageView.image = result.value
        }
        
//        cell.imageView.af_setImage(withURL: url!)
        
    }
	
	//    MARK: - Flowlayout delegate
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let size = collectionView.bounds.size
		let width = (size.width - (4.0 * 8.0)) / 3
		return CGSize(width: width, height: width)
	}
    
    // MARK: Selection
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(.gallery).instantiateViewController(PhotoListContoroller.self)
        var dataSource = [URL]()
        for idx in 0..<_instagramDataProvider.numberOfItems {
            let record = _instagramDataProvider.object(at: idx)
            if let url = URL(string: record["images"]["standard_resolution"]["url"].stringValue) {
                dataSource.append(url)
            }
        }
        vc.type = .instagram
        vc.dataSource = dataSource
        
        navigationController?.pushViewController(vc, animated: true)
    }
	
}

