//
//  VenueProvider.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON



class VenueProvider {
	
	static let clientId = "NO5FPYCJZZGGPWKNRYBM1RVUSKBZXPFAQDY14JHD3H4L2FGB"
	static let clientSecret = "RD3VVZFH22KUEB0B2NJ4GMPLIS1OZA4SFEFQQVKBARVHAPDB"
	static let version = "20140613"
	
	static let radiusInMeters = 10000
	static let categoryId = "4d4b7105d754a06374d81259" // - Food  //  "4bf58dd8d48988d1e0931735" - Coffe shop
	
	
	let session: URLSession
	var dataTask: URLSessionDataTask!
	
	init(session: URLSession = URLSession.shared) {
		self.session = session
	}
	
	lazy var components: URLComponents = {
		var components = URLComponents()
		components.scheme = "https"
		components.host = "api.foursquare.com"
		return components
	}()
	
	
	func fetch(_ venue: String? = nil, around coordinate: CLLocationCoordinate2D, completion: @escaping (_ response: [Venue]) -> Void) {
		
		components.path = "/v2/venues/search"
		var queries: [URLQueryItem] = [
			URLQueryItem(name: "ll", value: "\(coordinate.latitude),\(coordinate.longitude)")
		]
		
		if let venue = venue {
			queries.append(URLQueryItem(name: "query", value: venue))
		}
		
		queries += [
			URLQueryItem(name: "categoryId", value: "\(VenueProvider.categoryId)"),
			URLQueryItem(name: "radius", value: "\(VenueProvider.radiusInMeters)"),
			URLQueryItem(name: "client_id", value: "\(VenueProvider.clientId)"),
			URLQueryItem(name: "client_secret", value: "\(VenueProvider.clientSecret)"),
			URLQueryItem(name: "sortByDistance", value: "1"),
			URLQueryItem(name: "limit", value: "10"),
			URLQueryItem(name: "v", value: "\(VenueProvider.version)")
		]
		
		components.queryItems = queries
		
		guard let url = components.url else { fatalError() }
		
		let request = URLRequest(url: url)
		
		dataTask = session.dataTask(with: request, completionHandler: { (data, _, _) in
			guard let data = data else {
				return
			}
			
			completion(self.decode(data))
		})
		dataTask.resume()
		
	}
	
	func fetchImage(for venue: Venue, completion: @escaping (_ url: URL?) -> Void) {
		
//		https://api.foursquare.com/v2/venues/VENUE_ID/photos

		
		//
		components.path = "/v2/venues/\(venue.id)/photos"
		let queries = [
			URLQueryItem(name: "client_id", value: "\(VenueProvider.clientId)"),
			URLQueryItem(name: "client_secret", value: "\(VenueProvider.clientSecret)"),
			URLQueryItem(name: "v", value: "\(VenueProvider.version)")
		]
		components.queryItems = queries
		
		guard let url = components.url else {
			assertionFailure()
			return
		}
		
		let request = URLRequest(url: url)

		dataTask = session.dataTask(with: request, completionHandler: { (data, _, _) in
			guard let data = data else {
				return
			}
			
			let json = JSON(data: data)
			
			if let photo = json["response"]["photos"]["items"].first  {
				let prefix = photo.1["prefix"].stringValue
				let suffix = photo.1["suffix"].stringValue
				
				let sURL = prefix + "300x500" + suffix
				
				var components = URLComponents(string: sURL)
				let queries = [
					URLQueryItem(name: "client_id", value: "\(VenueProvider.clientId)"),
					URLQueryItem(name: "client_secret", value: "\(VenueProvider.clientSecret)"),
					URLQueryItem(name: "v", value: "\(VenueProvider.version)")
				]
				components?.queryItems = queries
				completion(components?.url)
				
			}
			
			
		})
		dataTask.resume()
		
		
	}
	
	fileprivate func decode(_ data: Data) -> [Venue] {
		let json = JSON(data: data)
		
		let places = NSMutableArray()
		for (_, value) in json["response"]["venues"] {
            
    
			
			guard let id = value["id"].string else { continue }
			guard let name = value["name"].string else { continue }
			guard let lat = value["location"]["lat"].double else { continue }
			guard let lng = value["location"]["lng"].double else { continue }
			let rate = value["raing"].int ?? 0
			
			guard let distance = value["location"]["distance"].double else { continue }
			
			let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
			
			let formattedAddress: String
			if let array = value["location"]["formattedAddress"].arrayObject as? [String] {
				formattedAddress = array.joined(separator: ", ")
			} else {
				formattedAddress = ""
			}
			
			places.add(Venue(id: id, name: name, address: formattedAddress, rate: rate, coordinate: coordinate, distance: distance))
			
		}
		
		let sortDescriptors = [
			/* NSSortDescriptor(key: "rate", ascending: false), */
			NSSortDescriptor(key: "distance", ascending: true)
		]
		
		
		guard let array = places.sortedArray(using: sortDescriptors) as? [Venue] else { return [] }
		return array
		
	}
}
