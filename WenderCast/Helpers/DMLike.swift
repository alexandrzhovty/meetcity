//
//  DMLike.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON
import Firebase

extension DMLike {
    enum CodingKeys {
        static let id = "id"
        static let senderID = "senderID"
        static let recipientID = "recipientID"
        static let created = "created"
        static let recipient_sender = "recipientSender"
        
        static let senderName = "senderName"
        static let senderBirthday = "senderBirthday"
        static let senderOccupation = "senderOccupation"
        static let senderPhoto = "senderPhoto"
        
        static let recipientName = "recipientName"
        static let recipientBirthday = "recipientBirthday"
        static let recipientOccupation = "recipientOccupation"
        static let recipientPhoto = "recipientPhoto"
    }
}

// MARK: - Overrids
extension DMLike {
    /// invoked after an insert
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        self.type = EventType.like
    }
}



// MARK: - Parsing
extension DMLike {
    
    /// Parsing server response
    ///
    /// - Parameters:
    ///   - response: should be `[key: value]` or `[[key: value]]`
    ///   - context: where the parsed results should be stored in
    ///   - user: parsed messages should belong to
    class func parse(_ response: ParsableResponse, into context: NSManagedObjectContext) {
        
//        let user = context.object(with: user.objectID) as! DMUser
        
        
        let responsToParse: [[String: Any]]
        
        switch response {
        case let snapshots as DataSnapshot:
            guard let value = snapshots.value as? [String: [String: Any]] else { return }
            responsToParse = value.map{ $1 }
            
        case let dictionary as Dictionary<String, Any>:
            responsToParse = [dictionary]
            
        default:
            assertionFailure("Incorrect data format")
            return
        }
        
//        print(responsToParse)
        
//        if user.like == nil {
//            user.like = DMLike(context: context)
//        }
        
        // User list
        var users = [String: DMUser]()
        if responsToParse.count > 0 {
            let recipientIDs = responsToParse.map{ $0[DMLike.CodingKeys.recipientID] as? String }.flatMap{ $0 }
            let senderIDs = responsToParse.map{ $0[DMLike.CodingKeys.senderID] as? String}.flatMap{ $0 }
            let unique = Array(Set(recipientIDs + senderIDs))
            
            let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
            request.predicate = NSPredicate(format: "%K in %@", #keyPath(DMUser.uid), unique)
            
            let results = try! context.fetch(request)
            results.forEach {
                users[$0.uid!] = $0
            }
        }
        
        // Like list
        var likes = [String: DMLike]()
        if responsToParse.count > 0 {
            let likeIds = responsToParse.map{ $0[DMLike.CodingKeys.id] as? String }.flatMap{ $0 }
            let request: NSFetchRequest<DMLike> = DMLike.fetchRequest()
            request.predicate = NSPredicate(format: "%K in %@", #keyPath(DMLike.id), likeIds)
            let results = try! context.fetch(request)
            results.forEach{
                likes[$0.id!] = $0
            }
            
        }
        
        
        // Parse likes
        for record in responsToParse {
            let json = JSON(record)
            guard let id = json[CodingKeys.id].string else { continue }
            guard let recepientId = json[CodingKeys.recipientID].string else { continue }
            guard let senderId = json[CodingKeys.senderID].string else { continue }
            
            let like = likes[id] ?? {
               let entity = DMLike(context: context)
                entity.id = id
                return entity
            }()
            
            
            like.createdValue = json[CodingKeys.created].doubleValue
            
            
            // Sender
            like.sender = users[senderId] ?? {
                let user = DMUser.new(with: senderId, in: context)
                users[senderId] = user
                return user
                }()
            
            like.sender.name = json[CodingKeys.senderName].stringValue
            like.sender.birthday = json[CodingKeys.senderBirthday].doubleValue
            like.sender.profilePhotoUrl = json[CodingKeys.senderPhoto].stringValue
            like.sender.occupation = json[CodingKeys.senderOccupation].string
            
            // Recipient
            like.recipient = users[recepientId] ?? {
                let user = DMUser.new(with: recepientId, in: context)
                users[recepientId] = user
                return user
                }()
            
            like.recipient.name = json[CodingKeys.recipientName].stringValue
            like.recipient.birthday = json[CodingKeys.recipientBirthday].doubleValue
            like.recipient.profilePhotoUrl = json[CodingKeys.recipientPhoto].stringValue
            like.recipient.occupation = json[CodingKeys.recipientOccupation].string
            
        }
        
    }
}


// MARK: - SynchronizationProtocol
extension DMLike: SynchronizationProtocol {
    @available(*, deprecated, message: "Use static property `synchronize(comletion: @escaping (Error?) -> Swift.Void)` instead")
    func synchronize() {}
    
    func synchronize(completion: @escaping (Result<DatabaseReference>) -> Swift.Void) {
        
        
        
        var json = JSON([:])
        json[CodingKeys.id].string = self.identifier.rawValue
        
        json[CodingKeys.created].string = "\(Date().timeIntervalSince1970)"
        json[CodingKeys.recipient_sender].string = self.recipientSenderIndex
        
        json[CodingKeys.senderID].string = self.sender.identifier.rawValue
        json[CodingKeys.senderName].string = self.sender.name
        json[CodingKeys.senderOccupation].string = self.sender.occupation
        json[CodingKeys.senderBirthday].double = self.sender.birthday
        
        
        json[CodingKeys.recipientID].string = self.recipient.identifier.rawValue
        json[CodingKeys.recipientName].string = self.recipient.name
        json[CodingKeys.recipientOccupation].string = self.recipient.occupation
        json[CodingKeys.recipientBirthday].double = self.recipient.birthday
        
        let photoRef = self.sender.profilePhotoeRef
        photoRef?.downloadURL(completion: { (url, error) in
            
            json[CodingKeys.senderPhoto].string = url?.absoluteString
            
            
            let recipienPhotoRef = self.recipient.profilePhotoeRef
            recipienPhotoRef?.downloadURL(completion: { (url, _) in
                json[CodingKeys.recipientPhoto].string = url?.absoluteString
                
                let likeRef = FirebaseStack.like(for: self.identifier.rawValue)
                
                likeRef.setValue(json.dictionaryObject) { (error, dbRef) in
                    
                    if let error = error {
                        completion(Result.failure(error))
                    } else {
                        completion(.success(dbRef))
                    }
                }
            })
        })
        
    }
}
