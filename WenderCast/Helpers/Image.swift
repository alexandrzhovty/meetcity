//
//  Image.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

//    MARK: - Asset image types
protocol ImageAssetType: Testable  {}


extension UIImage {
    enum logo: String, ImageAssetType {
        case middle = "logo-middle"
    }
    
    enum icon: String, ImageAssetType {
        case profile = "Icon-Profile"
        case filter = "Icon-Filter"
        case marker = "icon-mapmarker"
        case blank = "Icon-Blank"
        case dot = "Icon-Dot"
        case dotSelected = "Icon-Dot-Selected"
		case star = "Icon-Star"
		case starEmpty = "Icon-Star-Empty"
        
        
		static let locateMe = UIImage(named: "Icon-LocateMe")!
        static let shevron = UIImage(named: "Icon-Expand")!
    }
	
	enum text {
		static let time = UIImage(named: "Text-Time")!
		static let place = UIImage(named: "Text-Place")!
		static let notes = UIImage(named: "Text-Notes")!
	}
    
    enum backgorund: String, ImageAssetType {
        case navigationBar = "Backgorund-NavigationBar"
        case buttonFacebook = "Bkg-Btn-FB"
        case splash = "spash"
        
        
        static let badge = #imageLiteral(resourceName: "Bkg-Badge")
    }
    
    enum photo: String, ImageAssetType {
        case tourIphone = "tourIphone"
        case tourIphone2 = "tourIphone2"
        case tourIphone3 = "tourIphone3"
        case tourIphone4 = "tourIphone4"
    }
}

typealias Background = UIImage.backgorund
typealias Logo = UIImage.logo
typealias Icon = UIImage.icon
typealias Photo = UIImage.photo

//    MARK: - Initialization
extension UIImage {
    convenience init<T>(_ imageAssetType: T) where T: ImageAssetType, T: RawRepresentable, T.RawValue == String  {
        self.init(named: imageAssetType.rawValue)!
    }
}

//extension UIImage {
//	func fixOrientation() -> UIImage {
//		
//	
//	
//	// No-op if the orientation is already correct
//		if (self.imageOrientation == UIImageOrientationUp) { return self }
//	
//	// We need to calculate the proper transformation to make the image upright.
//	// We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
//	let transform = CGAffineTransform.identity
//	
//	switch self.imageOrientation {
//	case UIImageOrientationDown, UIImageOrientationDownMirrored:
//	transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
//	transform = CGAffineTransformRotate(transform, M_PI);
//	
//	case UIImageOrientationLeft, UIImageOrientationLeftMirrored:
//	transform = CGAffineTransformTranslate(transform, self.size.width, 0);
//	transform = CGAffineTransformRotate(transform, M_PI_2);
//	break;
//	
//	case UIImageOrientationRight, UIImageOrientationRightMirrored:
//	transform = CGAffineTransformTranslate(transform, 0, self.size.height);
//	transform = CGAffineTransformRotate(transform, -M_PI_2);
//	break;
//	case UIImageOrientationUp, UIImageOrientationUpMirrored:
//	break;
//	}
//	
//	switch (self.imageOrientation) {
//	case UIImageOrientationUpMirrored, UIImageOrientationDownMirrored:
//	transform = CGAffineTransformTranslate(transform, self.size.width, 0);
//	transform = CGAffineTransformScale(transform, -1, 1);
//	break;
//	
//	case UIImageOrientationLeftMirrored, UIImageOrientationRightMirrored:
//	transform = CGAffineTransformTranslate(transform, self.size.height, 0);
//	transform = CGAffineTransformScale(transform, -1, 1);
//	break;
//	case UIImageOrientationUp:
//	case UIImageOrientationDown:
//	case UIImageOrientationLeft:
//	case UIImageOrientationRight:
//	break;
//	}
//	
//	// Now we draw the underlying CGImage into a new context, applying the transform
//	// calculated above.
//	CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
//	CGImageGetBitsPerComponent(self.CGImage), 0,
//	CGImageGetColorSpace(self.CGImage),
//	CGImageGetBitmapInfo(self.CGImage));
//	CGContextConcatCTM(ctx, transform);
//	switch (self.imageOrientation) {
//	case UIImageOrientationLeft:
//	case UIImageOrientationLeftMirrored:
//	case UIImageOrientationRight:
//	case UIImageOrientationRightMirrored:
//	// Grr...
//	CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
//	break;
//	
//	default:
//	CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
//	break;
//	}
//	
//	// And now we just create a new UIImage from the drawing context
//	CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
//	UIImage *img = [UIImage imageWithCGImage:cgimg];
//	CGContextRelease(ctx);
//	CGImageRelease(cgimg);
//	return img;
//	}
//}
