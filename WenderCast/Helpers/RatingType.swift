//
//  RatingType.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation


enum RatingType: Int {
   case none = -1
   case experience
   case personality
   case appearance
   
   var description: String? {
      switch self {
      case .none: return nil
      case .experience: return Strings.RatingType.experience
      case .personality: return Strings.RatingType.personality
      case .appearance: return Strings.RatingType.appearance
      }
   }
}

enum RatingStatus: Int16 {
   case none = 0
   case bad
   case poor
   case indifferently
   case good
   case excellent
   
   var description: String? {
      switch self {
      case .none: return  nil
      case .bad: return Strings.RatingStatus.bad
      case .poor: return Strings.RatingStatus.poor
      case .indifferently: return Strings.RatingStatus.indifferently
      case .good: return Strings.RatingStatus.good
      case .excellent: return Strings.RatingStatus.excellent
      }
   }
   
   static var highestValue: RatingStatus = .excellent
}
