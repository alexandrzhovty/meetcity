//
//  HeaderView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class HeaderView: UIView {
	
	static let gradientHeight: CGFloat = 10
	
	var gradientView: GradientView!
	
	
	init() {
		super.init(frame: CGRect.zero)
		commonInit()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}
	
	private func commonInit() {
		gradientView = {
			let view = GradientView(frame: CGRect.zero)
			var frame = self.bounds
			frame.size.height = 10
			view.frame = frame
			return view
		}()
		
		backgroundColor = UIColor.clear
		
		self.addSubview(gradientView)
		
		gradientView.translatesAutoresizingMaskIntoConstraints = false
		gradientView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
		gradientView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
		gradientView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
		gradientView.heightAnchor.constraint(equalToConstant: HeaderView.gradientHeight).isActive = true
		
	}

}
