//
//  Identifier.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/17/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

struct StringIdentifier: RawRepresentable  {
    let rawValue: String
    init(rawValue: String) { self.rawValue = rawValue }
   
}

// MARK: - Hashable
extension StringIdentifier: Hashable {
    var hashValue: Int {
        return self.rawValue.hash
    }
}


// MARK: - Equatable
extension StringIdentifier: Equatable {
    static func ==(lhs: StringIdentifier, rhs: StringIdentifier) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}

