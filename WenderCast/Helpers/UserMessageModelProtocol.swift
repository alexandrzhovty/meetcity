//
//  UserMessageModelProtocol.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/3/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase

protocol UserMessageModelProtocol {
	var deviceTokens: [String] { get set }
	var fromUid: String { get }
	var fromImageUrl: URL? { get }
	mutating func update(_ token: String?)
}
