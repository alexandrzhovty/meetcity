//
//  ImageTests.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import XCTest
@testable import MeetCity

class ImageTests: XCTestCase {
    
    func testAssetImages() {
        checkImageAsset(UIImage.logo.self)
        checkImageAsset(UIImage.icon.self)
        checkImageAsset(UIImage.backgorund.self)
        XCTAssert(true, "Passed")
    }
    
    func checkImageAsset<T: RawRepresentable>(_: T.Type) where T: Iteratable, T: ImageAssetType, T: Hashable, T.RawValue == String {
        for asset in T.hashValues() {
            let imageName = asset.rawValue as! String
            print(imageName)
            let image = UIImage(named: imageName)
            XCTAssertNotNil(image, "\(imageName) not found")
        }
    }
}
