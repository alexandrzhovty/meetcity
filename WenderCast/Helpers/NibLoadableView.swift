//
//  NibLoadableView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
protocol NibLoadableView: class {
	/// The UINib that contains the view
	///
	/// Defaults to the swift class name if not implemented
	static var associatedNib : UINib { get }
}

extension NibLoadableView where Self: UIView {
	/// Creates a new instance from the associated Xib
	///
	/// - Returns: A new instance of this object loaded from xib
	static func instantiateFromInterfaceBuilder() -> Self {
		return associatedNib.instantiate(withOwner:nil, options: nil)[0] as! Self
	}
	
	static var associatedNib : UINib {
		let name = String(describing: self)
		return UINib(nibName: name, bundle: Bundle.main)
	}
	
	
}
