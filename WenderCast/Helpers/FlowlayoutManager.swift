//
//  FlowlayoutManager.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/12/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//



import Foundation
import Firebase
import SwiftyJSON
import FBSDKCoreKit
import SVProgressHUD
import AlamofireImage

enum AuthorizationStep {
	case applicationLaunched
	case userUnauthorized
	case userRegisteredAsMeetUser
	case userUnregisteredAsMeetUser
	case facebookInfoRecieved(error: Error?)
	case facebookPhotoIDsLoaded(photos: [String], error: Error?)
	case userHasLoadedPhotos
	case userHasNoLoadedPhotos
	
}

protocol FlowlayoutManagerViewController: class {
	func continueRegistration()
	func showDashboarad()
}

class FlowlayoutManager {
	
	var currentStep: AuthorizationStep = .applicationLaunched
	var currentController: FlowlayoutManagerViewController?
	
	fileprivate let _downloadGroup = DispatchGroup()
	
    private init() {}
	
	init(viewController: FlowlayoutManagerViewController, currentStep: AuthorizationStep = .userUnauthorized) {
		self.currentController  = viewController
		self.currentStep = currentStep
	}
	
	
	func performNextStep(executed: AuthorizationStep) {
		
//		guard let currentUser = AppDelegate.me else {
//			fatalError()
//		}
		
		
		switch executed {
		case .applicationLaunched:
			
			DispatchQueue.main.async {
				SVProgressHUD.showProgress(0.1, status: "checkUserAuthorization")
			}
			
			checkUserAuthorization()
		
		case .userUnauthorized:
			
			DispatchQueue.main.async {
				SVProgressHUD.showProgress(0.2, status: "userUnauthorized")
			}
			
			Presenter.presentLoginScreen()
			
			
		case .userUnregisteredAsMeetUser:
			
			DispatchQueue.main.async {
				SVProgressHUD.showProgress(0.3, status: "userUnregisteredAsMeetUser")
			}
			
			let userModel = UserModel(with: AppDelegate.me)
			if userModel.isFacebookInfoLoaded {
				self.loadInfoFromFacebook()
			} else {
				self.performNextStep(executed: .facebookInfoRecieved(error: nil))
			}
			
			
		case .userRegisteredAsMeetUser:
			DispatchQueue.main.async {
				SVProgressHUD.showProgress(0.4, status: "userRegisteredAsMeetUser")
			}

			self.checkUserPhotos()
		
		case .userHasNoLoadedPhotos:
			
			DispatchQueue.main.async {
				SVProgressHUD.showProgress(0.5, status: "userHasNoLoadedPhotos")
			}
			
			
			self.loadAlbums()
			
			
		
		case let .facebookInfoRecieved(error):
			
			DispatchQueue.main.async {
				SVProgressHUD.showProgress(0.7, status: "facebookInfoRecieved")
			}
			
			if let error = error {
				print(String(describing: type(of: self)),":", #function, "facebookInfoRecieved with ", error)
			} else {
				self.checkUserPhotos()
			}
		
		
		case let .facebookPhotoIDsLoaded(photoIDs, error):
			
			DispatchQueue.main.async {
				SVProgressHUD.showProgress(0.8, status: "facebookPhotoIDsLoaded")
			}
			
			if let error = error {
				print(String(describing: type(of: self)),":", #function, "facebookInfoRecieved with ", error)
			} else {
				self.loadFacebookPhotos(with: photoIDs)
			}
			
		case .userHasLoadedPhotos:
			DispatchQueue.main.async {
//				SVProgressHUD.showProgress(0.6, status: "userHasLoadedPhotos")
				
				SVProgressHUD.dismiss(completion: {
					let userModel = UserModel(with: AppDelegate.me)
					if userModel.isRegistrationInfoCompleted {
						self.currentController?.continueRegistration()
					} else {
						
						Presenter.presentNearByList()				
						
					}
					
				})
				
			}
			
		}
	}
}

extension FlowlayoutManager {
	func showError(error: Error) {
//		Alert.default.showOk("Error", message: .)
	}
	
	func checkUserAuthorization() {

		// It doesn't work with old version
		if UserDefaults.standard.bool(forKey: "SecondLogin") == false {
			self.performNextStep(executed: .userUnauthorized)
			
		}
		else if let currentUser = Auth.auth().currentUser {
			let userRef = FirebaseStack.users.child( currentUser.uid)
			
			userRef.observeSingleEvent(of: .value, with: { snapshot in
//				if snapshot.exists() {
//					print(String(describing: type(of: self)),":", #function)
//					AppDelegate.me = MeetUser(snapshot: snapshot)
//					self.performNextStep(executed: .userRegisteredAsMeetUser)
//					
//				} else {
//					var dict = [String: Any]()
//					dict["name"] = currentUser.displayName?.components(separatedBy: " ").first ?? ""
//					dict["email"] = currentUser.email
//					dict["joined"] = Date().timeIntervalSince1970
//					userRef.setValue(dict)
//					
//					self.performNextStep(executed: .userUnregisteredAsMeetUser)
//					
//				}t
			})
			
		} else {
			self.performNextStep(executed: .userUnauthorized)
			
		}
	}
	
	func checkUserPhotos() {
		
		
		
		if AppDelegate.me.photos.count == 0 {
			self.performNextStep(executed: .userHasNoLoadedPhotos)
		} else {
			self.performNextStep(executed: .userHasLoadedPhotos)
		}
	}
	
	func loadInfoFromFacebook() {
		FacebookFactory.info(user: "me").request({ json in
            
            let name = json["first_name"].string
			if AppDelegate.me.name.isEmptyOrNil || (AppDelegate.me.name != name && name.isEmptyOrNil == false) {
				AppDelegate.me.name = name!
			}
			
			let email = json["email"].string
			if AppDelegate.me.email.isEmptyOrNil == false  || (AppDelegate.me.email != email && email.isEmptyOrNil == false){
				AppDelegate.me.email = email
			}
			
			if AppDelegate.me.birthday == 0, let str = json["birthday"].string {
				let dateFormatter = DateFormatter()
				dateFormatter.dateFormat = "MM/dd/yyyy"
				AppDelegate.me.birthday = dateFormatter.date(from: str)?.timeIntervalSince1970 ?? 0
			}
            
            // Occupation
            if AppDelegate.me.occupation.isEmptyOrNil, let work = json["work"].array?.first {
                let array: [String?] = [
                    work["position"]["name"].string,
                    work["employer"]["name"].string
                ]
                
                AppDelegate.me.occupation = array.flatMap{ $0 }.joined(separator: " ")
            }
			
			self.performNextStep(executed: .facebookInfoRecieved(error: nil))
			
			
		}, failure: { error in
			self.performNextStep(executed: .facebookInfoRecieved(error: error))
		})
	}
	
	func loadAlbums() {
		FacebookFactory.albums(user: "me").request({ json in
			
			var found = false
			for val in json.array ?? [] {
				
				
				if (val["name"].stringValue) == "Profile Pictures" {
					found = true
					FacebookFactory.photos(album: val["id"].stringValue).request({ json in
						let val = json.array ?? []
						
						if val.count > 0 {
							let arraySlice = val[0..<min(val.count, 3)]
							var photoIDs = [String?]()
							arraySlice.forEach{ photoIDs.append($0["source"].string) }
					
							self.performNextStep(executed: .facebookPhotoIDsLoaded(photos: photoIDs.flatMap{ $0 }, error: nil))
							return
							
						}
					}, failure: { error in
						SVProgressHUD.showProgress(0.8, status: error.localizedDescription)
						self.performNextStep(executed: .facebookPhotoIDsLoaded(photos: [], error: error))
					})
				}
			}
			
			if found == false {
				SVProgressHUD.showProgress(0.8, status: "Profile Pictures not found")
				self.performNextStep(executed: .facebookPhotoIDsLoaded(photos: [], error: nil))
			}
			
			
			
			
			
		}, failure: { error in
			SVProgressHUD.showProgress(0.8, status: "Profile Pictures not found")
			self.performNextStep(executed: .facebookPhotoIDsLoaded(photos: [], error: error))
		})
	}
	
	func loadFacebookPhotos(with photoIDs: [String])  {
		guard let currentUser = AppDelegate.me else {
			fatalError()
		}
		
		
		// Create a storage reference from our storage service
		let storageRef = FirebaseStack.storageRef
		
		let imagesRef = storageRef.child(currentUser.uid!)
		
		let session = URLSession(configuration: URLSessionConfiguration.default)
		
		
		
//		var storedError: NSError?
		
//		var blocks: [DispatchWorkItem] = []
		
		var photos = [MeetPhoto]()
		
		let count = photoIDs.count
		
		if count == 0 {
			self.performNextStep(executed: .userHasLoadedPhotos)
			return
		}
		
		for (idx, value) in photoIDs.enumerated() {
			
			photos.append(MeetPhoto(with: idx))
			
			_downloadGroup.enter()
			let url = URL(string: value)!
			session.dataTask(with: url, completionHandler: { (data, response, error) in
				
				
				guard let data = data, let response = response, let url = response.url, error == nil else {
					print("Cannot load foto with url ", error!.localizedDescription)
//					storedError = error as? NSError
					self._downloadGroup.leave()
					return
				}
				
				let filename = url.pathComponents.last ?? UUID().uuidString + ".jpg"
				
				print(url.pathComponents.last ?? "Nothing")
				
				let photoRef = imagesRef.child(filename)
	
				
                if idx == 0 {
                    if let image = UIImage(data: data) {
                        
                        print(AppDelegate.me.identifier.rawValue)
                        
                        let small = image.af_imageScaled(to: Settings.Photos.smallProfileSize)
                        
                        let filename = Settings.Photos.smallProfileName
                        let photoRef = FirebaseStack.storageRef.child(AppDelegate.me.identifier.rawValue).child(filename)
                        
                        guard let data = UIImageJPEGRepresentation(small, 0) else {
                            assertionFailure()
                            return
                        }
                        
                        photoRef.putData(data, metadata: nil) { (_, error) in
                            if error != nil {
                                print(error!.localizedDescription)
                            }
                        }
                    }
                }
                
				photoRef.putData(data, metadata: nil) { (metadata, error) in
					guard let metadata = metadata else {
						print("Cannot upload ", error ?? "No error")
						self._downloadGroup.leave()
						return
					}
					
					// Metadata contains file metadata such as size, content-type, and download URL.
					let downloadURL = metadata.downloadURL()
					
					
					photos[idx].fileName = filename
					
					print("uploaded: ", downloadURL ?? "URL Not found" )
					self._downloadGroup.leave()
				}
				
				
			}).resume()
			
			var count = photos.count
			_downloadGroup.notify(queue: DispatchQueue.main) {
				count = count - 1
				if count == 0 {
//					currentUser.photos.removeAll()
					
					let context = AppDelegate.shared.mainContext
        			for (idx, photo) in photos.enumerated() where photo.fileName.isEmpty == false {
						
						let photo = DMPhoto.new(with: photo.fileName, for: currentUser, in: context)
						photo.sortOrder = Int16(idx)
//						currentUser.photos.append(photo)
					}
			
					currentUser.synchronize {
                        switch $0 {
                        case .failure(let error):
                            Alert.default.showError(message: error.localizedDescription)
                        case .success:
                            DispatchQueue.main.async {
                                self.currentController?.continueRegistration()
                            }
                            
                        }
                    }
				}
			}
			
		}

		
		
	}
}


struct MeetPhoto {
	var idx: Int
	var fileName: String!
	init(with idx: Int) {
		self.idx = idx
	}
}
