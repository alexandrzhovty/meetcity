//
//  InviteModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreLocation
import CoreData
import SwiftyJSON
import Firebase

struct InviteModel {
	fileprivate var _invite: DMInvite
	fileprivate var _managedObjectContext: NSManagedObjectContext
	
	fileprivate(set) var recipient: DMUser
	fileprivate(set) var sender: DMUser
	
	enum InviteModelError: Error {
		case emptyRecipient
		case emptySender
	}
	
	init(with invite: DMInvite, in context: NSManagedObjectContext) throws {
        
		guard let recipient = context.object(with: invite.recipient.objectID) as? DMUser else {
            throw Errors.IviteModelValidation.SenderIsNil
        }
		guard let sender = context.object(with: invite.sender.objectID) as? DMUser else {
            throw Errors.IviteModelValidation.RecipientIsNil
        }
        guard let newInvite = context.object(with: invite.objectID) as? DMInvite else {
            throw Errors.IviteModelValidation.CannotFindEntityInContext
        }
        
		_invite = newInvite
		_managedObjectContext = context
		
		self.recipient = recipient
		self.sender = sender
	}
}

extension InviteModel {
	var recipientModel: UserModel {
		return UserModel(with: recipient)
	}
	
	var senderModel: UserModel {
		return UserModel(with: sender)
	}
}

extension InviteModel {
	var databaseRef: DatabaseReference {
		return FirebaseStack.invitation(for: _invite.identifier)
	}
    
    var created: Date {
        get { return _invite.created }
        set { _invite.created = newValue }
    }
	
    
	var objectID: NSManagedObjectID { return _invite.objectID }
	var context: NSManagedObjectContext { return _managedObjectContext }
	var invite: DMInvite { return _invite }
	var id: String { return _invite.id!  }
	
	var isCanceled: Bool {
		if self.state == .canceledByRecipient || self.state == .canceledBySender {
			return true
		} else {
			return false
		}
	}
	
	
	/// Returns value for the application user
	var isCheckedIn: Bool {
		get {
			if self.sender.uid == AppDelegate.me.uid {
				return _invite.senderCheckined
			} else {
				return _invite.recipientCheckined
			}
		}
		
		set {
			if self.sender.uid == AppDelegate.me.uid {
				_invite.senderCheckined = true
			} else {
				_invite.recipientCheckined = true
			}
		}
	}
	
	/// Returns value of checked in for opponent
	var isOpponentCheckedIn: Bool {
		get {
			if self.sender.uid! != AppDelegate.me.uid {
				return _invite.senderCheckined
			} else {
				return _invite.recipientCheckined
			}
		}
		
		set {
			if self.sender.uid == AppDelegate.me.uid {
				_invite.senderCheckined = true
			} else {
				_invite.recipientCheckined = true
			}
		}
	}

	
	
	
	var opponentModel: UserModel {
		get {
			let user = self.sender.uid == AppDelegate.me.uid ? self.recipient : self.sender
			return UserModel(with: user)
		}
	}
	
	var opponent: DMUser {
		get {
			return self.sender.uid! == AppDelegate.me.uid! ? self.recipient : self.sender
		}
	}
	
	
	
	var title: String {
		get { return _invite.venueName ?? "" }
	}
	
	var notes: String? {
		get { return _invite.notes }
		set { _invite.notes = newValue }
	}
	
	
}

extension InviteModel {
	var coordinate: CLLocationCoordinate2D {
		get {
			return CLLocationCoordinate2D(latitude: _invite.venueLat, longitude: _invite.venueLng)
		}
		set {
			_invite.venueLat = newValue.latitude
			_invite.venueLng = newValue.longitude
		}
	}
	
	var location: CLLocation {
		get {
			return CLLocation(latitude: _invite.venueLat, longitude: _invite.venueLng)
		}
	}
	
	var midPoint: CLLocationCoordinate2D {
//		assert(sender != nil, "Sender cannot be nil")
//		assert(sender?.location != nil, "Sender location cannot be nil")
//		
//		print(String(describing: type(of: self)),":", #function)
//		print(sender!.location!.coordinate)
//		
//		return sender!.location!.coordinate
	
		//FIXME: It is necessary to another logic to calculate the mid point for venue searching
		return AppDelegate.me.location.coordinate
		
	}
	
	
}

enum UserRatingStatus: Int {
	case none = 0
	case bad
	case poor
	case indifferently
	case good
	case excellent
	
	var description: String? {
        return nil
//		switch self {
//		case .none: return nil
//		case .bad: return Strings.RatingStatus.иad
//		case .poor: return Strings.RatingStatus.Poor
//		case .indifferently: return Strings.RatingStatus.Indifferently
//		case .good: return Strings.RatingStatus.Good
//		case .excellent: return Strings.RatingStatus.Excellent
//		}
	}
}

enum UserRatingType {
	case experience
	case personality
	case appearance
	
	var description: String {
		switch self {
		case .experience: return Strings.RatingType.experience
		case .personality: return Strings.RatingType.personality
		case .appearance: return Strings.RatingType.appearance
		}
	}
}

protocol UserRatingProtocol {}
extension InviteModel: UserRatingProtocol {
//	func senderRating(for type: UserRatingType) -> UserRatingStatus {
//		
//	}
}


// MARK: - Venue Protocol
extension InviteModel: VenueProtocol {
	
	///Returned **Venue** will never be nil. `Nil` is only used to clear fields
	var venue: Venue! {
		get {
			return Venue(
				id: _invite.venueId ?? "",
				name: _invite.venueName ?? "",
				address: _invite.venueAddress ?? "",
				rate: Int(_invite.venueRate),
				coordinate: CLLocationCoordinate2D(latitude: _invite.venueLat, longitude: _invite.venueLng),
				distance: 0)
		
		}
		set {
			if let venue = newValue {
				_invite.venueId = venue.id
				_invite.venueName = venue.name
				_invite.venueAddress = venue.address.description
				_invite.venueLat = venue.location.coordinate.latitude
				_invite.venueLng = venue.location.coordinate.longitude
				_invite.venueRate = Int16(venue.rate)
				
				
			} else {
				_invite.venueId = nil
				_invite.venueName = nil
				_invite.venueAddress = nil
				_invite.venueLat = kCLLocationCoordinate2DInvalid.latitude
				_invite.venueLng = kCLLocationCoordinate2DInvalid.longitude
				_invite.venueRate = Int16(venue.rate)
			}
			
		}
	}
}

// MARK: - Cancel reasons
extension InviteModel {
	var cancelReason: CancelDateReasons {
		get {
			return CancelDateReasons(rawValue: Int(_invite.cancelReason)) ?? .none
		}
		set {
			_invite.cancelReason = Int16(newValue.rawValue)
		}
	}
}


// MARK: - Invite state
extension InviteModel {
    /// Dating has already begun
    var isDatingBegun: Bool {
        guard let delay = Calendar.current.date(byAdding: .hour, value: 12, to: self.scheduled) else { return false }
        return _invite.stateValue == Int64(InviteState.datingBegin.rawValue) && delay >= Date()
    }
    
    
    /// Invitation sent to recepient, but not yet seen by him/her
    var isWaitingForResponse: Bool {
        
        if _invite.stateValue == Int64(InviteState.sent.rawValue) {
            let timeToReview = _invite.inheritedFrom.isEmptyOrNil ?
                Constants.Invitation.waitForResponseToRequest :  Constants.Invitation.waitForResponseToModifyRequest
            
            return (_invite.created.timeIntervalSince1970 + timeToReview) > Date().timeIntervalSince1970
        }
        
        return false
    }
    
    
    /// Invitation has being seen be recipient, and we are waiting for accept or decline
    var isWaitingForReview: Bool {
        if _invite.stateValue == Int64(InviteState.reviewing.rawValue) {
            let timeToReview = _invite.inheritedFrom.isEmptyOrNil ?
                Constants.Invitation.waitingForReviewTime : Constants.Invitation.waitingForModifyeReview
            
            return (_invite.countdownStartedAt + timeToReview) > Date().timeIntervalSince1970
        }
        
        return false
    }
    
    var state: InviteState {
        get { return InviteState(rawValue: _invite.stateValue)! }
        set { _invite.stateValue = newValue.rawValue }
    }
    
    
}

// MARK: - Create and countdown time
extension InviteModel {
    var scheduled: Date {
        get {
            return Date(timeIntervalSince1970: _invite.scheduled)
        }
        set {
            _invite.scheduled = newValue.timeIntervalSince1970
        }
    }
    
    var countDownStarted: Date {
        get {
            return Date(timeIntervalSince1970: _invite.countdownStartedAt == 0 ? _invite.created.timeIntervalSince1970 : _invite.countdownStartedAt)
        }
        set {
            _invite.countdownStartedAt = newValue.timeIntervalSince1970
        }
    }
}
