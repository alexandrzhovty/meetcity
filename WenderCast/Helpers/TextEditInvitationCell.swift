//
//  TextEditInvitationCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class TextEditInvitationCell: UITableViewCell {

	@IBOutlet weak var separatorView: UIView!
	@IBOutlet weak var borderedView: BorderedView!
	@IBOutlet weak var textField: UITextField!
	
	enum Constants {
		static let height: CGFloat = 106
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
		size.height = max(size.height, TextEditInvitationCell.Constants.height)
		return size
	}
	
}

extension TextEditInvitationCell: NibLoadableView {}


