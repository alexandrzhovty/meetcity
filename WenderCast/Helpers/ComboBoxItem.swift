//
//  ComboBoxItem.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit


/// Abstratct class
class ComboBoxItem: UIControl {

    enum Constants {
        static let margin: CGFloat = 4
    }
    
    var containerView: UIView = UIView()
    var button: UIButton = UIButton(type: .custom)
    
    fileprivate var _expanded: Bool = false
    
    var title: String? {
        get { return button.title(for: .normal) }
        set { button.setTitle(newValue, for: .normal) }
    }
    
    //	MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupControl()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
        setupControl()
    }
    
    func setupControl() {
        self.backgroundColor = UIColor.white
    }
    
    @objc func didTap(_ button: UIButton) {
        sendActions(for: .touchUpInside)
    }

}
