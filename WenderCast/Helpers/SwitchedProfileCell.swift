//
//  SwitchProfileCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

extension SwitchedProfileCell: NibLoadableView {}

protocol SwitchedProfileCellProtocol: class {
	typealias Action = (_ sender: UISwitch) -> Void
	var title: String? { get set }
	var isOn: Bool { get set }
	var action: Action? { get set }
	
}


class SwitchedProfileCell: UITableViewCell {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var switchControl: UISwitch!
	
	var action: SwitchedProfileCellProtocol.Action?
	
	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
		size.height = max(size.height, Constants.TableView.profileCellHeight)
		return size
	}
	
	
	@IBAction func valueChanged(_ sender: UISwitch) {
		action?(sender)
	}
}



extension SwitchedProfileCell: SwitchedProfileCellProtocol {
	
	var isOn: Bool {
		get { return switchControl.isOn }
		set { switchControl.isOn = newValue }
	}

	var title: String? {
		get { return titleLabel.text }
		set { titleLabel.text = newValue }
	}
}
