//
//  InstagramLoginViewController.swift
//  InstagramLogin-Swift
//
//  Created by Aman Aggarwal on 2/7/17.
//  Copyright © 2017 ClickApps. All rights reserved.
//

import UIKit

class InstagramLoginViewController: UIViewController {
   
   @IBOutlet weak var loginWebView: UIWebView!
   @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
   @IBOutlet weak var loginIndicatorView: UIView!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      // Do any additional setup after loading the view.
      loginWebView.delegate = self
      unSignedRequest()
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   
   @IBAction func backButtonPressed(_ sender: Any) {
      close()
   }
   
   func unSignedRequest () {
      let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
      let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
      loginWebView.loadRequest(urlRequest)
   }
   
   func checkRequestForCallbackURL(request: URLRequest) -> Bool {
      
      let requestURLString = (request.url?.absoluteString)! as String
      
      if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
         let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
         handleAuth(authToken: requestURLString.substring(from: range.upperBound))
         return false;
      }
      return true
   }
   
   func handleAuth(authToken: String)  {
      
      print("Instagram authentication token ==", authToken)
      
//      if let currentUser = UsersManager.currentUser {
//         currentUser.instagramtoken = authToken
//         UsersManager.updateUser(currentUser) { _ in
//         }
//      }
      close()
   }
   
   fileprivate func close() {
      DispatchQueue.main.async {
         let _ = self.navigationController?.popViewController(animated: true)
      }
   }
}

// MARK: - UIWebViewDelegate

extension InstagramLoginViewController: UIWebViewDelegate {
   
   func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
      return checkRequestForCallbackURL(request: request)
   }
   
   func webViewDidStartLoad(_ webView: UIWebView) {
      loginIndicatorView.isHidden = false
      loginIndicator.startAnimating()
   }
   
   func webViewDidFinishLoad(_ webView: UIWebView) {
      loginIndicatorView.isHidden = true
      loginIndicator.stopAnimating()
   }
   
   func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
      webViewDidFinishLoad(webView)
   }
}
