//
//  NotificationMessage.swift
//  Pods
//
//  Created by Ben.Park on 2017. 1. 24..
//
//

import UIKit
import Firebase
import FirebaseStorage


public class NotificationMessage: UIView {
	
    typealias Completion = () -> Void
        
    fileprivate var containerWindow: UIWindow?
	
    
    fileprivate var animationDuration: TimeInterval = 0.3
    fileprivate var _delay: TimeInterval
    fileprivate var isShowing: Bool = false
    fileprivate var _didSelectHandelr: Completion?
	fileprivate var _cancelHandler: Completion?
	fileprivate var _messageView: NotificationMessageView
	fileprivate var _timer: Timer?
	
	deinit {
		print(String(describing: type(of: self)),":", #function)
	}
	
    public init(delay: TimeInterval = 1) {
        self._delay = delay
        self._didSelectHandelr = nil
		self._cancelHandler = nil
		
		
		_messageView = NotificationMessageView.instantiateFromInterfaceBuilder()
		
		super.init(frame: _messageView.frame)
		
		self.addSubview(_messageView)     // 4
		_messageView.translatesAutoresizingMaskIntoConstraints = false   // 5
		
		
		_messageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
		_messageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
		_messageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
		_messageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
		layoutIfNeeded()
		
		self.backgroundColor = UIColor.clear
		
		// Tap gesture
		let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
		_messageView.addGestureRecognizer(tap)
		
		// Pan gesture
		let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
		_messageView.addGestureRecognizer(pan)
		
    }
	
	
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
	
	
	@objc func handleTapGesture(_ recognizer: UITapGestureRecognizer) {
		stopTimer()
		self.hide(completion: self._didSelectHandelr)
	}
	
	@objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
		switch recognizer.state {
		case .began:
			stopTimer()
			print(String(describing: type(of: self)),":", #function)
			print(self.frame.minY)
		case .changed:
			let translation = recognizer.translation(in: self)
			
			let posY = self.frame.minY + translation.y
			if posY <= 0 {
				var viewPosition = self.center
				viewPosition.y += translation.y
				self.center = viewPosition
				recognizer.setTranslation(CGPoint.zero, in: self)
				
			}
			
		default:
			if self.frame.minY < 0 {
				self.hide(completion: self._cancelHandler)
			} else {
				self.frame.origin.y = 0
				startTimer()
			}
		}
	}
	
}

// MARK: - Timer
extension NotificationMessage {
	fileprivate func startTimer() {
		stopTimer()
		
		// View should be always shown
		guard _delay > 0 else { return }
		
		_timer = Timer.scheduledTimer(withTimeInterval: _delay, repeats: false, block: { [weak self] (_) in
			guard let `self` = self else { return }
			self.hide(completion: self._cancelHandler)
		})
	}
	
	fileprivate func stopTimer() {
		_timer?.invalidate()
		_timer = nil
	}
}

// MARK: chaning function and show / hide functions
extension NotificationMessage {
	
	public func title(_ text: String) -> Self {
		_messageView.textLabel.text = text
		return self
	}
	
    public func message(_ text: String) -> Self {
        _messageView.detailLabel.text = text
        return self
    }
	
	public func imageUrlStrig(_ sUrl: String) -> Self {
		
//		let httpsReference = FirebaseStack.storageRef.
//		
//		FirebaseStack.storageRef.url

		
		let storage = Storage.storage()
		let imageRef = storage.reference(forURL: sUrl)
		
		print(String(describing: type(of: self)),":", #function)
		print(imageRef)
		
		ImageCache.default.load(forKey: imageRef) { [weak self] image in
			guard let `self` = self else { return }
			self._messageView.imageView.image = image
		}
		
		
		
//		let storageRef = FirebaseStack.storageRef.putFile(from: url)
//		
//		let storageRef = storage.referenceFromURL(downloadURL)
//  // Download the data, assuming a max size of 1MB (you can change this as necessary)
//  storageRef.dataWithMaxSize(1 * 1024 * 1024) { (data, error) -> Void in
//	// Create a UIImage, add it to the array
//	let pic = UIImage(data: data)
//	picArray.append(pic)
//  })
//		
		return self
	}
	
	
    public func didSelect(_ completion: @escaping () -> Void) -> Self {
        self._didSelectHandelr = completion
        return self
    }
	
	
	public func didCancel(_ completion: @escaping () -> Void) -> Self {
		self._cancelHandler = completion
		return self
	}
	
    public func show(){
		DispatchQueue.main.async { [unowned self] in
			self.addAlertViewInContainerWindow()
			if self._delay > 0 {
				self.startTimer()
			}
		}
    }
}

// MARK: animation functions
extension NotificationMessage {
    
    fileprivate func startAnimation(completion: @escaping () -> Void) {
        guard !isShowing else {
            return
        }
        isShowing = true
        
        UIView.animate(withDuration: animationDuration, animations: {
            self.frame.origin.y = 0
            self.layoutIfNeeded()
        }) { _ in
            completion()
        }
    }
	
	
	fileprivate func hide(completion: Completion?) {
		UIView.animate(withDuration: animationDuration, animations: {
			self.frame.origin.y = -self.frame.height
			self.layoutIfNeeded()
		}) { _ in
			self.isShowing = false
			self.containerWindow?.isHidden = true
			self.containerWindow = nil
			completion?()
		}
	}
}

// MARK: manage window hierarchy functions
extension NotificationMessage {
    
    fileprivate func addAlertViewInContainerWindow() {
        containerWindow = UIWindow(frame: self.bounds)
        containerWindow?.backgroundColor = UIColor.clear
        containerWindow?.windowLevel = UIWindowLevelStatusBar
        containerWindow?.rootViewController = UIViewController()
        containerWindow?.rootViewController?.view.addSubview(self)
        containerWindow?.isHidden = false
    }
    
    fileprivate func addAlertViewInCurrentWindow() {
        guard let keyWindow = UIApplication.shared.keyWindow,
              let rootViewController = keyWindow.rootViewController as? UINavigationController else {
            return
        }
        let navigationBar = rootViewController.navigationBar
        rootViewController.view.insertSubview(self, belowSubview: navigationBar)
    }
}


