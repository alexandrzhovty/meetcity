//
//  PhotosPageControl.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/22/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

final class PhotosPageControl: GradientView {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	
	var numberOfPages: Int = 0  { didSet { configureView() } }
	open var currentPage: Int = 0 { didSet { configureView() } }
	open var hidesForSinglePage: Bool = true { didSet { configureView() } }
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.colors =  [UIColor.black.withAlphaComponent(0.4), UIColor.clear]
		self.tintColor = UIColor.white
		imageView.tintColor = UIColor.white
		
		
		configureView()
	}

	private func configureView() {
		if hidesForSinglePage && numberOfPages <= 1 {
			imageView.isHidden = true
			titleLabel.isHidden = true
		} else {
			imageView.isHidden = false
			titleLabel.isHidden = false
		}
		titleLabel.text = "\(currentPage + 1)/\(numberOfPages)"
	}

}
