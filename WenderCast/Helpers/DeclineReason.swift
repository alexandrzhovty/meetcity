//
//  DeclineReason.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

enum DeclineReason: Int {
	case sender = 3
	case recipient = 4
	case expired = 5
	
	init(with inviteState: InviteState) {
		assert(inviteState.isDeclined == true, "Incorrect in")
		switch inviteState {
		case .declinedByRecipient:	self = .recipient
		case .declinedBySender:		self = .sender
		default:					self = .expired
		}
	}
	
	var inviteState: InviteState {
		switch self {
		case .recipient:	return InviteState.declinedByRecipient
		case .sender:		return InviteState.declinedBySender
		case .expired: 		return InviteState.declinedExpired
		}
	}
	
}
