//
//  DMEvent.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import Firebase
import SwiftyJSON

// MARK: - Overrides
extension DMEvent {
    /// Invoked automaticly
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        
        self.createdValue = Date().timeIntervalSince1970
        self.id = UUID().uuidString
    }
    
    /// Create new invitation
    ///
    /// **Sender** is the application user
    ///
    /// - Parameters:
    ///   - user: **reciepient** of the invitation
    ///   - context: where new entity should be saved in
    class func new(for user: DMUser, in context: NSManagedObjectContext) -> DMInvite {
        let entity = DMInvite(context: context)
        entity.recipient = context.object(with: user.objectID) as! DMUser
        entity.sender = context.object(with: AppDelegate.me.objectID) as! DMUser
        
        return entity
        
    }
}

// MARK: - Event type
enum EventType: Int32 {
    case invite
    case like
    case visit
}


// MARK: - Fields
extension DMEvent {
    typealias Identifier = StringIdentifier
    
    var identifier: Identifier {
        get { return Identifier(rawValue: self.id!) }
        set { self.id = newValue.rawValue }
    }
    
    var type: EventType {
        get { return EventType(rawValue: self.typeValue)! }
        set { self.typeValue = newValue.rawValue }
    }
    
    var created: Date {
        get { return Date(timeIntervalSince1970: self.createdValue) }
        set { self.createdValue = newValue.timeIntervalSince1970 }
    }

    
    
    var recipient: DMUser {
        get { return recipientValue! }
        set { recipientValue = newValue }
    }
    
    var sender: DMUser {
        get { return senderValue! }
        set { senderValue = newValue }
    }
    

    /// Used as indexed search property
    var recipientSenderIndex: String {
        get { return "\(recipient.uid!)_\(sender.uid!)" }
    }
}

// MARK: - Opponent
extension DMEvent {
    /// Returns dating opponent name
    var opponent: DMUser {
        get { return self.sender.uid == AppDelegate.me.uid ? self.recipient : self.sender }
    }
    
    var meIsSender: Bool {
        get { return self.sender.uid == AppDelegate.me.uid }
    }
}

extension DMEvent {
    /// Parse JSON response from server
    ///
    /// - Parameters:
    ///   - json: array of JSON objects
    final func updateCommonFields(from json: JSON, into context: NSManagedObjectContext) {
        
//        
//        guard let id = json[DMLike.CodingKeys.id].string else { return }
//        guard let recepientId = json[DMLike.CodingKeys.recipientID].string else { return }
//        guard let senderId = json[DMLike.CodingKeys.senderID].string else { return }
//        
//        self.createdValue = json[DMLike.CodingKeys.created].doubleValue
//        
//        
//        // Sender
//        self.sender = users[senderId] ?? {
//            let user = DMUser.new(with: senderId, in: context)
//            users[senderId] = user
//            return user
//            }()
//        
//        self.sender.name = json[DMLike.CodingKeys.senderName].stringValue
//        self.sender.birthday = json[DMLike.CodingKeys.senderBirthday].doubleValue
//        self.sender.profilePhotoUrl = json[DMLike.CodingKeys.senderPhoto].stringValue
//        self.sender.occupation = json[DMLike.CodingKeys.senderOccupation].string
//        
//        // Recipient
//        self.recipient = users[recepientId] ?? {
//            let user = DMUser.new(with: recepientId, in: context)
//            users[recepientId] = user
//            return user
//            }()
//        
//        self.recipient.name = json[DMLike.CodingKeys.recipientName].stringValue
//        self.recipient.birthday = json[DMLike.CodingKeys.recipientBirthday].doubleValue
//        self.recipient.profilePhotoUrl = json[DMLike.CodingKeys.recipientPhoto].stringValue
//        self.recipient.occupation = json[DMLike.CodingKeys.recipientOccupation].string
//        
//        return true
        
    }
}


//// MARK: - SynchronizationProtocol
//extension DMEvent: SynchronizationProtocol {
//
//    enum CodingKeys {
//        static let id = "id"
//        static let senderID = "senderID"
//        static let recipientID = "recipientID"
//        static let created = "created"
//        static let recipient_sender = "recipientSender"
//        
//        static let senderName = "senderName"
//        static let senderBirthday = "senderBirthday"
//        static let senderOccupation = "senderOccupation"
//        static let senderPhoto = "senderPhoto"
//        
//        static let recipientName = "recipientName"
//        static let recipientBirthday = "recipientBirthday"
//        static let recipientOccupation = "recipientOccupation"
//        static let recipientPhoto = "recipientPhoto"
//        
//        
//    }
//    
//    @available(*, deprecated, message: "Use static property `synchronize(comletion: @escaping (Error?) -> Swift.Void)` instead")
//    func synchronize() {}
//    
//    func synchronize(completion: @escaping (Result<DatabaseReference>) -> Swift.Void) {
//        
//        
//        
//        
//        let photoRef = self.sender.profilePhotoeRef
//        photoRef?.downloadURL(completion: { (url, error) in
//            
//            var json = JSON([:])
//            json[CodingKeys.id].string = self.identifier.rawValue
//            
//            json[CodingKeys.created].string = "\(Date().timeIntervalSince1970)"
//            json[CodingKeys.recipient_sender].string = self.recipientSenderIndex
//            
//            json[CodingKeys.senderID].string = self.sender.identifier.rawValue
//            json[CodingKeys.senderName].string = self.sender.name
//            json[CodingKeys.senderOccupation].string = self.sender.occupation
//            json[CodingKeys.senderBirthday].double = self.sender.birthday
//            json[CodingKeys.senderPhoto].string = url?.absoluteString
//            
//            
//            json[CodingKeys.recipientID].string = self.recipient.identifier.rawValue
//            json[CodingKeys.recipientName].string = self.recipient.name
//            json[CodingKeys.recipientOccupation].string = self.recipient.occupation
//            json[CodingKeys.recipientBirthday].double = self.recipient.birthday
//            
//            
//            let recipienPhotoRef = self.recipient.profilePhotoeRef
//            recipienPhotoRef?.downloadURL(completion: { (url, _) in
//                json[CodingKeys.recipientPhoto].string = url?.absoluteString
//                
//                let likeRef = FirebaseStack.like(for: self.identifier.rawValue)
//                
//                likeRef.setValue(json.dictionaryObject) { (error, dbRef) in
//                    
//                    if let error = error {
//                        completion(Result.failure(error))
//                    } else {
//                        completion(.success(dbRef))
//                    }
//                }
//            })
//        })
//        
//    }
//}
