//
//  ComboBoxExpand.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/** @IBDesignable */ class ComboBoxExpand: ComboBoxItem {
    


    
    private(set) var shevronImageView: UIImageView!
    
    fileprivate var _expanded: Bool = false
    
    
    
    
    
    override func setupControl() {
        super.setupControl()
        
        // Container view
        
        containerView.backgroundColor = UIColor(rgbValue: 0xD8D8D8).withAlphaComponent(0.19)
        containerView.layer.cornerRadius = 4
        
        addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
     
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.margin * 2).isActive = true
        trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: Constants.margin * 2).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor, constant: Constants.margin).isActive = true
        bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: Constants.margin).isActive = true
        
        
        // Shevron image
        shevronImageView = UIImageView()
        shevronImageView.contentMode = .center
        shevronImageView.image = UIImage.icon.shevron
        shevronImageView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(shevronImageView)
        
        shevronImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, for: .horizontal)
        shevronImageView.setContentHuggingPriority(UILayoutPriorityDefaultHigh, for: .horizontal)
        
        containerView.trailingAnchor.constraint(equalTo: shevronImageView.trailingAnchor, constant: Constants.margin).isActive = true
        shevronImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: Constants.margin).isActive = true
        containerView.bottomAnchor.constraint(equalTo: shevronImageView.bottomAnchor, constant: Constants.margin).isActive = true
        
        
        shevronImageView.addConstraint(
            NSLayoutConstraint(item: shevronImageView, attribute: .height, relatedBy: .equal, toItem: shevronImageView, attribute: .width, multiplier: 1, constant: 1)
        )
        
        shevronImageView.transform = CGAffineTransform(rotationAngle: (360 * CGFloat.pi/180.0))
        
        // Button
        self.button.setTitleColor(UIColor(rgbValue: 0x464646), for: .normal)
    
        
        button.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFont.Avenir.medium.font(size: 17)
        button.setTitle("Combo box item", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(button)
        
        button.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        button.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: Constants.margin * 2).isActive = true
        button.trailingAnchor.constraint(equalTo: shevronImageView.leadingAnchor, constant: Constants.margin).isActive = true
        
    }
    
    

}

// MARK: - Expand
extension ComboBoxExpand {
    var isExpanded: Bool {
        get { return _expanded }
        set { setExpanded(newValue, animated: false) }
    }
    
    func setExpanded(_ expanded: Bool, animated: Bool) {
        let animations = { [unowned self] () -> Void in
            if let view = self.shevronImageView {
                let angle: CGFloat = expanded ? -180 : 360
                view.transform = CGAffineTransform(rotationAngle: (angle * CGFloat.pi/180.0))
            }
            
        }
        
        let completion = { [unowned self] (finished: Bool) -> Void in
            self._expanded = expanded
        }
        
        
        
        if animated {
            UIView.transition(with: self.shevronImageView, duration: 0.25, options: .transitionCrossDissolve, animations: animations, completion: completion)
        } else {
            animations()
            completion(true)
        }
    }
}


