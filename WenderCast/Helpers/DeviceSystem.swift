
//
//  DeviceSystem.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/28/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation


enum DeviceSystem: Int {
   case android
   case apple
}
