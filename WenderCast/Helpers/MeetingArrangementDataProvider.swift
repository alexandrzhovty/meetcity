//
//  MeetingArrangementDataProvider.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData

enum ArrangementRowType: Int {
	case recepientInfo
	case datingTime
	case place
	case notes
	case map
    
    var indexPath: IndexPath {
        return IndexPath(row: self.rawValue, section: 0)
    }
}

struct MeetingArrangementDataProvider {
	fileprivate let _items: [ArrangementRowType]
	fileprivate let _inviteModel: InviteModel
	
	init(with inviteModel: InviteModel) {
		self._inviteModel = inviteModel
		self._items = [.recepientInfo, .datingTime, .place, .notes, .map]
	}
	
}

extension MeetingArrangementDataProvider {
	var numberOfSection: Int { return 1 }
	func numberOfItems(in section: Int) -> Int {
		return _items.count
	}
	
	func object(at indexPath: IndexPath) -> ArrangementRowType {
		return _items[indexPath.row]
	}
}


