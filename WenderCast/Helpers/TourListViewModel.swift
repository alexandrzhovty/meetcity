//
//  TourViewModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

protocol TourCellProtocol {
    var message: String? { get set }
}

protocol  ImageTourCellProtocol: TourCellProtocol {
    var image: UIImage? { get set }
}

protocol VideoTourCellProtocol: TourCellProtocol {
    var videoURL: URL? { get set }
    func playVideo()
    func stopVideo()
}


struct TourListViewModel {
    let item: Tour
    static func registerNibs(for collectionView: UICollectionView) {
        collectionView.register(VideoTourCollectionCell.self)
        collectionView.register(ImageTourCollectionCell.self)
        
        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    func configure(_ cell: TourCellProtocol) {
        switch item.type {
        case let .image(image):
            guard var cell = cell as? ImageTourCellProtocol else {
                assertionFailure()
                return
            }
            cell.message = item.message
            cell.image = image
            
        case let .video(url):
            guard var cell = cell as? VideoTourCellProtocol else {
                assertionFailure()
                return
            }
            cell.message = item.message
            cell.videoURL = url
        }
    }
}

extension UICollectionView {
    func dequeueReusableCell(with type: TourType, for indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell
        switch type {
        case .image(_):
            cell = self.dequeueReusableCell(ImageTourCollectionCell.self, for: indexPath)
        case .video(_):
            cell = self.dequeueReusableCell(VideoTourCollectionCell.self, for: indexPath)
        }
        
        return cell
    }
}
