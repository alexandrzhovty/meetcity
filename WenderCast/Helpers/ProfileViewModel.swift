//
//  File.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit


class ProfileViewModel: NSObject {
	
	fileprivate let _item: Profile
	
	init(with item: Profile) {
		self._item = item
		super.init()
	}
	
	var type: ProfileType { return _item.type }
	
	func configure(_ cell: UITableViewCell) {
		switch _item.type {
		case let .switcher(tulip):
			guard let cell = cell as? SwitchedProfileCellProtocol else { break }
			cell.title = _item.title
			cell.isOn = tulip.currentValue
			cell.action = tulip.action
			
		case .selectable:
			guard let cell = cell as? SelectableProfileCellProtocol else { break }
			cell.title = _item.title
			
		case .buttonLike:
			guard let cell = cell as? ActionProfileCellProtocol else { return }
			cell.title = _item.title
			
		case let .textEditabled(tulip):
			guard let cell = cell as? TextEditProfileCellProtocol else { return }
			cell.message = tulip.text
			cell.textLimit = tulip.textLmit
			cell.action = tulip.action
		
			
		}
	}
}

