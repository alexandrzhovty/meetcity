//
//  ComboBoxBadge.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class ComboBoxBadge: ComboBoxItem {

   private(set) var badgeImageView: UIImageView!
   private(set) var label: UILabel = UILabel()
   
   var quantity: Int {
      get { return Int(label.text ?? "0") ?? 0 }
      set { label.text = "\(newValue)" }
   }
   
   override func setupControl() {
      super.setupControl()
      
      // Container view
      containerView.backgroundColor = UIColor(rgbValue: 0xD8D8D8).withAlphaComponent(0.19)
      containerView.layer.cornerRadius = 4
      
      addSubview(containerView)
      
      containerView.translatesAutoresizingMaskIntoConstraints = false
      
      containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.margin * 2).isActive = true
      trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: Constants.margin * 2).isActive = true
      containerView.topAnchor.constraint(equalTo: topAnchor, constant: Constants.margin).isActive = true
      bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: Constants.margin).isActive = true
      
      
      // Shevron image
      badgeImageView = UIImageView(image: UIImage.backgorund.badge)
//      badgeImageView.contentMode = .scaleToFill
//      badgeImageView.image =
      
      
      badgeImageView.translatesAutoresizingMaskIntoConstraints = false
      containerView.addSubview(badgeImageView)
   
      
      containerView.trailingAnchor.constraint(equalTo: badgeImageView.trailingAnchor, constant: Constants.margin).isActive = true
//      badgeImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: Constants.margin).isActive = true
//      containerView.bottomAnchor.constraint(equalTo: badgeImageView.bottomAnchor, constant: Constants.margin).isActive = true
      
      badgeImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
      badgeImageView.addConstraint(
         NSLayoutConstraint(item: badgeImageView, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: UIImage.backgorund.badge.size.width)
      )
//      badgeImageView.widthAnchor.constraint(greaterThanOrEqualTo: UIImage.backgorund.badge.size.width, multiplier: 0).isActive = true
//      let ss = NSLayoutDimension(
//      badgeImageView.widthAnchor.constraint(greaterThanOrEqualTo: NSLayoutDimension, multiplier: <#T##CGFloat#>)
      
      // Count label
      label.text = "0"
      label.textAlignment = .center
      label.textColor = UIColor.white
      label.font = UIFont.Avenir.medium.font(size: 17)
      label.backgroundColor = UIColor.clear
      
      label.translatesAutoresizingMaskIntoConstraints = false
      badgeImageView.addSubview(label)
      
      label.centerYAnchor.constraint(equalTo: badgeImageView.centerYAnchor, constant: 1).isActive = true
      label.leadingAnchor.constraint(equalTo: badgeImageView.leadingAnchor, constant: 8).isActive = true
      badgeImageView.trailingAnchor.constraint(equalTo: label.trailingAnchor, constant: 8).isActive = true
      
      label.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, for: .horizontal)
      label.setContentHuggingPriority(UILayoutPriorityDefaultHigh, for: .horizontal)
      
      
      // Button
      self.button.setTitleColor(UIColor(rgbValue: 0x464646), for: .normal)
      
      
      button.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
      button.contentHorizontalAlignment = .left
      button.titleLabel?.font = UIFont.Avenir.medium.font(size: 17)
      button.setTitle("Combo box item", for: .normal)
      button.translatesAutoresizingMaskIntoConstraints = false
      containerView.addSubview(button)
      
      button.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
      button.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: Constants.margin * 2).isActive = true
      button.trailingAnchor.constraint(equalTo: badgeImageView.leadingAnchor, constant: Constants.margin).isActive = true
      
   }
   

}
