//
//  HistoryViewModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import Firebase

struct HistoryEventViewModel {
    let event: DMEvent
    init(with event: DMEvent) {
        self.event = event
    }
   
    func configure(_ cell: HistoryTableCell) {
        
        cell.title = event.sender.title
        if let sURL = event.sender.profilePhotoUrl {
            
            let imageRef = Storage.storage().reference(forURL: sURL)
            let downloadId = ImageCache.DownloadIdentifier(rawValue: UUID().uuidString)
            
            ImageCache.default.load(forKey: imageRef, downloadID: downloadId, completion: { (image, responseID) in
                if downloadId == responseID {
                    cell.photo = image
                }
            })
        }
        
        
        switch event.type {
        case .like:
            cell.body = String.localizedStringWithFormat(Strings.History.likedYourProfile, event.created.relativeTime)
            
        case .invite:
            cell.body = String.localizedStringWithFormat(Strings.History.sentInvitation, event.created.relativeTime)
            
        case .visit:
            let visit = event as! DMVisit
            let format = Strings.History.visitedYourProfile
            cell.body = String.localizedStringWithFormat(format, visit.lastVisit.relativeTime)

        
        }
    }
}
