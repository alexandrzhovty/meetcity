//
//  EditInvitationCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class EditInvitationCell: UITableViewCell {
	
	
	@IBOutlet weak var textImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var editButton: UIButton!
	@IBOutlet weak var separatorView: UIView!
	@IBOutlet weak var borderedView: BorderedView!
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
}

extension EditInvitationCell: NibLoadableView {}
