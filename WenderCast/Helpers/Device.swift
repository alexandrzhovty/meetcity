//
//  Device.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/16/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

extension UIDevice {
	static var isSimulator: Bool {
		return ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil
	}
}
