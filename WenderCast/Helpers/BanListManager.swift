//
//  BanListManager.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import SwiftyJSON
import Firebase
import CoreData

enum BanType: Int {
    case block = 1
    case complain
}

class BanListManager {
    typealias CodingKeys = DMLike.CodingKeys
    
    fileprivate let _operationQueue = OperationQueue()
    
    var observers: [Any]
    
    fileprivate static let `default` = BanListManager()
    private  init() {
        observers = [Any]()
    }
    
    deinit {
        unregisterObserver()
    }
    
    
    
    fileprivate func startMonitoring() {
        registerObserver()
    }
   
}

// MARK: - ObserverProtocol
extension BanListManager: ObserverProtocol {
    func unregisterObserver() {
        observers.forEach {
            switch $0 {
            case let dbRef as DatabaseReference:
                dbRef.removeAllObservers()
            case let queryRef as DatabaseQuery:
                queryRef.removeAllObservers()
            default:
                NotificationCenter.default.removeObserver($0)
            }
        }
        observers.removeAll()
        
    }
    
    func registerObserver() {
        observers.append(banListListener())
        
    }
    
    private func banListListener() -> DatabaseReference {
        let banlistRef = FirebaseStack.bans
        
        banlistRef.observeSingleEvent(of: .value, with: { [weak self] snapshots in
            guard let `self` = self else { return }
            if snapshots.exists(), let dict = snapshots.value as? [String: [String: Any]] {
                DispatchQueue.global(qos: .background).async {
                    let ids = dict.map{ $0.value[CodingKeys.recipientID] as? String }.flatMap{ $0 }
                    
                    
                    let operation = ParseOperation(parsingHandler: { context in
                        self.banUsers(in: ids, from: context)
                    })
                    
                    self._operationQueue.addOperation(operation)
                }
            } else {
                
                
                let operation = ParseOperation(parsingHandler: { context in
                    self.clearLocalBanList(from: context)
                })
                
                self._operationQueue.addOperation(operation)
            }
        })
        
        
        banlistRef.observe(.childRemoved, with: { snapshot in
            print("childRemoved ", snapshot.value ?? "Empty")
            
            let value = snapshot.value as? [String: Any]
            print(value ?? "Empty")
            
        })
        
        banlistRef.observe(.childAdded, with: { snapshot in
//            guard
//                let value = snapshot.value as? [String: Any],
//                let uid = value[CodingKeys.id] as? String
//                else {
//                    return
//            }
//            
//            
//            let operation = ParseOperation(parsingHandler: { [uid] (context) in
//                
//                self.banUsers(in: [uid], from: context)
//                    
//                
//            })
//            
//            self._operationQueue.addOperation(operation)
            
        })

        
        return banlistRef
    
    }
    
    
    
}

// MARK: - Core data methods
extension BanListManager {
    fileprivate func banUsers(in identifiers: [String], from moc: NSManagedObjectContext) {
        
        let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
        let predicates = [
            NSPredicate(format: "%K = 0", #keyPath(DMUser.banned)),
            NSPredicate(format: "%K in %@", #keyPath(DMUser.uid), identifiers)
            ]
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        
        let results = try? moc.fetch(request)
        for enitity in results ?? [] {
            enitity.banned = 1
        }
    }
    
    fileprivate func clearLocalBanList(from moc: NSManagedObjectContext) {
        
        let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
        let predicate = NSPredicate(format: "%K > 0", #keyPath(DMUser.banned))
        request.predicate = predicate
        
        let results = try? moc.fetch(request)
        for enitity in results ?? []{
            enitity.banned = 0
        }
        
        
        
    }
}

// MARK: - Public methods
extension BanListManager {
    class func startMonitoring() {
        
        BanListManager.default.startMonitoring()
        
    }
    
    class func ban(_ user: DMUser, for banType: BanType, completion: @escaping (Result<DatabaseReference>) -> Swift.Void) {
        
        var json = JSON([:])
        
        let identifier = StringIdentifier(rawValue: UUID().uuidString)
        
        json[CodingKeys.id].string = identifier.rawValue
        
        json[CodingKeys.created].string = "\(Date().timeIntervalSince1970)"
        json[CodingKeys.recipient_sender].string = "\(user.identifier.rawValue)_\(AppDelegate.me.identifier.rawValue)"
        
        json[CodingKeys.senderID].string = AppDelegate.me.identifier.rawValue
        json[CodingKeys.senderName].string = AppDelegate.me.name
        json[CodingKeys.senderOccupation].string = AppDelegate.me.occupation
        json[CodingKeys.senderBirthday].double = AppDelegate.me.birthday
        
        
        json[CodingKeys.recipientID].string = user.identifier.rawValue
        json[CodingKeys.recipientName].string = user.name
        json[CodingKeys.recipientOccupation].string = user.occupation
        json[CodingKeys.recipientBirthday].double = user.birthday
        
        json["banType"].int = banType.rawValue
        
        let photoRef = AppDelegate.me.profilePhotoeRef
        photoRef?.downloadURL(completion: { (url, error) in
            
            json[CodingKeys.senderPhoto].string = url?.absoluteString
            
            
            let recipienPhotoRef = user.profilePhotoeRef
            recipienPhotoRef?.downloadURL(completion: { (url, _) in
                json[CodingKeys.recipientPhoto].string = url?.absoluteString
                
                let likeRef = FirebaseStack.ban(for: identifier)
                
                likeRef.setValue(json.dictionaryObject) { (error, dbRef) in
                    
                    if let error = error {
                        completion(Result.failure(error))
                    } else {
                        completion(.success(dbRef))
                    }
                }
            })
        })
        
    }
}
