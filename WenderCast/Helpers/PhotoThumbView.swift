//
//  PhotoThumbView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/6/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

@IBDesignable
class PhotoThumbView: UIView {

	var isMain: Bool = false
    var fileName: String!
    
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var deleteButton: UIButton!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		prepareForUsing()
		
	}
    
    func prepareForUsing() {
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor(rgbValue: 0xFFF6F9).cgColor
    }
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		xibSetup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		// 3. Setup view from .xib file
		xibSetup()
		
	}
	
	var view: UIView!
	func xibSetup() {
		
		backgroundColor = UIColor.clear
		view = loadViewFromNib()
		view.frame = bounds
		view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
		addSubview(view)
	}
	
	func loadViewFromNib() -> UIView {
		let bundle = Bundle(for: type(of: self))
		let nib = UINib(nibName: "PhotoThumbView", bundle: bundle)
		
		// Assumes UIView is top level and only object in CustomView.xib file
		let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
		return view
	}
	
	
	@IBAction func didTapDelete(_ sender: Any) {
		if image != nil {
			image = nil
		} else {
			print(String(describing: type(of: self)),":", #function)			
		}
	}
	
	fileprivate func configure(_ view: UIView) {
		switch view {
		case deleteButton:
			deleteButton.isSelected = imageView.image == nil
		default: break
		}
	}

}

protocol PhotoThumbViewProtocol {
	var image: UIImage? { get set }
}

extension PhotoThumbView: PhotoThumbViewProtocol {
	var image: UIImage? {
		get { return imageView.image }
		set {
			imageView.image = newValue
			configure(deleteButton)
		}
	}
}
