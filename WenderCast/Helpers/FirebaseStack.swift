//
//  DataService.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/8/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
https://bitbucket.org/snippets/alexandrzhovty/xnzpA
 http://swiftandpainless.com/creating-a-smart-file-template/
 
 */

import Foundation
import Firebase
import SwiftyJSON




class FirebaseStack: BundleIdentifier {
	
	let rootRef: DatabaseReference
	
	
//	fileprivate static let _concurrentQueue = DispatchQueue(label: FirebaseStack.budleIdenitifier, attributes: .concurrent)
	
	
	fileprivate var _users: DatabaseReference {
		return rootRef.child("users")
	}
    
    fileprivate var _bans: DatabaseReference {
        return rootRef.child("banlist")
    }
	
	fileprivate var _likes: DatabaseReference {
		return rootRef.child("likes")
	}
	
	fileprivate var _messages: DatabaseReference {
		return rootRef.child("messages")
	}
	
	fileprivate var _modifyRequests: DatabaseReference {
		return rootRef.child("requests")
	}
	
    fileprivate static let `default` = FirebaseStack()
    private init() {
//        rootRef = Database.database().reference(withPath: "production")
        rootRef = Database.database().reference(withPath: "ios")
//		rootRef = Database.database().reference(withPath: "testing")
	}
	
	fileprivate static func uid(from string: String) -> String {
		let id: String
		if string == "me" {
			assert(AppDelegate.me != nil, "General user is empty")
            guard let ddid = AppDelegate.me.uid else {
                assertionFailure("App delegate user ins't set yet")
                return ""
            }
            id = ddid
		} else {
			id = string
		}
		return id
	}
}

protocol FirebaseStackProtocol {
	static var users: DatabaseReference { get }
	static var likes: DatabaseReference { get }
	static var coordinates: DatabaseReference { get }
}

// MARK: - Visits
extension FirebaseStack {
    fileprivate var _visits: DatabaseReference {
        return rootRef.child("visits")
    }
    
    /// Reference to **Visits** table
    static var visits: DatabaseReference { return FirebaseStack.default._visits }
}

// MARK: - Invitation
extension FirebaseStack {
	
	fileprivate var _invitations: DatabaseReference {
		return rootRef.child("invites")
	}
	
	/// Reference to **Invites** table
	static var invitations: DatabaseReference { return FirebaseStack.default._invitations }
	
	/// Return reference to **Ivites** table
	///
	/// - Parameter iid: invitation ID
	/// - Returns: Reference to database
	static func invitation(for id: DMInvite.Identifier) -> DatabaseReference {
		return FirebaseStack.default._invitations.child(id.rawValue)
	}
	
	/// Query firebase for open invites
	///
	/// - Parameters:
	///   - uid: User ID or 'me' if 'ME' for general user
	///   - compretion: returns found invitation
	static func currentInvitation(for userId: String, compretion: @escaping (_ inviate: JSON?) -> Void) {
		
		let uid = FirebaseStack.uid(from: userId)
		let invitesRef = FirebaseStack.invitations
		let queries = [
			invitesRef.queryOrdered(byChild: DMInvite.CodingKeys.senderID).queryEqual(toValue : uid),
			invitesRef.queryOrdered(byChild: DMInvite.CodingKeys.recipientID).queryEqual(toValue : uid)
		]
		
		let dispatchGroup = DispatchGroup()
		var invites = [String: JSON]()
		
		
		queries.forEach {
			dispatchGroup.enter()
			$0.observeSingleEvent(of: .value, with: { snapshot in
				for snap in snapshot.children {
					guard let snap = snap as? DataSnapshot else { continue }
					guard let value = snap.value as? [String: Any] else { continue }
					invites[snap.key] = JSON(value)
				}
				dispatchGroup.leave()
			})
		}
		
		
		let higherPriority = DispatchQueue.global(qos: .userInitiated)
		dispatchGroup.notify(queue: higherPriority) {
            
            queries.forEach{ $0.removeAllObservers() }
            
			let filtered = invites.values.filter() {
				return InviteState.isOpen(for: $0[DMInvite.CodingKeys.state].intValue)
			}
			
			let array = filtered.sorted{
				$0[DMInvite.CodingKeys.created].doubleValue > $1[DMInvite.CodingKeys.created].doubleValue
			}
            
            let anotherArray = array.filter() {
                
                let json = JSON($0)
                
                if let state = json["state"].int64, state == InviteState.open.rawValue {
                    if let countdownStartedAt =  json["countdownStartedAt"].int, countdownStartedAt == 0 {
                        if let created = json[DMInvite.CodingKeys.created].double {
                            
                            let timestamp: Double
                            
                            if json["inheritedFrom"].string == nil {
                                timestamp =  created + Constants.Invitation.waitForResponseToRequest * 2
                            } else {
                                timestamp =  created + Constants.Invitation.waitForResponseToModifyRequest  *  1.2
                            }
                            
                            if timestamp < Date().timeIntervalSince1970 {
                                return true
                            }
                        }
                        
                    }
                }
                
                return false
            }
			
			DispatchQueue.main.async {
				compretion(anotherArray.first)
			}
		}
		
	}
	
}
	
	
// MARK: - Likes
extension FirebaseStack {
	/// Reference to **Likes** table
	static var likes: DatabaseReference { return FirebaseStack.default._likes }
	
	/// Return reference to Favorites 'table'
	///
	/// - Parameter id: entity ID
	/// - Returns: Reference to database
	static func like(for id: String) -> DatabaseReference {
		return FirebaseStack.default._likes.child(id)
	}
}

// MARK: - Messages
extension FirebaseStack {
	/// Reference to **Messages** table
	static var messages: DatabaseReference { return FirebaseStack.default._messages }
	
	
	/// Return reference to **Chat** table
	///
	/// - Parameter inviteId: Invitation ID used for chat room
	/// - Returns: reference to chat room
	static func chat(for inviteId: String) -> DatabaseReference {
		return FirebaseStack.default._messages.child(inviteId)
	}
}

// MARK: - Storage
extension FirebaseStack {
	static var storageRef: StorageReference { return Storage.storage().reference() }
	
}

// MARK: - User
extension FirebaseStack {
	/// Reference to 'Users' table
	static var users: DatabaseReference { return FirebaseStack.default._users }
	
	
	/// Return reference to Favorites 'table'
	///
	/// - Parameter uid: User ID or 'me' if 'ME' returns reference to general user favorites
	/// - Returns: Reference to database
	static func user(for uid: String) -> DatabaseReference {
		let id: String
		if uid == "me" {
			assert(AppDelegate.me != nil, "General user is empty")
			id = AppDelegate.me!.uid!
		} else {
			id = uid
		}
	
		return FirebaseStack.default._users.child(id)
	}
}

// MARK: - Modify requests
extension FirebaseStack {
	/// Reference to **requests** table
	static var modifyRequests: DatabaseReference { return FirebaseStack.default._modifyRequests }
	
	/// Return reference to **Modify requests** table
	///
	/// - Parameter inviteId: invitation unique identifier
	/// - Returns: database reference
	static func modifyRequest(for inviteId: String) -> DatabaseReference {
		return FirebaseStack.default._modifyRequests.child(inviteId)
	}
}


extension FirebaseStack {
    /// Reference to 'Users' table
    static var bans: DatabaseReference { return FirebaseStack.default._bans }
    
    
    /// Return reference to Favorites 'table'
    ///
    /// - Parameter uid: User ID or 'me' if 'ME' returns reference to general user favorites
    /// - Returns: Reference to database
    static func ban(for id: StringIdentifier) -> DatabaseReference {
        return FirebaseStack.default._bans.child(id.rawValue)
    }
}



//class UserDatabaseReference: DatabaseReference {
//	var email: DatabaseReference { return self.child(DMUser.Field.email) }
//	var name: DatabaseReference { return self.child(DMUser.Field.name) }
//	var birthday: DatabaseReference { return self.child(DMUser.Field.birthday) }
//	
//	func `as` (left: DatabaseReference, right: UserDatabaseReference) -> UserDatabaseReference {
//		return left as! UserDatabaseReference
//	}
//	
//	
//	
//	override init() {
//		super.init()
//	}
//	
//	
//	
//}
