//
//  HistoryDataProvider.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase
import CoreData
import SwiftyJSON

class HistoryDataProvider {
    fileprivate let _operationQueue = OperationQueue()
    var observers = [Any]()
    
    deinit {
        unregisterObserver()
        
        
        
        print(String(describing: type(of: self)),":", #function, " ", _operationQueue.operationCount)
    }
    
    func fetch() {
        registerObserver()
    }
   
}

// MARK: - ObserverProtocol
extension HistoryDataProvider: ObserverProtocol {
    func unregisterObserver() {
        observers.forEach {
            
            switch $0 {
            case let dbRef as DatabaseReference:
                dbRef.removeAllObservers()
            case let queryRef as DatabaseQuery:
                queryRef.removeAllObservers()
            default:
                NotificationCenter.default.removeObserver($0)
            }
        }
        observers.removeAll()
    }
    
    func registerObserver() {
        observers.append(likesObserver())
        observers.append(invitesObserver())
        observers.append(visitsObserver())
    }
    
    func visitsObserver() -> Any {
        let query = FirebaseStack.visits
            .queryOrdered(byChild: DMLike.CodingKeys.recipientID)
            .queryEqual(toValue: AppDelegate.me.identifier.rawValue)
        
        
        query.observe(.value, with: { [weak self] snapshots in
            guard let `self` = self else { return }
            
            if snapshots.exists() {
                let operation = ParseOperation(parsingHandler: { context in
                    DMVisit.parse(snapshots, into: context)
                })
                
                self._operationQueue.addOperation(operation)
            }
            
        })
        
        return query
    }
    
    func likesObserver() -> Any {
        let likes = FirebaseStack.likes
            .queryOrdered(byChild: DMLike.CodingKeys.recipientID)
            .queryEqual(toValue: AppDelegate.me.identifier.rawValue)
        
        
        likes.observe(.value, with: { [weak self] snapshots in
            guard let `self` = self else { return }
            
            if snapshots.exists() {
                let operation = ParseOperation(parsingHandler: { context in
                    DMLike.parse(snapshots, into: context)
                })
                
                self._operationQueue.addOperation(operation)
            }
            
        })
        
        return likes
        
    }
    
    func invitesObserver() -> Any {
        let likes = FirebaseStack.invitations
            .queryOrdered(byChild: DMLike.CodingKeys.recipientID)
            .queryEqual(toValue: AppDelegate.me.identifier.rawValue)
        
        likes.observe(.value, with: { [weak self] snapshots in
            guard let `self` = self else { return }
            
            if snapshots.exists() {
                let operation = ParseOperation(parsingHandler: { context in
                    DMInvite.parse(snapshots, into: context)
                })
                
                self._operationQueue.addOperation(operation)
            }
            
        })
        
        return likes
    }
}


