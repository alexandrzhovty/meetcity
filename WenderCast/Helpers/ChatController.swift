//
//  ChatController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import NextGrowingTextView


/*
SegueHandler implementation should be added to the MeetCity
https://bitbucket.org/snippets/alexandrzhovty/xn9G9
*/

//    MARK: - Properties & variables
final class ChatController: UIViewController {
	//    MARK: Public
	var inviteModel: InviteModel!
    
    var invite: DMInvite!
	var manaagedObjextContext: NSManagedObjectContext = AppDelegate.shared.mainContext
    var observers = [Any]()
	
	//    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var growingTextView: NextGrowingTextView!
	
	
	
	//    MARK: Private
	fileprivate var _dataProvider: ChatDataProvider!
	
	
	//    MARK: Enums & Structures
    
    
    deinit {
        unregisterObserver()
    }
}

//    MARK: - View life cycle
extension ChatController  {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		// Customize appearance
		Appearance.customize(viewController: self)
		
		tableView.estimatedRowHeight = 44
		tableView.rowHeight = UITableViewAutomaticDimension
        
        self.invite = inviteModel.invite
        self.manaagedObjextContext = AppDelegate.shared.mainContext
		
		
		_dataProvider = ChatDataProvider(with: inviteModel.invite, in: manaagedObjextContext, for: tableView)
		_dataProvider.cellForRowAtItemHandler {
			[weak self] (tableView, indexPath) -> UITableViewCell in
			
			let model = MessageViewModel(message: (self?._dataProvider.object(at: indexPath))!)
			switch model.messageType {
			case .incoming:
				let cell = tableView.dequeueReusableCell(IncomingMessageCell.self, for: indexPath)
				model.configure(cell)
				return cell
			case .outcoming:
				let cell = tableView.dequeueReusableCell(OutcomingMessageCell.self, for: indexPath)
				model.configure(cell)
				return cell
			}
			
			}.fetch()
		
		
		
		// Configure tableView
		configure(tableView)
        configure(growingTextView)
		
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterObserver()
    }
}



//    MARK: - Utilities
extension ChatController  {
	fileprivate func configure(_ view: UIView) {
		switch view {
        case tableView:
			tableView.tableHeaderView?.backgroundColor = UIColor.background.main
			tableView.estimatedRowHeight = 44
			tableView.rowHeight = UITableViewAutomaticDimension
            
            
            
        case growingTextView:
            // Do any additional setup after loading the view, typically from a nib.
            
            self.growingTextView.layer.cornerRadius = 4
            self.growingTextView.backgroundColor = UIColor(white: 0.9, alpha: 1)
            self.growingTextView.textView.textContainerInset = UIEdgeInsets(top: 8, left: 0, bottom: 4, right: 0)
            self.growingTextView.textView.font = UIFont.Avenir.book.font(size: 14)
            
            
            
////            self.growingTextView.placeholderAttributedText = NSAttributedString(string: "Placeholder text",
////                                                                                attributes: [NSFontAttributeName: self.growingTextView.textView.font!,
////                                                                                             NSForegroundColorAttributeName: UIColor.gray
////                ]
//            )
        
		default: break
		}
		
	}
}

// MARK: - ObserverProtocol
extension ChatController: ObserverProtocol {
    func registerObserver() {
        let ref = FirebaseStack.invitation(for: invite.identifier).child(DMInvite.CodingKeys.cancelReason)
        
        // Cancel invitation status
        observers.append(
            ref.observe(.value, with: { [weak self] snapshot in
                guard let `self` = self else { return }
                
                if let newValue = snapshot.value as? Int {
                    if let newState = CancelDateReasons(rawValue: newValue), newState != .none  {
                        
                        let inviteState: InviteState
                        if self.inviteModel.sender.uid == AppDelegate.me.uid {
                            inviteState = InviteState.canceledBySender
                        } else {
                            inviteState = InviteState.canceledByRecipient
                        }
                        
                        self.inviteModel.state = inviteState
                        self.inviteModel.cancelReason = newState
                        
                        self.executeSegue(.showFailure, sender: nil)
                    }
                }
            })
        )
        
        // Keyboards
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func unregisterObserver() {
        observers.forEach {
            if let dbRef = $0 as? DatabaseReference {
                dbRef.removeAllObservers()
            }
        }
        observers.removeAll()
        
        NotificationCenter.default.removeObserver(self)

    }
}

// MARK: - Key board
extension ChatController {
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.inputContainerViewBottom.constant =  0
                //textViewBottomConstraint.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func keyboardDidShow(_ sender: Notification) {
        guard tableView.numberOfSections > 0 else { return }
        guard tableView.numberOfRows(inSection: 0) > 0 else { return }
        
        let indexPath = IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    
    }
}

//    MARK: - Outlet functions
extension ChatController  {
	//    MARK: Buttons
    @IBAction func didTapSent(_ sender: Any) {
        let text = growingTextView.textView.text
        
        guard text.isEmptyOrNil == false else {
            growingTextView.shake()
            return
        }
        
        
        sendButton.isEnabled = false
        
        
        
        
        
        let message = DMMessage.new(for: invite, in: self.manaagedObjextContext)
        message.body = text
        message.incoming = false
        message.reviewed = true
        
        
        message.synchronize { [weak self] result in
            
            defer {
                self?.sendButton.isEnabled = true
            }
            
            switch result {
            case .failure(let error):
                Alert.default.showError(message: error.localizedDescription)
                
            case .success:
                //                    do    { try nestedContext.save() }
                //                    catch { print(error.localizedDescription) }
                
                DispatchQueue.main.async {
                    let msg = AppDelegate.shared.mainContext.object(with: message.objectID) as! DMMessage
                    MessageManager.default.send(.message(msg: msg), completion: nil)
                    
                    self?.growingTextView.textView.text = nil
                }
                
            }
        }
        
        
    }
}

// MARK: - Navigation & SegueHandler protocol
extension ChatController: SegueHandler {
	enum SegueType: String {
		case showFailure
	}
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		switch segueType(for: identifier) {
		case .showFailure: return true
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segueType(for: segue) {
		case .showFailure:
            let vc = segue.destination as! FailureController
            vc.inviteModel = self.inviteModel
		}
	}
}
