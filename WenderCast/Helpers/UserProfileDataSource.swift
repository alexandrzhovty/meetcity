//
//  UserProfileDataSource.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

final class UserProfileDataSource: DataSource {
	
	private weak var _viewController: UserProfileController?
	private typealias SegueType = UserProfileController.SegueType
	
	init(with viewController: UserProfileController) {
		_viewController = viewController
      
     let items: [Profile] = [
         Profile(title: Strings.ReadyToMeet, type: ProfileType.switcher(currentValue: viewController.user.readyToMeet) {
				sender in
				
				AppDelegate.me.readyToMeet = sender.isOn
            let context = AppDelegate.shared.mainContext
            
            
            let readyToMeetRef = FirebaseStack.user(for: "me").child(DMUser.CodingKeys.readyToMeet)
            readyToMeetRef.setValue(AppDelegate.me.readyToMeet, withCompletionBlock: { (error, dataRef) in
               
               guard error == nil else {
                  Alert.default.showError(message: error!.localizedDescription)
                  return
               }
               
               do { try context.save() }
               catch {
                  Alert.default.showError(message: error.localizedDescription)
                  return
               }
            })
            
			}),
			Profile(title: Strings.Settings, type: ProfileType.selectable {
				[weak _viewController] sender in
				guard let viewController = _viewController else { return }
				viewController.performSegue(SegueType.showSettings, sender: sender)
			}),
			Profile(title: Strings.History.name, type: ProfileType.selectable {
				[weak _viewController] sender in
				guard let viewController = _viewController else { return }
				viewController.performSegue(SegueType.showHistory, sender: sender)
			})
		]
		
		super.init(with: items)
		
	}
	
}
