//
//  WeatherManager.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import HomeKit
import MapKit
import SwiftyJSON


struct City {
	let id: Int
	let name: String
	let weather: String
}


class WeatherFetcher {
	
	var dataTask: URLSessionDataTask!
	
	func fetch(_ response: @escaping ([City]?) -> ()) {
		
		var component = URLComponents(url: OpenWeatherMap.url, resolvingAgainstBaseURL: true)!
		component.queryItems = OpenWeatherMap.parameters
		let request = URLRequest(url: component.url!)
		
		
		
		dataTask = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
			guard let `self` = self else { return }
			
			guard error == nil else {
				print(error!.localizedDescription)
				return
			}
			
			self.decode(data!)
			
		})
		dataTask.resume()
	
		
		
		
//
//		
//		networking.request(<#T##request: URLRequest##URLRequest#>, response: <#T##(Data?) -> ()#>)
//		
//		networking.request { data in
//			let cities = data.map { self.decode($0 as Data) }
//			response(cities)
//		}
	}
	
	fileprivate func decode(_ data: Data) -> [City] {
		let json = JSON(data: data)
		var cities = [City]()
		for (_, j) in json["list"] {
			if let id = j["id"].int {
				cities.append(City(id: id, name: j["name"].string ?? "", weather: j["weather"][0]["main"].string ?? ""))
			}
		}
		
		print(cities)
		
		return cities
	}
}
