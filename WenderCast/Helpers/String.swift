//
//  String.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/16/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

protocol OptionalString {}
extension String: OptionalString {}

extension Optional where Wrapped: OptionalString {
	var isEmptyOrNil: Bool {
		return ((self as? String) ?? "").isEmpty
	}
}
