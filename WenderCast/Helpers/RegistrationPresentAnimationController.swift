//
//  RegistrationPresentAnimationController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
 http://www.appcoda.com/custom-view-controller-transitions-tutorial/
 http://mathewsanders.com/animated-transitions-in-swift/
 */

import UIKit

class RegistrationPresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
   func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
      return 0.3
   }
   
   func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
      let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
      let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
      let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
      let containerView = transitionContext.containerView
      let bounds = UIScreen.main.bounds
      
      toViewController.view.frame = finalFrameForVC.offsetBy(dx: bounds.size.width, dy: 0)
      containerView.addSubview(toViewController.view)
      
      UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0, options:
         .curveLinear, animations: { 
            fromViewController.view.alpha = 0.5
            toViewController.view.frame = finalFrameForVC
      }) {
         finished in
         fromViewController.view.alpha = 1.0
         transitionContext.completeTransition(true)
      }
      
//      UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
//         fromViewController.view.alpha = 0.5
//         toViewController.view.frame = finalFrameForVC
//      }) {
//         finished in
//         fromViewController.view.alpha = 1.0
//         transitionContext.completeTransition(true)
//      }
      
   }
}
