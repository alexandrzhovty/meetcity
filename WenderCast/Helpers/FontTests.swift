//
//  FontTests.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
 https://bitbucket.org/snippets/alexandrzhovty/bLprj/font
 */

import XCTest
@testable import MeetCity

class FontTests: XCTestCase {
   func testFonts()  {
      checkFont(UIFont.Avenir.self)
      checkFont(UIFont.LucidaGrande.self)
      
      XCTAssert(true, "\(#function) passed")
   }
   
   func checkFont<T: RawRepresentable>(_: T.Type) where T: Iteratable, T: FontConvertible, T: Hashable, T.RawValue == String {
      for type in T.hashValues() {
         let font = UIFont(name: type.rawValue, size: 10)
         XCTAssertNotNil(font, "\(type.rawValue) not found")
      }
   }
}
