//
//  VideoTourCollectionCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import AVFoundation

class VideoTourCollectionCell: UICollectionViewCell, NibLoadableView {
    //    MARK: - Properties & Variables
    //    MARK: Public
    
    //    MARK: Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    
    //    MARK: Private
    fileprivate var url: URL?
    fileprivate var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    
    var observers = [Any]()
    
    //    MARK: - Intializations & overrids
    deinit {
        unregisterObservers()
        
        player?.pause()
        playerLayer?.removeFromSuperlayer()
        playerLayer = nil
        player = nil
    }
    
    
    //    MARK: - Utilities
    //    MARK: - Utilities
    fileprivate func loadVideo() {
        guard let url = self.url else {
            return
        }
        
        //this line is important to prevent background music stop
        
        self.unregisterObservers()
        player?.pause()
        playerLayer?.removeFromSuperlayer()
        player = nil
        playerLayer = nil
        
        
        /* Creates new layer an place it into the view */
        
        
        
        player = AVPlayer(url: url)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.backgroundColor = UIColor.clear.cgColor
        
        playerLayer?.frame = containerView.bounds
        
        //TODO: It is neccessary to redisign XIB files constraints
        playerLayer?.frame.size.width = self.bounds.width // Strange behavouir
        playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
       
        playerLayer!.zPosition = -1
        
        containerView.layer.addSublayer(playerLayer!)
        containerView.layer.needsDisplayOnBoundsChange = true
        
        player?.seek(to: kCMTimeZero)
        
        registerObservers()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        playerLayer?.frame = containerView.bounds
        playerLayer?.frame.size.width = self.bounds.width
    }
    
    //    MARK: - Observers
    private func registerObservers() {
        let center = NotificationCenter.default
        let queue = OperationQueue.main
        
        observers.append(center.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: queue, using: { [weak self] (notification) in
            if let strongSelf = self {
                strongSelf.player?.seek(to: kCMTimeZero)
                strongSelf.player?.play()
            }
        }))
        
        
    }
    
    private func unregisterObservers() {
        let center = NotificationCenter.default
        for obj in observers {
            center.removeObserver(obj)
        }
        observers.removeAll()
    }
    
}

extension VideoTourCollectionCell: VideoTourCellProtocol {
    var message: String? {
        get { return messageLabel.text }
        set {
            guard let str = newValue else {
                messageLabel.attributedText = nil
                return
            }
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 4
            style.alignment = .center
            messageLabel.attributedText = NSAttributedString(string: str, attributes: [NSParagraphStyleAttributeName: style])
        
        }
    }
    
    var videoURL: URL? {
        get { return self.url }
        set {
            if self.url != newValue {
                self.url = newValue
                self.loadVideo()
            }
            self.playVideo()
        }
    }
    
    func playVideo() {
        self.player?.play()
    }
    
    func stopVideo() {
        self.player?.pause()
    }
}
