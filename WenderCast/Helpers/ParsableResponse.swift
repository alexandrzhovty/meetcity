//
//  ParsableResponse.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/15/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase

protocol ParsableResponse {}
extension DataSnapshot: ParsableResponse{}
extension Array: ParsableResponse{}
extension Dictionary: ParsableResponse {}
