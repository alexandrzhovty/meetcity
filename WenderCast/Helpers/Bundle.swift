//
//  Bundle.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

extension Bundle {
	var releaseVersionNumber: String? {
		return infoDictionary?["CFBundleShortVersionString"] as? String
	}
	var buildVersionNumber: String? {
		return infoDictionary?["CFBundleVersion"] as? String
	}
}


protocol BundleIdentifier: class {}

extension BundleIdentifier where Self: Any {
	static var budleIdenitifier: String {
		let name = String(describing: self)
		return  "\(Bundle.main.bundleIdentifier!).\(name))"
	}
}
