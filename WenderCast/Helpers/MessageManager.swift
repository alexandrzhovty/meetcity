//
//  MessageManager.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/22/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//


import Foundation
import Firebase
import UserNotifications
import SwiftyJSON
import Alamofire
import SVProgressHUD


final class MessageManager: NSObject {
	
	/// Firebase **Cloud messaging** settings
	enum CloudMessaging {
		static let serverKey = "AIzaSyDkaQqP_W32WWX2IndxSXjle4mkz6cS2kg"
		static let serverId = "350923826524"
		static let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
	}
	
	static let `default` = MessageManager()
	fileprivate var _notificationView: NotificationMessage? = nil
	fileprivate let _sendmessageGroup = DispatchGroup()
	
	
	
	/// Device token, if Firebase is used then we store `fcmToken`
	var deviceToken: String? { return  Messaging.messaging().fcmToken }
	var senderId: String { return "AIzaSyDkaQqP_W32WWX2IndxSXjle4mkz6cS2kg@gcm.googleapis.com" }
	
	private override init() {
		super.init()
		
		let delegate = AppDelegate.shared as UIApplicationDelegate
		
		
		let swizzleSelectors:[(object: AnyObject, originalSelector: Selector, swizzledSelector: Selector)] = [
			(delegate, #selector(delegate.application(_:didRegisterForRemoteNotificationsWithDeviceToken:)), #selector(self.application(_:didRegisterForRemoteNotificationsWithDeviceToken:))),
			(delegate, #selector(delegate.application(_:didFailToRegisterForRemoteNotificationsWithError:)), #selector(self.application(_:didFailToRegisterForRemoteNotificationsWithError:)))
		]
		
		swizzleSelectors.forEach { self.swizzle(for: $0.object, originalSelector: $0.originalSelector, swizzledSelector: $0.swizzledSelector) }
		
	}
	
	private func swizzle(for object: AnyObject, originalSelector: Selector, swizzledSelector: Selector) {
		let originalMethod = class_getInstanceMethod(object_getClass(object), originalSelector)
		let swizzledMethod = class_getInstanceMethod(object_getClass(self), swizzledSelector)
		
		
		let didAddMethod = class_addMethod(object_getClass(object), originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
		
		if didAddMethod {
			class_replaceMethod(object_getClass(object), swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
		} else {
			method_exchangeImplementations(originalMethod, swizzledMethod);
		}
	}
	
	/// Updates device token if user is already registered and if device token has been changed
	func updateDeviceToken() {
		
		if AppDelegate.me != nil {
            
            
			var userMessageModel = UserMessageModel(with: AppDelegate.me, in: AppDelegate.shared.mainContext)
			userMessageModel.update(self.deviceToken)
            
            AppDelegate.me.tokenIOSValues = Messaging.messaging().fcmToken
            
            
			if userMessageModel.context.hasChanges {
                
				do { try userMessageModel.synchronize() }
				catch {
					print(String(describing: type(of: self)),":", #function, " ", error.localizedDescription)
				}
			}
		}
	}
	
}

// MARK: - Firebase Manager Protocol
extension MessageManager: MessageManagerProtocol {
	
	func registerRemoteNotifications(for application: UIApplication, completion: () -> Void) {
		
		
		
		UNUserNotificationCenter.current().delegate = self
		
		let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
		UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (success, error) in	}
		Messaging.messaging().delegate = self
		FirebaseApp.configure()
		
		application.registerForRemoteNotifications()
		
		completion()
		
	}
}

//    MARK: - Push notification
extension MessageManager: UIApplicationDelegate {
	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		#if DEBUG
			var token = ""
			for i in 0..<deviceToken.count {
				token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
			}
			print("Registration succeeded! Token: ", token)
		#endif
		
		let fcmToken = Messaging.messaging().fcmToken
		print("fcmToken", fcmToken ?? "NaN" )
//        self.deviceToken = fcmToken
		
		
		
		DispatchQueue.main.async {
			Messaging.messaging().apnsToken = deviceToken
		}
	}
	
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		#if DEBUG
			print("\(#function) Registration failed!")
		#endif
	}
}








// MARK: - Notification Center Delegate
extension MessageManager: UNUserNotificationCenterDelegate {
	// Firebase notification received
	@available(iOS 10.0, *)
	func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {

		UIApplication.shared.applicationIconBadgeNumber = 0
		showNotification(notification.request.content.userInfo)
		
	}
	
	@available(iOS 10.0, *)
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		
		let notification = response.notification
		let json = JSON(notification.request.content.userInfo)

		//		let messageType: MessageType
		switch json["messageType"].string {
		
        case MessageType.Identifier.invite?:
			guard let inviteId = json["invite"].string else {
				return
			}
			
            let inviteRef = FirebaseStack.invitation(for: DMInvite.Identifier(rawValue: inviteId))
            SVProgressHUD.show()
            inviteRef.observeSingleEvent(of: .value, with: { snapshot in
                defer {
                    if SVProgressHUD.isVisible() {
                        SVProgressHUD.dismiss()
                    }
                }
                
                guard snapshot.exists() else { return }
                guard let value = snapshot.value as? [String: Any] else { return }
                
                
                DispatchQueue.main.async {
                    
                    let context = AppDelegate.shared.mainContext
                    
                    let invite = DMInvite.invite(for: inviteId, in: context) ?? {
                        let entity = DMInvite(context: context)
                        entity.id = inviteId
                        return entity
                        }()
                    
                    invite.update(with: JSON(value), into: context)
                    let model = try! InviteModel(with: invite, in: context)
                    
                    SVProgressHUD.dismiss(completion: { 
                        Presenter.presentMeetingArragement(for: model)
                    })
                }
                
            })
         
        case MessageType.Identifier.finished?: showFinishDatingNotification(json, showMessage: false)
        case MessageType.Identifier.cancel?: declineInvitationNotification(json, showMessage: false)
        case MessageType.Identifier.checkedIn?: showCheckInNotification(json, showMessage: false)
		case MessageType.Identifier.message?: showMessageNotification(json, showMessage: false)
        case MessageType.Identifier.like?: showLikeNotification(json, showMessage: false)
		default: return
			
		}
		
	}
	
	func showNotification(_ userInfo: [AnyHashable : Any] ){
		
		let json = JSON(userInfo)
		
		switch json["messageType"].string {
		
			
		case MessageType.Identifier.invite?:
			let inviteId = json["invite"].stringValue
            SVProgressHUD.show()
            let inviteRef = FirebaseStack.invitation(for: DMInvite.Identifier(rawValue: inviteId))
            inviteRef.observeSingleEvent(of: .value, with: {  snapshot in
                if snapshot.exists(), let jsonValue = snapshot.value as? [String: Any] {
                    let context = AppDelegate.shared.mainContext
                    let json = JSON(jsonValue)
                    
                    let invite = DMInvite.invite(for: inviteId, in: context) ?? {
                        let entity = DMInvite(context: context)
                        entity.id = inviteId
                        return entity
                        }()
                    
                    invite.update(with: json, into: context)
                    
                    let inviteModel: InviteModel
                    do { inviteModel = try InviteModel(with: invite, in: context) }
                    catch {
                        Alert.default.showError(message: error.localizedDescription)
                        return
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        Presenter.presentMeetingArragement(for: inviteModel)
                    })

                    
                }
            })
            
            
			
		case MessageType.Identifier.cancel?: declineInvitationNotification(json)
		
        case MessageType.Identifier.finished?: showFinishDatingNotification(json)
        case MessageType.Identifier.checkedIn?: showCheckInNotification(json)
        case MessageType.Identifier.message?: showMessageNotification(json)
            
        case MessageType.Identifier.like?: showLikeNotification(json)
        
        break
        
            guard let uid = json["uid"].string else {
                return
            }
            
            let title = json["aps"]["alert"]["title"].stringValue
            let body = json["aps"]["alert"]["body"].stringValue
            
            print(json)
            
            if AppDelegate.shared.datingStatus == .searching {
                self._notificationView = NotificationMessage(delay: 10)
                    .title(title)
                    .message(body)
                    .imageUrlStrig(json["attachment-url"].stringValue)
                    .didSelect {
                        [unowned self] in
                        
                        Presenter.presentProfileController(for: uid)
                        self._notificationView = nil
                    }
                    .didCancel {
                        [unowned self] in
                        self._notificationView = nil
                }
                
                self._notificationView?.show()
            }
            
		default: return
			
		}
		
//		NotificationMessage()
//			.title(<#T##text: String##String#>)
		
	}
    
    private func showLikeNotification(_ json: JSON, showMessage: Bool = true ) {
        var executionBlock: (() -> Void)? = nil
        
        print(json)
        
        let topController = UIApplication.topViewController()
        switch topController {
        case let rootVC as UserProfileController where rootVC.viewControllerToReturn == nil:
//            let listVC = UIStoryboard(.dashboard).instantiateViewController(NearbyListController.self)
            
            let profileVC = UIStoryboard(.nearby).instantiateViewController(NearbyProfileController.self)
            
            let viewContext = AppDelegate.shared.mainContext
            
            profileVC.managedObjectContext = viewContext
            
            if let uid = json["uid"].string {
                let user = DMUser.user(for: UserIdentifier(rawValue: uid), from: viewContext) ?? DMUser.new(with: uid, in: viewContext)
                profileVC.user = user
                
                executionBlock = {
//                    rootVC.navigationController?.pushViewController(listVC, animated: false)
                    rootVC.navigationController?.pushViewController(profileVC, animated: true)
                }
            }
         
            
        case let rootVC as NearbyListController:
            let profileVC = UIStoryboard(.nearby).instantiateViewController(NearbyProfileController.self)
            
            let viewContext = AppDelegate.shared.mainContext
            
            profileVC.managedObjectContext = viewContext
            
            if let uid = json["uid"].string {
                let user = DMUser.user(for: UserIdentifier(rawValue: uid), from: viewContext) ?? DMUser.new(with: uid, in: viewContext)
                profileVC.user = user
                
                executionBlock = {
                    rootVC.navigationController?.pushViewController(profileVC, animated: true)
                }
            }
            
        case let rootVC as NearbyProfileController where rootVC.navigationController?.viewControllers.first is UserProfileController:
            let profileVC = UIStoryboard(.nearby).instantiateViewController(NearbyProfileController.self)
            
            let viewContext = AppDelegate.shared.mainContext
            
            profileVC.managedObjectContext = viewContext
            
            if let uid = json["uid"].string {
                let user = DMUser.user(for: UserIdentifier(rawValue: uid), from: viewContext) ?? DMUser.new(with: uid, in: viewContext)
                profileVC.user = user
                
                executionBlock = {
                    rootVC.navigationController?.pushViewController(profileVC, animated: true)
                }
            }
            
        default:
            return
        }
        
        if showMessage {
            
            let imageUrlString = json["attachment-url"].stringValue
            let title = json["aps"]["alert"]["title"].stringValue
            let body = json["aps"]["alert"]["body"].stringValue
            
            self._notificationView = NotificationMessage(delay: 100)
                .title(title)
                .message(body)
                .imageUrlStrig(imageUrlString)
                .didSelect {
                    [unowned self] in
                    executionBlock?()
                    self._notificationView = nil
                }
                .didCancel {
                    [unowned self] in
                    self._notificationView = nil
            }
            
            self._notificationView?.show()
        } else {
            executionBlock?()
        }
    }
    
    private func showFinishDatingNotification(_ json: JSON, showMessage: Bool = true ) {
        var executionBlock: (() -> Void)? = nil
        
        let topController = UIApplication.topViewController()
        switch topController {
        case let profileVC as UserProfileController where profileVC.viewControllerToReturn is DatingController:
            let vc = profileVC.viewControllerToReturn as! MeetingArrangementController
            
            
            let feedbackController = UIStoryboard(.dating).instantiateViewController(FeedbackController.self)
            vc.navigationItem.hidesBackButton = true
            feedbackController.invite = vc.invite
            feedbackController.managedObjectContext = vc.managedObjectContext
            
            executionBlock = {
                profileVC.navigationController?.pushViewController(feedbackController, animated: false)
                //                vc.executeSegue(DatingController.SegueType.showChat, sender: nil)
            }
            
            
        case let vc as DatingController:
            
            
            let feedbackController = UIStoryboard(.dating).instantiateViewController(FeedbackController.self)
            vc.navigationItem.hidesBackButton = true
            feedbackController.invite = vc.invite
            feedbackController.managedObjectContext = vc.manangedObjectContext
            
            executionBlock = {
                vc.navigationController?.pushViewController(feedbackController, animated: false)
                //                vc.executeSegue(DatingController.SegueType.showChat, sender: nil)
            }
            
        
            
            
        default:
            return
        }
        
        if showMessage {
            
            let imageUrlString = json["attachment-url"].stringValue
            let title = json["aps"]["alert"]["title"].stringValue
            let body = json["aps"]["alert"]["body"].stringValue
            
            self._notificationView = NotificationMessage(delay: 10)
                .title(title)
                .message(body)
                .imageUrlStrig(imageUrlString)
                .didSelect {
                    [unowned self] in
                    executionBlock?()
                    self._notificationView = nil
                }
                .didCancel {
                    [unowned self] in
                    self._notificationView = nil
            }
            
            self._notificationView?.show()
        } else {
            executionBlock?()
        }
    }
    
    
    private func declineInvitationNotification(_ json: JSON, showMessage: Bool = true ) {
        var executionBlock: (() -> Void)? = nil
        
        let topController = UIApplication.topViewController()
        switch topController {
        case is FailureController: return
        case is MeetingArrangementController: return
            
        case let profileVC as UserProfileController where profileVC.viewControllerToReturn is MeetingArrangementController:
            let vc = profileVC.viewControllerToReturn as! MeetingArrangementController
            
            executionBlock = {
                profileVC.navigationController?.pushViewController(vc, animated: false)
                //                vc.executeSegue(DatingController.SegueType.showChat, sender: nil)
            }
            
            
        case let profileVC as UserProfileController where profileVC.viewControllerToReturn is RequestingController:
            let vc = profileVC.viewControllerToReturn as! RequestingController
            
            executionBlock = {
                profileVC.navigationController?.pushViewController(vc, animated: false)
//                vc.executeSegue(DatingController.SegueType.showChat, sender: nil)
            }
        default:
            break
        }
        
        if showMessage {
            
            let imageUrlString = json["attachment-url"].stringValue
            let title = json["aps"]["alert"]["title"].stringValue
            let body = json["aps"]["alert"]["body"].stringValue
            
            self._notificationView = NotificationMessage(delay: 10)
                .title(title)
                .message(body)
                .imageUrlStrig(imageUrlString)
                .didSelect {
                    [unowned self] in
                    executionBlock?()
                    self._notificationView = nil
                }
                .didCancel {
                    [unowned self] in
                    self._notificationView = nil
            }
            
            self._notificationView?.show()
        } else {
            executionBlock?()
        }
    }
    
    
    
    
    private func showCheckInNotification(_ json: JSON, showMessage: Bool = true ) {
        var executionBlock: (() -> Void)? = nil
        
        let topController = UIApplication.topViewController()
        switch topController {
        case let profileVC as UserProfileController where profileVC.viewControllerToReturn is DatingController:
            let vc = profileVC.viewControllerToReturn as! DatingController
            
            executionBlock = {
                profileVC.navigationController?.pushViewController(vc, animated: false)
                vc.executeSegue(DatingController.SegueType.showChat, sender: nil)
            }
        default:
            break
        }
        
        if showMessage {
            
            let imageUrlString = json["attachment-url"].stringValue
            let title = json["aps"]["alert"]["title"].stringValue
            let body = json["aps"]["alert"]["body"].stringValue
            
            self._notificationView = NotificationMessage(delay: 10)
                .title(title)
                .message(body)
                .imageUrlStrig(imageUrlString)
                .didSelect {
                    [unowned self] in
                    executionBlock?()
                    self._notificationView = nil
                }
                .didCancel {
                    [unowned self] in
                    self._notificationView = nil
            }
            
            self._notificationView?.show()
        } else {
            executionBlock?()
        }
    }
    
    private func showMessageNotification(_ json: JSON, showMessage: Bool = true ) {
       
        
        
        var executionBlock: (() -> Void)? = nil
        
        let topController = UIApplication.topViewController()
        switch topController {
        case is ChatController:  return // do nothing
        case let vc as DatingController:  // show message on click goto chat controller
            executionBlock = {
                vc.executeSegue(DatingController.SegueType.showChat, sender: nil)
            }
        
        case let profileVC as UserProfileController where profileVC.viewControllerToReturn is DatingController:
            let vc = profileVC.viewControllerToReturn as! DatingController
            
            executionBlock = {
                profileVC.navigationController?.pushViewController(vc, animated: false)
                vc.executeSegue(DatingController.SegueType.showChat, sender: nil)
            }
        default:
            break
        }
        
        if showMessage {
            
            let imageUrlString = json["attachment-url"].stringValue
            let title = json["aps"]["alert"]["title"].stringValue
            let body = json["aps"]["alert"]["body"].stringValue
            
            self._notificationView = NotificationMessage(delay: 10)
                .title(title)
                .message(body)
                .imageUrlStrig(imageUrlString)
                .didSelect {
                    [unowned self] in
                    executionBlock?()
                    self._notificationView = nil
                }
                .didCancel {
                    [unowned self] in
                    self._notificationView = nil
            }
            
            self._notificationView?.show()
        } else {
            executionBlock?()
        }
        
        
       
    }
	
	
}


// MARK: - Messaging Delegate
extension MessageManager: MessagingDelegate {
	func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
		print(#function)
		updateDeviceToken()
	}
	
	
	
}

// MARK: - Send message protocol
extension MessageManager: SendMessageProtocol {
	func send(_ type: MessageType, completion: (() -> Void)?) {
		
		var request = URLRequest(url: CloudMessaging.url)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.setValue("key=\(CloudMessaging.serverKey)", forHTTPHeaderField: "Authorization")
		
		
		for jsonData in type.notifications {
            
            print(jsonData)
            
			request.httpBody = jsonData
			_sendmessageGroup.enter()
			let task = URLSession.shared.dataTask(with: request) { data, response, error in
				
				guard let data = data, let response = response, error == nil else {
					print("Error=\(error!.localizedDescription)")
					return
				}
				
				if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
					// check for http errors
					print("Status Code should be 200, but is \(httpStatus.statusCode)")
					print("Response = \(response)")
				}
				
				let responseString = String(data: data, encoding: .utf8) ?? "NaN"
				print("responseString = \(responseString)")
				
				self._sendmessageGroup.leave()
			}
			task.resume()
			
		}
		
		_sendmessageGroup.notify(queue: DispatchQueue.main) { 
			completion?()
		}
		
		
	}
}



