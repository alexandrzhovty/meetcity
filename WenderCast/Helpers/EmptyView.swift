//
//  EmptyView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/22/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class EmptyView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.isHidden = true
    }
    
    var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    var image: UIImage? {
        get { return imageView.image }
        set { imageView.image = newValue }
    }
    
    override var isHidden: Bool {
        get { return imageView.isHidden }
        set {
            guard imageView != nil else { return }
            guard titleLabel != nil else { return }
            
            imageView.isHidden = newValue
            titleLabel.isHidden = newValue
            
        }
    }
    
    func setHidden(_ hidden: Bool, animated: Bool) {
        guard imageView != nil else { return }
        guard titleLabel != nil else { return }
        
        // Prepare for animations
        if isHidden {
            self.imageView.alpha = 0
            self.titleLabel.alpha = 0
            self.imageView.isHidden = false
            self.titleLabel.isHidden = false
        }
        
        /// Animations
        let animations = { [unowned self] () -> Void in
            let alpha: CGFloat = hidden ? 0.0 : 1.0
            self.imageView.alpha = alpha
            self.titleLabel.alpha = alpha
        }
        
        /// COmpletion
        let completion = { [unowned self] (finished: Bool) -> Void in
            self.isHidden = hidden
        }
        
        UIView.animate(withDuration: 0.25, animations: animations, completion: completion)
    }

}
