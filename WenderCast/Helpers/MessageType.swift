//
//  File.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON



enum MessageRecieveType {
	case like(from: DMUser)
	case invite(request: DMInvite)
    
	
	init?(with json: JSON, in context: NSManagedObjectContext) {
		switch json["messageType"].string {
		case MessageType.Identifier.like?:
			guard let uid = json["uid"].string else {
				assertionFailure("Missing `uid` field")
				return nil
			}
			
			let user = DMUser.user(for: UserIdentifier(rawValue: uid), from: context) ?? {
				let entity = DMUser(context: context)
				entity.uid = uid
				entity.name = json["notification"]["body"].stringValue.components(separatedBy: ", ").first
				return entity
				}()
			
			self = .like(from: user)
			
		case MessageType.Identifier.invite?:
//			guard let id = json["invite"].string else {
//				return
//			}
			

			return nil
			
			
			
		default: return nil
			
		}
	}

	
}

enum MessageType {
	case like(to: DMUser)
	case invite(request: DMInvite)
    case accept(invite: DMInvite)
	case cancel(request: DMInvite, reason: InviteState)
    case modify(newInvitatio: DMInvite, from: DMInvite)
    case checkedIn(invite: DMInvite)
    case message(msg: DMMessage)
    case finish(dating: DMInvite)
	
	struct Identifier {
		static let like = "MeetCity.Messaging.like"
		static let invite = "MeetCity.Messaging.invite.send"
        static let accept = "MeetCity.Messaging.invite.accepted"
		static let cancel = "MeetCity.Messaging.invite.declined"
        static let modify = "MeetCity.Messaging.invite.modified"
        static let checkedIn = "MeetCity.Messaging.invite.checkedIn"
        static let message = "MeetCity.Messaging.invite.message"
        static let finished = "MeetCity.Messaging.invite.finished"
	}
	
	var identifier: String {
		switch self {
		case .like: return Identifier.like
		case .invite: return Identifier.invite
        case .accept: return Identifier.accept
		case .cancel: return Identifier.cancel
        case .checkedIn: return Identifier.checkedIn
        case .modify: return Identifier.modify
        case .message: return Identifier.message
        case .finish: return Identifier.finished
		}
	}
}

	
// MARK: - Send notification
extension MessageType {
	
	var title: String {
		return AppDelegate.me.title
	}
	
	var body: String {
		switch self {
		case .like:
			return Strings.messageType.somebodyLikedYou
		case .invite:
            return Strings.messageType.sentInvitation
            
        case .accept:
            return Strings.messageType.acceptedInvitation
            
		case let .cancel(_, reason):
			switch reason {
            case .declinedByRecipient:
                return Strings.messageType.declineInvitation
            case .canceledByRecipient:
                return Strings.messageType.declineInvitation
                
            case .declinedExpired:
                return Strings.messageType.canceledInvitation
                
            default:
                return Strings.messageType.canceledInvitation
			}
        case let .checkedIn(invite):
            let format = Strings.messageType.checkedIn
            return String.localizedStringWithFormat(format, invite.venueName ?? "the venue")
          
            
        case .modify:
            return "Asked you modidy dating"
            
        case let .message(msg):
            return msg.body ?? Strings.SentMessage
            
        case .finish: return Strings.messageType.finishedDating
		}
	}
	
	/// Array of notifications which should be sent
	var notifications: [Data] {
		var array = [Data]()
		
		switch self {
		case let .like(user):
			
            
            
            
    
            
            
			for token in user.tokens(for: .apple) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].string = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
				json["to"].stringValue = token
                json["notification"] = JSON([:])
                json["notification"]["body"].string = self.body
                json["notification"]["title"].string = self.title
                json["notification"]["sound"].string = "default"
                
                json["content_available"].boolValue = false
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }

			}
            
            for token in user.tokens(for: .android) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].string = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["to"].stringValue = token
                json["data"]["body"].string = self.body
                json["data"]["title"].string = self.title
                json["data"]["sound"].string = "default"
                
                
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            

			
		case let .invite(request):
//			let inviteModel = try! InviteModel(with: request, in: request.managedObjectContext!)
//			let userMessageModel = UserMessageModel(with: inviteModel.opponent, in: inviteModel.context)
//			
//			for token in userMessageModel.deviceTokens {
//				var json = JSON([:])
//				
//				json["to"].stringValue = token
//				
//				json["notification"] = JSON([:])
//				json["notification"]["body"].string = self.body
//				json["notification"]["title"].string = self.title
//                json["notification"]["sound"].string = "default"
//				
//				json["data"] = JSON([:])
//				json["data"]["uid"].stringValue = userMessageModel.fromUid
//				json["data"]["messageType"].stringValue = self.identifier
//                if let sURL = AppDelegate.me.profilePhotoUrl {
//                    json["data"]["attachment-url"].string = sURL
//                }
//				json["data"]["invite"].string = inviteModel.id
//				
//				
//				json["content_available"].boolValue = false
//				
//				do {
//					let jsonData =  try json.rawData(options: .prettyPrinted)
//					array.append(jsonData)
//				}
//				catch {
//					print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
//					assertionFailure()
//					continue
//				}
//			}
            
            
            for token in request.opponent.tokens(for: .apple) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                json["data"]["invite"].string = request.identifier.rawValue
                json["to"].stringValue = token
                json["notification"] = JSON([:])
                json["notification"]["body"].string = self.body
                json["notification"]["title"].string = self.title
                json["notification"]["sound"].string = "default"
                
                json["content_available"].boolValue = false
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
            for token in request.opponent.tokens(for: .android) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                json["data"]["invite"].string = request.identifier.rawValue
                json["to"].stringValue = token
                json["data"]["body"].string = self.body
                json["data"]["title"].string = self.title
                json["data"]["sound"].string = "default"
                
                
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
			
		case let .cancel(request, reason):
//			let inviteModel = try! InviteModel(with: request, in: request.managedObjectContext!)
//			let userMessageModel = UserMessageModel(with: inviteModel.opponent, in: inviteModel.context)
//			
//			for token in userMessageModel.deviceTokens {
//				var json = JSON([:])
//				
//				json["to"].stringValue = token
//				
//				json["notification"] = JSON([:])
//				json["notification"]["body"].string = self.body
//				json["notification"]["title"].string = self.title
//                json["notification"]["sound"].string = "default"
//				
//				json["data"] = JSON([:])
//				json["data"]["uid"].stringValue = userMessageModel.fromUid
//				json["data"]["messageType"].stringValue = self.identifier
//                if let sURL = AppDelegate.me.profilePhotoUrl {
//                    json["data"]["attachment-url"].string = sURL
//                }
//				json["data"]["invite"].string = inviteModel.id
//				json["data"]["inviteState"].int64 = reason.rawValue
//				
//				json["content_available"].boolValue = false
//				
//				do {
//					let jsonData =  try json.rawData(options: .prettyPrinted)
//					array.append(jsonData)
//				}
//				catch {
//					print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
//					assertionFailure()
//					continue
//				}
//			}
            
            
            for token in request.opponent.tokens(for: .apple) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = request.identifier.rawValue
                json["data"]["inviteState"].int64 = reason.rawValue
                
                json["to"].stringValue = token
                json["notification"] = JSON([:])
                json["notification"]["body"].string = self.body
                json["notification"]["title"].string = self.title
                json["notification"]["sound"].string = "default"
                
                json["content_available"].boolValue = false
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
            for token in request.opponent.tokens(for: .android) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = request.identifier.rawValue
                json["data"]["inviteState"].int64 = reason.rawValue
                
                json["to"].stringValue = token
                json["data"]["body"].string = self.body
                json["data"]["title"].string = self.title
                json["data"]["sound"].string = "default"
                
                
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
        case let .checkedIn(request):
//            let inviteModel = try! InviteModel(with: request, in: request.managedObjectContext!)
//            let userMessageModel = UserMessageModel(with: inviteModel.opponent, in: inviteModel.context)
//            
//            for token in userMessageModel.deviceTokens {
//                var json = JSON([:])
//                
//                json["to"].stringValue = token
//                
//                json["notification"] = JSON([:])
//                json["notification"]["body"].string = self.body
//                json["notification"]["title"].string = self.title
//                json["notification"]["sound"].string = "default"
//
//                
//                json["data"] = JSON([:])
//                json["data"]["uid"].stringValue = userMessageModel.fromUid
//                json["data"]["messageType"].stringValue = self.identifier
//                if let sURL = AppDelegate.me.profilePhotoUrl {
//                    json["data"]["attachment-url"].string = sURL
//                }
//                json["data"]["invite"].string = inviteModel.id
//                
//                
//                json["content_available"].boolValue = false
//                
//                do {
//                    let jsonData =  try json.rawData(options: .prettyPrinted)
//                    array.append(jsonData)
//                }
//                catch {
//                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
//                    assertionFailure()
//                    continue
//                }
//            }
            
            
            for token in request.opponent.tokens(for: .apple) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = request.identifier.rawValue
            
                
                json["to"].stringValue = token
                json["notification"] = JSON([:])
                json["notification"]["body"].string = self.body
                json["notification"]["title"].string = self.title
                json["notification"]["sound"].string = "default"
                
                json["content_available"].boolValue = false
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
            for token in request.opponent.tokens(for: .android) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = request.identifier.rawValue
               
                
                json["to"].stringValue = token
                json["data"]["body"].string = self.body
                json["data"]["title"].string = self.title
                json["data"]["sound"].string = "default"
                
                
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
        case let .modify(newInvitatio, from):
            break
            
        case let .accept(invite):
            
//            for token in invite.opponent.deviceTokens {
//                var json = JSON([:])
//                
//                json["to"].stringValue = token
//                
//                json["notification"] = JSON([:])
//                json["notification"]["body"].string = self.body
//                json["notification"]["title"].string = self.title
//                json["notification"]["sound"].string = "default"
//                
//                json["data"] = JSON([:])
//                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
//                json["data"]["messageType"].stringValue = self.identifier
//                if let sURL = AppDelegate.me.profilePhotoUrl {
//                    json["data"]["attachment-url"].string = sURL
//                }
//                json["data"]["invite"].string = invite.identifier.rawValue
//                json["data"]["inviteState"].int64 = invite.stateValue
//                
//                json["content_available"].boolValue = false
//                
//                do {
//                    let jsonData =  try json.rawData(options: .prettyPrinted)
//                    array.append(jsonData)
//                }
//                catch {
//                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
//                    assertionFailure()
//                    continue
//                }
//            }
            
            
            for token in invite.opponent.tokens(for: .apple) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = invite.identifier.rawValue
                json["data"]["inviteState"].int64 = invite.stateValue
                
                json["to"].stringValue = token
                json["notification"] = JSON([:])
                json["notification"]["body"].string = self.body
                json["notification"]["title"].string = self.title
                json["notification"]["sound"].string = "default"
                
                json["content_available"].boolValue = false
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
            for token in invite.opponent.tokens(for: .android) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = invite.identifier.rawValue
                json["data"]["inviteState"].int64 = invite.stateValue
                
                json["to"].stringValue = token
                json["data"]["body"].string = self.body
                json["data"]["title"].string = self.title
                json["data"]["sound"].string = "default"
                
                
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
        case let .message(msg):
            
//            let tokens = msg.invite?.opponent.deviceTokens ?? []
//            for token in tokens {
//                var json = JSON([:])
//                
//                json["to"].stringValue = token
//                
//                json["notification"] = JSON([:])
//                json["notification"]["body"].string = msg.body
//                json["notification"]["title"].string = self.title
//                json["notification"]["sound"].string = "default"
//                
//                json["data"] = JSON([:])
//                json["data"]["uid"].string = AppDelegate.me.identifier.rawValue
//                json["data"]["messageType"].stringValue = self.identifier
//                if let sURL = AppDelegate.me.profilePhotoUrl {
//                    json["data"]["attachment-url"].string = sURL
//                }
//                
//                do {
//                    let jsonData =  try json.rawData(options: .prettyPrinted)
//                    array.append(jsonData)
//                }
//                catch {
//                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
//                    assertionFailure()
//                    continue
//                }
//            }
            
            guard let invite = msg.invite else { break }
            
            for token in invite.opponent.tokens(for: .apple) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
//                json["data"]["invite"].string = invite.identifier.rawValue
//                json["data"]["inviteState"].int64 = invite.stateValue
                
                json["to"].stringValue = token
                json["notification"] = JSON([:])
                json["notification"]["body"].string = msg.body
                json["notification"]["title"].string = self.title
                json["notification"]["sound"].string = "default"
                
                json["content_available"].boolValue = false
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
            for token in invite.opponent.tokens(for: .android) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
//                json["data"]["invite"].string = invite.identifier.rawValue
//                json["data"]["inviteState"].int64 = invite.stateValue
                
                json["to"].stringValue = token
                json["data"]["body"].string = msg.body
                json["data"]["title"].string = self.title
                json["data"]["sound"].string = "default"
                
                
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
        case let .finish(invite):
//            for token in invite.opponent.deviceTokens {
//                var json = JSON([:])
//                
//                json["to"].stringValue = token
//                
//                json["notification"] = JSON([:])
//                json["notification"]["body"].string = self.body
//                json["notification"]["title"].string = self.title
//                json["notification"]["sound"].string = "default"
//                
//                json["data"] = JSON([:])
//                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
//                json["data"]["messageType"].stringValue = self.identifier
//                if let sURL = AppDelegate.me.profilePhotoUrl {
//                    json["data"]["attachment-url"].string = sURL
//                }
//                json["data"]["invite"].string = invite.identifier.rawValue
//                json["data"]["inviteState"].int64 = invite.stateValue
//                
//                json["content_available"].boolValue = false
//                
//                do {
//                    let jsonData =  try json.rawData(options: .prettyPrinted)
//                    array.append(jsonData)
//                }
//                catch {
//                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
//                    assertionFailure()
//                    continue
//                }
//            }
            
            
            for token in invite.opponent.tokens(for: .apple) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = invite.identifier.rawValue
                json["data"]["inviteState"].int64 = invite.stateValue
                
                json["to"].stringValue = token
                json["notification"] = JSON([:])
                json["notification"]["body"].string = self.body
                json["notification"]["title"].string = self.title
                json["notification"]["sound"].string = "default"
                
                json["content_available"].boolValue = false
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
            
            for token in invite.opponent.tokens(for: .android) {
                
                var json = JSON([:])
                
                json["data"] = JSON([:])
                json["data"]["uid"].stringValue = AppDelegate.me.identifier.rawValue
                json["data"]["messageType"].stringValue = self.identifier
                if let sURL = AppDelegate.me.profilePhotoUrl {
                    json["data"]["attachment-url"].string = sURL
                }
                
                json["data"]["invite"].string = invite.identifier.rawValue
                json["data"]["inviteState"].int64 = invite.stateValue
                
                json["to"].stringValue = token
                json["data"]["body"].string = self.body
                json["data"]["title"].string = self.title
                json["data"]["sound"].string = "default"
                
                
                
                do {
                    let jsonData =  try json.rawData(options: .prettyPrinted)
                    array.append(jsonData)
                }
                catch {
                    print(String(describing: type(of: self)),":", #function, "  ", error.localizedDescription)
                    assertionFailure()
                    continue
                }
                
            }
		}
		
		return array
	}
	
	
}

extension Strings {
	enum messageType {
		public static let somebodyLikedYou = NSLocalizedString("liked you profile", comment: "")
		public static let sentInvitation = NSLocalizedString("sent invitation", comment: "")
		public static let declineInvitation = NSLocalizedString("declined invitation", comment: "")
        public static let canceledInvitation = NSLocalizedString("canceled invitation", comment: "")
		public static let DolgoDymaesh = NSLocalizedString("expired", comment: "")
        public static let checkedIn = NSLocalizedString("has arrived to %@.", comment: "")
        public static let timeIsGone = NSLocalizedString("time is gonea", comment: "")
        public static let acceptedInvitation = NSLocalizedString("has accepted your invitation", comment: "")
        public static let finishedDating = NSLocalizedString("finished dating", comment: "")
		
	}
}
