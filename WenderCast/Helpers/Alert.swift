//
//  Alert.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 4/11/2015.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit


final class Alert {
   
   static let `default` = Alert()
   
   var popupWindow : UIWindow!
   var rootVC : UIViewController!
   
   
   @available(*, deprecated, message: "Use `default` property instead")
   class var sharedInstance: Alert {
      struct SingletonWrapper {
         static let sharedInstance = Alert()
      }
      
      return SingletonWrapper.sharedInstance;
   }
   
   
   fileprivate init() {
      let screenBounds = UIScreen.main.bounds
      popupWindow = UIWindow(frame: CGRect(x: 0, y: 0, width: screenBounds.width, height: screenBounds.height))
      popupWindow.windowLevel = UIWindowLevelStatusBar + 1
      
      rootVC = StatusBarShowingViewController()
      popupWindow.rootViewController = rootVC
      
   }

   func showButton(_ title: String, message: String, buttonTitle: String, onComplete: @escaping ()->Void = { _ in }) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onComplete()
         }))
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   
   func showOk(_ title: String, message: String, onComplete: @escaping ()->Void = { _ in }) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onComplete()
         }))
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
	
	func showError(_ title: String = Strings.Error, message: String, onComplete: @escaping ()->Void = { _ in }) {
		DispatchQueue.main.async {
			self.popupWindow.isHidden = false
			let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { _ in
				self.resignPopupWindow()
				onComplete()
			}))
			self.rootVC.present(alert, animated: true, completion: nil)
		}
	}
	
   func showOkCancel(_ title: String, message: String, onComplete: ((Void)->Void)?, onCancel: ((Void)->Void)?) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onComplete?()
         })
         let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { _ in
            self.resignPopupWindow()
            onCancel?()
         })
         alert.addAction(cancelAction)
         alert.addAction(okAction)
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   func showYesNo(_ title: String, message: String, onYes: @escaping ()->Void = {_ in}, onNo: @escaping ()->Void = {_ in}) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onYes()
         })
         let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onNo()
         })
         alert.addAction(cancelAction)
         alert.addAction(okAction)
         
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   func showConfirmCancel(_ title: String, message: String, onConfirm: @escaping ()->Void = {_ in}, onCancel: @escaping ()->Void = {_ in}) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onConfirm()
         })
         let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onCancel()
         })
         alert.addAction(cancelAction)
         alert.addAction(okAction)
         
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   func showConfirmChange(_ title: String, message: String, onConfirm: @escaping ()->Void = {_ in}, onChange: @escaping ()->Void = {_ in}) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onConfirm()
         })
         let cancelAction = UIAlertAction(title: "Change", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onChange()
         })
         alert.addAction(cancelAction)
         alert.addAction(okAction)
         
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   func showOkChange(_ title: String, message: String, onOk: @escaping ()->Void = {_ in}, onChange: @escaping ()->Void = {_ in}) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onOk()
         })
         let cancelAction = UIAlertAction(title: "Change", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onChange()
         })
         alert.addAction(cancelAction)
         alert.addAction(okAction)
         
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   func showLetsgoLater(_ title: String, message: String, onLetsGo: @escaping ()->Void = {_ in}, onLater: @escaping ()->Void = {_ in}) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         let letsGoAction = UIAlertAction(title: "Let's Go", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onLetsGo()
         })
         let laterAction = UIAlertAction(title: "Later", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onLater()
         })
         alert.addAction(laterAction)
         alert.addAction(letsGoAction)
         
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   func showOkNo(_ title: String, message: String, onOk: @escaping ()->Void = {_ in}, onNo: @escaping ()->Void = {_ in}) {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = false
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
         let letsGoAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onOk()
         })
         let laterAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { _ in
            self.resignPopupWindow()
            onNo()
         })
         alert.addAction(laterAction)
         alert.addAction(letsGoAction)
         
         self.rootVC.present(alert, animated: true, completion: nil)
      }
   }
   
   func resignPopupWindow() {
      DispatchQueue.main.async {
         self.popupWindow.isHidden = true
      }
   }
   
}

final class StatusBarShowingViewController: UIViewController {
	override var prefersStatusBarHidden : Bool {
		return false
	}
}
