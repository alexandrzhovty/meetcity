//
//  ProfilePresenter.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

protocol ProfilePresenterControllerProtocol: class {
	var dataSource: DataSource! { get set }
}


class ProfilePresenter: NSObject {
	var profilePresenterController: ProfilePresenterControllerProtocol
	
	init(with profilePresenterController: ProfilePresenterControllerProtocol) {
		self.profilePresenterController = profilePresenterController
		super.init()
	}
	
	func setup(_ tableView: UITableView) {
		
		tableView.estimatedRowHeight = Constants.TableView.profileCellHeight
		tableView.rowHeight = UITableViewAutomaticDimension
		
		tableView.register(SwitchedProfileCell.self)
		tableView.register(SelectableProfileCell.self)
		tableView.register(TextEditProfileCell.self)
		tableView.register(ActionProfileCell.self)
		
		if tableView.style == .grouped {
			tableView.register(UINib.init(nibName: "HeaderSectionView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderSectionView")
		}
		
		if tableView.delegate == nil {
			tableView.delegate = self
		}
		if tableView.dataSource == nil {
			tableView.dataSource = self
		}
		
	}
}


extension UITableView {
	
	func dequeueReusableCell(with model: ProfileViewModel, for indexPath: IndexPath) -> UITableViewCell {
		let cell: UITableViewCell
		switch model.type {
		case .switcher:
			cell = self.dequeueReusableCell(SwitchedProfileCell.self, for: indexPath)
			model.configure(cell)
		case .selectable:
			cell = self.dequeueReusableCell(SelectableProfileCell.self, for: indexPath)
			model.configure(cell)
		case .textEditabled:
			cell = self.dequeueReusableCell(TextEditProfileCell.self, for: indexPath)
			model.configure(cell)
		case .buttonLike:
			cell = self.dequeueReusableCell(ActionProfileCell.self, for: indexPath)
			model.configure(cell)
		}
		
		return cell
	}
}

//   MARK: - Table view protocol
extension ProfilePresenter: UITableViewDataSource, UITableViewDelegate {
	//	MARK: Data source
	func numberOfSections(in tableView: UITableView) -> Int {
		return profilePresenterController.dataSource.numberOfSections
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return profilePresenterController.dataSource.numberOfItems(in: section)
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let model = profilePresenterController.dataSource.objectAt(at: indexPath)
		return tableView.dequeueReusableCell(with: model, for: indexPath)
	}
	
	//	MARK: Delegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let model = profilePresenterController.dataSource.objectAt(at: indexPath)
		let cell = tableView.cellForRow(at: indexPath)
		switch model.type {
		case .switcher: break
		case .textEditabled:
			guard let cell = tableView.cellForRow(at: indexPath) as? TextEditProfileCell else { break }
			if !cell.textView.isFirstResponder {
				cell.textView.becomeFirstResponder()
			}
		case let .buttonLike(action):
			action(cell)
			tableView.deselectRow(at: indexPath, animated: true)
		case let .selectable(action):
			action(cell)
		}
	}
    
    
	
	//	MARK: Section header & footer information
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if tableView.style == .grouped {
			return tableView.sectionHeaderHeight * 2
		} else {
			return tableView.sectionHeaderHeight
		}
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		if tableView.style == .grouped {
			let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderSectionView") as! HeaderSectionView
			headerView.title = profilePresenterController.dataSource[section].title
			return headerView
		} else {
			return nil
		}
		
	}
	
}

