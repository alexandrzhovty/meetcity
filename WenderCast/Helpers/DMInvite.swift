//
//  DMInvite.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/22/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation
import SwiftyJSON
import Firebase

// MARK: - Overrides
extension DMInvite {
    /// Create new invite, filling in with default values
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        
        super.type = .invite
        
        let scheduledDate = Calendar.current.date(byAdding: .hour, value: 2, to: Date())
        self.scheduled = scheduledDate!.timeIntervalSince1970
    }
    
}

extension DMInvite {

    
    /// Inherites a new invite from the the old one
    ///
    /// - Parameters:
    ///   - invite: the entity that should be inherited
    ///   - context: where the new invite should be stored in
    /// - Returns: newly created entity
    class func inherit(from invite: DMInvite, into context: NSManagedObjectContext) -> DMInvite {
        let inviteUsersModel = InviteUsersViewModel(with: invite, for: context)
        
        // Creates new enityt but now sender will be ME, the opponent will be recipient
        let entity = DMInvite.new(for: invite.opponent, in: context)
        
        
        // Copy venues parametrs
        entity.venueId = invite.venueId
        entity.venueName = invite.venueName
        entity.venueAddress = invite.venueAddress
        entity.venueLat = invite.venueLat
        entity.venueLng = invite.venueLng
        entity.venueRate = invite.venueRate
        
        // Copy scheduled and notes, created time will be changed when request will be sent to the recipient
        entity.scheduled = invite.scheduled
        entity.notes = invite.notes
        
        // Reference to the the invite the new one was inherited from
        entity.inheritedFrom = invite.id
        
        return entity
    }
	
	/// Serch for Invite with given id
	///
	/// - Parameters:
	///   - id: Invite ID
	///   - context: where should be searched in
	/// - Returns: found entity ortherwise `nil`
	class func invite(for id: String, in context: NSManagedObjectContext) -> DMInvite? {
		let request: NSFetchRequest<DMInvite> = DMInvite.fetchRequest()
		request.predicate = NSPredicate(format: "%K = %@", #keyPath(DMInvite.id), id)
		request.fetchLimit = 1
		let results = try! context.fetch(request)
		return results.first
	}
	
	
}

//    MARK: - Parsing
extension DMInvite {
	/// Parse JSON response from server
	///
	/// - Parameters:
	///   - json: array of JSON objects
	func update(with record: JSON, into context: NSManagedObjectContext) {
		assert(self.managedObjectContext == context, "User stored in another context")
        
		
		// Main fields
		self.stateValue = record[CodingKeys.state].int64 ?? Int64(InviteState.error.rawValue)
		self.notes = record[CodingKeys.notes].stringValue
        self.createdValue = record[CodingKeys.created].doubleValue
		self.venueAddress = record[CodingKeys.placeAddress].stringValue
		self.venueId = record[CodingKeys.placeID].string
		self.venueLat = record[CodingKeys.placeLat].double ?? 0
		self.venueLng = record[CodingKeys.placeLng].double ?? 0
		self.venueName = record[CodingKeys.placeName].string
		self.venueRate = record[CodingKeys.placeRate].int16Value
		self.scheduled = record[CodingKeys.scheduled].double ?? 0
		self.recipientCheckined = record[CodingKeys.recipientCheckined].bool ?? false
		self.senderCheckined = record[CodingKeys.senderCheckined].bool ?? false
		self.cancelReason = record[CodingKeys.cancelReason].int16 ?? -1
        self.inheritedFrom = record[CodingKeys.inheritedFrom].string
        self.countdownStartedAt = record[CodingKeys.countdownStartedAt].doubleValue
        
        
        
        
//		let recipientID = record[CodingKeys.recipientID].stringValue
//		
//		self.recipient = DMUser.user(for: UserIdentifier(rawValue: recipientID), from: context) ?? {
//			let entity = DMUser(context: context)
//			entity.uid = recipientID
//			return entity
//		}()
//		
//		let senderID = record[CodingKeys.senderID].stringValue
//		self.sender = DMUser.user(for: UserIdentifier(rawValue: senderID), from: context) ?? {
//			let entity = DMUser(context: context)
//			entity.uid = senderID
//			return entity
//		}()
        
        
        
        
        
        // Sender
        let senderID = record[CodingKeys.senderID].stringValue
        self.sender = DMUser.user(for: UserIdentifier(rawValue: senderID), from: context) ?? {
            let user = DMUser.new(with: senderID, in: context)
//            users[senderId] = user
            return user
            }()
        
        self.sender.name = record[CodingKeys.senderName].stringValue
        self.sender.birthday = record[CodingKeys.senderBirthday].doubleValue
        self.sender.profilePhotoUrl = record[CodingKeys.senderPhoto].stringValue
        self.sender.occupation = record[CodingKeys.senderOccupation].string
        
        // Recipient
        let recipientID = record[CodingKeys.recipientID].stringValue
        self.recipient = DMUser.user(for: UserIdentifier(rawValue: recipientID), from: context) ??  {
            let user = DMUser.new(with: recipientID, in: context)
//            users[recepientId] = user
            return user
            }()
        
        self.recipient.name = record[CodingKeys.recipientName].stringValue
        self.recipient.birthday = record[CodingKeys.recipientBirthday].doubleValue
        self.recipient.profilePhotoUrl = record[CodingKeys.recipientPhoto].stringValue
        self.recipient.occupation = record[CodingKeys.recipientOccupation].string
        
		
	}
    
    
    /// Parsing server response
    ///
    /// - Parameters:
    ///   - response: should be `[key: value]` or `[[key: value]]`
    ///   - context: where the parsed results should be stored in
    ///   - user: parsed messages should belong to
    class func parse(_ response: ParsableResponse, into context: NSManagedObjectContext) {
        
        //        let user = context.object(with: user.objectID) as! DMUser
        
        
        let responsToParse: [[String: Any]]
        
        switch response {
        case let snapshots as DataSnapshot:
            guard let value = snapshots.value as? [String: [String: Any]] else { return }
            responsToParse = value.map{ $1 }
            
        case let dictionary as Dictionary<String, Any>:
            responsToParse = [dictionary]
            
        default:
            assertionFailure("Incorrect data format")
            return
        }
        
//        print(responsToParse)
        
        //        if user.like == nil {
        //            user.like = DMLike(context: context)
        //        }
        
        // User list
        var users = [String: DMUser]()
        if responsToParse.count > 0 {
            let recipientIDs = responsToParse.map{ $0[DMLike.CodingKeys.recipientID] as? String }.flatMap{ $0 }
            let senderIDs = responsToParse.map{ $0[DMLike.CodingKeys.senderID] as? String}.flatMap{ $0 }
            let unique = Array(Set(recipientIDs + senderIDs))
            
            let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
            request.predicate = NSPredicate(format: "%K in %@", #keyPath(DMUser.uid), unique)
            
            let results = try! context.fetch(request)
            results.forEach {
                users[$0.uid!] = $0
            }
        }
        
        // Invites list
        var invites = [String: DMInvite]()
        if responsToParse.count > 0 {
            let likeIds = responsToParse.map{ $0[DMLike.CodingKeys.id] as? String }.flatMap{ $0 }
            let request: NSFetchRequest<DMInvite> = DMInvite.fetchRequest()
            request.predicate = NSPredicate(format: "%K in %@", #keyPath(DMLike.id), likeIds)
            let results = try! context.fetch(request)
            results.forEach{
                invites[$0.id!] = $0
            }
            
        }
        
        
        // Parse likes
        for record in responsToParse {
            let json = JSON(record)
            guard let id = json[CodingKeys.id].string else { continue }
            guard let recepientId = json[CodingKeys.recipientID].string else { continue }
            guard let senderId = json[CodingKeys.senderID].string else { continue }
            
            let entity = invites[id] ?? {
                let entity = DMInvite(context: context)
                entity.id = id
                return entity
                }()
            
            
            entity.createdValue = json[CodingKeys.created].doubleValue
            
            
            // Sender
            entity.sender = users[senderId] ?? {
                let user = DMUser.new(with: senderId, in: context)
                users[senderId] = user
                return user
                }()
            
            entity.sender.name = json[CodingKeys.senderName].stringValue
            entity.sender.birthday = json[CodingKeys.senderBirthday].doubleValue
            entity.sender.profilePhotoUrl = json[CodingKeys.senderPhoto].stringValue
            entity.sender.occupation = json[CodingKeys.senderOccupation].string
            
            // Recipient
            entity.recipient = users[recepientId] ?? {
                let user = DMUser.new(with: recepientId, in: context)
                users[recepientId] = user
                return user
                }()
            
            entity.recipient.name = json[CodingKeys.recipientName].stringValue
            entity.recipient.birthday = json[CodingKeys.recipientBirthday].doubleValue
            entity.recipient.profilePhotoUrl = json[CodingKeys.recipientPhoto].stringValue
            entity.recipient.occupation = json[CodingKeys.recipientOccupation].string
            
            
            // Main fields
            entity.stateValue = json[DMInvite.CodingKeys.state].int64 ?? Int64(InviteState.error.rawValue)
            entity.notes = json[DMInvite.CodingKeys.notes].stringValue
            entity.venueAddress = json[DMInvite.CodingKeys.placeAddress].stringValue
            entity.venueId = json[DMInvite.CodingKeys.placeID].string
            entity.venueLat = json[DMInvite.CodingKeys.placeLat].double ?? 0
            entity.venueLng = json[DMInvite.CodingKeys.placeLng].double ?? 0
            entity.venueName = json[DMInvite.CodingKeys.placeName].string
            entity.venueRate = json[DMInvite.CodingKeys.placeRate].int16Value
            entity.scheduled = json[DMInvite.CodingKeys.scheduled].double ?? 0
            entity.recipientCheckined = json[DMInvite.CodingKeys.recipientCheckined].bool ?? false
            entity.senderCheckined = json[DMInvite.CodingKeys.senderCheckined].bool ?? false
            entity.cancelReason = json[DMInvite.CodingKeys.cancelReason].int16 ?? -1
            entity.inheritedFrom = json[DMInvite.CodingKeys.inheritedFrom].string
            entity.countdownStartedAt = json[DMInvite.CodingKeys.countdownStartedAt].doubleValue
            
        }
        
    }
}


extension DMInvite {
	
	enum CodingKeys {
        
        static let id = "id"
        static let senderID = "senderID"
        static let recipientID = "recipientID"
        static let created = "created"
        static let recipient_sender = "recipientSender"
        
        static let senderName = "senderName"
        static let senderBirthday = "senderBirthday"
        static let senderOccupation = "senderOccupation"
        static let senderPhoto = "senderPhoto"
        
        static let recipientName = "recipientName"
        static let recipientBirthday = "recipientBirthday"
        static let recipientOccupation = "recipientOccupation"
        static let recipientPhoto = "recipientPhoto"

        static let recipientIndex = "recipientIndex"
        
		static let scheduled = "scheduled"
		static let state = "state"
		static let placeID = "placeId"
		static let placeName = "placeName"
		static let placeAddress = "placeAddress"
		static let placeLat = "placeLat"
		static let placeLng = "placeLng"
		static let placeRate = "placeRate"
		static let notes = "notes"
		static let senderCheckined = "senderCheckined"
		static let recipientCheckined = "recipientCheckined"
		static let senderRating = "senderRating"
		static let recipientRating = "recipientRating"
		static let cancelReason = "cancelReason"
        static let inheritedFrom = "inheritedFrom"
        static let countdownStartedAt = "countdownStartedAt"
	}
    
	
	/// Save data to firebase
   func synchronize(completion: @escaping (Result<DatabaseReference>) -> Swift.Void) {
        
        var json = JSON([:])
        json[CodingKeys.id].string = self.identifier.rawValue
        
        json[CodingKeys.created].double = self.createdValue
        json[CodingKeys.recipient_sender].string = self.recipientSenderIndex
    
    
        print(String(describing: type(of: self)),":", #function, " ", json)
        
        json[CodingKeys.senderID].string = self.sender.identifier.rawValue
        json[CodingKeys.senderName].string = self.sender.name
        json[CodingKeys.senderOccupation].string = self.sender.occupation
        json[CodingKeys.senderBirthday].double = self.sender.birthday
        
        json[CodingKeys.recipientID].string = self.recipient.identifier.rawValue
        json[CodingKeys.recipientName].string = self.recipient.name
        json[CodingKeys.recipientOccupation].string = self.recipient.occupation
        json[CodingKeys.recipientBirthday].double = self.recipient.birthday
        
        
        json[CodingKeys.scheduled].double = scheduled
        json[CodingKeys.state].int = Int(stateValue)
        
        // Place
        json[CodingKeys.placeID].string = venueId
        json[CodingKeys.placeName].string = venueName
        json[CodingKeys.placeAddress].string = venueAddress
        json[CodingKeys.placeLat].double = venueLat
        json[CodingKeys.placeLng].double = venueLng
        json[CodingKeys.placeRate].int16 = venueRate
        json[CodingKeys.cancelReason].int16 = cancelReason
        json[CodingKeys.inheritedFrom].string = inheritedFrom
        json[CodingKeys.countdownStartedAt].double = countdownStartedAt
    
    
    
    
        json[CodingKeys.recipientIndex].string = String(format: "%@_%d", self.opponent.identifier.rawValue, self.stateValue)
    
    
        
        // Notes
        json[CodingKeys.notes].string = notes
        
        
        // Check in
        json[CodingKeys.senderCheckined].bool = senderCheckined
        json[CodingKeys.recipientCheckined].bool = recipientCheckined
        
        
        
        // Rating
        let addRating = { (ratingType: RatingType) -> Void in
            if let propertyName = self.oppoentRatingPropertyName(for: ratingType),
                let val = self.value(forKey: propertyName) as? Int,
                val > 0 {
                json[propertyName].int = val
            }
        }
        
        addRating(RatingType.appearance)
        addRating(RatingType.experience)
        addRating(RatingType.personality)
        
        
        
        // Photos
        let photoRef = self.sender.profilePhotoeRef
        photoRef?.downloadURL(completion: { (url, error) in
            
            json[CodingKeys.senderPhoto].string = url?.absoluteString
            
            
            
            
            let recipienPhotoRef = self.recipient.profilePhotoeRef
            recipienPhotoRef?.downloadURL(completion: { (url, _) in
                json[CodingKeys.recipientPhoto].string = url?.absoluteString
                
                let inviteRef = FirebaseStack.invitation(for: self.identifier)
                
    
                
                inviteRef.setValue(json.dictionaryObject) { (error, dbRef) in
                    
                    if let error = error {
                        completion(Result.failure(error))
                    } else {
                        completion(.success(dbRef))
                    }
                }
            })
        })
        
    }
}



// MARK: - Ratings
extension DMInvite {
    func opponentRating(for ratingType: RatingType) -> RatingStatus {
        
        switch ratingType {
        case .none: return RatingStatus.none
        default:
            let propertyName = self.oppoentRatingPropertyName(for: ratingType)!
            let val = self.value(forKey: propertyName) as? Int16 ?? RatingStatus.none.rawValue
            guard let ratingStatus = RatingStatus(rawValue: val) else {
                return RatingStatus.none
            }
            
            return ratingStatus
        }
    }
    
     func oppoentRatingPropertyName(for ratingType: RatingType) -> String? {
        let pathName = self.meIsSender ?  "recipientRating" : "senderRating"
        let fieldName: String
        switch ratingType {
        case .none: return nil
        case .appearance: fieldName = "Appearance"
        case .experience: fieldName = "Experience"
        case .personality: fieldName = "Personality"
        }
        
        return pathName + fieldName
    }
}


