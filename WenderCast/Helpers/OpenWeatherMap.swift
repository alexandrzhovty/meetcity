//
//  File.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import MapKit

struct OpenWeatherMap {
	private static let apiKey = "b5a23d1c81255016cdb39b7a71c12231"
	private static let sUrl = "http://api.openweathermap.org/data/2.5/weather"
	
	static func request(for coordinate: CLLocationCoordinate2D) -> URLRequest {
		
		
		guard var compoenents = URLComponents(string: OpenWeatherMap.sUrl) else { fatalError() }
		compoenents.queryItems = [
			URLQueryItem(name: "APPID", value: OpenWeatherMap.apiKey),
			URLQueryItem(name: "lat", value: "\(coordinate.latitude)"),
			URLQueryItem(name: "lon", value: "\(coordinate.longitude)")
		]
		guard let url = compoenents.url else { fatalError() }
		let request = URLRequest(url: url)
		return request
	}
	
	static func url(for iconName: String) -> URL {
		return URL(string: "http://openweathermap.org/img/w/\(iconName).png")!
	}
}
