//
//  DMVisit.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/23/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import Firebase
import SwiftyJSON


// MARK: - Overrides
extension DMVisit {
    
    /// Fill in default propertins on creation
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        super.type = .visit
    }
    
    
    /// Used as indexed search property
    override var recipientSenderIndex: String {
        get {
            return "\(recipient.uid!)_\(sender.uid!)_\(Formatters.date.visit.string(from: self.created))"
        }
    } 
}

// MARK: - Fields
extension DMVisit {
    var lastVisit: Date {
        get { return Date(timeIntervalSince1970: self.lastVisitValue) }
        set { self.lastVisitValue = newValue.timeIntervalSince1970 }
    }
}

// MARK: - Parsing
extension DMVisit {
    
    /// Parsing server response
    ///
    /// - Parameters:
    ///   - response: should be `[key: value]` or `[[key: value]]`
    ///   - context: where the parsed results should be stored in
    ///   - user: parsed messages should belong to
    class func parse(_ response: ParsableResponse, into context: NSManagedObjectContext) {
        
        //        let user = context.object(with: user.objectID) as! DMUser
        
        
        let responsToParse: [[String: Any]]
        
        switch response {
        case let snapshots as DataSnapshot:
            guard let value = snapshots.value as? [String: [String: Any]] else { return }
            responsToParse = value.map{ $1 }
            
        case let dictionary as Dictionary<String, Any>:
            responsToParse = [dictionary]
            
        default:
            assertionFailure("Incorrect data format")
            return
        }
        
        //        print(responsToParse)
        
        //        if user.like == nil {
        //            user.like = DMVisit(context: context)
        //        }
        
        // User list
        var users = [String: DMUser]()
        if responsToParse.count > 0 {
            let recipientIDs = responsToParse.map{ $0[DMVisit.CodingKeys.recipientID] as? String }.flatMap{ $0 }
            let senderIDs = responsToParse.map{ $0[DMVisit.CodingKeys.senderID] as? String}.flatMap{ $0 }
            let unique = Array(Set(recipientIDs + senderIDs))
            
            let request: NSFetchRequest<DMUser> = DMUser.fetchRequest()
            request.predicate = NSPredicate(format: "%K in %@", #keyPath(DMUser.uid), unique)
            
            let results = try! context.fetch(request)
            results.forEach {
                users[$0.uid!] = $0
            }
        }
        
        // Like list
        var visits = [String: DMVisit]()
        if responsToParse.count > 0 {
            let likeIds = responsToParse.map{ $0[DMVisit.CodingKeys.id] as? String }.flatMap{ $0 }
            let request: NSFetchRequest<DMVisit> = DMVisit.fetchRequest()
            request.predicate = NSPredicate(format: "%K in %@", #keyPath(DMVisit.id), likeIds)
            let results = try! context.fetch(request)
            results.forEach{
                visits[$0.id!] = $0
            }
            
        }
        
        
        // Parse likes
        for record in responsToParse {
            let json = JSON(record)
            guard let id = json[CodingKeys.id].string else { continue }
            guard let recepientId = json[CodingKeys.recipientID].string else { continue }
            guard let senderId = json[CodingKeys.senderID].string else { continue }
            
            let entity = visits[id] ?? {
                let entity = DMVisit(context: context)
                entity.id = id
                return entity
                }()
            
            // Visit entity properties
            entity.lastVisitValue = json[CodingKeys.lastVisit].doubleValue
            entity.amount = json[CodingKeys.amount].int64Value
            
            // Common
            entity.createdValue = json[CodingKeys.created].doubleValue
            // Sender
            entity.sender = users[senderId] ?? {
                let user = DMUser.new(with: senderId, in: context)
                users[senderId] = user
                return user
                }()
            
            entity.sender.name = json[CodingKeys.senderName].stringValue
            entity.sender.birthday = json[CodingKeys.senderBirthday].doubleValue
            entity.sender.profilePhotoUrl = json[CodingKeys.senderPhoto].stringValue
            entity.sender.occupation = json[CodingKeys.senderOccupation].string
            
            // Recipient
            entity.recipient = users[recepientId] ?? {
                let user = DMUser.new(with: recepientId, in: context)
                users[recepientId] = user
                return user
                }()
            
            entity.recipient.name = json[CodingKeys.recipientName].stringValue
            entity.recipient.birthday = json[CodingKeys.recipientBirthday].doubleValue
            entity.recipient.profilePhotoUrl = json[CodingKeys.recipientPhoto].stringValue
            entity.recipient.occupation = json[CodingKeys.recipientOccupation].string
            
            
            
            
        }
        
    }
}

// MARK: - Keys
extension DMVisit {
    enum CodingKeys {
        
        static let id = "id"
        static let senderID = "senderID"
        static let recipientID = "recipientID"
        static let created = "created"
        static let recipient_sender_date = "recipientSender"
        
        static let senderName = "senderName"
        static let senderBirthday = "senderBirthday"
        static let senderOccupation = "senderOccupation"
        static let senderPhoto = "senderPhoto"
        
        static let recipientName = "recipientName"
        static let recipientBirthday = "recipientBirthday"
        static let recipientOccupation = "recipientOccupation"
        static let recipientPhoto = "recipientPhoto"
        
        
        static let amount = "amount"
        static let lastVisit = "lastVisit"
    }
}
