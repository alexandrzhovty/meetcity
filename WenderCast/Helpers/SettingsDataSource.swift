//
//  SettingsDataSource.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/9/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase

final class SettingsDataSource: DataSource {
	
	private weak var _viewController: SettingsController?
	private typealias SegueType = SettingsController.SegueType
	
	init(with viewController: SettingsController) {
		_viewController = viewController
		
		let items: [Profile] = [
			Profile(title: Strings.ShowMe, type: ProfileType.switcher(currentValue: viewController.userModel.showMe) {
				[weak _viewController] sender in
				guard let _viewController = _viewController else { return }
				_viewController.userModel.showMe = sender.isOn
			}),
			Profile(title: Strings.LocationHidden, type: ProfileType.switcher(currentValue: viewController.userModel.locationHidden) {
				[weak _viewController] sender in
				guard let _viewController = _viewController else { return }
				_viewController.userModel.locationHidden = sender.isOn
			}),
//			Profile(title: Strings.NotofocationsEnabled, type: ProfileType.switcher(currentValue: viewController.userModel.notificationsEnabeld) {
//				[weak _viewController] sender in
//				guard let _viewController = _viewController else { return }
//				_viewController.userModel.notificationsEnabeld = sender.isOn
//			}),
//			Profile(title: Strings.Sounds, type: ProfileType.switcher(currentValue: viewController.userModel.sounds) {
//				[weak _viewController] sender in
//				guard let _viewController = _viewController else { return }
//				_viewController.userModel.sounds = sender.isOn
//			}),
			Profile(title: Strings.PrivacyPolicy, type: ProfileType.selectable {
				[weak _viewController] sender in
				guard let viewController = _viewController else { return }
				viewController.performSegue(SegueType.showPrivatePolicy, sender: sender)
			}),
			Profile(title: Strings.SignOut, type: ProfileType.buttonLike {
				[weak _viewController] sender in
				
				let alertController = UIAlertController(title: Strings.Authorization, message: Strings.DoYouRealyWantToExit, preferredStyle: .alert)
				alertController.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
				alertController.addAction(UIAlertAction(title: Strings.SignOut, style: .destructive) { _ in
					do {
                        
                        // Sing out from Fireabase
						try Auth.auth().signOut()
                        
                        // Clear Instagram token
                        InstagramManager.accessToken = nil
                        
                        // Restore user defaults values
                        Settings.resetStandardUserDefaults()
                        
                        
						Presenter.presentLoginScreen()
						_viewController?.dismiss(animated: true, completion: nil)
					} catch {
						Alert.default.showOk(Strings.Authorization, message: error.localizedDescription)
					}
				})
				
				_viewController?.present(alertController, animated: true, completion: nil)
				
			})
		]
		
		super.init(with: items)
		
	}
	
}
