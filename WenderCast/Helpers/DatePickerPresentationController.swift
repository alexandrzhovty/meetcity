//
//  DatePickerPresentationController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class DatePickerPresentationController: UIPresentationController {
	
	var dimmingView: UIView! = UIView()
	
	//	deinit {
	//		print(String(describing: type(of: self)),":", #function)
	//	}
	
	override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
		super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
		dimmingView.backgroundColor = UIColor(white:0.0, alpha:0.4)
		dimmingView.alpha = 0.0
	}
	
	func chromeViewTapped(_ gesture: UIGestureRecognizer) {
		if (gesture.state == UIGestureRecognizerState.ended) {
			presentingViewController.dismiss(animated: true, completion: nil)
		}
	}
	
	override var frameOfPresentedViewInContainerView : CGRect {
		let containerBounds = containerView!.bounds
		let presentingSize = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerBounds.size)
		let presentedViewFrame = containerBounds.insetBy(dx: (containerBounds.width - presentingSize.width) / 2 , dy: (containerBounds.height - presentingSize.height) / 2)
		
//		presentedViewFrame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerBounds.size)
//		presentedViewFrame.origin.x = containerBounds.size.width - presentedViewFrame.size.width
		return presentedViewFrame
	}
	
	override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
		return CGSize(width: parentSize.width - 40, height: 216 + 40)
	}
	
	override func presentationTransitionWillBegin() {
		
		// Setup diminig view
		dimmingView.frame = (self.containerView?.bounds)!
		dimmingView.alpha = 0.0
		dimmingView.isOpaque = false
		containerView?.insertSubview(dimmingView, at:0)
		
		// Tap handler
		let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
		dimmingView.addGestureRecognizer(tap)
		
		//		let swipe = UIPanGestureRecognizer(target: self, action: #selector(handlerPanGesture(_:)))
		//		presentedViewController.view.addGestureRecognizer(swipe)
		
		let coordinator = presentedViewController.transitionCoordinator
		if (coordinator != nil) {
			coordinator!.animate(alongsideTransition: {
				(context:UIViewControllerTransitionCoordinatorContext!) -> Void in
				self.dimmingView.alpha = 1.0
			}, completion:nil)
		} else {
			dimmingView.alpha = 1.0
		}
	}
	
	override func presentationTransitionDidEnd(_ completed: Bool) {
		if completed == false {
			self.dimmingView = nil
		}
	}
	
	override func dismissalTransitionWillBegin() {
		let coordinator = presentedViewController.transitionCoordinator
		if (coordinator != nil) {
			coordinator!.animate(alongsideTransition: { (context:UIViewControllerTransitionCoordinatorContext!) -> Void in
				self.dimmingView.alpha = 0.0
			}, completion:nil)
		} else {
			dimmingView.alpha = 0.0
		}
	}
	
	override func dismissalTransitionDidEnd(_ completed: Bool) {
		if completed == true {
			self.dimmingView = nil
		}
	}
	
	override func containerViewWillLayoutSubviews() {
		dimmingView.frame = (containerView?.bounds)!
		presentedView!.frame = frameOfPresentedViewInContainerView
	}
	
	// MARK: - Outlets
	// MARK: Recognizer handlers
	@objc func handleTapGesture(_ recognizer: UITapGestureRecognizer) {
		self.presentingViewController.dismiss(animated: true, completion: nil)
	}
	
}
