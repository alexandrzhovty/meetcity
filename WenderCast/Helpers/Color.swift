//
//  Color.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

//    MARK: - Color types
enum ColorType: UInt32 {
	case pink = 0xff3366
	case crimson = 0xb8083e
	case burgundy = 0x56071f
	case blue = 0x0081f4
	case gray = 0x979797
    case darkGray = 0x3D3D3D
    case lime = 0x70ff00
	
	var color: UIColor { return UIColor(self) }
}

//    MARK: - Initialization
extension UIColor {
	convenience init(_ type: ColorType) {
		self.init(rgbValue: type.rawValue)
	}
	
	convenience init(rgbaValue: UInt32) {
		let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
		let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
		let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
		let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0
		
		self.init(red: red, green: green, blue: blue, alpha: alpha)
	}
	
	convenience init(rgbValue: UInt32, alpha: CGFloat = 1) {
		let red   = CGFloat((rgbValue >> 16) & 0xff) / 255.0
		let green = CGFloat((rgbValue >>  8) & 0xff) / 255.0
		let blue  = CGFloat((rgbValue      ) & 0xff) / 255.0
		
		self.init(red: red, green: green, blue: blue, alpha: alpha)
	}
}


//    MARK - Color scheme
extension UIColor {
	static var tint: UIColor { return UIColor.black }
	
	enum navigation {
		static var background: UIColor { return UIColor(.pink) }
		static var tintColor = UIColor.white
	}
	
	enum separator {
		static var loginController = UIColor(.gray)
		static var tableViewCells = UIColor(.gray).withAlphaComponent(0.5)
	}
	
	enum clickable {
		static var text = UIColor(rgbValue: 0x0071FF)
	}
	
	enum background {
		static var forAdditionalInfo = UIColor(rgbValue: 0xfff6f8)
		static var settings = UIColor.white
		static var main = UIColor.white
		
	}
	
	enum tintColor {
		static let textView = UIColor(.blue)
	}
	
    enum readyToMeet {
        static var `true`  = UIColor(.lime)
        static var `false` = UIColor(.pink)
    }
    
	enum text {
		static let title = UIColor(rgbValue: 0x464646)
		static let subtitle = UIColor(rgbValue: 0x838383)
	}
}



/**

cp -R /Applications/Xcode.app/Contents/Developer/Library/Xcode/Templates/File\ Templates/Source/Swift\ File.xctemplate/ ~/Library/Developer/Xcode/Templates/File\ Templates/Mine/Bundle.xctemplate

 */
