//
//  DatePickerSegue.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class DatePickerSegue: UIStoryboardSegue {
	var presentation: DatePickerPresentationController!
	
	override func perform() {
		destination.modalPresentationStyle = .custom
		destination.transitioningDelegate = self
		super.perform()
	}
	
	//	deinit {
	//		print(String(describing: type(of: self)),":", #function)
	//	}
}

extension DatePickerSegue: UIViewControllerTransitioningDelegate {
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		presentation = DatePickerPresentationController(presentedViewController: presented, presenting: presenting)
		return presentation
	}
	
}
