//
//  VenueProtocol.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

protocol VenueProtocol {
	var venue: Venue! { get set }
}
