//
//  VenueRankedTableCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class VenueRankedTableCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var ratingControl: RatingControl!
	@IBOutlet weak var separatorView: UIView!
	
	
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension VenueRankedTableCell: NibLoadableView {}
