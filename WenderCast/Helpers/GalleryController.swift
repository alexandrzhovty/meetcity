//
//  GalleryController.swift
//  LIQR
//
//  Created by Aleksandr Zhovtyi on 4/18/17.
//  Copyright © 2017 Lindenvalley GmbH. All rights reserved.
//

import UIKit


final class GalleryController: UIViewController {
    //    MARK: - Properties & variables
    //    MARK: Public
    var images: [UIImage]!
	var currentPage: Int = 0
	var galleryViewModel: GalleryViewModel! {
		didSet { reloadData() }
	}
		
    //    MARK: Outlets
    @IBOutlet weak var containetView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //    MARK: Private
    var widthConstraint: NSLayoutConstraint!
    var heightConstraint: NSLayoutConstraint!
    
    var pageViewController: UIPageViewController!
    
    
    //    MARK: Enums & Structures
	
	
    
    //    MARK: - View life cycle
    deinit {
        unregisterObserver()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Constriaint
        containetView.translatesAutoresizingMaskIntoConstraints = false
        
		
            
		
		containetView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
		containetView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
		
		widthConstraint = containetView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
		widthConstraint.isActive = true
		heightConstraint = containetView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height)
		heightConstraint.isActive = true
		containetView.frame = UIScreen.main.bounds
		containetView.layoutIfNeeded()
		
		registerObserver()
            
		
        // Page controler
        pageControl.numberOfPages = galleryViewModel.numberOfObjects()
    }
	
	//    MARK: Status bar
    override var prefersStatusBarHidden: Bool { return true }
	
    
    //    MARK: - Observers
    private func registerObserver() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(statusBarOrientationChange(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: UIDevice.current)
    }
    
    private func unregisterObserver() {
        let center = NotificationCenter.default
        center.removeObserver(self)
    }
    
    
    //    MARK: - Navigation
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        switch segueIdentifier(for: identifier) {  // SegueHandler
//        case .unwindToBarProfile: return true
//        case .showPageController: return true
//        }
//    }
	
	
	func reloadData() {
//		guard let vc = pageController(for: 0) else { return }
//		pageViewController.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
//		pageViewController.dataSource = self
//		pageViewController.delegate = self
		

	}
	
    //    MARK: - Outlet functions
	//    MARK: Buttons
	@IBAction func didTapClose(_ sender: UIButton) {
		dismiss(animated: true, completion: nil)
	}
    
	
	//    MARK: Page control
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        guard
            let vc = pageViewController.viewControllers?.first as? ImagePreviewController,
            let imageVC = pageController(for: pageControl.currentPage)
            
        else { return }
        
        let direction: UIPageViewControllerNavigationDirection = vc.pageIndex < pageControl.currentPage ? .forward : .reverse
        
        pageViewController.setViewControllers([imageVC], direction: direction, animated: true, completion: nil)
        
    }
    
    //    MARK: - Utilities
    private func rotate(to angle: CGFloat, animated: Bool) {
        
        if abs(angle) == 90 {
            widthConstraint.constant = UIScreen.main.bounds.height
            heightConstraint.constant = UIScreen.main.bounds.width
        } else {
            widthConstraint.constant = UIScreen.main.bounds.width
            heightConstraint.constant = UIScreen.main.bounds.height
        }
        
        UIView.animate(withDuration: 0.25) { [unowned self] in
            self.containetView.transform = CGAffineTransform(rotationAngle: (angle * CGFloat.pi/180.0))
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
        
    }
    
    //    MARK: Orientation
    @objc func statusBarOrientationChange(_ notification: Notification) {
        let orientation = UIDevice.current.orientation
        switch orientation {
        case .portrait:             rotate(to: 0, animated: true)
        case .portraitUpsideDown:   rotate(to: 0, animated: true)
        case .landscapeLeft:        rotate(to: 90, animated: true)
        case .landscapeRight:       rotate(to: -90, animated: true)
        default:                    break
        }
    }
}

// MARK: - Navigation & SegueHandler protocol
extension GalleryController: SegueHandler {
	enum SegueType: String {
		case embedPageController
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segueType(for: segue) {
		case .embedPageController:
			
			guard let vc = pageController(for: currentPage) else { return }
			let pageViewController = segue.destination as! UIPageViewController
			pageViewController.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
			pageViewController.dataSource = self
			pageViewController.delegate = self
			
			self.pageViewController = pageViewController
		}
	}
}


// MARK: -
extension GalleryController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    //    MARK: - Page view controller protocol
    //    MARK: Datasource
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentVC = viewController as! ImagePreviewController
        let nextIndex = currentVC.pageIndex - 1
        return pageController(for: nextIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentVC = viewController as! ImagePreviewController
        let nextIndex = currentVC.pageIndex + 1
        return pageController(for: nextIndex)
    }
    
    func pageController(for pageIndex: Int) -> ImagePreviewController? {
		guard galleryViewModel != nil else { return nil }
		
		if pageIndex < 0 || pageIndex >= galleryViewModel.numberOfObjects() {
			return nil
		}
		
        let vc = UIStoryboard(.gallery).instantiateViewController(ImagePreviewController.self)  //   ImagePreviewController.instatiate(fromStoryboard: .gallery)
		vc.pageIndex = pageIndex
		vc.imageRef = galleryViewModel.object(at: pageIndex)

        return vc
    }
    
    //    MARK: Delegate
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed == true {
            
            let array = pageViewController.viewControllers as! [ImagePreviewController]
            for vc in array {
                pageControl.currentPage = vc.pageIndex
            }
        }
        
        
    }
    
}
