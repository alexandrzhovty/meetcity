//
//  PhotosListCollectionCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/22/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class PhotosListCollectionCell: UICollectionViewCell {
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
