//
//  InstagramCollectionCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class InstagramCollectionCell: UICollectionViewCell {
	@IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension InstagramCollectionCell: NibLoadableView {}
