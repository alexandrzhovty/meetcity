//
//  CheckInController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData
import AlamofireImage
import SVProgressHUD

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class CheckInController: UIViewController {
    //    MARK: Public
    var inviteModel: InviteModel!
    var invite: DMInvite!
    var managedObjectContext: NSManagedObjectContext!
    
    //    MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var bodyLabel: UILabel!
	@IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
	
	@IBOutlet weak var imageView: UIImageView!
	
	
    //    MARK: Private
	fileprivate var _venueProvider: VenueProvider!
    
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension CheckInController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
		// Assertion
		assert(inviteModel != nil, "Invite model cannot be nil")
        self.invite = inviteModel.invite
        self.managedObjectContext = inviteModel.context
		
        // Customize appearance
        self.navigationController?.navigationItem.setHidesBackButton(true, animated: false)
        Appearance.customize(viewController: self)
		navigationItem.title = inviteModel.venue.name
		
		imageView.image = nil
		
		// Get images
		_venueProvider = VenueProvider()
		_venueProvider.fetchImage(for: inviteModel.venue) { [weak self] url in
			print(String(describing: type(of: self)),":", #function)
			guard let `self` = self else { return }
			guard let url = url else {
				DispatchQueue.main.async {
					self.activityIndicatorView.stopAnimating()
					self.imageView.image = UIImage(named: "restaurant-5")
				}
				return
			}
			
			let queue = DispatchQueue.main			
			self.imageView.af_setImage(
				withURL: url,
				placeholderImage: nil,
				filter: nil, progress: nil,
				progressQueue: queue,
				imageTransition: UIImageView.ImageTransition.crossDissolve(0.25),
				runImageTransitionIfCached: false
			) { [weak self] _ in
				guard let `self` = self else { return }
				self.activityIndicatorView.stopAnimating()
			}
			
		}
        
        configure(titleLabel)
        configure(bodyLabel)
		
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        MessageManager.default.send(.checkedIn(invite: invite), completion: {})
    }
}

//    MARK: - Displaying
extension CheckInController  {
    fileprivate func configure(_ view: UIView) {
		switch view {
        case titleLabel:
            titleLabel.text = Strings.CheckInController.YourSuccesfullyCheckined
            
        case bodyLabel:
            let format = Strings.CheckInController.OpponentRecievedNotification
            let opponentName = invite.opponent.name ?? ""
            
            let singleOut = [NSForegroundColorAttributeName: UIColor(.pink)]
            let regular = [NSForegroundColorAttributeName: UIColor(.darkGray)]
            
            let str = String.localizedStringWithFormat(format, opponentName) + "\n"
            let attrStr = NSMutableAttributedString(string: str, attributes: regular)
            
            attrStr.singleOut(opponentName, with: singleOut)
            
            
            let opponentCheckedIn = invite.sender.uid == AppDelegate.me.uid ? invite.recipientCheckined : invite.senderCheckined
            if opponentCheckedIn {
                attrStr.append(NSAttributedString(string: Strings.CheckInController.OpponentAlreadyInPlace, attributes: regular))
            } else {
                attrStr.append(NSAttributedString(string: Strings.CheckInController.OpponentWillBeInPlace, attributes: regular))
            }
            
            bodyLabel.attributedText = attrStr
            
		default: break
		}
        
    }
}
