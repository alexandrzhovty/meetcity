//
//  GalleryViewModel.swift
//  LIQR
//
//  Created by Aleksandr Zhovtyi on 4/25/17.
//  Copyright © 2017 Lindenvalley GmbH. All rights reserved.
//

import UIKit
import Firebase

class GalleryViewModel {
	
	var model: UserModel!
	var galleryController: GalleryController!
    
    fileprivate var _dataSource = [StorageReference]()
    
	
	init(_ model: UserModel) {
		self.model = model
		
		let photosRef = model.photosRef
		for value in model.photos {
			let imageRef = photosRef.child(value.fileName!)
			_dataSource.append(imageRef)
		}
		
	}
	
	
    func object(at index: Int) -> StorageReference {
		return _dataSource[index]
    }
    
    func numberOfObjects() -> Int {
        return _dataSource.count
    }
    
}
