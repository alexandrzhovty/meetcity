//
//  Strings.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

struct Strings {
	private init() {}
}



extension Strings {
	static let OKString = NSLocalizedString("OK", comment: "OK button")
	static let CancelString = NSLocalizedString("Cancel", comment: "Label for Cancel button")
	static let DeleteString = NSLocalizedString("Delete", comment: "Label for Delete button")
	static let Close = NSLocalizedString("Close", comment: "Label for Close button")
	static let Error = NSLocalizedString("Error", comment: "Error title")
}

extension Strings {
    enum Instagram {
        public static let CannotGetUserInfo = "Failured to get Instagram user info."
    }
}

extension Strings {
    enum History {
        public static let allHistory = NSLocalizedString("All History", comment: "")
        public static let dates = NSLocalizedString("dates", comment: "")
        public static let likes = NSLocalizedString("likes", comment: "")
        public static let visitors = NSLocalizedString("visitors", comment: "")
        public static let name = NSLocalizedString("History", comment: "")
        public static let likedYourProfile = NSLocalizedString("liked your profile %@", comment: "")
        public static let sentInvitation = NSLocalizedString("sent invitation %@", comment: "")
        public static let visitedYourProfile = NSLocalizedString("visited your profile %@", comment: "")
    }
}

extension Strings {
    enum CheckInController {
        public static let YourSuccesfullyCheckined = NSLocalizedString("You've successfully checked in", comment: "")
        public static let OpponentRecievedNotification = NSLocalizedString("%@ was notified", comment: "")
        public static let OpponentWillBeInPlace = NSLocalizedString("and will arrive soon", comment: "")
        public static let OpponentAlreadyInPlace = NSLocalizedString("and has already arrived", comment: "")
        public static let MeCheckedIn = NSLocalizedString("has checked in", comment: "")
    }
}

extension Strings {
    enum RatingType {
        public static let experience = NSLocalizedString("Overall Experince", comment: "")
        public static let personality = NSLocalizedString("Personality", comment: "")
        public static let appearance = NSLocalizedString("Appearance", comment: "")
    }
    
    enum RatingStatus {
        public static let bad = NSLocalizedString("Bad", comment: "")
        public static let poor = NSLocalizedString("Poor", comment: "")
        public static let indifferently = NSLocalizedString("Indifferently", comment: "")
        public static let good = NSLocalizedString("Good", comment: "")
        public static let excellent = NSLocalizedString("Excellent", comment: "")
    }
}

extension Strings {
	public static let Refuse = NSLocalizedString("Refuse", comment: "")
	public static let Accept = NSLocalizedString("Accept", comment: "")
	public static let NewInvitation = NSLocalizedString("New Invitation", comment: "")
	public static let AreYouSureYouWantToRefuseInvitation = NSLocalizedString("Are you sure you want to refuse the invitation", comment: "")
	public static let AreYouSureYouWantToCancelDating = NSLocalizedString("Are you sure you want to cancel the dating", comment: "")
	public static let TimeIsGone = NSLocalizedString("Time is gone", comment: "")
	public static let SelectPlace = NSLocalizedString("Select place", comment: "")
	public static let CancelDate = NSLocalizedString("Cancel Date", comment: "")
	public static let ModifyDate = NSLocalizedString("Modify Date", comment: "")
	public static let CheckIn = NSLocalizedString("Check In", comment: "")
	public static let SendMessage = NSLocalizedString("Send Message", comment: "")
	public static let EndDate = NSLocalizedString("End Date", comment: "")
    public static let SentMessage = NSLocalizedString("sent a message to you", comment: "")
}

extension Strings {
	enum InviteState {
		public static let expired = NSLocalizedString("Expired", comment: "")
		public static let reviewing = NSLocalizedString("In review", comment: "")
		public static let canceled = NSLocalizedString("Canceled", comment: "")
		public static let datingBegun = NSLocalizedString("Dating has begun", comment: "")
		public static let declined  = NSLocalizedString("Declined", comment: "")
		public static let finished = NSLocalizedString("Finished", comment: "")
		public static let sent = NSLocalizedString("Sent", comment: "")
        public static let modified = NSLocalizedString("Modified", comment: "")
	}
	
	
}

extension Strings {
	public static let FindAnotherDate = NSLocalizedString("Find another date\non nearby screen!", comment: "Find another date on nearby screen!")
	public static let CongratulationsAcceptedYourInvite = NSLocalizedString("Congratulations!\n%@ accepted your invite", comment: "")
	
	
	
}

extension Strings {
    enum Reachability {
        public static let notReachable = NSLocalizedString("Internet connection lost", comment: "")
    }
}

extension Strings {
    enum Profile {
        public static let blockUser = NSLocalizedString("Block", comment: "")
        public static let reportUser = NSLocalizedString("Report", comment: "")
    }
}

// MARK: - Start invitation controller
extension Strings {
	enum Invitation {
		static let WeatherForecast = NSLocalizedString("Weather Forecast", comment: "")
		static let NoPlacesFound = NSLocalizedString("No placess found", comment: "")
		static let LetsDrinkCoffeeTonight = NSLocalizedString("Let`s drink coffee tonight?", comment: "")
		static let SendInviteForDate = NSLocalizedString("Send Invite for a date", comment: "Send Invite for a date")
		static let CancelInvite = NSLocalizedString("Cancel Invite", comment: "")
		static let VenueIsInvalid = NSLocalizedString("This place cannot be used as a venue.\nPlease select another one.", comment: "")
		static let name = NSLocalizedString("Invitation", comment: "Invitation")
		static let WaitForResponse = NSLocalizedString("Please wait for %@ response", comment: "")
		static let his = NSLocalizedString("his", comment: "")
		static let her = NSLocalizedString("her", comment: "")
		static let inReview = NSLocalizedString("%@ is reviewing\nyour invite", comment: "")
		static let sentTo = NSLocalizedString("Invitation sent to %@", comment: "")
		static let UserUnaccesibleNow = NSLocalizedString("%@ isn't accessible right now.\nPlease try again later.", comment: "")
		static let UserIsNotReadyToMeet = NSLocalizedString("%@ isn't ready to meet now.", comment: "")
	}
}

// MARK: - Login controller
extension Strings {
	public static let LogInWithFacebook = NSLocalizedString("Log in with Facebook", comment: "Start controller")
	public static let WeDontPostAnything = NSLocalizedString("We don`t post anything on Facebook.\nBy signing in, you agree to our Privacy Policy", comment: "Start controller")
	public static let PrivacyPolicy = NSLocalizedString("Privacy Policy", comment: "")
	
	public static let InternalErrorTryLater = NSLocalizedString("Internal error. Please repeat later", comment: "")
	
	public static let RegistrationFailed = NSLocalizedString("Registration is failed", comment: "")
	
	public static let PleaseTryLater = NSLocalizedString("Please try it again later", comment: "")
}

// MARK: - Tour list data provider
extension Strings {
	public static let Tour0Text = NSLocalizedString("FIND NEW PEOPLE\nNEARBY WHO WANT\nTO MEET NOW", comment: "Tour")
	public static let Tour1Text = NSLocalizedString("Find Someone Nearby\nand Meet Them Today", comment: "Tour")
	public static let Tour2Text = NSLocalizedString("Send an Invite and Meet Now,\nin real time", comment: "Tour")
	public static let Tour3Text = NSLocalizedString("View Profiles and Rate Your\nMeeting Experiences", comment: "Tour")
	public static let Tour4Text = NSLocalizedString("Easily Navigate Map\nfor Your Meeting", comment: "Tour")
}


// MARK: - Browser
extension Strings {
	public static let UnableToOpenURLErrorTitle = NSLocalizedString("Browser", comment: "")
	public static let UnableToOpenURLError = NSLocalizedString("Unable to open URL", comment: "")
}

// MARK: - Additional info controller
extension Strings {
	public static let AdditionalInfo = NSLocalizedString("Additional Info", comment: "")
}




extension Strings {
	public static let cannotLodPhotoFromFacebook = NSLocalizedString("Can't download photo from Facebook", comment: "")
}

// MARK: - Version view
extension Strings {
	public static let version = NSLocalizedString("Version %@", comment: "Version of the application")
}

// MARK: - User Profile
extension Strings {
	public static let ReadyToMeet = NSLocalizedString("Ready To Meet", comment: "")
	public static let NotReadyToMeet = NSLocalizedString("Not ready to meet", comment: "")
	public static let Settings = NSLocalizedString("Settings", comment: "")
	public static let ShowMe = NSLocalizedString("Show me on Meetcity", comment: "")
	public static let LocationHidden = NSLocalizedString("Hide My Exact Location on Map", comment: "")
	public static let NotofocationsEnabled = NSLocalizedString("Notifications", comment: "")
	public static let Sounds = NSLocalizedString("Sounds", comment: "")
}

// MARK: - Profile manager
extension Strings {
	public static let user = NSLocalizedString("user", comment: "")
	public static let AboutMe = NSLocalizedString("About me", comment: "Aboutme")
	public static let Occupation = NSLocalizedString("Occupation", comment: "Occupation")
	public static let ShowInstagramPhotos = NSLocalizedString("Show Instagram Photos", comment: "")
	public static let AddInstagramAccound = NSLocalizedString("Add Instagram Account", comment: "")
	public static let Iam = NSLocalizedString("I'm", comment: "I'm")
	public static let SelectedImageWillBeDeleted = NSLocalizedString("The selected image will be deleted!", comment: "")
}

extension Strings {
	public static let Gender = NSLocalizedString("Gender", comment: "")
	public static let Boy = NSLocalizedString("Guy", comment: "")
	public static let Girl = NSLocalizedString("Girl", comment: "")
	public static let Both = NSLocalizedString("Both", comment: "")
}

// MARK: - Profile manager
extension Strings {
	public static let Authorization = NSLocalizedString("Authorization", comment: "")
	public static let DoYouRealyWantToExit = NSLocalizedString("Do you realy want to exit", comment: "")
	public static let SignOut = NSLocalizedString("Sign out", comment: "")
}


// MARK: - Nearby list
extension Strings {
	public static let PeopleNearby = NSLocalizedString("People Nearby", comment: "")
}

// MARK: - Nearby profile
extension Strings {
	public static let ReportBlock = NSLocalizedString("Report/Block %@", comment: "Report/Block Rosaline")
}


