//
//  View.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

extension UIView {
	/// Add shake animation
	func shake() {
		let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
		animation.duration = 0.6
		animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
		layer.add(animation, forKey: "shake")
	}
   
   func scaleAndBack() {
      let animation = CAKeyframeAnimation(keyPath: "transform.scale")
      animation.values = [1, 2.5, 1]
      animation.duration = 0.25
      layer.add(animation, forKey: "scaleAndBack")
   }
}

extension UIView {
	
	/// Returns the UIViewController object that manages the receiver.
	///
	/// - Returns: UIViewController
	public func viewController() -> UIViewController? {
		
		var nextResponder: UIResponder? = self
		
		repeat {
			nextResponder = nextResponder?.next
			
			if let viewController = nextResponder as? UIViewController {
				return viewController
			}
			
		} while nextResponder != nil
		
		return nil
	}
}
