//
//  ModifyDatingController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/28/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData
import Firebase

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class ModifyDatingController: UITableViewController {
    //    MARK: Public
	var invite: DMInvite!
    var managedObjectContext: NSManagedObjectContext!
    
	
    //    MARK: Outlets
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var placeLabel: UILabel!
	
    //    MARK: Private
    typealias CompletionHandler = (_ modifyRequest: DMInvite, _ context: NSManagedObjectContext) -> Void
	fileprivate var _competionHandler: CompletionHandler!
    fileprivate var _newInviteModel: InviteModel!
	
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension ModifyDatingController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
        // Customize appearance
        Appearance.customize(viewController: self)
        
        
        do { _newInviteModel = try InviteModel(with: invite, in: managedObjectContext) }
        catch {
            Alert.default.showError(message: error.localizedDescription)
        }
		
		// Configure tableView
		configure(tableView)
		configure(timeLabel)
		configure(placeLabel)
		
		
		
		self.preferredContentSize = CGSize(width: self.view.frame.width, height: CGFloat(tableView.numberOfRows(inSection: 0)) * tableView.rowHeight)
		
    }
	
	@discardableResult
	func comletion(_ completion: @escaping CompletionHandler) -> Self {
		_competionHandler = completion
		return self
	}
}

//    MARK: - Utilities
extension ModifyDatingController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case timeLabel:
			timeLabel.text = Formatters.date.long.string(from: _newInviteModel.scheduled)
		case placeLabel:
			placeLabel.text = _newInviteModel.title
		default: break
        }
		
    }
}

//    MARK: - Outlet functions
extension ModifyDatingController  {
    //    MARK: Buttons
	@IBAction func didTapSave(_ sender: UIBarButtonItem) {
        
        guard
            let inheriteId = invite.inheritedFrom,
            let oldInvite = DMInvite.invite(for: inheriteId, in: managedObjectContext)
        else {
            assertionFailure("Cannot find inherite ID")
            Alert.default.showError(message: Errors.ModifyDateController.InheriteIdEmpty)
            return
        }
        
        if invite.venueId == oldInvite.venueId && invite.scheduled == oldInvite.scheduled {
            timeLabel.shake()
            placeLabel.shake()
            return
        }
        
		dismiss(animated: true) {
            self._competionHandler(self.invite, self.managedObjectContext)
        }
        
	}
}

// MARK: - Navigation & SegueHandler protocol
extension ModifyDatingController: SegueHandler {
    enum SegueType: String {
        case exitToDating
		case selectDate
		case selectPlace
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		switch segueType(for: identifier) {
        case .selectDate, .selectPlace: return true
		case .exitToDating: return true
		}
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .exitToDating: break
		case .selectDate:
			let vc = (segue.destination as! UINavigationController).topViewController as! DatePickerController
			vc.date = _newInviteModel.scheduled
            
            let calendar = Calendar.current
            vc.minimumDate = calendar.date(byAdding: .hour, value: 1, to: Date())
            vc.maximumDate = calendar.date(byAdding: .hour, value: 24, to: Date())
			vc.completion = { [weak self] date in
				guard let `self` = self else { return }
				self._newInviteModel.scheduled = date
				self.configure(self.timeLabel)
			}
		case .selectPlace:
			let vc = (segue.destination as! UINavigationController).topViewController as! VenueListController
			vc.completion = { [weak self] venue in
				guard let `self` = self else { return }
				self._newInviteModel.venue = venue
				self.configure(self.placeLabel)
			}
        }
    }
}
