//
//  BorderedView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

@IBDesignable class BorderedView: UIView {
	
	@IBInspectable var borderedColor: UIColor = UIColor(.pink) {
		didSet { setupControl() }
	}

	//	MARK: - Initialization
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupControl()
	}
	
	required init(coder: NSCoder) {
		super.init(coder: coder)!
		setupControl()
	}
	
	private func setupControl() {
		self.layer.borderColor = borderedColor.cgColor
		self.layer.borderWidth = 1
		self.clipsToBounds = true
	}
	
}
