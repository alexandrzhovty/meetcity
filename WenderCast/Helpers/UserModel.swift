//
//  UserModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase
import CoreLocation
import CoreData
import SwiftyJSON

protocol UserModelProtocol {
//	var title: String { get }
//	var occupation: String? { get set }
//	var readyToMeet: Bool { get set }
//	var user: DMUser { get }
//	var uid: String { get }
//	var gender: Gender { get set }
//	var lookFor: LookFor { get set }
//	var showMe: Bool { get set }
//	var locationHidden: Bool { get set }
////	var notificationsEnabeld: Bool { get set }
//	var profilePhoto: DMPhoto? { get }
//	var profilePhotoeRef: StorageReference? { get }
//	var photosRef: StorageReference { get }
//	var photosObserver: DatabaseReference { get }
//	var photos: [DMPhoto] { get  }
//	
//	var name: String { get set }
//	var distanse: Double { get set }
//	var email: String? { get set }
//	var birthday: Date? { get set }
//	
//	var isFacebookInfoLoaded: Bool { get }
//	var isRegistrationInfoCompleted: Bool { get }
	
}



struct UserModel {
	fileprivate let _user: DMUser
	init(with user: DMUser) {
		_user = user
	}
}

extension UserModel: UserModelProtocol {
	var photos: [DMPhoto] {
		let sortOrder = NSSortDescriptor(key: #keyPath(DMPhoto.sortOrder), ascending: true)
		let array = _user.photoValues?.sortedArray(using: [sortOrder])
		return array as! [DMPhoto]
	}

	

	var birthday: Date? {
		get { return _user.birthday.isZero ? nil : Date(timeIntervalSince1970: _user.birthday) }
		set {
			if newValue == nil {
				_user.birthday = 0
//				FirebaseStack.user(for: _user.uid).email.removeValue()
			} else {
				_user.birthday = newValue!.timeIntervalSince1970
//				FirebaseStack.user(for: _user.uid).birthday.setValue(_user.birthday)
			}
		}
	}

	var email: String? {
		get { return _user.email }
		set {
			_user.email = newValue
//			FirebaseStack.user(for: _user.uid).email.setValue(newValue)
		}
	}
	
	var name: String {
		get { return _user.name ?? "" }
		set {
			_user.name = newValue
//			FirebaseStack.user(for: _user.uid).name.setValue(newValue)
			
		}
	}
	

	var isFacebookInfoLoaded: Bool {
		return ( !_user.name.isEmptyOrNil || !_user.birthday.isZero || !_user.occupation.isEmptyOrNil)
	}
	
	var isRegistrationInfoCompleted: Bool {
		return _user.genderValue != .none || _user.lookForValue != .none || _user.aboutMe.isEmptyOrNil == false
	}
	

	

	
	

	var profilePhotoeRef: StorageReference? {
		guard let fileName = self.profilePhoto?.fileName else { return nil }
		return FirebaseStack.storageRef.child(_user.uid!).child(fileName)
	}

	var photosObserver: DatabaseReference {
		return FirebaseStack.users.child(_user.uid!).child(DMUser.CodingKeys.photos)
	}

	var photosRef: StorageReference {
		
		
		// Get a reference to the storage service using the default Firebase App
		let storage = Storage.storage()
		
		// Create a storage reference from our storage service
		let storageRef = storage.reference()
		
		
		return storageRef.child(_user.uid!)
	}

	
	
	var profilePhoto: DMPhoto? {
		return self.photos.first
	}
	
	var locationHidden: Bool {
		get { return _user.locationHidden }
		set {
			_user.locationHidden = newValue
		}
	}
	
	var showMe: Bool {
		get { return _user.showMe }
		set {
			_user.locationHidden = newValue
			FirebaseStack.users.child(_user.uid!).child(DMUser.CodingKeys.showMe).setValue(newValue)
			
		}
	}
	
	var lookFor: LookFor {
		get { return user.lookFor }
		set { user.lookFor = newValue }
	}
	
	
	var gender: Gender {
		get { return user.gender }
		set { user.gender = newValue }
	}
	
	var uid: String { return _user.uid! }
	var user: DMUser { return _user }
	var aboutMe: String? {
		get { return _user.aboutMe }
		set { _user.aboutMe = newValue }
	}
	
	var title: String {
		var array: [String?] = [_user.name]
		if _user.birthday > 0 {
			let date1 = Date(timeIntervalSince1970: _user.birthday)
			if let age = Calendar.current.dateComponents([.year], from: date1, to: Date()).year {
				array.append("\(age)")
			}
		}
		
		return array.flatMap{ $0 }.joined(separator: ", ")
	}
	
	
	var occupation: String? {
		get { return _user.occupation }
		set { _user.occupation = newValue }
	}
}






// MARK: - Managed Object Protocol
extension UserModel: ManagedObjectProtocol {
	var objectID: NSManagedObjectID { return _user.objectID }
}

