//
//  NotificationMessageView.swift
//  NotificationMessage
//
//  Created by Aleksandr Zhovtyi on 7/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class NotificationMessageView: UIView {

	@IBOutlet weak var contentView: GradientView!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var textLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var shadowView: GradientView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		shadowView.colors = [UIColor.black.withAlphaComponent(0.4), UIColor.clear]
		contentView.colors = [UIColor(rgbValue: 0xfff1f4), UIColor(rgbValue: 0xf1f7fc)]
		
		
		imageView.layer.cornerRadius = imageView.bounds.width / 2
		imageView.clipsToBounds = true
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		imageView.layer.cornerRadius = imageView.bounds.width / 2
	}

}

extension NotificationMessageView: NibLoadableView {}
