//
//  ProfileManagerController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import SVProgressHUD

import InstagramSimpleOAuth

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */


//    MARK: - Observers protocol



//    MARK: - Properties & variables
extension ProfileManagerController: ProfilePresenterControllerProtocol {}

final class ProfileManagerController: UIViewController {
    //    MARK: Public
	var userModel: UserModel!
	var presenter: ProfilePresenter!
	var dataSource: DataSource!
	let imagePicker = UIImagePickerController()
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	
    //    MARK: Private
	fileprivate var _observers = [Any]()
	fileprivate var _photoSelectionView: PhotoSelectionView!
    fileprivate var _instagramLoginController: UIViewController?
	
    //    MARK: Enums & Structures
    fileprivate enum IndexPathTo {
        static let InstagramRow = IndexPath(row: 0, section: 2)
    }
	
	//    MARK: Initializations
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commotInt()
	}
	
	private func commotInt() {
		
	}
	
	deinit {
		unregisterObserver()
	}
	
}

//    MARK: - View life cycle
extension ProfileManagerController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEVELOPMENT
            navigationItem.title = NSLocalizedString("Development", comment: "").localizedUppercase
        #endif
        
        // Customize appearance
        Appearance.customize(viewController: self)
		
		
		dataSource = ProfileManagerDataSource(with: self)
		presenter = ProfilePresenter(with: self)
		
		// Configure tableView
		configure(tableView)
        tableView.delegate = self
        tableView.dataSource = self
		presenter.setup(tableView)
		
		
		
		registerObserver()
		
    }
	
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if self._photoSelectionView == nil {
			_photoSelectionView = PhotoSelectionView.instantiateFromInterfaceBuilder()
			
			let view = UIView()
			let size = CGSize(width: self.view.bounds.width, height: 2000.0)
			view.backgroundColor = UIColor.clear
			
			view.frame.size = PhotoSelectionView.sizeThatFits(size)
			
			_photoSelectionView = PhotoSelectionView.instantiateFromInterfaceBuilder()
			_photoSelectionView.frame = view.bounds
			_photoSelectionView.autoresizingMask = [ .flexibleWidth, .flexibleHeight ]
			view.addSubview(_photoSelectionView)
			tableView.tableHeaderView = view
			
			_photoSelectionView.managerController = self
			
			
		}
	}
	


}

// MARK: - ObserverProtocol
extension ProfileManagerController: ObserverProtocol {
	var observers: [Any] {
		get { return _observers }
		set { _observers = newValue }
	}
	
	func registerObserver() {
		userModel.user.addObserver(self, forKeyPath: #keyPath(DMUser.aboutMe), options: .new, context: nil)
		userModel.user.addObserver(self, forKeyPath: #keyPath(DMUser.occupation), options: .new, context: nil)
//		userModel.user.addObserver(self, forKeyPath: #keyPath(Gender), options: .new, context: nil)
	}
	
	func unregisterObserver() {
		userModel.user.removeObserver(self, forKeyPath: #keyPath(DMUser.aboutMe))
		userModel.user.removeObserver(self, forKeyPath: #keyPath(DMUser.occupation))
//		userModel.user.removeObserver(self, forKeyPath: #keyPath(Gender))
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		switch keyPath ?? "" {
		case #keyPath(DMUser.aboutMe), #keyPath(DMUser.occupation):
			
			// Update row height for dynamic TextView
			let currentOffset = tableView.contentOffset
			UIView.setAnimationsEnabled(false)
			tableView.beginUpdates()
			tableView.endUpdates()
			UIView.setAnimationsEnabled(true)
			tableView.setContentOffset(currentOffset, animated: false)
			
		default:
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
}

//    MARK: - Utilities
extension ProfileManagerController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case tableView:
			tableView.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.tableHeaderView?.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.estimatedRowHeight = Constants.TableView.profileCellHeight
			tableView.rowHeight = UITableViewAutomaticDimension
			
        default: break
        }
		
    }
    
    func disconnectFromInstagram() {
        let title = NSLocalizedString("Disconnect Instagram", comment: "")
        let msg = NSLocalizedString("All your photos from Instagram will be remved.", comment: "")
        let alertVC = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
        alertVC.addAction(
            UIAlertAction(title: NSLocalizedString("Disconnect", comment: ""), style: .destructive) { _ in
                InstagramManager.logOut()
                self.tableView.reloadSections(IndexSet([IndexPathTo.InstagramRow.section]), with: .fade)
            }
        )
        present(alertVC, animated: true) {
            self.tableView.deselectRow(at: IndexPathTo.InstagramRow, animated: true)
        }
    }
    
    func connectToInstagram() {
        
        
        let callBackURL = URL(string: InstagramManager.redirectUri)
        let vc = InstagramSimpleOAuthViewController(clientID:  InstagramManager.cleintID, clientSecret: InstagramManager.clientSecret, callbackURL: callBackURL) {
            [weak self] (response, error) in
            
            guard let response = response else {
                let errorMsg = error?.localizedDescription ?? Strings.Instagram.CannotGetUserInfo
                Alert.default.showError(message: errorMsg)
                return
            }
            
            AppDelegate.me.instagramUserId = response.user.userID
            AppDelegate.me.instagramUserName = response.user.username
            InstagramManager.accessToken = response.accessToken
            
            
            guard let `self` = self else { return }
            self.updateInstgramData()
            
            
        }
        
        vc?.shouldShowErrorAlert = false
        vc?.permissionScope = ["basic", "comments", "relationships", "likes"]
        
        vc?.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismiss(_:)))
        
        let navVC = UINavigationController(rootViewController: vc!)
        present(navVC, animated: true, completion: nil)
        
        self._instagramLoginController = navVC
        
    }
    
    @objc func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateInstgramData() {
        
        let synchronizationBlock = { () -> Void in
            SVProgressHUD.show()
            AppDelegate.me.synchronize(completion: { [weak self] result in
                guard let `self` = self else { return }
                
                SVProgressHUD.dismiss(completion: {
                    switch result {
                    case let .failure(error):
                        Alert.default.showError(message: error.localizedDescription)
                    case .success:
                        let section = IndexSet([IndexPathTo.InstagramRow.section])
                        self.tableView.reloadSections(section, with: .automatic)
                    }
                })
            })
        }
        
        if let vc = self._instagramLoginController {
            vc.dismiss(animated: true, completion: synchronizationBlock)
        } else {
            synchronizationBlock()
        }
    }
    
    
}

//    MARK: - Outlet functions
extension ProfileManagerController  {
    //    MARK: Buttons
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension ProfileManagerController: SegueHandler {
    enum SegueType: String {
        case exitFromProfileManager
    }
	
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .exitFromProfileManager: break		
        }
    }

}

//	MARK: - Table view protocol
extension ProfileManagerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		
		if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
			
//			UIImage *pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
//			UIImage *resized = [pickedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:pickedImage.size interpolationQuality:kCGInterpolationHigh];

			
			_photoSelectionView.add(pickedImage)
			
//			if let cgi = context!.makeImage() {
//			}
			
//			
//			guard let data = UIImagePNGRepresentation(image) else { return }
//			guard let tempImage = UIImage(data: data) else { return }
//			let fixedOrientationImage = UIImage(cgImage: tempImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)
//			image = fixedOrientationImage
//			
//			
//			let minSize: CGFloat = 600
//			let size = image.size
//			let ratio: CGFloat
//			if size.width > size.height {
//				ratio = size.width / minSize
//			} else {
//				ratio = size.height / minSize
//			}
//			let newSize = CGSize(width: size.width / ratio, height: size.height / ratio)
//			
//			let cgImage = image.cgImage!
//			
//			let bitsPerComponent = cgImage.bitsPerComponent
//			let bytesPerRow = cgImage.bytesPerRow
//			let colorSpace = cgImage.colorSpace
//			let bitmapInfo = cgImage.bitmapInfo
//			
//			
//			let context = CGContext(data: nil, width: Int(newSize.width), height: Int(newSize.height), bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace!, bitmapInfo: bitmapInfo.rawValue)
//			context?.interpolationQuality = .high
//			context?.draw(cgImage, in: CGRect(x: 0.0,y: 0.0, width: newSize.width, height: newSize.height))
//
//			
////			var scaledImage = UIImage(cgImage: context!.makeImage()!)
//			
//			if let cgi = context!.makeImage() {
//				_photoSelectionView.add(UIImage(cgImage: cgi))
//			}
			
			
		}
		
		dismiss(animated: true, completion: nil)
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
	}
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ProfileManagerController: UITableViewDataSource, UITableViewDelegate {
    //	MARK: Data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfItems(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath == IndexPathTo.InstagramRow && InstagramManager.isAuthorized {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            cell.textLabel?.text = AppDelegate.me.instagramUserName
            return cell
        } else {
            let model = dataSource.objectAt(at: indexPath)
            return tableView.dequeueReusableCell(with: model, for: indexPath)
        }
        
    }
    
    //	MARK: Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == IndexPathTo.InstagramRow {
            if InstagramManager.isAuthorized {
                self.disconnectFromInstagram()
            } else {
                self.connectToInstagram()
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            return
        }
        
        let model = dataSource.objectAt(at: indexPath)
        let cell = tableView.cellForRow(at: indexPath)
        switch model.type {
        case .switcher: break
        case .textEditabled:
            guard let cell = tableView.cellForRow(at: indexPath) as? TextEditProfileCell else { break }
            if !cell.textView.isFirstResponder {
                cell.textView.becomeFirstResponder()
            }
        case let .buttonLike(action):
            
            if indexPath == IndexPathTo.InstagramRow {
                if InstagramManager.isAuthorized {
                    self.disconnectFromInstagram()
                } else {
                    self.connectToInstagram()
                }
            } else {
                action(cell)
            }
            tableView.deselectRow(at: indexPath, animated: true)
            
        case let .selectable(action):
            action(cell)
        }
    }
    
    
    
    //	MARK: Section header & footer information
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView.style == .grouped {
            return tableView.sectionHeaderHeight * 2
        } else {
            return tableView.sectionHeaderHeight
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView.style == .grouped {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderSectionView") as! HeaderSectionView
            headerView.title = dataSource[section].title
            return headerView
        } else {
            return nil
        }
        
    }
}
