//
//  DMPhoto.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/21/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

extension DMPhoto {
	
	enum Fields {
		static let fileName = "filename"
	}
	
	// MARK: - Utilities
	
	/// Creares hash table of existing entities
	///
	/// - Parameters:
	///   - ids: array of ID used to filter enitities
	///   - user: the photos belongs to
	///   - context: where enitities are taken from
	/// - Returns: list of Category entities
	class func list(for fileNames: [String],  for user: DMUser, from context: NSManagedObjectContext) -> [String: DMPhoto] {
		assert(user.managedObjectContext == context, "User stored in another context")
		
		guard fileNames.count > 0 else { return [:] }
		
		let request: NSFetchRequest<DMPhoto> = DMPhoto.fetchRequest()
		let predicates = [
			NSPredicate(format: "%K in %@", #keyPath(DMPhoto.fileName),  fileNames),
			NSPredicate(format: "%K = %@", #keyPath(DMPhoto.user), user)
		]
		request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
		let results = try! context.fetch(request)
		
		if results.count > 0 {
			let keys = results.map({ $0.fileName }) as! [NSCopying]
			let dictionary = NSDictionary(objects: results, forKeys: keys)
			return dictionary as! [String: DMPhoto]
			
		} else {
			return [:]
		}
	}
	
	
	/// Creates new entity
	///
	/// - Parameters:
	///   - id: unique ID for entity
	///   - user: the photo will belongs to
	///   - context: where the new entity will created in
	/// - Returns: new entity
	class func new(with fileName: String, for user: DMUser, in context: NSManagedObjectContext) -> DMPhoto {
		assert(user.managedObjectContext == context, "User stored in another context")
		
		let entity: DMPhoto
		if #available(iOS 10.0, *) {
			entity = DMPhoto(context: context)
		} else {
			entity = NSEntityDescription.insertNewObject(forEntityName: String(describing: DMPhoto.self), into: context) as! DMPhoto
		}
		entity.user = user
		entity.fileName = fileName
		
		return entity
	}
	
	//    MARK: - Parsing
	/// Parse JSON response from server
	///
	/// - Parameters:
	///   - json: array of JSON objects
	///   - user: the photos belongs to
	///   - context: context where parsed entities will be stored into
	class func parse(_ response: [[String: Any]], for user: DMUser, into context: NSManagedObjectContext) {
		assert(user.managedObjectContext == context, "User stored in another context")
		
		let fileNames = response.flatMap{ $0[Fields.fileName] as? String }
		let list = DMPhoto.list(for: fileNames, for: user, from: context)
		
		// Delete old values
		let request: NSFetchRequest<DMPhoto> = DMPhoto.fetchRequest()
		let predicates = [
			NSPredicate(format: "NOT (%K in %@)", #keyPath(DMPhoto.fileName),  fileNames),
			NSPredicate(format: "%K = %@", #keyPath(DMPhoto.user), user)
		]
		
		request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
		
		do {
			let results = try context.fetch(request)
			results.forEach{ context.delete($0) }
		} catch {}
		
		
		let json = JSON(response)
		
		for (idx, record) in (json.array ?? []).enumerated() {
			guard let fileName = record[Fields.fileName].string else { continue }
			let entity: DMPhoto = list[fileName] ?? DMPhoto.new(with: fileName, for: user, in: context)
			entity.sortOrder = Int16(idx)
			
			do {
				if entity.isInserted {
					try entity.validateForInsert()
				} else {
					try entity.validateForUpdate()
				}
				
			} catch {
				context.delete(entity)
			}
			
		}
	}
}
