//
//  RegistrationController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/3/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import SVProgressHUD
import Firebase

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 http://candycode.io/self-sizing-uitextview-in-a-uitableview-using-auto-layout-like-reminders-app/
 */

//    MARK: - Properties & variables
class RegistrationController: UITableViewController {
	//    MARK: Public
	
	//    MARK: Outlets
	@IBOutlet weak var genderSegmentedControl: UISegmentedControl!
	@IBOutlet weak var lookingForSegmentedControl: UISegmentedControl!
	@IBOutlet weak var doneButton: UIBarButtonItem!
	@IBOutlet weak var countLabel: UILabel!
	
	
	//    MARK: Private
	fileprivate var aboutMessage: String? {
		get { return AppDelegate.me.aboutMe }
		set { AppDelegate.me.aboutMe = newValue}
	}
	
	//    MARK: Enums & Structures
	
	
}

//    MARK: - View life cycle
extension RegistrationController  {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.title = Strings.AdditionalInfo
		
		// Customize appearance
		Appearance.customize(viewController: self)
		navigationItem.setHidesBackButton(true, animated: false)
		
		
		
		// About me
		configure(genderSegmentedControl)
		configure(lookingForSegmentedControl)
		configure(countLabel)
		configure(doneButton)
		
		
		// Table view
		configure(tableView)
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		// Fake 'hide/show' navigatin bar
		if let navBar = navigationController?.navigationBar {
			navBar.isTranslucent = false
			navBar.setBackgroundImage( UIImage(Background.navigationBar), for: .default)
			navBar.shadowImage = UIImage(Icon.blank)
			
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if SVProgressHUD.isVisible() {
			SVProgressHUD.dismiss()
		}
	}
	
}

//    MARK: - Utilities
extension RegistrationController  {
	fileprivate func configure(_ barItem: UIBarButtonItem) {
		switch barItem {
		case doneButton:
			doneButton.isEnabled = { [unowned self] in
				return self.genderSegmentedControl.selectedSegmentIndex > -1
					&& self.lookingForSegmentedControl.selectedSegmentIndex > -1
					&& self.aboutMessage.isEmptyOrNil == false
				}()
			
		default: break
		}
	}
	
	fileprivate func configure(_ view: UIView) {
		switch view {
		case countLabel:
			let left = Constants.AboutMeMaxCount - (aboutMessage?.characters.count ?? 0)
			if left >= 0 {
				countLabel.text = "\(left)"
				countLabel.textColor = UIColor.black
			} else {
				countLabel.textColor = UIColor.red
				countLabel.text = "\(0)"
			}
			
		case genderSegmentedControl:
			genderSegmentedControl.selectedSegmentIndex = Int(AppDelegate.me.gender.rawValue)
			
			
		case lookingForSegmentedControl:
			lookingForSegmentedControl.selectedSegmentIndex = Int(AppDelegate.me.lookFor.rawValue)
			
			
		case tableView:
			tableView.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.tableHeaderView?.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.estimatedRowHeight = 70
			tableView.rowHeight = UITableViewAutomaticDimension
			
		default: break
		}
		
	}
}

//    MARK: - Outlet functions
extension RegistrationController  {
	//    MARK: Buttons
	@IBAction func didTapDone(_ button: Any) {
		guard let navVC = AppDelegate.shared.window?.rootViewController as? UINavigationController else {
			assertionFailure()
			return
		}
		
		let vc = UIStoryboard(.profile).instantiateViewController(UserProfileController.self)
		let vc2 = UIStoryboard(.dashboard).instantiateViewController(NearbyListController.self)
		navVC.viewControllers = [vc, vc2]
		
		
        
		AppDelegate.me.gender = Gender(rawValue: Int16(genderSegmentedControl.selectedSegmentIndex)) ?? Gender.none
		AppDelegate.me.lookFor = LookFor(rawValue: Int16(lookingForSegmentedControl.selectedSegmentIndex)) ?? LookFor.none
		AppDelegate.me.aboutMe = self.aboutMessage ?? ""
        
        
        AppDelegate.me.synchronize {
            switch $0 {
            case .failure(let error):
                SVProgressHUD.dismiss {
                    Alert.default.showError(message: error.localizedDescription)
                }
                
            case .success:
                SVProgressHUD.dismiss {
                    // Fake 'hide/show' navigatin bar
                    let navBar = navVC.navigationBar
                    navBar.isTranslucent = false
                    navBar.setBackgroundImage( UIImage(Background.navigationBar), for: .default)
                    navBar.shadowImage = UIImage(Icon.blank)
                    
                    
                    
                    UserDefaults.standard.set(true, forKey: "SecondLogin")
                    UserDefaults.standard.synchronize()
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }
            }
        }
		
	}
	
	
	//    MARK: Swith
	@IBAction func didChangeValue(_ sender: Any) {
		configure(doneButton)
	}
	
	//    MARK: Gesture handlers
}


//    MARK: - Table view protocol
extension RegistrationController {
	//   MARK: Data source
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(RegistrationTableCell.self, for: indexPath)
		cell.textView.delegate = self
		cell.textView.text = self.aboutMessage
		return cell
	}
}


//    MARK: - Text view protocol
extension RegistrationController: UITextViewDelegate {
	func textViewDidChange(_ textView: UITextView) {
		self.aboutMessage = textView.text
		
		// Configure outlets
		configure(doneButton)
		configure(countLabel)
		
		// Update row height
		let currentOffset = tableView.contentOffset
		UIView.setAnimationsEnabled(false)
		tableView.beginUpdates()
		tableView.endUpdates()
		UIView.setAnimationsEnabled(true)
		tableView.setContentOffset(currentOffset, animated: false)
		
	}
}

