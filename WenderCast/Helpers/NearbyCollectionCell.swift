//
//  NearbyCollectionCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class NearbyCollectionCell: UICollectionViewCell {
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var dotImageView: UIImageView!
   
   
	override func awakeFromNib() {
		super.awakeFromNib()
		contentView.backgroundColor = UIColor.white
		gradientView.colors = [UIColor.clear, UIColor.black.withAlphaComponent(0.7)]
		dotImageView.tintColor = UIColor.readyToMeet.false
        
		prepareForReuse()
        
	}
	
	override func prepareForReuse() {
        
        super.prepareForReuse()
        
		imageView.image = nil
		titleLabel.text = nil
		distanceLabel.text = nil
		
	}
}


extension NearbyCollectionCell: NearbyCellProtocol {
    var distance: CLLocationDistance {
        get { return 0 }
        set {
            if newValue > 0 {
                distanceLabel.text = Formatters.distance.default.string(fromDistance: newValue)
            } else {
                distanceLabel.text = nil
            }
        }
    }

    
    
    var image: UIImage? {
        get { return imageView.image }
        set { imageView.image = newValue }
    }

    var readyToMeet: Bool {
        get { return dotImageView.tintColor == UIColor.readyToMeet.true }
        set {
            
            if newValue {
                dotImageView.tintColor = UIColor.readyToMeet.true
                dotImageView.isHidden = false
            } else {
                dotImageView.tintColor = UIColor.readyToMeet.false
                dotImageView.isHidden = true
            }
        }
    }

    var title: String? {
        get { return self.titleLabel.text }
        set { titleLabel.text = newValue }
    }

    
}

