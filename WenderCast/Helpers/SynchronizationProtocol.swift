//
//  SynchronizationProtocol.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase

protocol SynchronizationProtocol {
//	func synchronize() throws
   
    func synchronize(completion: @escaping (Result<DatabaseReference>) -> Swift.Void)
}
