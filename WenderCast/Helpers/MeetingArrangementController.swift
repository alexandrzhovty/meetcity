//
//  MeetingArrangementController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
import Firebase
import CoreData


//    MARK: - Properties, variables & initialization
final class MeetingArrangementController: UIViewController {
	//    MARK: Public
	var observers = [Any]()
	var inviteModel: InviteModel!
    var invite: DMInvite!
    var managedObjectContext: NSManagedObjectContext!
	
	
	//    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var refuseButton: UIButton!
	@IBOutlet weak var acceptButton: UIButton!
	
	//    MARK: Private
	fileprivate let _distanceFormatter = MKDistanceFormatter()
	fileprivate var _instagramDataSource: [UIImage]!
	fileprivate var _isFavorited: Bool = false
	
	/// Timer
	fileprivate var _timer: Timer?
	/// Timer duration
	fileprivate var _remainedTime: TimeInterval = Constants.Invitation.waitingForReviewTime
	fileprivate var _viewControllerIsVisible = false
    fileprivate var _failureAlreadyShown = false
	
	fileprivate var _dataProvider: MeetingArrangementDataProvider!
	fileprivate var _model: MeetingArrangementViewModel!
	
	// MARK: Structures and Enums
	fileprivate struct IndexPathTo {
		private init(){}
		static let infoCell = IndexPath(row: 0, section: 0)
		
	}
	
	
	
	
	//    MARK: - Initializations
	deinit {
		unregisterObserver()
	}
}

//    MARK: - View life cycle
extension MeetingArrangementController  {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		
		// Customize appearance
		Appearance.customize(viewController: self)
        
        self.invite = inviteModel.invite
        self.managedObjectContext = inviteModel.context
        
        if self.invite.inheritedFrom.isEmptyOrNil {
            // New request
            _remainedTime = invite.created.timeIntervalSince1970 + Constants.Invitation.waitForResponseToRequest - Date().timeIntervalSince1970
			
        } else {
            // Modify requst
            if invite.countdownStartedAt == 0 {
                _remainedTime = invite.created.timeIntervalSince1970 + Constants.Invitation.waitingForModifyeReview - Date().timeIntervalSince1970
            } else {
                _remainedTime = invite.countdownStartedAt + Constants.Invitation.waitingForModifyeReview - Date().timeIntervalSince1970
            }
            
        }
       
		
		// DataProvider and model
		_dataProvider = MeetingArrangementDataProvider(with: self.inviteModel)
		_model = MeetingArrangementViewModel(with: self.inviteModel)
		
		navigationItem.title = Strings.NewInvitation
		MeetingArrangementViewModel.registerNibs(for: tableView)
		
		
		// Configure tableView
		configure(refuseButton)
		configure(acceptButton)
		configure(tableView)
		
		registerObserver()
		
		
	}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpTimer()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		_viewControllerIsVisible = true
		if inviteModel.state == .sent {
			inviteModel.state = .reviewing
            inviteModel.countDownStarted = Date()
      
            
            invite.synchronize { result in
                switch result {
                case .failure(let error):
                    Alert.default.showError(message: error.localizedDescription)
                case .success:
                    if self.invite.inheritedFrom.isEmptyOrNil {
                        // New request
                        self._remainedTime = self.invite.countdownStartedAt + Constants.Invitation.waitingForReviewTime - Date().timeIntervalSince1970
                        
                    } else {
                        // Modify requst
                        self._remainedTime = self.invite.countdownStartedAt + Constants.Invitation.waitingForModifyeReview - Date().timeIntervalSince1970
                        
                    }
                    
                }
            }
            
            
			
		}
        
        
        if self.invite.inheritedFrom.isEmptyOrNil {
            // New request
            self._remainedTime = self.invite.countdownStartedAt + Constants.Invitation.waitingForReviewTime - Date().timeIntervalSince1970
            
        } else {
            // Modify requst
            self._remainedTime = self.invite.countdownStartedAt + Constants.Invitation.waitingForModifyeReview - Date().timeIntervalSince1970
            
        }
        
        
        if let cell = self.tableView.cellForRow(at: IndexPathTo.infoCell) as? RecepientArangementTableCell {
            cell.stopwatchView.timeInterval = self._remainedTime
        }
        
        startTimer()
		
		
        if _remainedTime == 0  {
			timeIsGone()
		}
        
        
        if observers.count == 0 {
            registerObserver()
        }
		
		
		
    }
    
    
    
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		_viewControllerIsVisible = false
        stopTimer()
		unregisterObserver()
	}
}


//    MARK: - Observers
extension MeetingArrangementController: ObserverProtocol {
	
	func registerObserver() {
		
		unregisterObserver()
		
        observers.append(fetchInviteState())
        observers.append(fetchOpponent())
	}
	
	func unregisterObserver() {
		observers.forEach {
			if let dbRef = $0 as? DatabaseReference {
				dbRef.removeAllObservers()
			}
		}
		observers.removeAll()
		
	}
    
    private func fetchInviteState() -> DatabaseReference {
        let inviteFef = FirebaseStack.invitation(for: invite.identifier).child(DMInvite.CodingKeys.state)
        inviteFef.observe(.value, with: { [weak self] snapshot in
            guard let `self` = self else { return }
            
            if let newValue = snapshot.value as? Int64 {
                
                self.inviteModel.state  = InviteState(rawValue: newValue) ?? InviteState.error
                if self.inviteModel.state.isDeclined {
                    self.executeSegue(.showFailure, sender: nil)
                }
                
            }
        })
        return inviteFef
    }
	
    private func fetchOpponent() -> DatabaseReference {
        let inviteUsersModel = InviteUsersViewModel(with: self.invite, for: managedObjectContext)
        let opponentRef = FirebaseStack.user(for: inviteUsersModel.opponent.uid!)
        opponentRef.observe(.value, with: { [weak self] snapshot in
            guard let `self` = self else { return }
            
            let nestedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            nestedContext.parent = AppDelegate.shared.mainContext
            nestedContext.perform {
                let user = nestedContext.object(with: inviteUsersModel.opponent.objectID) as! DMUser
                user.update(with: snapshot, into: nestedContext)
                
                if nestedContext.hasChanges {
                    do { try nestedContext.save() }
                    catch {
                        print("Cannot not parse the server response")
                    }
                }
                
                DispatchQueue.main.async {
                    let array = [
                        ArrangementRowType.recepientInfo.indexPath,
                        ArrangementRowType.map.indexPath
                    ]
                    
                    
                    for indexPath in array {
                        if let cell = self.tableView.cellForRow(at: indexPath) {
                            let rowType = self._dataProvider.object(at: indexPath)
                            self._model.configure(cell, for: rowType)
                        }
                    }
                    
                    
                }
                
            }
            
        })
        
        return opponentRef
        
        
    }
}

//    MARK: - Utilities
extension MeetingArrangementController  {
	//	MARK: Displaying
	fileprivate func configure(_ view: UIView) {
		switch view {
		case refuseButton:
			refuseButton.setTitle(Strings.Refuse, for: .normal)
			
		case acceptButton:
			acceptButton.setTitle(Strings.Accept, for: .normal)
			
		case tableView:
			tableView.backgroundColor = UIColor.background.main
			tableView.estimatedRowHeight = 120
			tableView.rowHeight = UITableViewAutomaticDimension
			
		default: break
		}
		
	}
}

//    MARK: - Timer
extension MeetingArrangementController  {
    /// Shows if the current invite was inherited or completely new
    var isInviteModification: Bool {
        return !self.inviteModel.invite.inheritedFrom.isEmptyOrNil
    }
    
	fileprivate func setUpTimer() {
        
        let startTime = invite.countdownStartedAt == 0 ? invite.createdValue : invite.countdownStartedAt
        if self.isInviteModification {
            self._remainedTime = startTime + Constants.Invitation.waitingForModifyeReview - Date().timeIntervalSince1970
        } else {
            self._remainedTime = startTime + Constants.Invitation.waitingForReviewTime - Date().timeIntervalSince1970
        }
        
        if let cell = self.tableView.cellForRow(at: IndexPathTo.infoCell) as? RecepientArangementTableCell {
            cell.stopwatchView.timeInterval = self._remainedTime
        }
        
        
        if self._remainedTime < 1 && ( invite.stateValue <= InviteState.reviewing.rawValue )  {
            self.stopTimer()
            timeIsGone()
        }
        
        
    }
    
    
	fileprivate func startTimer() {
		if _timer != nil {
			_timer?.invalidate()
		}
		
		_timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
			[weak self] timer in
			guard let `self` = self else { return }
			
			self._remainedTime -= 1
            
			
			if let cell = self.tableView.cellForRow(at: IndexPathTo.infoCell) as? RecepientArangementTableCell {
				cell.stopwatchView.timeInterval = self._remainedTime
			}
			
			// Time is gone
			if self._remainedTime < 1 {
				
				self.timeIsGone()
                
			}
			
		})
	}
	
	fileprivate func stopTimer() {
		_timer?.invalidate()
		_timer = nil
	}
	
	fileprivate func timeIsGone() {
        self.stopTimer()
        unregisterObserver()
        
		guard _viewControllerIsVisible == true else { return }
		self.inviteModel.state = .declinedExpired
        do { try self.managedObjectContext.save() }
        catch {
            Alert.default.showError(message: error.localizedDescription)
            executeSegue(.presentDashboard, sender: nil)
            return
        }
        
        self.invite.synchronize { [weak self] (result) in
            switch result {
            case .failure(let error):
                Alert.default.showError(message: error.localizedDescription)
            case .success:
                self?.executeSegue(.showFailure, sender: nil)
            }
        }
        
		
//		self.executeSegue(.showFailure, sender: nil)
	}
}

//    MARK: - Outlet functions
extension MeetingArrangementController  {
	//    MARK: Buttons
	@IBAction func didTapRefuse(_ button: UIButton) {
		let alertVC = UIAlertController(title: Strings.NewInvitation, message: Strings.AreYouSureYouWantToRefuseInvitation, preferredStyle: .alert)
		alertVC.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
		alertVC.addAction(UIAlertAction(title: Strings.Refuse, style: .destructive, handler: { [unowned self] _ in
			self.unregisterObserver()
            
            MessageManager.default.send(.cancel(request: self.invite, reason: InviteState.declinedByRecipient), completion: nil)
            
			self.executeSegue(.presentDashboard, sender: nil)
		}))
		AppDelegate.shared.rootVC.present(alertVC, animated: true, completion: nil)
	}

	
	
}

// MARK: - Navigation & SegueHandler protocol
extension MeetingArrangementController: SegueHandler {
	enum SegueType: String {
		case embedGallery
		case presentDating
		case presentDashboard
		case exitToUserProfile
		case showFailure
        
	}
	
	@IBAction func exitToMeetingArrangementController(_ segue: UIStoryboardSegue) {}
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		switch segueType(for: identifier) {
		case .embedGallery, .exitToUserProfile: break
		case .presentDashboard:
			stopTimer()
			if !inviteModel.state.isDeclined {
				inviteModel.state = InviteState.declinedByRecipient
				
				MessageManager.default.send(.cancel(request: inviteModel.invite, reason: inviteModel.state), completion: nil)
				
			}
            
            self.invite.synchronize(completion: { _ in
                // DO nothing
            })
            
			do { try self.managedObjectContext.save() }
			catch {
				let error = error as NSError
				Alert.default.showOk(Strings.Error, message: error.localizedDescription)
				return false
			}
		
		case .showFailure:
            stopTimer()
            unregisterObserver()
            if _failureAlreadyShown == false {
                _failureAlreadyShown = true
            } else {
                return false
            }
    
			
		case .presentDating:
			stopTimer()
			inviteModel.state = InviteState.datingBegin
            
            MessageManager.default.send(.accept(invite: self.invite), completion: nil)
            self.invite.synchronize(completion: { _ in
            })
            
			do { try self.managedObjectContext.save() }
			catch {
				let error = error as NSError
				Alert.default.showOk(Strings.Error, message: error.localizedDescription)
				return false
			}
		}
		
		
		return true
			

	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segueType(for: segue) {
		case .presentDashboard: break
			
		case .exitToUserProfile:
			let vc = segue.destination as! UserProfileController
			vc.viewControllerToReturn = self

			
		case .embedGallery:
			let vc = segue.destination as! PhotoListContoroller
			vc.userModel = UserModel(with: inviteModel.sender)
			
		case .presentDating:
			let vc = segue.destination as! DatingController
			vc.inviteModel = self.inviteModel
			
		case .showFailure:
			let vc = segue.destination as! FailureController
			vc.inviteModel = self.inviteModel
		
		}
	}
}

// MARK: - Table view protocol
extension MeetingArrangementController: UITableViewDataSource, UITableViewDelegate {
	//	MARK: Datasource
	func numberOfSections(in tableView: UITableView) -> Int {
		return _dataProvider.numberOfSection
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return _dataProvider.numberOfItems(in: section)
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let rowType = _dataProvider.object(at: indexPath)
		let cell = tableView.dequeueReusableCell(with: rowType, for: indexPath)
		_model.configure(cell, for: rowType)
		return cell
	}
	
}



