//
//  Error.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation



// MARK: - Error String
extension String: Error {
	/*
	Then you can just throw a string:
	`throw "Some Error"`
	*/
}


extension String: LocalizedError {
	public var errorDescription: String? { return self }
}


struct Errors {}
extension Errors {
	enum IviteModelValidation {
		public static let SenderIsNil = NSLocalizedString("The sender cannot be nil.\nWe are already working on this error", comment: "")
		public static let RecipientIsNil = NSLocalizedString("The recipient cannot be nil.\nWe are already working on this error", comment: "")
      public static let CannotFindEntityInContext = NSLocalizedString("Cannot find entity in the given context", comment: "")
		
	}
   
   enum DatingController {
      public static let IncorrectControllersStack = NSLocalizedString("Incorrect controllers stack in the Dating controller", comment: "")
   }
   
   enum ModifyDateController {
      public static let InheriteIdEmpty = NSLocalizedString("Cannot find the previous dating info.", comment: "")
   }
}
