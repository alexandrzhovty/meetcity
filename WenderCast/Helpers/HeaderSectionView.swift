//
//  HeaderSectionView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/6/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

protocol HeaderSectionViewProtocol: class {
	var title: String? { get set }
}

class HeaderSectionView: UITableViewHeaderFooterView {
	@IBOutlet weak var titleLabel: UILabel!
	
}

extension HeaderSectionView: NibLoadableView {}

extension HeaderSectionView: HeaderSectionViewProtocol {
	var title: String? {
		get { return titleLabel.text }
		set { titleLabel.text = newValue }
	}
}
