//
//  Tour.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

enum TourType {
    case image(image: UIImage)
    case video(url: URL)
}

class Tour {
    let message: String
    let type: TourType
    
    init(message: String, type: TourType) {
        self.message = message
        self.type = type
    }
}
