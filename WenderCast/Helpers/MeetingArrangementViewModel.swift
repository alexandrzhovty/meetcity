//
//  MeetingArrangementViewModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

struct MeetingArrangementViewModel {
	fileprivate var _inviteModel: InviteModel
	fileprivate var _recepientModel: UserModel
	fileprivate var _senderModel: UserModel

	
	
	init(with inviteModel: InviteModel) {		
		_inviteModel = inviteModel
		_recepientModel = UserModel(with: _inviteModel.recipient)
		_senderModel = UserModel(with: _inviteModel.sender)
	}
	
	var navigatinTitle: String { return _recepientModel.title }
	
	
	static func registerNibs(for tableView: UITableView) {
		tableView.register(RecepientArangementTableCell.self)
		tableView.register(InfoArangementTableCell.self)
		tableView.register(MapNearbyCell.self)
		tableView.register(InstagramPhotosCell.self)
		tableView.register(InstagramAddAccountCell.self)
	}
	
	func configure(_ cell: UITableViewCell, for arrangementRowType: ArrangementRowType) {
		switch arrangementRowType {
		case .recepientInfo:
			let cell = cell as! RecepientArangementTableCell
			cell.titleLabel.text = _senderModel.title
			cell.occupationLabel.text = _senderModel.occupation
			
		case .datingTime:
			let cell = cell as! InfoArangementTableCell
			cell.iconImageView.image = UIImage.text.time
			cell.titleLabel.text =  Formatters.date.long.string(from: _inviteModel.scheduled)
		
		case .place:
			let cell = cell as! InfoArangementTableCell
			
			var venueName: String = _inviteModel.title
			
			let venueLocation = _inviteModel.location
			
			
			///FIXME Error handling for user location
//			if let userLocation = _recepientModel.location {
				let distance = venueLocation.distance(from: _recepientModel.user.location)
				venueName.append(" (\(Formatters.distance.default.string(fromDistance: distance)))")
//			}
			
			cell.iconImageView.image = UIImage.text.place
			cell.titleLabel.text = venueName
			
			
		case .notes:
			let cell = cell as! InfoArangementTableCell
			cell.iconImageView.image = UIImage.text.notes
			cell.titleLabel.text = _inviteModel.notes
			
		case .map:
			let cell = cell as! MapNearbyCell
            if cell.mapPreseneter == nil {
                cell.mapPreseneter = MapPresenter(with: cell.mapView, type: .date(invite: _inviteModel.invite))
                cell.mapPreseneter.locateGroup()
                
            } else {
                
                cell.mapPreseneter.updateMy(AppDelegate.shared.userLocation)
                cell.mapPreseneter.display(_inviteModel.invite)
                cell.mapPreseneter.locateGroup()
                
            }
			cell.user = _senderModel.user
		}
	
		
	}
	
}


extension UITableView {
	
	func dequeueReusableCell(with arrangementRowType: ArrangementRowType, for indexPath: IndexPath) -> UITableViewCell {
		let cell: UITableViewCell
		switch arrangementRowType {
		case .recepientInfo:
			cell = self.dequeueReusableCell(RecepientArangementTableCell.self, for: indexPath)
		case .datingTime, .place, .notes:
			cell = self.dequeueReusableCell(InfoArangementTableCell.self, for: indexPath)
		case .map:
			cell = self.dequeueReusableCell(MapNearbyCell.self, for: indexPath)
		}
		return cell
		
	}
}

