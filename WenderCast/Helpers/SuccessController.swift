//
//  SuccessController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/21/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class SuccessController: UIViewController {
    //    MARK: Public
	var inviteModel: InviteModel!
    
    //    MARK: Outlets
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var placeLabel: UILabel!
	@IBOutlet weak var cancelButton: UIButton!
    
    //    MARK: Private
	fileprivate lazy var _dateFormatter: DateFormatter = {
		var dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .short
		return dateFormatter
	}()
    
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension SuccessController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		navigationItem.title = inviteModel.opponentModel.title
		navigationItem.hidesBackButton = true
		
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
		// Configure tableView
		configure(imageView)
		configure(titleLabel)
		configure(timeLabel)
		configure(placeLabel)
		configure(cancelButton)
		
    }
}

//    MARK: - Utilities
extension SuccessController  {
    fileprivate func configure(_ view: UIView) {
		switch view {
		case imageView:
			
			if let fileRef = inviteModel.opponentModel.profilePhotoeRef {
				ImageCache.default.load(forKey: fileRef) { [weak self] image  in
					guard let `self` = self else { return }
					self.imageView.image = image
				}
			}
			
		case titleLabel:
			let format = Strings.CongratulationsAcceptedYourInvite
			let name = inviteModel.opponentModel.name
			let str = String.localizedStringWithFormat(format, name)
			let attrStr = NSMutableAttributedString(string: str, attributes: [NSFontAttributeName: titleLabel.font])
			attrStr.singleOut(name, with: [NSForegroundColorAttributeName: UIColor(.pink)])
			titleLabel.attributedText = attrStr
			
		case timeLabel:
			timeLabel.text = _dateFormatter.string(from: inviteModel.scheduled)
			
		case placeLabel:
			placeLabel.text = inviteModel.venue.name
			
		case cancelButton:
			cancelButton.setTitle(Strings.Close, for: .normal)
			
		default: break
        }
        
    }
}

// MARK: - Navigation & SegueHandler protocol
extension SuccessController: SegueHandler {
    enum SegueType: String {
        case exitToNearbyList
		case presentDating
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .exitToNearbyList, .presentDating: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .exitToNearbyList: break
		case .presentDating:
			let vc = segue.destination as! DatingController
			vc.inviteModel = self.inviteModel
        }
    }
}
