//
//  FacebookFactory.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import SwiftyJSON

enum FacebookFactory: Graphable {
	
	case albums(user: String)
	case photos(album: String)
	case info(user: String)
	
	var path: String {
		switch self {
		case let .albums(uid):
			return "\(uid)/albums"
		case let .photos(aid):
			return "\(aid)/photos?fields=source"
		case let .info(uid):
			return "\(uid)"
		}
	}
	
	var method: String {
		switch self {
		case .albums, .photos, .info:
			return "GET"
		}
	}
	
	var params: [String : Any] {
		switch self {
		case .albums:
			return [:]
		case .photos:
			return [
				"width": 300,
				"height": 300,
				"redirect": false
			]
		case .info:
			return [
				"fields": "id, name, link, first_name, last_name, age_range, work, gender, email, birthday,  picture.type(large)"
			]
		}
		
	}
}

protocol Graphable {
	
	var path: String { get }
	var method: String { get}
	var params: [String : Any] { get }
}

extension Graphable {
	
//	typealias JSON = [String:Any]
	
	func request(_ handler: @escaping (JSON!) -> (), failure: @escaping (Error) -> () = { print($0) }) {
		
		let connection = FBSDKGraphRequestConnection()
		let request = FBSDKGraphRequest(
			graphPath: path,
			parameters: params,
			httpMethod: method
		)
		
		connection.add(request) {
			_ = $0.1.map ({
				
				if let json = $0 as? [String: Any], let data = json["data"] {
					handler(JSON(data))
				} else {
					handler(JSON($0))
				}
				
//				print($0)
//				print($0)
				
//				handler(($0 as? JSON)?["data"] as? [JSON])
//				handler(JSON($0))
			}) ?? $0.2.map ({ failure($0) })
		}
		connection.start()
	}
}











