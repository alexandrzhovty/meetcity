//
//  InfoNearbyCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class InfoNearbyCell: UITableViewCell, NibLoadableView {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var statusView: StatusView!
	@IBOutlet weak var ocuppationLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var aboutMeLabel: UILabel!
	@IBOutlet weak var favoritesButton: UIButton!
	@IBOutlet weak var gradienView: GradientView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		
		gradienView.colors = [UIColor.black.withAlphaComponent(0.2), UIColor.clear]
		
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
