//
//  CancelReasonCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/7/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class CancelReasonCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		titleLabel.highlightedTextColor = UIColor(.pink)
		
		let view = UIView()
		view.backgroundColor = UIColor.white
		self.selectedBackgroundView = view
	}

}
