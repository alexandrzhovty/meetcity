//
//  InviteState.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation


/// <#Description#>
enum InviteState: Int64 {
	/// Reserved value
	case error = -1
	
	/// The invitation has been created by **Sender**
	case open = 0
	
	/// The invitation was sent to the **Recipient**
	case sent = 10
	
	/// The invitation is being reviewed by **Recipient**
	case reviewing = 20
    
    /// The invitation has been accepted by **Recipient**
    case datingBegin = 30
    
    
    /// The dating was modified
    case modified = 40

	/// The invitation has been declined by **Sender**
	case declinedBySender = 50
	
	
	/// The invitation has been declined by **Recipient**
	case declinedByRecipient = 60
	
	/// The answer hasn't been recieved during the given time
	case declinedExpired = 70
	
	
	///  The dating has been declined by **Sender** after the dating had begun
	case canceledBySender = 80
	
	///  The dating has been declined by **Recipient** after the dating had begun
	case canceledByRecipient = 90
    
	
	/// The dating has been finished
	case finished = 100
	
    
    
	/// Description of state
	var description: String {
		switch self {
		case .error: return Strings.Error
		case .open: return Strings.InviteState.reviewing
		case .sent: return Strings.InviteState.sent
		case .reviewing: return Strings.InviteState.reviewing
		case .datingBegin: return Strings.InviteState.datingBegun
		case .declinedBySender: return Strings.InviteState.canceled
		case .declinedByRecipient: return Strings.InviteState.declined
		case .declinedExpired: return Strings.InviteState.expired
		case .canceledBySender: return Strings.InviteState.canceled
		case .canceledByRecipient: return Strings.InviteState.canceled
        case .modified: return Strings.InviteState.canceled
		case .finished: return Strings.InviteState.modified
		}
	}
	
	/// Invite was declined
	var isDeclined: Bool {
		switch self {
		case .declinedBySender, .declinedExpired, .declinedByRecipient:
			return true
		default: return false
		}
	}
    
    
	/// Open states
	static let openValues: [InviteState] = [.open, .sent, .reviewing, .datingBegin]
	
	/// Check if the raw state is **Open**
	///
	/// - Parameter index: raw value, which should be checked
	/// - Returns: true if invitation is open
	static func isOpen(for index: Int) -> Bool {
//		let array = InviteState.openValues.map { $0.rawValue }
//		return array.contains(Int64(index))
        return index <= Int(InviteState.datingBegin.rawValue)
            && index != Int(InviteState.finished.rawValue) && index > Int(InviteState.open.rawValue)
	}
}
