//
//  FailureController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/21/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class FailureController: UIViewController {
    //    MARK: Public
	var inviteModel: InviteModel!
    
    //    MARK: Outlets
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var cancelButton: UIButton!
	
	
    //    MARK: Private
    
	
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle  ailure
extension FailureController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
		navigationItem.title = "Rosline, 33"
		navigationItem.hidesBackButton = true
		
        // Customize appearance
        Appearance.customize(viewController: self)
		
		// Configure
		configure(imageView)
		configure(titleLabel)
		configure(descriptionLabel)
		configure(cancelButton)
		
    }
}

//    MARK: - Displaying
extension FailureController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case imageView:
			self.navigationItem.title = inviteModel.opponentModel.title
			if let fileRef = inviteModel.opponentModel.profilePhotoeRef {
				ImageCache.default.load(forKey: fileRef) { [weak self] image  in
					guard let `self` = self else { return }
					self.imageView.image = image
				}
			}
		case titleLabel:
			titleLabel.text = inviteModel.state.description
			
		case descriptionLabel:
			descriptionLabel.text = Strings.FindAnotherDate
		case cancelButton:
			cancelButton.setTitle(Strings.Close, for: .normal)
        default: break
        }
        
    }
}

//    MARK: - Outlet functions
extension FailureController  {
    //    MARK: Buttons
    @IBAction func didTapCancel(_ sender: Any) {
        var first = AppDelegate.shared.rootVC.viewControllers.first
        if first is UserProfileController {
        } else{
            first = UIStoryboard(.dashboard).instantiateViewController(UserProfileController.self)
        }
        let second = UIStoryboard(.dashboard).instantiateViewController(NearbyListController.self)
        
        if self.presentingViewController != nil {
            let viewControllers = AppDelegate.shared.rootVC.viewControllers
            if viewControllers.first is UserProfileController
                && (viewControllers.last is UserProfileController || viewControllers.last is NearbyListController || viewControllers.last is NearbyProfileController)
            {
                dismiss(animated: true, completion: nil)
            } else {
                AppDelegate.shared.rootVC.setViewControllers([first!, second], animated: false)
                dismiss(animated: true, completion: nil)
            }
        } else {
            AppDelegate.shared.rootVC.setViewControllers([first!, second], animated: true)
        }
        
        
        
    }
}

// MARK: - Navigation & SegueHandler protocol
extension FailureController: SegueHandler {
    enum SegueType: String {
        case exitToNearbyList
		case exitToDashboard
    }
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
		case .exitToDashboard: break
        case .exitToNearbyList: break
        }
    }
}
