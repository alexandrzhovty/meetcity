//
//  HistoryListController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class HistoryListController: UIViewController {
    //    MARK: Public
	var managedObjectContext: NSManagedObjectContext = AppDelegate.shared.mainContext
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var comBox: UIView!
    
    @IBOutlet weak var historyComboItem: ComboBoxExpand!
	@IBOutlet weak var datesComboItem: ComboBoxBadge!
    @IBOutlet weak var likesComboItem: ComboBoxBadge!
    @IBOutlet weak var visitorsComboItem: ComboBoxBadge!
	
    @IBOutlet weak var historyTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var datesTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var likesTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var visitorsTopConstraint: NSLayoutConstraint!
    
    
    //    MARK: Private
	fileprivate var _fetchedResultsController: NSFetchedResultsController<DMEvent>!
    fileprivate var _comboItems: [ComboBoxItem]!
    fileprivate var _dataProvider: HistoryDataProvider!
    fileprivate var _selectedType: EventType? = nil
    
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension HistoryListController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _comboItems = [historyComboItem, datesComboItem, likesComboItem, visitorsComboItem]
        
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        // Configure tableView
//        configure(tableView)
        
        configure(historyComboItem)
        configure(datesComboItem)
        configure(likesComboItem)
        configure(visitorsComboItem)
        
        
        setComboItemsHidden(true, animated: false, activeItem: historyComboItem)
        
        
        _dataProvider = HistoryDataProvider()
        
        DispatchQueue.global(qos: .background).async {
            self._dataProvider.fetch()            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

//    MARK: - Utilities
extension HistoryListController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
            /*
             case tableView:
             tableView.backgroundColor = UIColor.background.forAdditionalInfo
             tableView.tableHeaderView?.backgroundColor = UIColor.background.forAdditionalInfo
             tableView.estimatedRowHeight = 70
             tableView.rowHeight = UITableViewAutomaticDimension
             */
            
        case historyComboItem:
            historyComboItem.title = Strings.History.allHistory
            
        case datesComboItem:
            datesComboItem.title = Strings.History.dates.localizedCapitalized
            
            let request: NSFetchRequest<DMEvent> = DMInvite.fetchRequest()
            let predicates =  [
                NSPredicate(format: "%K = %@", #keyPath(DMEvent.recipientValue), AppDelegate.me),
                NSPredicate(format: "%K = %d", #keyPath(DMEvent.typeValue), EventType.invite.rawValue)
            ]
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            
            datesComboItem.quantity = try! managedObjectContext.count(for: request) 
            
            
        case likesComboItem:
            likesComboItem.title = Strings.History.likes.localizedCapitalized
            
            let request: NSFetchRequest<DMEvent> = DMInvite.fetchRequest()
            let predicates =  [
                NSPredicate(format: "%K = %@", #keyPath(DMEvent.recipientValue), AppDelegate.me),
                NSPredicate(format: "%K = %d", #keyPath(DMEvent.typeValue), EventType.like.rawValue)
            ]
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            
            likesComboItem.quantity = try! managedObjectContext.count(for: request)
            
            
        case visitorsComboItem:
            visitorsComboItem.title = Strings.History.visitors.localizedCapitalized
            
            let request: NSFetchRequest<DMEvent> = DMInvite.fetchRequest()
            let predicates =  [
                NSPredicate(format: "%K = %@", #keyPath(DMEvent.recipientValue), AppDelegate.me),
                NSPredicate(format: "%K = %d", #keyPath(DMEvent.typeValue), EventType.visit.rawValue)
            ]
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)

            visitorsComboItem.quantity = try! managedObjectContext.count(for: request)
            
            
        default: break
        }
        
    }
}

//    MARK: - Outlet functions
extension HistoryListController  {
    //    MARK: Combo items
    @IBAction func didTapComboItem(_ comboItem: ComboBoxItem) {
        
        
        if historyComboItem.isExpanded {
            let request = self.fetchedResultsController.fetchRequest
            var predicates = [
                NSPredicate(format: "%K = %@", #keyPath(DMEvent.recipientValue), AppDelegate.me)
            ]
            if let type = EventType(rawValue: Int32(comboItem.tag - 1)) {
                predicates.append(
                    NSPredicate(format: "%K = %d", #keyPath(DMEvent.typeValue), type.rawValue)
                )
            } else {
                
            }
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            do {
                try fetchedResultsController.performFetch()
                tableView.reloadData()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
            
        }
        
        let newValue = historyComboItem.isExpanded
        historyComboItem.setExpanded(!newValue, animated: true)
        setComboItemsHidden(newValue, animated: true, activeItem: comboItem)
        
    }
    
    func setComboItemsHidden(_ hidden: Bool, animated: Bool, activeItem: ComboBoxItem) {
        if hidden {
            comBox.bringSubview(toFront: activeItem)
            let constraint = topConstraint(for: activeItem)
            constraint.constant = 8
           
            for item in _comboItems where item != activeItem {
                let constraint = topConstraint(for: item)
                constraint.constant = 8
            }
            
            
            if animated {
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                self.view.layoutIfNeeded()
                
            }
            
        } else {
            var step: CGFloat = 8
            for item in _comboItems {
                let constraint = topConstraint(for: item)
                constraint.constant = step
                
                step += item.bounds.height
            }
            if animated {
                UIView.animate(withDuration: 0.25, animations: { 
                    self.view.layoutIfNeeded()
                })
            } else {
                self.view.layoutIfNeeded()
                
            }
        }
    }
    
    func topConstraint(for item: ComboBoxItem) -> NSLayoutConstraint {
        switch item {
        case historyComboItem: return historyTopConstraint
        case datesComboItem: return datesTopConstraint
        case likesComboItem: return likesTopConstraint
        case visitorsComboItem: return visitorsTopConstraint
        default:
            return historyTopConstraint
        }
    }
    
    
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension HistoryListController: SegueHandler {
    enum SegueType: String {
        case showNearbyProfile
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .showNearbyProfile:
			if tableView.indexPathForSelectedRow == nil {
				return false
			}
        }
		
		return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .showNearbyProfile:
			let vc = segue.destination as! NearbyProfileController
            vc.managedObjectContext = AppDelegate.shared.mainContext
            let indexPath = tableView.indexPathForSelectedRow!
            let event = fetchedResultsController.object(at: indexPath)
            vc.user = event.opponent
            
        }
    }
}

// MARK: - UITableViewDataSource
extension HistoryListController: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.fetchedResultsController.sections?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let sectionInfo = self.fetchedResultsController.sections![section]
		return sectionInfo.numberOfObjects
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(HistoryTableCell.self, for: indexPath)
        let model = HistoryEventViewModel(with: self.fetchedResultsController.object(at: indexPath))
        model.configure(cell)
		return cell
	}
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			let context = fetchedResultsController.managedObjectContext
			context.delete(fetchedResultsController.object(at: indexPath))
			
			do {
				try context.save()
			} catch {
				// Replace this implementation with code to handle the error appropriately.
				// fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
		}
	}
	
}


// MARK: - NSFetchedResultsControllerDelegate
extension HistoryListController: NSFetchedResultsControllerDelegate {
	var fetchedResultsController: NSFetchedResultsController<DMEvent> {
		if _fetchedResultsController != nil {
			return _fetchedResultsController!
		}
		
		let fetchRequest: NSFetchRequest<DMEvent> = DMEvent.fetchRequest()
		fetchRequest.fetchBatchSize = 20
		
		fetchRequest.sortDescriptors = [
			NSSortDescriptor(key: #keyPath(DMEvent.createdValue), ascending: false)
		]
		
		fetchRequest.predicate = NSPredicate(format: "%K = %@", #keyPath(DMEvent.recipientValue), AppDelegate.me)
		
		let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
		aFetchedResultsController.delegate = self
		_fetchedResultsController = aFetchedResultsController
		
		do {
			try _fetchedResultsController!.performFetch()
		} catch {
			let nserror = error as NSError
			fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
		}
		
		return _fetchedResultsController!
	}
	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		self.tableView.beginUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
		switch type {
		case .insert:
			self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
		case .delete:
			self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
		default:
			return
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		switch type {
		case .insert:
			tableView.insertRows(at: [newIndexPath!], with: .fade)
		case .delete:
			tableView.deleteRows(at: [indexPath!], with: .fade)
		case .update:
			break
//			if let _: IndexPath = indexPath, let cell = tableView.cellForRow(at: indexPath!), let bar = anObject as? Bar {
//				self.configure(cell, withEntity: bar)
//			}
		case .move:
			tableView.moveRow(at: indexPath!, to: newIndexPath!)
		}
	}
	
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        configure(datesComboItem)
        configure(likesComboItem)
        configure(visitorsComboItem)
        
		self.tableView.endUpdates()
	}
}

