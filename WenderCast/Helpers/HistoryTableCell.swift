//
//  HistoryTableCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import AlamofireImage

class HistoryTableCell: UITableViewCell {

    @IBOutlet weak var icomImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.title = nil
        self.body = nil
        self.photo = nil
    }
}

extension HistoryTableCell {
    var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    var body: String? {
        get { return bodyLabel.text }
        set { bodyLabel.text = newValue }
    }
    
    var photo: UIImage? {
        get { return icomImageView.image }
        set { icomImageView.image = newValue?.af_imageRoundedIntoCircle() }
    }
    
}
