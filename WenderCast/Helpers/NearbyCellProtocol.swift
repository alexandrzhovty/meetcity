//
//  NearbyCellProtocol.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreLocation

protocol NearbyCellProtocol {
   var title: String? { get set }
   var readyToMeet: Bool { get set }
   var image: UIImage? { get set }
   var distance: CLLocationDistance { get set }
}
