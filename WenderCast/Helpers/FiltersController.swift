//
//  FiltersController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

protocol FiltersControllerDelegate: NSObjectProtocol {
	func filtersControllerDidUpdateFilterParametres(_ viewController: FiltersController)
}

//    MARK: - Properties & variables
final class FiltersController: UIViewController {
    //    MARK: Public
	var userModel: UserModel!
	weak var delegate: FiltersControllerDelegate?
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var lookForSegmentControl: UISegmentedControl!
    @IBOutlet weak var statusSegmentControl: UISegmentedControl!
//	@IBOutlet weak var rangeSlider: RangeSlider!
	
	@IBOutlet weak var rangeSlider: MARKRangeSlider!
	@IBOutlet weak var rangeLabel: UILabel!
	
    //    MARK: Private
	
	enum Settings {
		static let minValueKey = "FiltersController.Settings.minValueKey"
		static let maxValueKey = "FiltersController.Settings.maxValueKey"
        static let readyToMeetStatusKey = "FiltersController.Settings.readyToMeetStatusKey"
	}
	
	
}

//    MARK: - View life cycle
extension FiltersController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
        assert(userModel != nil, "User model cannot be nil")
		
        // Customize appearance
        Appearance.customize(viewController: self)
		
		// Configure
		configure(lookForSegmentControl)
        configure(statusSegmentControl)
		configure(rangeSlider)
		configure(rangeLabel)
        configure(self.view)
        
    }
}

//    MARK: - Utilities
extension FiltersController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
        case self.view:
            view.backgroundColor = UIColor.white.withAlphaComponent(0.75)
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.insertSubview(blurEffectView, at: 0)
            
		case lookForSegmentControl:
			lookForSegmentControl.selectedSegmentIndex = Int(userModel.lookFor.rawValue)
            
        case statusSegmentControl:
            statusSegmentControl.selectedSegmentIndex = UserDefaults.standard.integer(forKey: Settings.readyToMeetStatusKey)
			
		case rangeSlider:
//			[self.rangeSlider setMinValue:0.0 maxValue:1.0];
//			[self.rangeSlider setLeftValue:0.2 rightValue:0.7];
//			
//			self.rangeSlider.minimumDistance = 0.2;
			rangeSlider.backgroundColor = UIColor.clear
			rangeSlider.setMinValue(CGFloat(Constants.allowedAge.min), maxValue: CGFloat(Constants.allowedAge.max))
			rangeSlider.setLeftValue(CGFloat(UserDefaults.standard.float(forKey: Settings.minValueKey)), rightValue: CGFloat(UserDefaults.standard.float(forKey: Settings.maxValueKey)))
			rangeSlider.minimumDistance = 4
			rangeSlider.addTarget(self, action: #selector(rangeSliderValueChanged(_:)), for: .valueChanged)
			
		case rangeLabel:
			rangeLabel.text = "\(Int(rangeSlider.leftValue)) - \(Int(rangeSlider.rightValue))"
			
        default: break
        }
        
    }
}

//    MARK: - Outlet functions
extension FiltersController  {
	//    MARK: Segmented control
	@IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        
        switch sender {
        case lookForSegmentControl:
            userModel.lookFor = LookFor(rawValue: Int16(sender.selectedSegmentIndex)) ?? LookFor.none
            FirebaseStack.user(for: "me").child(DMUser.CodingKeys.lookFor).setValue(sender.selectedSegmentIndex)
            delegate?.filtersControllerDidUpdateFilterParametres(self)
            
        case statusSegmentControl:
            UserDefaults.standard.set(statusSegmentControl.selectedSegmentIndex, forKey: Settings.readyToMeetStatusKey)
            UserDefaults.standard.synchronize()
            delegate?.filtersControllerDidUpdateFilterParametres(self)
            
        default: break
        }
	}
	
	//    MARK: Range slider
	@IBAction func rangeSliderValueChanged(_ sender: MARKRangeSlider) {
		
		UserDefaults.standard.set(Int(rangeSlider.leftValue), forKey: Settings.minValueKey)
		UserDefaults.standard.set(Int(rangeSlider.rightValue), forKey: Settings.maxValueKey)
		UserDefaults.standard.synchronize()
		
		configure(rangeLabel)
		
		delegate?.filtersControllerDidUpdateFilterParametres(self)
		
	}``````````````````````````
	
}

// MARK: - Navigation & SegueHandler protocol
extension FiltersController: SegueHandler {
    enum SegueType: String {
        case exitToNearbyList
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .exitToNearbyList: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .exitToNearbyList: break
        }
    }
}
