//
//  LookFor.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

enum LookFor: Int16 {
	case none = -1
	case boy
	case girl
	case both
	
	var description: String? {
		switch self {
		case .none:	return nil
		case .boy:	return Strings.Boy
		case .girl:	return Strings.Girl
		case .both:	return Strings.Both
		}
	}
	
//	init(value: Int?) {
//		self = LookFor(rawValue: value ?? LookFor.none.rawValue) ?? LookFor.none
//	}
}
