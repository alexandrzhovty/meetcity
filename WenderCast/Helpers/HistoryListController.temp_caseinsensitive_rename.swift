//
//  HistoryListController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class HistoryListController: UIViewController {
    //    MARK: Public
    
    //    MARK: Outlets
    
    //    MARK: Private
    
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension HistoryListController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = Strings.History
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        /*
         // Configure tableView
         configure(tabelView)
         */
    }
}

//    MARK: - Utilities
extension HistoryListController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
            /*
             case tableView:
             tableView.backgroundColor = UIColor.background.forAdditionalInfo
             tableView.tableHeaderView?.backgroundColor = UIColor.background.forAdditionalInfo
             tableView.estimatedRowHeight = 70
             tableView.rowHeight = UITableViewAutomaticDimension
             */
        default: break
        }
        
    }
}

//    MARK: - Outlet functions
extension HistoryListController  {
    //    MARK: Buttons
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension HistoryListController: SegueHandler {
    enum SegueType: String {
        case none
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .none: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .none: break
        }
    }
}
