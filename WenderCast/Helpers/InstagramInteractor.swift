//
//  InstagramInteractor.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 5/26/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

// https://www.instagram.com/developer/endpoints/users/#get_users_feed
//
// For example, if you want to look for images from tag #awesomeevent, you will do an api query to:
// https://api.instagram.com/v1/tags/awesomeevent/media/recent?access_token=ACCESS-TOKEN
//


// gomeetcity@gmail.com
// Cf4PiLbcGFZF


//https://stackoverflow.com/questions/20470722/fetch-photos-from-instagram
//
//https://api.instagram.com/v1/users/3/media/recent/?access_token=ACCESS-TOKEN


import UIKit
//import Alamofire
import SwiftyJSON

class InstagramInteractor: UIView {
   
//   func requestUserPhotos(for user: UserModel, completion: @escaping ([String]) -> Void) {
//      if let accessToken = user.instagramtoken, let instaUserId = user.instagramid, accessToken.characters.count > 0, instaUserId.characters.count > 0 {
//         self.requestPhotos(for: instaUserId, accessToken: accessToken, completion: completion)
//      } else if let accessToken = user.instagramtoken, accessToken.characters.count > 0 {
//         requestMediaCount(for: accessToken) { countMedia, instaUserId in
//            if let instaUserId = instaUserId {
//               user.instagramid = instaUserId
//               UsersManager.updateUser(user) { _ in
//                  self.requestPhotos(for: instaUserId, accessToken:  accessToken, completion: completion)
//               }
//            } else {
//               completion([])
//            }
//         }
//      } else {
//         completion([])
//      }
//   }
	
   fileprivate func requestPhotos(for userId: String, accessToken: String, completion: @escaping ([String]) -> Void) {

//      https://api.instagram.com/v1/users/{user-id}/media/recent/?access_token=ACCESS-TOKEN
//      Get the most recent media published by a user.
//      The public_content scope is required if the user is not the owner of the access_token.

//      REQUIREMENTS
//      Scope: public_content

//      PARAMETERS
//      ACCESS_TOKEN	A valid access token.
//      MAX_ID	Return media earlier than this max_id.
//      MIN_ID	Return media later than this min_id.
//      COUNT	Count of media to return.

// RESPONCE:
      
// {
//   "pagination" : {
//
//   },
//   "meta" : {
//     "code" : 200
//   },
//   "data" : [
//     {
//       "id" : "854257683028320280_213140839",
//       "created_time" : "1416055473",
//       "caption" : null,
//       "comments" : {
//         "count" : 0
//       },
//       "link" : "https:\/\/www.instagram.com\/p\/va7tYvjSAY\/",
//       "likes" : {
//         "count" : 4
//       },
//       "tags" : [
//
//       ],
//       "type" : "image",
//       "users_in_photo" : [
//
//       ],
//       "location" : null,
//       "filter" : "Normal",
//       "images" : {
//         "low_resolution" : {
//           "width" : 320,
//           "url" : "https:\/\/scontent.cdninstagram.com\/t51.2885-15\/s320x320\/e15\/10808766_987837194566433_65973101_n.jpg",
//           "height" : 320
//         },
//         "standard_resolution" : {
//           "width" : 640,
//           "url" : "https:\/\/scontent.cdninstagram.com\/t51.2885-15\/e15\/10808766_987837194566433_65973101_n.jpg",
//           "height" : 640
//         },
//         "thumbnail" : {
//           "width" : 150,
//           "url" : "https:\/\/scontent.cdninstagram.com\/t51.2885-15\/s150x150\/e15\/10808766_987837194566433_65973101_n.jpg",
//           "height" : 150
//         }
//       },
//       "user_has_liked" : false,
//       "user" : {
//         "username" : "sergey_krotkih",
//         "id" : "213140839",
//         "full_name" : "Sergiy Krotkih",
//         "profile_picture" : "https:\/\/scontent-ams3-1.cdninstagram.com\/t51.2885-19\/11906329_960233084022564_1448528159_a.jpg"
//       },
//       "attribution" : null
//     }
//   ]
// }
      
//      Alamofire.request("https://api.instagram.com/v1/users/\(userId)/media/recent/?access_token=\(accessToken)").responseJSON { (responseData) -> Void in
//         if((responseData.result.value) != nil) {
//            let swiftyJsonVar = JSON(responseData.result.value!)
//            
////            print("\(swiftyJsonVar)")
//            
//            var urls = [String]()
//            if let data = swiftyJsonVar["data"].arrayObject {
//               for index in 0..<data.count {
//                  if let dataItem = data[index] as? Dictionary<String, Any> {
//                     if let images = dataItem["images"] as? Dictionary<String, Any> {
//                        if let lowresolution = images["low_resolution"] as? Dictionary<String, Any> {
//                           if let url = lowresolution["url"] as? String {
//                              urls.append(url)
//                           }
//                        }
//                     }
//                  }
//               }
//            }
//            completion(urls)
//         } else {
//            completion([])
//         }
//      }
   }
   
   fileprivate func requestMediaCount(for accessToken: String, completion: @escaping (Int, String?) -> Void) {
      //      https://api.instagram.com/v1/users/self/?access_token=ACCESS-TOKEN
      //      Get information about the owner of the access_token.
      
      //      {
      //         "meta" : {
      //            "code" : 200
      //         },
      //         "data" : {
      //            "counts" : {
      //               "media" : 1,
      //               "follows" : 9,
      //               "followed_by" : 19
      //            },
      //            "website" : "",
      //            "id" : "213140839",
      //            "profile_picture" : "https:\/\/scontent-dft4-3.cdninstagram.com\/t51.2885-19\/11906329_960233084022564_1448528159_a.jpg",
      //            "bio" : "",
      //            "username" : "sergey_krotkih",
      //            "full_name" : "Sergiy Krotkih"
      //         }
      //      }
      
      
//      Alamofire.request("https://api.instagram.com/v1/users/self/?access_token=\(accessToken)").responseJSON { (responseData) -> Void in
//         if((responseData.result.value) != nil) {
//            let swiftyJsonVar = JSON(responseData.result.value!)
//            
//            print("\(swiftyJsonVar)")
//            
//            var mediaCount = 0
//            var userId = ""
//            if let data = swiftyJsonVar["data"].dictionaryObject {
//               if let counts = data["counts"] as? Dictionary<String, Any> {
//                  if let media = counts["media"] as? Int {
//                     mediaCount = media
//                  }
//                  if let id = data["id"] as? String {
//                     userId = id
//                  }
//               }
//            }
//            completion(mediaCount, userId)
//         } else {
//            completion(0, nil)
//         }
//      }
   }
}
