//
//  MapNearbyCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import Firebase
import SwiftyJSON
import GoogleMaps
import AlamofireImage

final class MapNearbyCell: UITableViewCell {
	// MARK: Static
	static let cellHeight: CGFloat = 200
    
    var mapPreseneter: MapPresenter!
	
	// MARK: Public
	var user: DMUser! {
		didSet {
//			unregisterObserver()
//			registerObserver()
			
			updateUserMarker()
		}
	}
	
	
	
	var observers = [Any]()
	
	// MARK: Private
	fileprivate var _marker: GMSMarker!
	
	
	// MARK: Outlet
	@IBOutlet weak var mapView: GMSMapView!
	
	
	// MARK:  Life cycle
	deinit {
		unregisterObserver()
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		// Settings
		mapView.isMyLocationEnabled = false
		mapView.settings.compassButton = false
		mapView.settings.myLocationButton = false
		mapView.settings.scrollGestures = false
		mapView.settings.zoomGestures = false
		
		// Style
//		let kMapStyle = try! String(contentsOf: Resources.JSON.mapStyle.url)
//		do {
//			mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
//		} catch {
//			print(String(describing: type(of: self)),":", #function, " One or more of the map styles failed to load. \(error)")
//		}
		
	}
	
	private func updateUserMarker() {
        
        guard  mapPreseneter == nil else {
            return
        }
		
		guard CLLocationCoordinate2DIsValid(user.location.coordinate) else {
			_marker.map = nil
			return
		}
		
		self.mapView.camera = GMSCameraPosition.camera(withTarget: user.location.coordinate, zoom: 15.0)
		self.mapView.settings.myLocationButton = false
		
		if self._marker == nil {
			
			let markerMapView = MapMarkerView.instantiateFromInterfaceBuilder()
			
			let marker = GMSMarker()
			marker.position = user.location.coordinate
			marker.title = user.title
			marker.map = self.mapView
			
			if let photoRef = user.profilePhotoeRef {
				ImageCache.default.load(forKey: photoRef, completion: { [weak markerMapView] image in
					guard let markerMapView = markerMapView else { return }
					markerMapView.iconImageView.image = image?.af_imageRoundedIntoCircle()
					
				})
				
			}
			
			
			marker.iconView = markerMapView
			marker.groundAnchor = CGPoint(x: 1, y: 0.5)
			
			////				marker.userData = entity
		}

	}

	
}


extension MapNearbyCell: NibLoadableView {}

extension MapNearbyCell: ObserverProtocol {
	func registerObserver() {
		
        guard  mapPreseneter == nil else {
            return
        }
		
		// FIXMWE: Very very strange source code
		let locationRef = FirebaseStack.user(for: user.identifier.rawValue)
		locationRef.observe(.value, with: { [weak self] snapshot in
			guard let `self` = self else { return }
			guard snapshot.exists() else { return }
			
			self.mapView.camera = GMSCameraPosition.camera(withTarget: self.user.location.coordinate, zoom: 15.0)
			self.mapView.settings.myLocationButton = false
			
			if self._marker == nil {
				let marker = GMSMarker()
				marker.position = self.user.location.coordinate
				marker.title = self.user.title
				marker.map = self.mapView
				marker.icon = UIImage(named: "icon-mapmarker")
			}

		})
		
		
		observers.append(locationRef)
	}
	
	func unregisterObserver() {
		observers.forEach{
			if let ref = $0 as? DatabaseReference {
				ref.removeAllObservers()
			}
		}
	}
}
