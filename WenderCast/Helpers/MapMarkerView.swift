//
//  MapMarkerView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

final class MapMarkerView: UIView {

	@IBOutlet weak var iconImageView: UIImageView!
	@IBOutlet weak var markerImageView: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
	
		iconImageView.backgroundColor = UIColor.gray
		iconImageView.layer.cornerRadius = iconImageView.bounds.width / 2
		iconImageView.clipsToBounds = true
		
	}

}

extension MapMarkerView: NibLoadableView {}
