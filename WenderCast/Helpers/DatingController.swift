//
//  DatingController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import GoogleMaps

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class DatingController: UIViewController  {
    //    MARK: Public
	var inviteModel: InviteModel!
    var invite: DMInvite!
    var manangedObjectContext: NSManagedObjectContext!
	var observers = [Any]()
    
    
	
    //    MARK: Outlets
	@IBOutlet weak var timeLeftLabel: UILabel!
	@IBOutlet weak var placeNameLabel: UILabel!
	@IBOutlet weak var actionButton: UIButton!
	@IBOutlet weak var sendButton: UIButton!
	
	@IBOutlet weak var mapView: GMSMapView!
    
    //    MARK: Private
	
	/// Timer
	fileprivate var _timer: Timer?
	/// Timer duration
	fileprivate var _timeLeftToDating: TimeInterval = 0
	fileprivate var _timerIsAlreadyStarted = false
	fileprivate var _viewControllerIsVisible = false
	fileprivate var _meInside = false
	fileprivate var _observerQueue =  DispatchQueue(label: DatingController.budleIdenitifier, attributes: .concurrent)
	
	
	fileprivate var _mapPresenter: MapPresenter!
    
    //    MARK: Enums & Structures
	
	deinit {
		unregisterObserver()
	}
	
}

extension DatingController: BundleIdentifier {}

//    MARK: - View life cycle
extension DatingController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
		assert(inviteModel.context == AppDelegate.shared.mainContext, "The context should be main")
        
        self.invite = inviteModel.invite
        self.manangedObjectContext = inviteModel.context
        
        // Customize appearance
        Appearance.customize(viewController: self)
		navigationItem.title = inviteModel.opponentModel.title
		
		// Time left
		_timeLeftToDating = inviteModel.scheduled.timeIntervalSince1970 - Date().timeIntervalSince1970
		inviteModel.state = InviteState.datingBegin

		if _timeLeftToDating < 0 {
			let calendar = Calendar.autoupdatingCurrent
			inviteModel.scheduled = calendar.date(byAdding: .hour, value: 2, to: Date())!
			_timeLeftToDating = inviteModel.scheduled.timeIntervalSince1970 - Date().timeIntervalSince1970
		}
		
		
		
		_mapPresenter = MapPresenter(with: mapView, type: MapPresenterType.date(invite: self.inviteModel.invite))
		
		
		
		
        // Configure tableView
        configure(timeLeftLabel)
		configure(placeNameLabel)
		configure(actionButton)
		configure(sendButton)
		
		configure(mapView)
        mapView.isHidden = true
		
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		_timeLeftToDating = inviteModel.scheduled.timeIntervalSince1970 - Date().timeIntervalSince1970
		configure(timeLeftLabel)
		
		startTimer()
		
        _mapPresenter.locateGroup()
        mapView.isHidden = false
        
        _meInside = _mapPresenter.updateMy(AppDelegate.shared.userLocation)
        configure(actionButton)
        
        if inviteModel.isCanceled == false {
            registerObserver()
        }
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		stopTimer()
		unregisterObserver()
	}
	
}

//    MARK: - Displaying
extension DatingController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case actionButton:
			
			let currentState = (_meInside, inviteModel.isCheckedIn)
			
			switch currentState {
			case (false, false):
				actionButton.setBackgroundImage(UIImage(named: "Decline-Btn-Bkg"), for: .normal)
				actionButton.setTitle(Strings.CancelDate, for: .normal)
				sendButton.setTitle(Strings.ModifyDate, for: .normal)
				sendButton.isHidden = false
			
			case (true, false):
				actionButton.setBackgroundImage(UIImage(named: "CheckIn-Btn-Bkg"), for: .normal)
				actionButton.setTitle(Strings.CheckIn, for: .normal)
				sendButton.isHidden = true
				
			case (_, true):
				actionButton.setBackgroundImage(UIImage(named: "CheckIn-Btn-Bkg"), for: .normal)
				actionButton.setTitle(Strings.EndDate, for: .normal)
				sendButton.setTitle(Strings.SendMessage, for: .normal)
				sendButton.isHidden = false
			}
			
		case timeLeftLabel:
			if _timeLeftToDating < 0 {
				timeLeftLabel.text = "Opps"
			} else {
				let ti = Int(_timeLeftToDating)
				
				let seconds = ti % 60
				let minutes = (ti / 60) % 60
				let hours = (ti / 3600)
				
				
				timeLeftLabel.text = String(format: "%0.2d:%0.2d:%0.2d", hours, minutes, seconds)
			}
			
		case placeNameLabel:
			placeNameLabel.text = inviteModel.title
			
			
        default: break
        }
    }
	
}

// MARK: - Timer
extension DatingController {
	fileprivate func startTimer() {
		if _timer != nil {
			_timer?.invalidate()
		}
		
		_timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
			[weak self] timer in
			guard let `self` = self else { return }
			
			self._timeLeftToDating -= 1
			self.configure(self.timeLeftLabel)
			
			// Time is gone
			if self._timeLeftToDating == 0 {
				self.stopTimer()
				self.timeIsGone()
			}
			
		})
	}
	
	fileprivate func stopTimer() {
		_timer?.invalidate()
		_timer = nil
	}
	
	fileprivate func timeIsGone() {
		
	}
	
	
	//MARK: Server date
	fileprivate func synchronizeInviteModel() -> Bool {
        
        invite.synchronize { _ in
            // TODO: Noting
        }
        
		do { try manangedObjectContext.save() }
		catch {
			Alert.default.showOk(Strings.Error, message: error.localizedDescription)
			return false
		}
		return true
	}
}

// MARK: - ObserverProtocol
extension DatingController: ObserverProtocol {
	func registerObserver() {
        
        
		
		// dating cancelation
		let ref = FirebaseStack.invitation(for: invite.identifier).child(DMInvite.CodingKeys.cancelReason)
        ref.observe(.value, with: { [weak self] snapshot in
            guard let `self` = self else { return }
            
            if let newValue = snapshot.value as? Int {
                if let newState = CancelDateReasons(rawValue: newValue), newState != .none  {
                    
                    let inviteState: InviteState
                    if self.inviteModel.sender.uid == AppDelegate.me.uid {
                        inviteState = InviteState.canceledBySender
                    } else {
                        inviteState = InviteState.canceledByRecipient
                    }
                    
                    if self.inviteModel.state != inviteState {
                        self.inviteModel.state = inviteState
                        self.inviteModel.cancelReason = newState
                        self.executeSegue(.showFailure, sender: nil)
                    }
                }
            }
        })
        observers.append(ref)
        
        // Invite state 
        let stateRef = FirebaseStack.invitation(for: invite.identifier).child(DMInvite.CodingKeys.state)
        stateRef.observe(.value, with: { [weak self] snapshot in
            guard let `self` = self else { return }
            guard let newValue = snapshot.value as? Int64 else { return }
            if newValue == InviteState.finished.rawValue {
                self.invite.stateValue = newValue
                self.performSegue(.showRates, sender: nil)
            }
            
        })
        observers.append(stateRef)
		
		
		
		// My location
		let center = NotificationCenter.default
        let queue = OperationQueue.main
        observers.append(
            center.addObserver(forName: Notification.Name.LocationManager.didChangeLocation, object: nil, queue: queue) {
                [weak self] note in
                guard let `self` = self else { return }
                guard let location = note.object as? CLLocation else { return }
                let oldValue = self._meInside
                self._meInside = self._mapPresenter.updateMy(location)
                
                if oldValue != self._meInside {
                    self.configure(self.actionButton)
                    
                }
                
                
            }
        )
        
        
        // Opponet check in
        let listenerCheckinField = invite.sender.uid == AppDelegate.me.uid ? DMInvite.CodingKeys.recipientCheckined : DMInvite.CodingKeys.senderID
        let opponentCheckInRef = FirebaseStack.invitation(for: invite.identifier).child(listenerCheckinField)
        opponentCheckInRef.observe(.value, with: { [weak self] snapshot in
            guard let `self` = self else { return }
            
            
            self.navigationItem.title = self.invite.opponent.title
            
            if self.invite.sender.uid == AppDelegate.me.uid {
                self.invite.recipientCheckined = snapshot.value as? Bool ?? false
            } else {
                self.invite.senderCheckined = snapshot.value as? Bool ?? false
            }
            
        })
        observers.append(opponentCheckInRef)
        
        
        
	}
	
	func unregisterObserver() {
		observers.forEach {
			if let dbRef = $0 as? DatabaseReference {
				dbRef.removeAllObservers()
            } else {
                NotificationCenter.default.removeObserver($0)
            }
		}
		observers.removeAll()
	}
}

//    MARK: - Outlet functions
extension DatingController  {
    //    MARK: Buttons
	@IBAction func didTapAction(_ button: UIButton) {
		
        let currentState = (_meInside, inviteModel.isCheckedIn, button == actionButton)
        
        let segueToGo: SegueType
        
        switch currentState {
        case (false, false, true): segueToGo = .showCancelReasons
        case (false, false, false): segueToGo = .modifyDating
        case (true, false, _): segueToGo = .showCheckIn
        case (_, true, true): segueToGo = .showRates
        case (_, true, false): segueToGo = .showChat
        default: return
        }
        
        self.executeSegue(segueToGo, sender: button)
		
		
	}
}

// MARK: - Navigation & SegueHandler protocol
extension DatingController: SegueHandler {
    enum SegueType: String {
        case exitToUserProfile
		case modifyDating
		case exitToDashboard
		case showCheckIn
		case showChat
		case showRates
		case showCancelReasons
		case showFailure
        case presentArrangement
    }
	
	@IBAction func exitToDatingController(_ segue: UIStoryboardSegue) {
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
		case .showCheckIn:
			inviteModel.isCheckedIn = true
			return synchronizeInviteModel()
			
		case .showRates:
            invite.stateValue = InviteState.finished.rawValue
            invite.synchronize(completion: { [weak self] result in
                guard let `self` = self else { return }
                if case .success = result {
                    MessageManager.default.send(.finish(dating: self.invite), completion: nil)
                }
            })
			
		default: break
        }
		return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
		case .exitToDashboard: break
		case .exitToUserProfile:
			let vc = segue.destination as! UserProfileController
			vc.viewControllerToReturn = self
			
		case .showCheckIn:
			let vc = segue.destination as! CheckInController
            vc.navigationItem.hidesBackButton = true
			vc.inviteModel = self.inviteModel
			configure(actionButton)
			
		case .showChat:
            let vc = segue.destination as! ChatController
			vc.inviteModel = self.inviteModel
			
		case .showRates:
			let vc = segue.destination as! FeedbackController
			vc.navigationItem.hidesBackButton = true
			vc.invite = self.invite
            vc.managedObjectContext = self.manangedObjectContext
			
			
		case .showFailure:
			let vc = segue.destination as! FailureController
			vc.inviteModel = self.inviteModel
			
		case .showCancelReasons:
			let vc = (segue.destination as! UINavigationController).topViewController as! CancelReasonListController
            
			vc.competion { reason in
				
				let inviteState: InviteState
				if self.inviteModel.sender.uid == AppDelegate.me.uid {
					inviteState = InviteState.canceledBySender
				} else {
					inviteState = InviteState.canceledByRecipient
				}
				
				self.inviteModel.cancelReason = reason
				self.inviteModel.state = inviteState
				
                self.invite.synchronize { _ in
                    // TODO: Nothing
                }
                
                do { try self.manangedObjectContext.save() }
				catch {
					Alert.default.showError(message: error.localizedDescription)
				}
                
				self.executeSegue(.exitToDashboard, sender: nil)
			
			}
			
        case .presentArrangement:
            let vc = (segue.destination as! UINavigationController).topViewController as! MeetingArrangementController
            vc.inviteModel = self.inviteModel
            
            
        case .modifyDating:
            let vc = (segue.destination as! UINavigationController).topViewController as! ModifyDatingController
            
            let nestedContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            nestedContext.parent = AppDelegate.shared.mainContext
        
            
            let newInvite = DMInvite.inherit(from: self.invite, into: nestedContext)
            
            vc.invite = newInvite
            vc.managedObjectContext = nestedContext
            
            
            vc.comletion { (newInvite, context) in
                // Save in the nested context
                do { try context.save() }
                catch {
                    Alert.default.showError(message: error.localizedDescription)
                    return
                }
                
                
                
                let createdInvite = self.manangedObjectContext.object(with: newInvite.objectID) as! DMInvite
                print(createdInvite)
                
                // Synchronize with firebase
                createdInvite.synchronize(completion: { [weak self] (result) in
                    
                    guard let `self` = self else { return }
                    
                    switch result {
                    case let .failure(error):
                        Alert.default.showError(message: error.localizedDescription)
                    case .success:
                        
                        // Second it is neccessary to close the previous invite
                        self.invite.stateValue = Int64(InviteState.modified.rawValue)
                        
                        // Save main context
                        do { try self.manangedObjectContext.save() }
                        catch {
                            Alert.default.showError(message: error.localizedDescription)
                            return
                        }
                        
                        // Synchronize with firebase
                        self.invite.synchronize(completion: { [weak self] (result) in
                            guard let `self` = self else { return }
                            switch result {
                            case let .failure(error):
                                Alert.default.showError(message: error.localizedDescription)
                                
                            case .success:
                                // Show request controller with new invite request
                                let vc = UIStoryboard(.invite).instantiateViewController(RequestingController.self)
                                vc.invite = createdInvite
                                vc.managedObjectContext = AppDelegate.shared.mainContext
                                
                                
                                // The first controller must be UserProfileController
                                guard let first = self.navigationController?.viewControllers.first as? UserProfileController else {
                                    Alert.default.showError(message: Errors.DatingController.IncorrectControllersStack)
                                    return
                                }
                                self.navigationController?.setViewControllers([first, vc], animated: true)
                                
                                
                            }
                        })
                        
                        
                        
                    }
                    
                })
                
                
                
                
            }
        }
        
    }
}
