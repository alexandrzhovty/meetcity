//
//  InteractorTransition.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/24/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class InteractorTransition: UIPercentDrivenInteractiveTransition {
	var hasStarted = false
	var shouldFinish = false
}
