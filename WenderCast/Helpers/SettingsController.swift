//
//  SettingsController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class SettingsController: UIViewController, ProfilePresenterControllerProtocol {
    //    MARK: Public
	var userModel: UserModel!
	var dataSource: DataSource!
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var shareButton: UIButton!
	
    //    MARK: Private
	fileprivate var _presenter: ProfilePresenter!
	

}

//    MARK: - View life cycle
extension SettingsController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
        // Customize appearance
        Appearance.customize(viewController: self)
		
		dataSource = SettingsDataSource(with: self)
		_presenter = ProfilePresenter(with: self)
		
		navigationItem.title = Strings.Settings
        
		
		// Configure tableView
		configure(tableView)
		_presenter.setup(tableView)
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if let indexPath = tableView.indexPathForSelectedRow {
			tableView.deselectRow(at: indexPath, animated: true)
		}
	}
	
}

//    MARK: - Utilities
extension SettingsController  {
    fileprivate func configure(_ view: UIView) {
		switch view {
		case tableView:
			tableView.backgroundColor = UIColor.background.settings
			
			tableView.estimatedRowHeight = Constants.TableView.profileCellHeight
			tableView.rowHeight = UITableViewAutomaticDimension
			
			let view = UIView()
			view.backgroundColor = UIColor.separator.tableViewCells
			view.frame.size.height = 0.5
			tableView.tableFooterView = view
			
		default: break
		}
		
    }
}

//    MARK: - Outlet functions
extension SettingsController  {
    //    MARK: Buttons
	@IBAction func didTapShare(_ button: UIButton) {
//		dismiss(animated: true, completion: nil)
	}
	
	
	@IBAction func didTapExit(_ sender: Any) {
		
		let alertController = UIAlertController(title: Strings.Authorization, message: Strings.DoYouRealyWantToExit, preferredStyle: .alert)
		alertController.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
		alertController.addAction(
            UIAlertAction(title: Strings.SignOut, style: .destructive) { [unowned self] _ in
                switch AppDelegate.logOut() {
                case .success:
                    Presenter.presentLoginScreen()
                    self.dismiss(animated: true, completion: nil)
                    
                case let .failure(error):
                    Alert.default.showError(message: error.localizedDescription)
                }
                
                
            }
        )
		
		present(alertController, animated: true, completion: nil)
		
	}
	
	@IBAction func didTapBack(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	
	
}

// MARK: - Navigation & SegueHandler protocol
extension SettingsController: SegueHandler {
    enum SegueType: String {
        case showPrivatePolicy
    }
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .showPrivatePolicy:
			let vc = segue.destination as! DocumentController
			vc.navigationItem.rightBarButtonItem = nil
			vc.title = Strings.PrivacyPolicy
			vc.url = Resources.PDF.privacyPolicy.url
        }
    }
}
