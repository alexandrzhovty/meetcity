//
//  MessageViewModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData


enum ChatMessageType: Int {
	case incoming
	case outcoming
}

struct MessageViewModel {
	fileprivate(set) var message: DMMessage
	
}



// MARK: - Incoming type
extension MessageViewModel {
	var messageType: ChatMessageType {
		return message.incoming ? .incoming : .outcoming
	}
}

// MARK: - Displaying
extension MessageViewModel {
	func configure(_ cell: IncomingMessageCell) {
		cell.messageLabel.text = message.body
        
        let inviteModel = try! InviteModel(with: message.invite!, in: message.managedObjectContext!)
        
        
        if let fileRef = inviteModel.opponentModel.profilePhotoeRef {
            ImageCache.default.load(forKey: fileRef) { [weak cell] (image)  in
                cell?.userImageView.image = image
            }
        } else {
            cell.userImageView.image = nil
        }
        
	}
	
	func configure(_ cell: OutcomingMessageCell) {
		cell.messageLabel.text = message.body
	}
	
}


