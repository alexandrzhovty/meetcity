//
//  UserMessageModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/3/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData

/// Model used to store devie tokens
struct UserMessageModel {
	
	enum Settings {
		static let separator = ","
		static let tokenStoreKey = "CurrentDeviceToken"
	}
	
	
	fileprivate var _user: DMUser
	fileprivate(set) var context: NSManagedObjectContext
	
	init(with user: DMUser, in context: NSManagedObjectContext){
		assert(user.managedObjectContext == context, "Given context is incorrect")
		self._user = user
		self.context = context
	}
}

// MARK: - Message Model Protocol
extension UserMessageModel: UserMessageModelProtocol {
    
    
	
	/// Firestorage reference
	var fromImageUrl: URL? {
		guard let sURL = AppDelegate.me.profilePhotoUrl else { return nil }
		return URL(string: sURL)
	}
	
	/// User ID
	var fromUid: String { return AppDelegate.me.uid! }
	

	/// List of user device tokens
	var deviceTokens: [String] {
		get {
			let tokens = _user.tokenIOSValues ?? ""
			return tokens.components(separatedBy: Settings.separator)
		}
		set {
			_user.tokenIOSValues = newValue.joined(separator: Settings.separator)
		}
	}
	
	
	
	/// Update device token for the current device
	///
	/// - Parameter token: new token
	mutating func update(_ token: String?) {
		
		// Do nothing if token is empty
		guard let token = token, token.isEmpty == false else { return }
		
		let oldKey = UserDefaults.standard.string(forKey: Settings.tokenStoreKey) ?? ""
		if oldKey != token {
			UserDefaults.standard.set(token, forKey: Settings.tokenStoreKey)
			UserDefaults.standard.synchronize()
			
			var tokens = self.deviceTokens
			if let idx = tokens.index(of: token) {
				tokens[idx] = token
			} else {
				tokens.append(token)
			}
			
			self.deviceTokens = tokens
		}
		
	}
}

// MARK: - Synchronizationч]
extension UserMessageModel {
    func synchronize() throws {
        try context.save()
        FirebaseStack.user(for: "me").child(DMUser.CodingKeys.tokensIOS).setValue(self.deviceTokens.joined(separator: Settings.separator))
    }
}
