//
//  UserImageView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase

final class UserImageView: UIImageView {

	var userModel: UserModel! {
		didSet {
			unregisterObservers()
			updateProfileImage()
			registerObservers()
		}
	}
	
	fileprivate var _activityIndicator: UIActivityIndicatorView!
	
	
	// MARK: Private
	fileprivate var _observers = [DatabaseReference]()
	fileprivate var _filename: String? {
		didSet { self.updateProfileImage() }
	}
	
	deinit {
		unregisterObservers()
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.image = nil
		
		_activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
		_activityIndicator.hidesWhenStopped = true
		self.addSubview(_activityIndicator)
		_activityIndicator.translatesAutoresizingMaskIntoConstraints = false
		_activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
		_activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
		
		
	}
	
	private func updateProfileImage() {
		if _filename == nil, let photo = userModel.profilePhoto {
			self._filename = photo.fileName
			return
		}
		
		guard let filename = _filename else {
			self.image = nil
			return
		}
		
		let storageRef = FirebaseStack.storageRef
		let imageRef = storageRef.child(userModel.uid).child(filename)
		
		_activityIndicator.startAnimating()
		ImageCache.default.load(forKey: imageRef) { [weak self] image in
			guard let `self` = self else { return }
			self.image = image
			self._activityIndicator.stopAnimating()
		}
		
		
		
	}
	
	
	// MARK: - Observers
	func registerObservers() {
		
		// Profile image
		let photosRef = FirebaseStack.users.child(userModel.uid).child(DMUser.CodingKeys.photos).child("0").child(DMPhoto.Fields.fileName)
		photosRef.observe(.value, with: {
			[weak self] snapshot in
			guard let `self` = self else { return }
			
			guard let value = snapshot.value as? String else {
				return
			}
			
			if self._filename != value {
				self._filename = value
			}
			
		})
	}
	
	
	func unregisterObservers() {
		_observers.forEach{ $0.removeAllObservers() }
		_observers.removeAll()
	}

}
