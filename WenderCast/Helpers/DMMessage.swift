//
//  DMMessage.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData
import Firebase




// MARK: - Server fields
extension DMMessage {
	enum field {
		static let id = "id"
		static let message = "message"
		static let created = "created"
		static let reviewed = "reviewed"
		static let senderId = "senderId"
        static let inviteID = "inviteID"
	}
}

// MARK: - Creating
extension DMMessage {
    /// invoked after an insert
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        self.id = UUID().uuidString
    }
    
    
	/// Creates a new message entity
	///
	/// - Parameters:
	///   - invite: the new message should belong to
	///   - context: where the new message will be stored in
	/// - Returns: newly created invite
	class func new(for invite: DMInvite, in context: NSManagedObjectContext) -> DMMessage {
		let entity = DMMessage(context: context)
		entity.invite = invite.managedObjectContext == context ? invite : context.object(with: invite.objectID) as? DMInvite
		entity.incoming = true
		return entity
	}
}

extension DMMessage {
    typealias Identifier = StringIdentifier
    var identifier: Identifier {
        get { return Identifier(rawValue: self.id!) }
        set { self.id = newValue.rawValue }
    }
}

// MARK: - Parsing
extension DMMessage {
	
	/// Parsing server response
	///
	/// - Parameters:
	///   - response: should be `[[key: value]]` (array of dictionaries)
	///   - context: where the parsed results should be stored in
	///   - invite: parsed messages should belong to
	class func parse(_ response: ParsableResponse, into context: NSManagedObjectContext, for invite: DMInvite) {
		
		let parentInvite = context.object(with: invite.objectID) as! DMInvite
		
		
		
		let responsToParse: [String: Any]
		
		switch response {
		case let snapshots as DataSnapshot:
			guard let value = snapshots.value as? [String: Any] else { return }
			responsToParse = value
            
        case let dictionary as Dictionary<String, Any>:
            responsToParse = dictionary
			
		default: return
		}
		
		let keys = [String] (responsToParse.keys)
		
		var messages = [String: DMMessage]()
		
		if keys.count > 0 {
			let request: NSFetchRequest<DMMessage> = DMMessage.fetchRequest()
			let predicates = [
				NSPredicate(format: "%K = %@", #keyPath(DMMessage.invite), parentInvite),
				NSPredicate(format: "%K in %@", #keyPath(DMMessage.id), keys)
			]
			
			request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
			let results = try? context.fetch(request)
			for entity in results ?? []{
				guard let mid = entity.id else { continue }
				messages[mid] = entity
			}
		}
		
		
		for (key, value) in responsToParse {
			
			let message = messages[key] ?? DMMessage.new(for: invite, in: context)
			let json = JSON(value)
			message.id = key
			message.created = json[field.created].doubleValue
			message.body = json[field.message].string
			message.invite = parentInvite
			message.incoming = json[field.senderId].stringValue != AppDelegate.me.uid
			
		}
		
		
		
		
		
	}
}

extension DMMessage: SynchronizationProtocol {
    func synchronize(completion: @escaping (Result<DatabaseReference>) -> Void) {
        var json = JSON([:])
        
        json[field.id].string = self.identifier.rawValue
        json[field.created].int = Int(Date().timeIntervalSince1970)
        json[field.message].string = self.body
        json[field.senderId].string = AppDelegate.me.identifier.rawValue
        json[field.inviteID].string = self.invite?.identifier.rawValue
        
        
        let messageRef = FirebaseStack.chat(for: self.invite!.identifier.rawValue).child(self.identifier.rawValue)
        messageRef.setValue(json.dictionaryObject) { (error, dbRef) in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(dbRef))
            }
        }
    }
}














