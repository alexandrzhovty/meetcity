//
//  ComboBox.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/15/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

@IBDesignable final class ComboBox: UIControl {
    fileprivate var heightConstraint: NSLayoutConstraint!
    
    var isExpanded: Bool {
        return false
    }
    
    enum Constants {
        static let rowHeight: CGFloat = 50
        static let margin: CGFloat = 4
    }
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupControl()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupControl()
    }
    
    //	MARK: - Size
    override var intrinsicContentSize: CGSize {
        return CGSize(width: super.intrinsicContentSize.width, height: controlHeight)
    }
    
    
}

// MARK: - Utility
extension ComboBox {
    fileprivate func setupControl() {
        
        // Setup table position
        
        self.translatesAutoresizingMaskIntoConstraints = false
        heightConstraint = heightAnchor.constraint(equalToConstant: controlHeight)
        heightConstraint.isActive = true
        
    }
    
    var controlHeight: CGFloat {
        let qty = isExpanded ? 4 : 1
        return Constants.rowHeight * CGFloat(qty) + Constants.margin
    }
    
}


