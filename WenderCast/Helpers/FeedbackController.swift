//
//  FeedbackController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import AlamofireImage
import CoreData

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class FeedbackController: UIViewController {
    //    MARK: Public
    var invite: DMInvite!
    var managedObjectContext: NSManagedObjectContext = AppDelegate.shared.mainContext
    
    //    MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
	
    
    fileprivate var _ratesCellQty = 1
    fileprivate let _allRatesCellQty = 3
	
    //    MARK: Private
	
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension FeedbackController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
        // Customize appearance
        Appearance.customize(viewController: self)
        

		
		
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension FeedbackController: UITableViewDataSource, UITableViewDelegate {
    //MARK: Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return _ratesCellQty
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(UserFeedbackCell.self, for: indexPath)
            
            
            
            if let profileRef = invite.opponent.profilePhotoeRef  {
                ImageCache.default.load(forKey: profileRef) {
                    [weak cell] image in
                    guard let cell = cell else { return }
                    cell.userImage = image
                    
                }
            } else {
                cell.iconImageView.image = nil
            }
            
            cell.title = invite.opponent.title
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(RatingFeedbackCell.self, for: indexPath)
            guard let ratingType = RatingType(rawValue: indexPath.row) else {
                return cell
            }
            
            
            cell.ratingControl.tag = Int(ratingType.rawValue)
            cell.ratingControl.valueChaned {
                [weak self, weak cell] (ratingControl, rate) in
                guard let `self` = self else { return }
                guard let ratingType = RatingType(rawValue: ratingControl.tag) else { return }
                guard let ratingStatus = RatingStatus(rawValue: Int16(rate)) else { return }
                
                if ratingControl.tag == 0 &&  self._ratesCellQty != self._allRatesCellQty && Int16(rate) < RatingStatus.highestValue.rawValue {
                    self._ratesCellQty = self._allRatesCellQty
                    self.tableView.beginUpdates()
                    let arrays = [
                        IndexPath(row: 1, section: 1),
                        IndexPath(row: 2, section: 1)
                    ]
                    self.tableView.insertRows(at: arrays, with: .fade)
                    self.tableView.endUpdates()
                }
                
                if let propertyName = self.invite.oppoentRatingPropertyName(for: ratingType) {
                    self.invite.setValue(ratingStatus.rawValue, forKey: propertyName)
                }
                
                guard let cell = cell else { return }
                cell.body = ratingStatus.description
            }
            
            
            
            cell.title = ratingType.description
            let ratingStatus = invite.opponentRating(for: ratingType)
            cell.body = ratingStatus.description
            cell.rating = Int(ratingStatus.rawValue)
            
            return cell
        }
    }

    //MARK: Displaying
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row % 2 == 0 {
                cell.backgroundView?.backgroundColor = UIColor.white
            } else {
                cell.backgroundView?.backgroundColor = UIColor(rgbValue: 0xd8d8d8).withAlphaComponent(0.18)
            }
        }
    }
    
    
    //MARK: Heights
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let qty = 1 + _ratesCellQty
        return tableView.bounds.height / CGFloat(qty)
    }
    
}

// MARK: - Navigation & SegueHandler protocol
extension FeedbackController: SegueHandler {
    enum SegueType: String {
        case exitToDashboard
        case saveAndExitToDashboard
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .exitToDashboard: break
        case .saveAndExitToDashboard:
            
            do { try managedObjectContext.save() }
            catch {
                Alert.default.showError(message: error.localizedDescription)
                return false
            }
            
            let inviteRef = FirebaseStack.invitation(for: invite.identifier)
            if invite.sender == AppDelegate.me {
                
                if invite.recipientRatingAppearance == 5 &&  invite.recipientRatingExperience == 0 {
                    invite.recipientRatingExperience = 5
                }
                
                if invite.recipientRatingAppearance == 5 &&  invite.recipientRatingPersonality == 0 {
                    invite.recipientRatingPersonality = 5
                }
                
                inviteRef.child(DMInvite.CodingKeys.recipientRating + "Appearance" ).setValue(invite.recipientRatingAppearance)
                inviteRef.child(DMInvite.CodingKeys.recipientRating + "Experience" ).setValue(invite.recipientRatingExperience)
                inviteRef.child(DMInvite.CodingKeys.recipientRating + "Personality" ).setValue(invite.recipientRatingPersonality)
                
            } else {
                if invite.senderRatingAppearance == 5 &&  invite.senderRatingExperience == 0 {
                    invite.senderRatingExperience = 5
                }
                
                if invite.senderRatingAppearance == 5 &&  invite.senderRatingPersonality == 0 {
                    invite.senderRatingPersonality = 5
                }
                
                
                inviteRef.child(DMInvite.CodingKeys.senderRating + "Appearance" ).setValue(invite.senderRatingAppearance)
                inviteRef.child(DMInvite.CodingKeys.senderRating + "Experience" ).setValue(invite.senderRatingExperience)
                inviteRef.child(DMInvite.CodingKeys.senderRating + "Personality" ).setValue(invite.senderRatingPersonality)
            }
            
//            // TODO: Weak self
//            invite.synchronize(completion: { result in
//                switch result {
//                case let .failure(error):
//                    Alert.default.showError(message: error.localizedDescription)
//                case .success: break
//                }
//            })
            
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .exitToDashboard: break
        case .saveAndExitToDashboard: break
        }
    }
}
