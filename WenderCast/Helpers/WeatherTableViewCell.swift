//
//  WeatherTableViewCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import AlamofireImage

class WeatherTableViewCell: UITableViewCell {
	
	@IBOutlet weak var separatorView: UIView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var temperatureLabel: UILabel!
	@IBOutlet weak var weatherImageView: UIImageView!
	@IBOutlet weak var placeLabel: UILabel!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
	private var _array: [UIView]!
	
	public var weather: Weather? { didSet { configureView() } }
	
	enum Constants {
		static let height: CGFloat = 56
	}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		titleLabel.text = Strings.Invitation.WeatherForecast
		
		_array = [temperatureLabel, weatherImageView, placeLabel]
		
		prepareForReuse()
    }

	override func prepareForReuse() {
		super.prepareForReuse()
		
		temperatureLabel.text = nil
		weatherImageView.image = nil
		placeLabel.text = nil
		
	}
	
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
		size.height = max(size.height, WeatherTableViewCell.Constants.height)
		return size
	}

	
	func startAnimating() {
		guard activityIndicator.isAnimating == false else { return }
		_array.forEach{ $0.isHidden = true }
		activityIndicator.startAnimating()
	}
	
	func stopAnimating() {
		_array.forEach{ $0.isHidden = false }
		activityIndicator.stopAnimating()
	}
	
	private func configureView() {
		if let weather = weather {
			placeLabel.text = weather.place
//			temperatureLabel.text = weather.temperature
			
			if let url = weather.icon {
				weatherImageView.af_setImage(withURL: url)
			} else {
				weatherImageView.image = nil
			}
			
			
			if #available(iOS 10.0, *) {
				let formatter = MeasurementFormatter()
				formatter.unitStyle = .medium
//				formatter.unitOptions = .temperatureWithoutUnit
				let temperatureInFarenheit = Measurement(value: weather.temperature, unit: UnitTemperature.kelvin)
				formatter.locale = Locale.current
				temperatureLabel.text = formatter.string(from: temperatureInFarenheit)
			} else {
				// Fallback on earlier versions
			}
			
			
			
			self.stopAnimating()
		} else {
			self.startAnimating()
		}
	}
	
    
}

extension WeatherTableViewCell: NibLoadableView {}
