//
//  VisitManamger.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/23/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase
import CoreData
import SwiftyJSON

struct VisitManamger {
    
    typealias CodingKeys = DMVisit.CodingKeys
    
    static func visit(_ user: DMUser) {
        
        
        
        DispatchQueue.global(qos: .background).async {
            let tableRef = FirebaseStack.visits
            
            let array = [
                user.identifier.rawValue,
                AppDelegate.me.identifier.rawValue,
                Formatters.date.visit.string(from: Date())
            ]
            let searchIndetifier = array.joined(separator: "_")
            
            
            let query = tableRef.queryOrdered(byChild: DMVisit.CodingKeys.recipient_sender_date).queryEqual(toValue: searchIndetifier)
            query.observeSingleEvent(of: .value, with: { [weak user] (snapshot) in
                
                guard let reciever = user else { return }
                
                
                if snapshot.exists(), var json = JSON(snapshot.value!).first {
                    json.1[CodingKeys.amount].int = json.1[CodingKeys.amount].intValue + 1
                    json.1[CodingKeys.lastVisit].double = Date().timeIntervalSince1970
                    
                    FirebaseStack.visits.child(json.0).setValue(json.1.dictionaryObject)
                    
                } else {
                    
                    var json = JSON([:])
                    let visitIdentifier = UUID().uuidString
                    json[CodingKeys.id].string = visitIdentifier
                    
                    json[CodingKeys.created].double = Date().timeIntervalSince1970
                    json[CodingKeys.recipient_sender_date].string = searchIndetifier
                    
                    json[CodingKeys.senderID].string = AppDelegate.me.identifier.rawValue
                    json[CodingKeys.senderName].string = AppDelegate.me.name
                    json[CodingKeys.senderOccupation].string = AppDelegate.me.occupation
                    json[CodingKeys.senderBirthday].double = AppDelegate.me.birthday
                    
                    json[CodingKeys.recipientID].string = reciever.identifier.rawValue
                    json[CodingKeys.recipientName].string = reciever.name
                    json[CodingKeys.recipientOccupation].string = reciever.occupation
                    json[CodingKeys.recipientBirthday].double = reciever.birthday
                    
                    
                    json[CodingKeys.amount].int = 1
                    json[CodingKeys.lastVisit].double = Date().timeIntervalSince1970
                    
                    
                    let photoRef = AppDelegate.me.profilePhotoeRef
                    photoRef?.downloadURL(completion: { (url, error) in
                        
                        json[CodingKeys.senderPhoto].string = url?.absoluteString
                        
                        
                        let recipienPhotoRef = reciever.profilePhotoeRef
                        recipienPhotoRef?.downloadURL(completion: { (url, _) in
                            json[CodingKeys.recipientPhoto].string = url?.absoluteString
                            
                            FirebaseStack.visits.child(visitIdentifier).setValue(json.dictionaryObject)
                            
//                            let likeRef = FirebaseStack.like(for: self.identifier.rawValue)
//                            
//                            likeRef.setValue(json.dictionaryObject) { (error, dbRef) in
//                                
//                                if let error = error {
//                                    completion(Result.failure(error))
//                                } else {
//                                    completion(.success(dbRef))
//                                }
//                            }
                        })
                    })
                    
                    
                    
                    
                }
            })
            
        }
        
        
        
        
    }
}
