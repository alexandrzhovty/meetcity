//
//  NearbyListDataProvider.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/21/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation
import Firebase
import SwiftyJSON
import MapKit

class NearbyListDataProvider: NSObject {
	let managedObjectContext: NSManagedObjectContext
	var _fetchedResultsController: NSFetchedResultsController<DMUser>? = nil
	var blockOperations = [BlockOperation]()
    
	let collectionView: UICollectionView
    let mapController: NearbyMapController
    
    
	var shouldReloadCollectionView = false
	fileprivate let _distanceFormatter = MKDistanceFormatter()
	
	
	private var _usersRef: DatabaseReference
	fileprivate let _operationQueue = OperationQueue()
	
	deinit {
        for operation: BlockOperation in blockOperations {
            operation.cancel()
        }
        
        blockOperations.removeAll(keepingCapacity: false)
        _usersRef.removeAllObservers()
	}
	
    init(with collectionView: UICollectionView, mapController: NearbyMapController,  context: NSManagedObjectContext = CoredataStack.mainContext) {
		self.managedObjectContext = context
		self.collectionView = collectionView
        self.mapController = mapController
		self._usersRef = FirebaseStack.users
        
		super.init()
        
        _operationQueue.maxConcurrentOperationCount = 1
		
        
	}
	
	func fetch(for coordinate: CLLocationCoordinate2D, competion: @escaping (_ array: [UserModel]) -> Void) {
        
//        _usersRef.observe(.value, with: { (snapshot) in
//            
//        })
        
        _usersRef.observeSingleEvent(of: .value, with: { [weak self] snapshots in
            guard let `self` = self else { return }
            guard let json = snapshots.value as? [String: Any] else { return }
            
            
            let responseToParse = json.flatMap{ $0.1 as? [String: Any] }
            if snapshots.exists() {
                let operation = ParseOperation(parsingHandler: { context in
                    DMUser.parse(responseToParse, into: context)
                    
                })
                
                self._operationQueue.addOperation(operation)
            }
        })
		
	}
	
//	MARK: Database functions
	func reload(competion: @escaping (_ array: [UserModel]) -> Void) {
		
		DispatchQueue.main.async {
			self.fetchedResultsController.fetchRequest.predicate = self.predicate
			do {
				try self.fetchedResultsController.performFetch()
				self.collectionView.reloadData()}
			catch {
				print(String(describing: type(of: self)),":", #function, " ", error.localizedDescription)
			}
		}
		
	}
    
    func numberOfSections() -> Int {
       return self.fetchedResultsController.sections?.count ?? 0
    }
	
    func numberOfItemsIn(_ section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
	func object(at indexPath: IndexPath) -> DMUser {
		return fetchedResultsController.object(at: indexPath)
	}
}

// MARK: - NSFetchedResultsControllerDelegate
extension NearbyListDataProvider: NSFetchedResultsControllerDelegate {
	var predicate: NSPredicate {
        var predicates: [NSPredicate] = [
            NSPredicate(format: "SELF != %@", AppDelegate.me),
            NSPredicate(format: "%K == 0", #keyPath(DMUser.banned))
        ]
		
		
		
		let minAge = UserDefaults.standard.integer(forKey: FiltersController.Settings.minValueKey)
		let maxAge = UserDefaults.standard.integer(forKey: FiltersController.Settings.maxValueKey)
		
		var ages = [NSPredicate]()
		let calendar = Calendar.current
		
		if minAge != Constants.allowedAge.min, let birthday = calendar.date(byAdding: .year, value: minAge * (-1), to: Date()) {
			ages.append(NSPredicate(format: "%K <= %lf", #keyPath(DMUser.birthday), birthday.timeIntervalSince1970))
		}
		if maxAge != Constants.allowedAge.max, let birthday = calendar.date(byAdding: .year, value: maxAge * (-1), to: Date()) {
			ages.append(NSPredicate(format: "%K >= %lf", #keyPath(DMUser.birthday), birthday.timeIntervalSince1970))
		}
		
		if ages.count > 0 {
			predicates.append(NSCompoundPredicate(andPredicateWithSubpredicates: ages))
		}
        
        if UserDefaults.standard.bool(forKey: FiltersController.Settings.readyToMeetStatusKey) {
            predicates.append(
                NSCompoundPredicate(andPredicateWithSubpredicates: [
                    NSPredicate(format: "%K == 1", #keyPath(DMUser.readyToMeetValue)),
                    NSPredicate(format: "%K > %f", #keyPath(DMUser.lastUpdate), (Date().timeIntervalSince1970 - Constants.ReadyToMeet.timeIntervalFromLastVisit))
                    ])
            )
        }
        
        
		
		//		Constants.
		
		let userModel = UserModel(with: AppDelegate.me)
		
		if case LookFor.both = userModel.lookFor {
		} else {
			let gender: Gender
			switch userModel.lookFor {
			case .boy: gender = Gender.boy
			case .girl: gender = Gender.girl
			default: gender = .none
			}
			
			predicates.append(NSPredicate(format: "%K == %d", #keyPath(DMUser.genderValue), gender.rawValue))
		}
		
		return NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
	}
	
	var fetchedResultsController: NSFetchedResultsController<DMUser> {
		if _fetchedResultsController != nil {
			return _fetchedResultsController!
		}
		
		let fetchRequest: NSFetchRequest<DMUser> = DMUser.fetchRequest()
		let managedObjectContext = AppDelegate.shared.mainContext
		
		fetchRequest.predicate = self.predicate
		
		// sort by item text
		fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: #keyPath(DMUser.distance), ascending: true),
			NSSortDescriptor(key: #keyPath(DMUser.name), ascending: true),
			NSSortDescriptor(key: #keyPath(DMUser.uid), ascending: true)
		]
		
		
		let resultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
		
		resultsController.delegate = self;
		_fetchedResultsController = resultsController
		
		do {
			try _fetchedResultsController!.performFetch()
		} catch {
			let nserror = error as NSError
			fatalError("Unresolved error \(nserror)")
		}
		return _fetchedResultsController!
	}
	
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        if type == NSFetchedResultsChangeType.insert {
//            print("Insert Object: \(String(describing: newIndexPath))")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionView.insertItems(at: [newIndexPath!])
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.update {
//            print("Update Object: \(String(describing: indexPath))")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionView.reloadItems(at: [indexPath!])
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.move {
//            print("Move Object: \(String(describing: indexPath))")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionView.moveItem(at: indexPath!, to: newIndexPath!)
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.delete {
//            print("Delete Object: \(String(describing: indexPath))")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionView.deleteItems(at: [indexPath!])
                    }
                })
            )
        }
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        
        if type == NSFetchedResultsChangeType.insert {
//            print("Insert Section: \(sectionIndex)")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.update {
//            print("Update Section: \(sectionIndex)")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionView.reloadSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.delete {
//            print("Delete Section: \(sectionIndex)")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        
        var array =  [UserModel]()
        
        let sectionInfo = self.fetchedResultsController.sections![0]
        let qty = sectionInfo.numberOfObjects
        for row in 0..<qty {
            let indexPath = IndexPath(item: row, section: 0)
            let user = self.fetchedResultsController.object(at: indexPath)
            array.append(UserModel(with: user))
        }
        self.mapController.addUsers(array)
        
        
        
        collectionView.performBatchUpdates({ () -> Void in
            for operation: BlockOperation in self.blockOperations {
                operation.start()
            }
        }, completion: { (finished) -> Void in
            self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
	
	
}

