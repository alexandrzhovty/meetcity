//
//  RatingFeedbackCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class RatingFeedbackCell: UITableViewCell {
   
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    
    
    var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    var body: String? {
        get { return bodyLabel.text }
        set { bodyLabel.text = newValue }
    }
    
    var rating: Int {
        get { return ratingControl.rating }
        set { ratingControl.rating = newValue }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.selectionStyle = .none
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        let view = UIView()
        view.backgroundColor = UIColor.white
        self.backgroundView = view
        
    }


}
