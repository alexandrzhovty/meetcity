//
//  EmptyInvitationCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/20/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

enum EmptyInvitationCellType: Int {
	case loading
	case noRecords
}

class EmptyInvitationCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
	@IBOutlet weak var separatorView: UIView!
	var cellType: EmptyInvitationCellType = .loading { didSet { configureView() } }
	
	enum Constants {
		static let height: CGFloat = 56
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
		
		titleLabel.text = Strings.Invitation.NoPlacesFound
		configureView()
    }

	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
		size.height = max(size.height, EmptyInvitationCell.Constants.height)
		return size
	}
	
	private func configureView() {
		switch cellType {
		case .loading:
			activityIndicator.startAnimating()
			titleLabel.isHidden = true
		
		case .noRecords:
			activityIndicator.stopAnimating()
			titleLabel.isHidden = false
		}
	}
	
}

extension EmptyInvitationCell: NibLoadableView {}
