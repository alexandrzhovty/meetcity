//
//  InstagramDataProvider.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/17/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class InstagramDataProvider {
    
    enum Status {
        case loading
        case failured
        case success
    }
    
    var loadingStatus: Status = .loading
    
    fileprivate(set) var user: DMUser
    fileprivate var _dataSource: [JSON]
    var contentOffset: CGPoint = .zero
    
    init(with user: DMUser){
        self.user = user
        self._dataSource = [JSON]()
    }
    
    func fetch(completion: @escaping (Result<[JSON]>) -> Void) {
        let url = InstagramManager.imagesFetchUrl(for: user)
        
        if self.loadingStatus != .success {
            self.loadingStatus = .loading
        }
        
        Alamofire.request(url).responseJSON {
            [weak self] (response) in
            guard let `self` = self else { return }
            
            
            guard response.result.error == nil else {
                self.loadingStatus = .failured
                completion(.failure(response.result.error!))
                return
            }
            
            self._dataSource.removeAll()
            
            if let json = response.result.value {
                
//                print(json)
                
                var jsonObj = JSON(json)
                if let data = jsonObj["data"].arrayValue as [JSON]? {
                    self._dataSource = data
                }
            }
            self.loadingStatus = .success
            completion(.success(self._dataSource))
            
        }
        
    }
    
    var numberOfItems: Int {
        return _dataSource.count
    }
    
    func object(at indexPath: IndexPath) -> JSON {
        return _dataSource[indexPath.row]
    }
    
    func object(at index: Int) -> JSON {
        return _dataSource[index]
    }
}
