//
//  NearbyListController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import Firebase
import SwiftyJSON
import CoreData
import SVProgressHUD

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

struct MockUser {
	let name: String
	let age: Int
	let distanse: Double
	let image: UIImage
}


protocol NearbyListControllerProtocol: class {
	func setNearbyListHidden(_ hidden: Bool, animated: Bool)
}

//    MARK: - Properties & variables
final class NearbyListController: UIViewController {
    //    MARK: Public
	var managedObjectContext: NSManagedObjectContext = AppDelegate.shared.mainContext
    
    //    MARK: Outlets
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var mapContainer: UIView!
	@IBOutlet weak var mapHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var mapTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var collectionTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var logoImageView: UIImageView!
	@IBOutlet weak var logoVerticalConstraint: NSLayoutConstraint!
	@IBOutlet weak var logoWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emptyView: EmptyView!
    
    
	
    //    MARK: Private
	fileprivate var _mapController: NearbyMapController!
	fileprivate var _headerView: UIView!
	fileprivate var _mapContainerHeight: CGFloat = 0
	
	fileprivate var _logoImageViewWidth: CGFloat = 0
	
	fileprivate var _dataProvider: NearbyListDataProvider!
	
	
	
    
    //    MARK: - Initializations
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}
	
	private func commonInit() {
		
		_headerView = HeaderView(frame: CGRect.zero)
	}
	
}

//    MARK: - View life cycle
extension NearbyListController  {
    override func viewDidLoad() {
        super.viewDidLoad()

		_dataProvider = NearbyListDataProvider(with: self.collectionView, mapController: _mapController)
		
		navigationItem.title = Strings.PeopleNearby

        
        // Customize appearance
        Appearance.customize(viewController: self)
		self.navigationItem.setHidesBackButton(true, animated: false)
		
		
		// Header view
		configure(_headerView)
		configure(collectionView)
		
		_mapContainerHeight = mapHeightConstraint.constant
		
        _logoImageViewWidth = logoImageView.frame.width  //logoWidthConstraint.constant
		/*
         // Configure tableView
         configure(tableView)
         */
		
		
		let coor = CLLocationCoordinate2D(latitude: 0, longitude: 0)
		_dataProvider.fetch(for: coor) { array in
			DispatchQueue.main.async {
				self._mapController.addUsers(array)
			}
		}
		
		
		// Start monitoring location
		LocationManager.default.startUpdatingLocation()
		
		// Update user token if it is neccessary 
		MessageManager.default.updateDeviceToken()
        
        
        DispatchQueue.once {
            BanListManager.startMonitoring()
        }
		
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        
        let event: DMEvent
        
        event.id = "asdfasdf"
        
		
		if SVProgressHUD.isVisible() {
			SVProgressHUD.dismiss()
		}
	}
	
//	override func viewDidAppear(_ animated: Bool) {
//		super.viewDidAppear(animated)
//		
//		if let option = AppDelegate.shared.options {
//			let str = (option as! NSDictionary).description
//			Alert.default.showOk("", message: str)
//		}
//	}
}


//    MARK: - Utilities
extension NearbyListController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case _headerView:
			var frame = CGRect.zero
			frame.size.width = self.view.bounds.width
			frame.size.height = self.mapHeightConstraint.constant
			frame.origin.y = frame.height * (-1.0)
			
			_headerView.frame = frame
			_headerView.autoresizingMask = [ .flexibleWidth ]
			
			let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
			_headerView.addGestureRecognizer(tap)
			
		case collectionView:
			collectionView.contentInset = UIEdgeInsets(top: _headerView.frame.height, left: 0, bottom: 0, right: 0)
			collectionView.addSubview(_headerView)
			collectionView.backgroundColor = UIColor.clear
			self.view.backgroundColor = UIColor(.pink)
			
        default: break
        }
        
    }
}

//    MARK: - Outlet functions
extension NearbyListController  {
    //    MARK: Buttons
	@IBAction func didTapProfile(_ button: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
    //    MARK: Gesture handlers
	@objc func handleTapGesture(_ recognizer: UITapGestureRecognizer) {
		setNearbyListHidden(true, animated: true)
	}
}

// MARK: - Navigation & SegueHandler protocol
extension NearbyListController: SegueHandler {
    enum SegueType: String {
        case presentFilter
		case embedMap
		case showNearByProfile
    }
	
	@IBAction func exitToNearbyListController(_ segue: UIStoryboardSegue) {}
	
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
		case .showNearByProfile:
			guard let indexPath = collectionView.indexPathsForSelectedItems?.first else {
				break
			}
			let vc = segue.destination as! NearbyProfileController
			vc.managedObjectContext = self.managedObjectContext
			vc.user = _dataProvider.object(at: indexPath)
            
			
		case .embedMap:
			_mapController = segue.destination as! NearbyMapController
			_mapController.parentController = self
			
        case .presentFilter:
			let vc = (segue.destination as! UINavigationController).topViewController as! FiltersController
			vc.delegate = self
			vc.userModel = UserModel(with: AppDelegate.me)
			
		}
    }
}

// MARK: - Filter controller delegate
extension NearbyListController: FiltersControllerDelegate {
	func filtersControllerDidUpdateFilterParametres(_ viewController: FiltersController) {
		self._dataProvider.reload {
			self._mapController.addUsers($0)
		}
	}
}


// MARK: - Neearby list protocol
extension NearbyListController: NearbyListControllerProtocol {
	func setNearbyListHidden(_ hidden: Bool, animated: Bool) {
        
        
        
		if hidden {
			_mapController.peopleButton.alpha = 0
			_mapController.peopleButton.isHidden = false
			
			collectionTopConstraint.constant = self.view.bounds.height
			mapHeightConstraint.constant = collectionView.bounds.height
			
			UIView.animate(withDuration: 0.25, animations: {
				[unowned self] in
				self._mapController.peopleButton.alpha = 1
				
				self.view.layoutIfNeeded()
            }, completion: { finished in
                
                self._mapController.bigMarkers = true
                
            })
            
		} else {
			
			collectionTopConstraint.constant = 0
			mapHeightConstraint.constant = _mapContainerHeight
			
			UIView.animate(withDuration: 0.25, animations: {
				[unowned self] in
				self._mapController.peopleButton.alpha = 0
				
				self.view.layoutIfNeeded()
			}, completion: { finished in
				self._mapController.peopleButton.isHidden = true
				
				if let coordinate = self._mapController.mapView.myLocation?.coordinate {
					let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
					self._mapController.mapView.animate(to: camera)
				}
                
                self._mapController.bigMarkers = false
                
			})
		}
	}
}


// MARK: - Collection View Protocol
extension NearbyListController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSourcePrefetching {
    //    MARK: Datatsourse
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return _dataProvider.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let qty = _dataProvider.numberOfItemsIn(section)
        if qty == 0 && emptyView.isHidden {
            emptyView.setHidden(false, animated: true)
        }
        else if qty > 0 && !emptyView.isHidden {
            emptyView.setHidden(true, animated: true)
        }
        
        return qty
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NearbyCollectionCell.self, for: indexPath) as! NearbyCollectionCell
        return cell
    }
    
    // MARK: Prefetching
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//        indexPaths.forEach {
//            let user = self._dataProvider.object(at: $0)
//            if let fileRef = user.profilePhotoeRef {
//                ImageCache.default.load(forKey: fileRef, completion: { _ in })
//            }
//        }
    }
    
    
    // MARK: Displaying
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? NearbyCollectionCell else { return }
        let nearUserViewModel = NearbyUserViewModel(user: _dataProvider.object(at: indexPath))
        nearUserViewModel.configurte(cell)
    }
	
	//    MARK: Deleage flow layout
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = collectionView.bounds.width / 2.0
		let height = width / 375 * 262
		return CGSize(width: width, height: height)
	}
}

// MARK: - Scroll view protocol
extension NearbyListController:UIScrollViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let offset = scrollView.contentOffset
		let delta = mapHeightConstraint.constant + offset.y
		if delta > 0 && delta < mapHeightConstraint.constant {
			
			mapTopConstraint.constant = delta / 2 * (-1)
			let alpha = mapHeightConstraint.constant / 200 * delta / 600
			_headerView.backgroundColor = UIColor.black.withAlphaComponent(alpha)
			
			logoWidthConstraint.constant = _logoImageViewWidth
			
		}
		else if delta <= 0 {
			_headerView.backgroundColor = UIColor.black.withAlphaComponent(0)
			let decreaser = (mapTopConstraint.constant - delta) / 10
			let newValue = _logoImageViewWidth + decreaser
			logoWidthConstraint.constant = newValue
			mapTopConstraint.constant = delta * (-1)
			logoVerticalConstraint.constant = mapTopConstraint.constant / 2
			
		}
		
	}
}



