//
//  Gender.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
enum Gender: Int16 {
	case none = -1
	case boy
	case girl
	
	var description: String? {
		switch self {
		case .none:	return nil
		case .boy:	return Strings.Boy
		case .girl:	return Strings.Girl
		}
	}
	
//	init(value: Int?) {
//		self = Gender(rawValue: value ?? Gender.none.rawValue) ?? Gender.none
//	}
}
