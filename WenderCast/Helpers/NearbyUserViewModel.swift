//
//  NearbyUserViewModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

struct NearbyUserViewModel {
   var user: DMUser
   
//   func configure(_ cell: NearbyCollectionCell) {
//
//   }

    func configurte(_ cell: NearbyCollectionCell) {
        
        cell.title = user.title
//        cell.image = nil
        
        let photoRef = FirebaseStack.storageRef.child(user.identifier.rawValue).child(Settings.Photos.smallProfileName)
        
        
        
//        print(photoRef)
        
        let downloadID = ImageCache.DownloadIdentifier(rawValue: UUID().uuidString)
        ImageCache.default.load(forKey: photoRef, downloadID: downloadID) { [weak cell] (image, receiptID)  in
            if downloadID == receiptID {
                cell?.image = image
            } else {
                cell?.image = nil
            }
        }
        
        cell.distance = user.distance
        cell.readyToMeet = user.readyToMeet
    }
}
