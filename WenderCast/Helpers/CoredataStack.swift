//
//  CoredataStack.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/26/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import CoreData

@available(iOS 10.0, *)
final class CoredataStack {
    // MARK: - Core Data stack
	
	
	private static let `default` = CoredataStack()
	private init() {}
	
    static var container: NSPersistentContainer {
        return CoredataStack.default.persistentContainer
    }
	
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "MeetCity")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
				
				if let url = storeDescription.url {
					try? FileManager.default.removeItem(at: url)
					abort()
				}
				
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
	
    static var viewContext: NSManagedObjectContext {
        return CoredataStack.default.persistentContainer.viewContext
    }
    
	static var mainContext: NSManagedObjectContext {
		return CoredataStack.default.persistentContainer.viewContext
	}
	
	
    // MARK: - Core Data Saving support
	static func saveContext() {
		CoredataStack.default.saveContext()
	}
	
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
