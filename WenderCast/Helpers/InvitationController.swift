//
//  InvitationController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/19/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit
import CoreData

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */



//
//struct MockInvite {
//	var requestDate: Date
//	var scheduledDate: Date!
//	var placeCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(50.052351, 36.204860)
//	var notes: String?
//	var venue: Venue?
//	var state: InviteState = .open
//	
//	init(requestDate: Date!) {
//		self.requestDate = requestDate
//		self.scheduledDate = requestDate
//	}
//	
//	func save() {
//		print(String(describing: type(of: self)),":", #function)
//	}
//}

//    MARK: - Properties & variables
final class InvitationController: UIViewController {
    //    MARK: Public
    var observers = [Any]()
    
    var invite: DMInvite!
    var managedObjectContext: NSManagedObjectContext!
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    //    MARK: Private
	fileprivate lazy var _dateFormatter: DateFormatter = {
		var dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .short
		return dateFormatter
	}()
	
	fileprivate var _weatherInterator: WeartherInteractor = WeartherInteractor()
	fileprivate var _weather: Weather?
	fileprivate var _placesInteractor: VenueProvider = VenueProvider()
	fileprivate var _venues: [Venue]?
	fileprivate let _distanceFormatter: MKDistanceFormatter = Formatters.distance.default
    fileprivate var _inviteModel: InviteModel!
    
    //    MARK: Enums & Structures
	enum Constants {
		static let maxVenueCount = 4
		static let notesIndexPath = IndexPath(row: 0, section: 2)
		static let venueIndexPath = IndexPath(row: 0, section: 1)
	}
	
	enum StartInvitationCells: Int {
		case datingDate
		case wearther
		
		static var count: Int { return 2 }
		var indexPath: IndexPath {
			return IndexPath(row: self.rawValue, section: 0)
		}
		
		static func type(from indexPath: IndexPath) -> StartInvitationCells {
			guard let sType = StartInvitationCells(rawValue: indexPath.row) else {
				fatalError()
			}
			return sType
		}
	}
    
    deinit {
        unregisterObserver()
    }
}

//    MARK: - View life cycle
extension InvitationController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
        
        _inviteModel = try! InviteModel(with: invite, in: managedObjectContext)
        
		
        // Customize appearance
        Appearance.customize(viewController: self)
        
		// Configure tableView
		configure(tableView)
		configure(continueButton)
		
		
		_placesInteractor.fetch(around: _inviteModel.midPoint) { [weak self] venues in
			guard let `self` = self else { return }
			DispatchQueue.main.async {
				self._venues = venues
				self._inviteModel.venue = venues.first
				self.tableView.reloadData()
			}
		}
        
        registerObserver()
		
    }
}

// MARK: - ObserverProtocol
extension InvitationController: ObserverProtocol {
    func registerObserver() {
        // Keyboards
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unregisterObserver() {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Keyboard
extension InvitationController {
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.bottomConstraint.constant =  0
                //textViewBottomConstraint.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            
            print(userInfo)
            
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0.20
                self.bottomConstraint.constant = keyboardHeight
                UIView.animate(withDuration: duration, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
}

//    MARK: - Utilities
extension InvitationController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case tableView:
			tableView.backgroundColor = UIColor.background.main
			tableView.estimatedRowHeight = 70
			tableView.rowHeight = UITableViewAutomaticDimension
			
			tableView.register(EditInvitationCell.self)
			tableView.register(WeatherTableViewCell.self)
			tableView.register(EmptyInvitationCell.self)
			tableView.register(VenueRankedTableCell.self)
			tableView.register(TextEditInvitationCell.self)
			
		case continueButton:
			continueButton.setTitle(Strings.Invitation.SendInviteForDate, for: .normal)
			
		default: break
        }
		
    }
}


//    MARK: - Outlet functions
extension InvitationController  {
    //    MARK: Buttons
	@IBAction func didTapCancel(_ button: Any?) {
		dismiss(animated: true, completion: nil)
	}
	
	@objc func didTapDate(_ button: UIButton) {
		self.performSegue(.showDatePicker, sender: nil)
	}
	
	@objc func didTapLocation(_ button: UIButton) {
		self.performSegue(.presentPlacepicker, sender: nil)
	}
	
    //    MARK: Text field
	@objc func textFieldDidChange(_ textField: UITextField) {
		_inviteModel.notes = textField.text
	}
}

// MARK: - Navigation & SegueHandler protocol
extension InvitationController: SegueHandler {
    enum SegueType: String {
        case showDatePicker
		case sendInvitation
		case presentPlacepicker
    }
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		switch segueType(for: identifier) {
		case .showDatePicker, .presentPlacepicker: return true
		case .sendInvitation:
			
			// Update fileds
			guard let venue = _venues?.first else { return false }
			_inviteModel.venue = venue
			_inviteModel.state = InviteState.open
			
			// Validate notes
			if _inviteModel.notes.isEmptyOrNil {
				tableView.scrollToRow(at: InvitationController.Constants.notesIndexPath, at: .middle, animated: true)
				let cell = tableView.cellForRow(at: InvitationController.Constants.notesIndexPath) as! TextEditInvitationCell
				cell.borderedView.shake()
				return false
			}
			
			// Validate venue
			if _inviteModel.venue!.isValid == false {
				tableView.scrollToRow(at: InvitationController.Constants.venueIndexPath, at: .middle, animated: true)
				let cell = tableView.cellForRow(at: InvitationController.Constants.venueIndexPath) as! EditInvitationCell
				cell.borderedView.shake()
				Alert.default.showOk(Strings.Invitation.name, message: Strings.Invitation.VenueIsInvalid)
				return false
			}
			
            _inviteModel.created = Date()
			
			// Save data
//			do { try _inviteModel.synchronize() }
//			catch {
//                
//				Alert.default.showOk(Strings.Error, message: error.localizedDescription)
//                return false
//			}
//			
//			// Select invite entity from main context
//			let moc = AppDelegate.shared.mainContext
//			let invite = moc.object(with: _inviteModel.objectID) as! DMInvite
//			do { self._inviteModel = try InviteModel(with: invite, in: moc) }
//			catch {
//				Alert.default.showOk(Strings.Error, message: error.localizedDescription)
//				return false
//			}
            
            //TODO: Validate fields
            do {
                try managedObjectContext.save()
            }
            catch {
                Alert.default.showError(message: error.localizedDescription)
                return false
            }
			
			
			return true
		}
	}
	
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .showDatePicker:
			let vc = (segue.destination as! UINavigationController).topViewController as! DatePickerController
			vc.minimumDate = Calendar.current.date(byAdding: .hour, value: 2, to: Date())!
			vc.maximumDate = Calendar.current.date(byAdding: .hour, value: 26, to: Date())
			
			vc.date = _inviteModel.scheduled
			vc.completion = { [weak self] date in
				guard let `self` = self else { return }
				self._inviteModel.scheduled = date
				self.tableView.reloadRows(at: [StartInvitationCells.datingDate.indexPath], with: .automatic)
			}
			
		case .presentPlacepicker:
			let vc = (segue.destination as! UINavigationController).topViewController as! VenueListController
			vc.completion = { [weak self] venue in
				guard let `self` = self else { return }
				
				self._inviteModel.venue = venue
				
				self._venues?.insert(venue, at: 0)
				if (self._venues?.count)! > 3 {
					self._venues?.removeLast()
				}
				let indices: IndexSet = [1]
				self.tableView.reloadSections(indices, with: .automatic)
			}
			
		case .sendInvitation:
			let vc = segue.destination as! RequestingController
            vc.managedObjectContext = AppDelegate.shared.mainContext
            vc.invite = vc.managedObjectContext.object(with: invite.objectID) as! DMInvite
		
        }
    }
}

//    MARK: - Outlet functions
extension InvitationController: UITableViewDataSource, UITableViewDelegate {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 3
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		switch section {
		case 0:
			return StartInvitationCells.count
			
		case 1:
			if let venues = _venues, venues.count > 0 {
				return min(venues.count, InvitationController.Constants.maxVenueCount)
			} else {
				return 1
			}
		default:
			return 1
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		switch indexPath.section {
		case 0:
			switch StartInvitationCells.type(from: indexPath) {
			case .datingDate:
				let cell = tableView.dequeueReusableCell(EditInvitationCell.self, for: indexPath)
				cell.titleLabel.text = _dateFormatter.string(from: _inviteModel.scheduled)
				cell.textImageView.image = UIImage(named: "Text-Time")
				cell.editButton.addTarget(self, action: #selector(didTapDate(_:)), for: .touchUpInside)
				return cell
				
			case .wearther:
				
				let cell = tableView.dequeueReusableCell(WeatherTableViewCell.self, for: indexPath)
				
				cell.startAnimating()
				_weatherInterator.fetch(for: _inviteModel.midPoint) { [weak cell] weather in
					guard let cell = cell else { return }
					DispatchQueue.main.async {
						cell.weather = weather
					}
				}
				
				
				return cell
			}
			
		case 1:
			
			if let venues = _venues {
				if venues.count > 0 {
					let venue = venues[indexPath.row]
					
					if indexPath.row == 0 {
						let cell = tableView.dequeueReusableCell(EditInvitationCell.self, for: indexPath)
						cell.textImageView.image = UIImage(named: "Text-Place")
						cell.titleLabel.text = "\(venue.name) (\(_distanceFormatter.string(fromDistance: venue.distance)))"
						cell.editButton.addTarget(self, action: #selector(didTapLocation(_:)), for: .touchUpInside)
						return cell
						
					} else {
						let cell = tableView.dequeueReusableCell(VenueRankedTableCell.self, for: indexPath)
						cell.titleLabel.text = venue.name
						cell.distanceLabel.text = "(\(_distanceFormatter.string(fromDistance: venue.distance)))"
						cell.ratingControl.rating = venue.rate
						return cell
					}
					
				} else {
					// no venues has been found
					let cell = tableView.dequeueReusableCell(EditInvitationCell.self, for: indexPath)
					cell.textImageView.image = UIImage(named: "Text-Place")
					cell.titleLabel.text = "No venue found"
					cell.editButton.addTarget(self, action: #selector(didTapLocation(_:)), for: .touchUpInside)
					return cell
				}
				
			} else {
				// loading
				let cell = tableView.dequeueReusableCell(EmptyInvitationCell.self, for: indexPath)
				return cell
			}
			
		default:
			let cell = tableView.dequeueReusableCell(TextEditInvitationCell.self, for: indexPath)
			cell.textField.text = _inviteModel.notes
			cell.textField.placeholder = Strings.Invitation.LetsDrinkCoffeeTonight
			cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            cell.textField.delegate = self
			return cell
			
		}
		
	}
	
	// MARK: Selection
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.section == 1 && indexPath.row > 0 {
			guard var venues = _venues else { return }
			let venue = venues[indexPath.row]
			venues.remove(at: indexPath.row)
			venues.insert(venue, at: 0)
			_venues = venues
			_inviteModel.venue = _venues?.first
			tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
		}
	}
	
}

// MARK: - UITextFieldDelegate
extension InvitationController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isFirstResponder {
            textField.resignFirstResponder()
        }
        return true
    }
}
