//
//  ChatDataProvider.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 7/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import CoreData
import Firebase


class ChatDataProvider: NSObject {
	var observers = [Any]()
	
	fileprivate(set) var invite: DMInvite
	fileprivate(set) var managedObjectContext: NSManagedObjectContext
	fileprivate(set) var tableView: UITableView
	fileprivate var _fetchedResultsController: NSFetchedResultsController<DMMessage>? = nil
    fileprivate var _newItems = false
	
	typealias CellForRowAtIndexPath = (_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell
	fileprivate var _cellForRowAtIndexPathHandler: CellForRowAtIndexPath?
	
	
	init(with invite: DMInvite, in context: NSManagedObjectContext, for tableView: UITableView) {
		self.managedObjectContext = context
		self.invite = context.object(with: invite.objectID) as! DMInvite
		self.tableView = tableView
		super.init()
		
		self.tableView.dataSource = nil
        registerObserver()
		
	}
	
	deinit {
        unregisterObserver()
	}
    
    fileprivate func scrollToLast(animated: Bool) {
        let sectionInfo = self.fetchedResultsController.sections![0]
        let qty = sectionInfo.numberOfObjects
        if  qty > 0 {
            let indexPath = IndexPath(row: qty - 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        }
    }
}

extension ChatDataProvider {
	@discardableResult
	func cellForRowAtItemHandler(_ handler: @escaping CellForRowAtIndexPath) -> Self {
		self._cellForRowAtIndexPathHandler = handler
		return self
	}
}

// MARK: - Fetch data from the server
extension ChatDataProvider {
	func fetch() {
		
		tableView.dataSource = self
		
		let chatRef = FirebaseStack.chat(for: self.invite.id!)
		
		
		chatRef.observeSingleEvent(of: .value, with: { [weak self] snaphots in
			guard let `self` = self else { return }
			
			let nested = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
			nested.parent = self.managedObjectContext
			nested.perform {
				DMMessage.parse(snaphots, into: nested, for: self.invite)
				do { try nested.save() }
				catch {
					print(String(describing: type(of: self)),":", #function, " : ", error.localizedDescription)
				}
                DispatchQueue.main.async {
                    self._newItems = true
                    self.scrollToLast(animated: true)
                }
			}
			
		})
		
	}
	
	func object(at indexPath: IndexPath) -> DMMessage {
		return fetchedResultsController.object(at: indexPath)
	}
}

// MARK: - ObserverProtocol
extension ChatDataProvider: ObserverProtocol {
	
	func registerObserver() {
		let listerRef = FirebaseStack.chat(for: invite.id!)
        observers.append(
            listerRef.observe(.childAdded, with: { [weak self] (snapshot) in
                guard let `self` = self else { return }
                guard self._newItems == true else { return }
                guard let value = snapshot.value as? [String: Any] else { return }
                guard let key = value[DMMessage.field.id] as? String else { return }
                
                
                let nested = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                nested.parent = self.managedObjectContext
                nested.perform {
                    DMMessage.parse([key: value], into: nested, for: self.invite)
                    do { try nested.save() }
                    catch {
                        print(String(describing: type(of: self)),":", #function, " : ", error.localizedDescription)
                    }
                    
                    let deadlineTime = DispatchTime.now() + .milliseconds(4)
                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                        self.scrollToLast(animated: true)
                    }
                }
            })
        )
	}
	
	func unregisterObserver() {
        observers.forEach {
            if let dbRef = $0 as? DatabaseReference {
                dbRef.removeAllObservers()
            }
        }
        observers.removeAll()
	}
}

// MARK: - UITableViewDataSource
extension ChatDataProvider: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.fetchedResultsController.sections?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let sectionInfo = self.fetchedResultsController.sections![section]
		return sectionInfo.numberOfObjects
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		return _cellForRowAtIndexPathHandler!(tableView, indexPath)
	}
}




// MARK: - NSFetchedResultsControllerDelegate
extension ChatDataProvider: NSFetchedResultsControllerDelegate {
	var fetchedResultsController: NSFetchedResultsController<DMMessage> {
		if _fetchedResultsController != nil {
			return _fetchedResultsController!
		}
		
		let fetchRequest: NSFetchRequest<DMMessage> = DMMessage.fetchRequest()
		let managedObjectContext = AppDelegate.shared.mainContext
		
		fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(DMMessage.invite), invite)
		
		// sort by item text
		fetchRequest.sortDescriptors = [
			NSSortDescriptor(key: #keyPath(DMMessage.created), ascending: true)
		]
		
		
		let resultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
		
		resultsController.delegate = self;
		_fetchedResultsController = resultsController
		
		do {
			try _fetchedResultsController!.performFetch()
		} catch {
			let nserror = error as NSError
			fatalError("Unresolved error \(nserror)")
		}
		return _fetchedResultsController!
	}

	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		self.tableView.beginUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
		switch type {
		case .insert:
			self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
		case .delete:
			self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
		default:
			return
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		switch type {
		case .insert:
			tableView.insertRows(at: [newIndexPath!], with: .bottom)
		case .delete:
			tableView.deleteRows(at: [indexPath!], with: .fade)
		case .update:
			tableView.reloadRows(at: [indexPath!], with: .automatic)
		case .move:
			tableView.moveRow(at: indexPath!, to: newIndexPath!)
		}
	}
	
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		self.tableView.endUpdates()
	}
}
